
@component('mail::message')

# Hello, {{$data['name']}}

Congratulations your profile have been successfully approved and granted by our professionals.
Please login with following credentials

Your Email is : **{{$data['email']}}**
<br>
Your Password is : **{{$data['password']}}**


@component('mail::button', ['url' => $data['url'] ])
Login
@endcomponent
Incase you find difficulty in logging in click here to contact us. @component('mail::button', ['url' => 'http://www.mobihealthinternational.com/contact' ])
Contact US
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent

