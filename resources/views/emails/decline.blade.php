@component('mail::message')
# Hi, {{$data['name']}}

We are sorry to inform you that you have failed to match our technical aspects for the registration.
Please contact support for the further information.

@component('mail::button', ['url' => 'http://34.237.167.24/contact' ])
Contact US
@endcomponent


Thanks,<br>
{{ config('app.name') }}
@endcomponent
