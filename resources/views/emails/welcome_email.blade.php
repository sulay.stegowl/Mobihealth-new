@component('mail::message')
# Hi, {{$data['name']}}

Welcome to Mobihealth We are glad to have you on board, your profile is currently under review by our team. Upon approval you will receive an email with your Login Credentials.




Thanks,<br>
{{ config('app.name') }}
@endcomponent