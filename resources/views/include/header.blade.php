<style type="text/css">
     ul.navbar-nav.pull-right
     {
           position: absolute;
            left: calc(100% - 120px);

        }
        img.rounded-circle {
    object-fit: cover;
}
</style>

 <nav class="navbar navbar-primary col-lg-12 col-12 fixed-top d-flex flex-row">
        <div class="text-center navbar-brand-wrapper">
            <a class="navbar-brand brand-logo" href="{{route('dashboard')}}">
             <img src="{{asset('/assets/images/logo2.png')}}" height="80" width="80" />
            </a>
            <a class="navbar-brand brand-logo-mini" href="{{route('dashboard')}}"> <img src="{{asset('/assets/images/logo2.png')}}" height="50" width="50" /></a>
        </div>
        <div class="navbar-menu-wrapper d-flex align-items-center">
            <button class="navbar-toggler navbar-toggler d-none d-lg-block align-self-center mr-2" type="button" data-toggle="minimize">
                <span class="mdi mdi-menu"></span>
            </button>
            <div class="nav-profile">
               
              <span>Hi, {{Auth::user()->name}}</span>
              
            </div>
           {{--  <form class="form-inline mt-2 mt-md-0 d-none d-lg-block ml-lg-auto">
                <input class="form-control search" type="text" placeholder="Search">
            </form> --}}
            <ul class="navbar-nav pull-right">
                <li class="nav-item dropdown">
                    <a class="nav-link count-indicator" id="notificationDropdown" href="#" data-toggle="dropdown">
                        <i class="icon-user"></i>
                      
                    </a>
                    <div class="dropdown-menu navbar-dropdown notification-drop-down" aria-labelledby="notificationDropdown">
                        <a class="dropdown-item" href="{{route('change_password')}}">
                            <i class="icon-lock-open"></i>
                            <span class="notification-text">Change Password</span>
                        </a>
                        <a class="dropdown-item" href="{{route('logout')}}">
                            <i class="icon-logout"></i>
                            <span class="notification-text">Sign Out</span>
                        </a>
                    </div>
                </li>
               
            </ul>
            <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
                <span class="mdi mdi-menu"></span>
              </button>
        </div>
    </nav>