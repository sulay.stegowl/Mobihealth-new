  <div class="container-fluid page-body-wrapper">
      <div class="row row-offcanvas row-offcanvas-right">
  <nav class="sidebar sidebar-offcanvas" id="sidebar">
 <ul class="nav">
                <!--main pages start-->
               
                @if(Auth::user()->role==1)
                <li class="nav-item">
                    <a class="nav-link"  href="{{route('dashboard')}}" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-dashboard"></i>
                        <span class="menu-title">Dashboard</span>
                    </a>
                </li>
                        <li class="nav-item">
                    <a class="nav-link" data-toggle="collapse" href="#dashboardSubmenu" aria-expanded="false" aria-controls="collapseExample">
                        <i class="mdi mdi-compass-outline"></i>
                        <span class="menu-title">Website CMS</span>
                        <i class="mdi mdi-chevron-down"></i>
                    </a>
                    <div class="collapse" id="dashboardSubmenu">
                        <ul class="nav flex-column sub-menu">
                            <li class="nav-item">
                                <a class="nav-link active" href="{{route('aboutus_text')}}">About Us</a>
                            </li>

                             <li class="nav-item">
                                <a class="nav-link" href="{{route('homepage_text')}}">Home Page</a>
                            </li>

                             <li class="nav-item">
                                <a class="nav-link" href="{{route('telemedicine_text')}}">What is Telemedicine</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="pages/dashboard/dashboard-2.html">Contact US</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="pages/dashboard/dashboard-2.html">FAQ</a>
                            </li>
                            
                            <li class="nav-item">
                                <a class="nav-link" href="pages/dashboard/dashboard-2.html">Services</a>
                            </li>
                             <li class="nav-item">
                                <a class="nav-link" href="pages/dashboard/dashboard-2.html">News</a>
                            </li>
                        </ul>
                    </div>
                     <li class="nav-item">
                    <a class="nav-link"  href="#" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-user-md"></i>
                        <span class="menu-title">Doctors</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"  href="#" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-wheelchair"></i>
                        <span class="menu-title">Pateints</span> 
                    </a>
                </li>
                 <li class="nav-item">
                    <a class="nav-link"  href="#" aria-expanded="false" aria-controls="collapseExample">
                         <i class="icon-chemistry"></i>
                        <span class="menu-title">Pathology</span> 
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"  href="#" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-medkit"></i>
                        <span class="menu-title">Pharmacies</span>
                    </a>
                </li>
                 <li class="nav-item">
                    <a class="nav-link"  href="#" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-hospital-o"></i>
                        <span class="menu-title">Hospital</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"  href="#" aria-expanded="false" aria-controls="collapseExample">
                         <i class="mdi mdi-compass-outline"></i>
                        <span class="menu-title">Radiology</span>
                    </a>
                </li>
             {{--    <li class="nav-item">
                    <a class="nav-link"  href="{{route('services')}}" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-plus-circle"></i>
                        <span class="menu-title">Add Services</span> 
                    </a>
                </li> --}}
                <li class="nav-item">
                    <a class="nav-link"  href="{{route('contact_query')}}" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-phone"></i>
                        <span class="menu-title">Contact Query</span>  
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"  href="#" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-support"></i>
                        <span class="menu-title">Support Ticket</span>
                    </a>
                </li>
                </li>
                @elseif(Auth::user()->role==2)
                <li class="nav-item">
                    <a class="nav-link"  href="{{route('paitent.dashboard')}}" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-dashboard"></i>
                        <span class="menu-title">Dashboard</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"  href="#" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-wheelchair"></i>
                        <span class="menu-title">Patient</span>
                    </a>
                </li>
                 <li class="nav-item">
                    <a class="nav-link"  href="#" aria-expanded="false" aria-controls="collapseExample">
                         <i class="mdi mdi-compass-outline"></i>
                        <span class="menu-title">Add a Patient</span> 
                    </a>
                </li>
                 <li class="nav-item">
                    <a class="nav-link"  href="#" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-plus-square"></i>
                        <span class="menu-title">Subscription</span>
                    </a>
                </li>
                 <li class="nav-item">
                    <a class="nav-link"  href="#" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-file-text-o"></i>
                        <span class="menu-title">Patients history</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"  href="#" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-ticket"></i>
                        <span class="menu-title">Raise a Ticket</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"  href="#" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-support"></i>
                        <span class="menu-title">Support Ticket</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"  href="#" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-bell"></i>
                        <span class="menu-title">Send Notification</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"  href="#" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-comment"></i>
                        <span class="menu-title">Messages</span>
                    </a>
                </li>
                @elseif(Auth::user()->role==3)
                <li class="nav-item">
                    <a class="nav-link"  href="{{route('doctor.dashboard')}}" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-dashboard"></i>
                        <span class="menu-title">Dashboard</span>
                    </a>
                </li>
                  <li class="nav-item">
                    <a class="nav-link"  href="{{route('doctor.subscriber')}}" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-handshake-o"></i>
                        <span class="menu-title">Registrant Request </span>
                    </a>
                </li>
                 <li class="nav-item">
                    <a class="nav-link"  href="{{route('doctor.list')}}" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-user-md"></i>
                        <span class="menu-title">Doctors</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"  href="{{route('dentists.list')}}" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-stethoscope"></i>
                        <span class="menu-title">Dentist</span> 
                    </a>
                </li>
                 <li class="nav-item">
                    <a class="nav-link"  href="{{route('laborarty.list')}}" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-flask"></i>
                        <span class="menu-title">Laboratory Technician</span>
                    </a>
                </li>
                 <li class="nav-item">
                    <a class="nav-link"  href="{{route('nurse.list')}}" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-female"></i>
                        <span class="menu-title">Nurse</span>
                    </a>
                </li>
                 <li class="nav-item">
                    <a class="nav-link"  href="{{route('physiotherapist.list')}}" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-universal-access"></i>
                        <span class="menu-title">Physiotherapist</span> 
                    </a>
                </li>
                 <li class="nav-item">
                    <a class="nav-link"  href="{{route('pharmacist.list')}}" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-medkit"></i>
                        <span class="menu-title">Pharmacist</span> 
                    </a>
                </li>
                 <li class="nav-item">
                    <a class="nav-link"  href="{{route('main_subscription')}}" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-user-md"></i>
                        <span class="menu-title">Patients Request</span>
                    </a>
                </li>
              {{--   <li class="nav-item">
                    <a class="nav-link"  href="{{route('add.doctor')}}" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-user-plus"></i>
                        <span class="menu-title">Add a Medical Professional</span>   
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"  href="#" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-file-text-o"></i>
                        <span class="menu-title">Patients History</span> 
                    </a>
                </li> --}}
                {{-- <li class="nav-item">
                    <a class="nav-link"  href="#" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-ticket"></i>
                        <span class="menu-title">Raise a Ticket</span> 
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"  href="#" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-support"></i>
                        <span class="menu-title">Support Ticket</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"  href="#" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-bell"></i>
                        <span class="menu-title">Send Notification</span> 
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"  href="#" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-comment"></i>
                        <span class="menu-title">Messages</span>
                    </a>
                </li> --}}
                 @elseif(Auth::user()->role==4)
                <li class="nav-item">
                    <a class="nav-link"  href="{{route('lab.dashboard')}}" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-dashboard"></i>
                        <span class="menu-title">Dashboard</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"  href="{{route('subscribers')}}" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-handshake-o"></i>
                        <span class="menu-title">Subscriber</span>
                    </a>
                </li>
                  <li class="nav-item">
                    <a class="nav-link"  href="{{route('lab.list')}}" aria-expanded="false" aria-controls="collapseExample">
                         <i class="icon-chemistry"></i>
                        <span class="menu-title">Pathology</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"  href="{{route('add_addtional_services')}}" aria-expanded="false" aria-controls="collapseExample">
                         <i class="mdi mdi-compass-outline"></i>
                        <span class="menu-title">Add a Pathology</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"  href="#" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-list-alt"></i>
                        <span class="menu-title">History</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"  href="#" aria-expanded="false" aria-controls="collapseExample">
                         <i class="mdi mdi-compass-outline"></i>
                        <span class="menu-title">Request</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"  href="#" aria-expanded="false" aria-controls="collapseExample">
                         <i class="mdi mdi-compass-outline"></i>
                        <span class="menu-title">Prescriptions</span>
                    </a>
                </li>
                 <li class="nav-item">
                    <a class="nav-link"  href="#" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-ticket"></i>
                        <span class="menu-title">Raise a Ticket</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"  href="#" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-support"></i>
                        <span class="menu-title">Support Ticket</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"  href="#" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-bell"></i>
                        <span class="menu-title">Send Notification</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"  href="#" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-comment"></i>
                        <span class="menu-title">Messages</span>
                    </a>
                </li>
                 @elseif(Auth::user()->role==5)
                 <li class="nav-item">
                    <a class="nav-link"  href="{{route('pharmacies.dashboard')}}" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-dashboard"></i>
                        <span class="menu-title">Dashboard</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"  href="{{route('subscribers')}}" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-handshake-o"></i>
                        <span class="menu-title">Subscriber</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"  href="{{route('pharma.list')}}" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-medkit"></i>
                        <span class="menu-title">Pharmacies </span>
                    </a>
                </li>
                 <li class="nav-item">
                    <a class="nav-link"  href="{{route('add_addtional_services')}}" aria-expanded="false" aria-controls="collapseExample">
                         <i class="mdi mdi-compass-outline"></i>
                        <span class="menu-title">Add a Pharmacies </span>
                    </a>
                </li>
                 <li class="nav-item">
                    <a class="nav-link"  href="#" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-list-alt"></i>
                        <span class="menu-title">History</span>
                    </a>
                </li>
                 <li class="nav-item">
                    <a class="nav-link"  href="#" aria-expanded="false" aria-controls="collapseExample">
                         <i class="mdi mdi-compass-outline"></i>
                        <span class="menu-title">Request </span> 
                    </a>
                </li>
                  <li class="nav-item">
                    <a class="nav-link"  href="#" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-ticket"></i>
                        <span class="menu-title">Raise a Ticket</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"  href="#" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-support"></i>
                        <span class="menu-title">Support Ticket</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"  href="#" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-bell"></i>
                        <span class="menu-title">Send Notification</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"  href="#" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-comment"></i>
                        <span class="menu-title">Messages</span>
                    </a>
                </li>
                @elseif(Auth::user()->role==6)
                 <li class="nav-item">
                    <a class="nav-link"  href="{{route('hospital.dashboard')}}" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-dashboard"></i>
                        <span class="menu-title">Dashboard</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"  href="{{route('subscribers')}}" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-handshake-o"></i>
                        <span class="menu-title">Subscriber</span>
                    </a>
                </li>
                  <li class="nav-item">
                    <a class="nav-link"  href="{{route('hospital.list')}}" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-hospital-o"></i>
                        <span class="menu-title">Hospitals</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"  href="{{route('add_addtional_services')}}" aria-expanded="false" aria-controls="collapseExample">
                         <i class="mdi mdi-compass-outline"></i>
                        <span class="menu-title">Add a Hospitals</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"  href="#" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-ticket"></i>
                        <span class="menu-title">Raise a Ticket</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"  href="#" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-support"></i>
                        <span class="menu-title">Support Ticket</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"  href="#" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-bell"></i>
                        <span class="menu-title">Send Notification</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"  href="#" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-comment"></i>
                        <span class="menu-title">Messages</span>
                    </a>
                </li>
                 @elseif(Auth::user()->role==7)
                     <li class="nav-item">
                    <a class="nav-link"  href="{{route('radiology.dashboard')}}" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-dashboard"></i>
                        <span class="menu-title">Dashboard</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"  href="{{route('subscribers')}}" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-handshake-o"></i>
                        <span class="menu-title">Subscriber</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"  href="{{route('radiology.list')}}" aria-expanded="false" aria-controls="collapseExample">
                         <i class="mdi mdi-compass-outline"></i>
                        <span class="menu-title">Radiology </span>
                    </a>
                </li>
                 <li class="nav-item">
                    <a class="nav-link"  href="{{route('add_addtional_services')}}" aria-expanded="false" aria-controls="collapseExample">
                         <i class="mdi mdi-compass-outline"></i>
                        <span class="menu-title">Add a Radiology </span>
                    </a>
                </li>
                 <li class="nav-item">
                    <a class="nav-link"  href="#" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-list-alt"></i>
                        <span class="menu-title">History</span>
                    </a>
                </li>
                 <li class="nav-item">
                    <a class="nav-link"  href="#" aria-expanded="false" aria-controls="collapseExample">
                         <i class="mdi mdi-compass-outline"></i>
                        <span class="menu-title">Request </span>
                    </a>
                </li>
                  <li class="nav-item">
                    <a class="nav-link"  href="#" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-ticket"></i>
                        <span class="menu-title">Raise a Ticket</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"  href="#" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-support"></i>
                        <span class="menu-title">Support Ticket</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"  href="#" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-bell"></i>
                        <span class="menu-title">Send Notification</span> 
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"  href="#" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-comment"></i>
                        <span class="menu-title">Messages</span>
                    </a>
                </li> 
                 @elseif(Auth::user()->role==8)
                 <li class="nav-item">
                    <a class="nav-link"  href="{{route('doctors.dashboard')}}" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-dashboard"></i>
                        <span class="menu-title">Dashboard</span>
                    </a>
                </li>
                @elseif(Auth::user()->role==9)
                 <li class="nav-item">
                    <a class="nav-link"  href="{{route('patients.dashboard')}}" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-dashboard"></i>
                        <span class="menu-title">Dashboard</span>
                    </a>
                </li>
                @elseif(Auth::user()->role==10)
                 <li class="nav-item">
                    <a class="nav-link"  href="{{route('laboratry.dashboard')}}" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-dashboard"></i>
                        <span class="menu-title">Dashboard</span>
                    </a>
                </li>
                @elseif(Auth::user()->role==11)
                 <li class="nav-item">
                    <a class="nav-link"  href="{{route('pharmacy.dashboard')}}" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-dashboard"></i>
                        <span class="menu-title">Dashboard</span>  
                    </a>
                </li>
                @elseif(Auth::user()->role==12)
                 <li class="nav-item">
                    <a class="nav-link"  href="{{route('hospitals.dashboard')}}" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-dashboard"></i>
                        <span class="menu-title">Dashboard</span>
                        
                    </a>
                </li>
                @elseif(Auth::user()->role==13)
                 <li class="nav-item">
                    <a class="nav-link"  href="{{route('radiologys.dashboard')}}" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-dashboard"></i>
                        <span class="menu-title">Dashboard</span>  
                    </a>
                </li>
                @else
                 <li class="nav-item">
                    <a class="nav-link"  href="{{route('data.dashboard')}}" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-dashboard"></i>
                        <span class="menu-title">Dashboard</span>  
                    </a>
                </li>
                   <li class="nav-item">
                    <a class="nav-link"  href="{{route('services')}}" aria-expanded="false" aria-controls="collapseExample">
                         <i class="fa fa-plus-circle"></i>
                        <span class="menu-title">Add Services</span> 
                    </a>
                </li>
                @endif
            </ul>
        </nav>
</div>
</div>