@extends('layouts.app')
@section('content')
<div class="content-wrapper">
  <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal"><i class="mdi mdi-check btn-label btn-label-left"></i>Accept</button>
  <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal1"><i class="mdi mdi-close btn-label btn-label-left"></i>Decline</button>
<div class="row custom_css_bold" style="padding-top: 10px;display: flex;">
              <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 mb-4">
                  <div class="card" style="min-height:395px;height: 100%">
                      <div class="card-body">
                          <h5 class="card-title">Personal Information</h5>
                          <hr>
                          <div class="text-center">
                              <img src="{{url('/').'/storage/doctor/'.$doctor->image}}" class="rounded-circle" width="100" height="100">
                          </div>
                         {{--  <h5 class="text-center font-weight-bold txt-brand-color">{{$doctor->first_name.' '.$doctor->last_name}}</h5> --}}
                          <p class="font-italic text-muted mt-3 text-center">
                             {{$doctor->about}}
                          </p>
                          <h5 class="text-center font-weight-bold txt-brand-color">{{$doctor->First_Name.' '.$doctor->Surname}}, <span class="text-muted">{{$doctor->Profession}}</span></h5>
                          
                           <label><b>Speciality:</b></label> {{$doctor->Speciality}} <br/>
                          <label><b>Email:</b></label> {{$doctor->Email_Address}} <br/>
                          <label><b>Date Of Birth:</b></label> {{$doctor->Date_of_Birth}} <br/>
                          {{-- <label><b>Gender:</b></label> {{$doctor->gender}} <br/> --}}
                          <label><b>Residential Address:</b></label> {{$doctor->Current_Address}} <br/>
                          <label><b>Country:</b></label> {{$doctor->countries}} <br/>
                          {{-- <label><b>Country Of Birth:</b></label> {{$doctor->country_of_birth}} <br/>  Marital_Status --}}
                          {{-- <label><b>Other Names:</b></label> {{$doctor->Other_Names}} <br/> --}}
                          <label><b>Mobile:</b></label> {{$doctor->Mobile}} <br/>
                         {{--  <label><b>Post Code:</b></label> {{$doctor->Post_Code}} <br/> --}}
                         <label><b>Grade:</b></label> {{$doctor->grade}} <br/>
                          <label><b>Resume:</b></label>
                          <a href="{{url('/').'/storage/doctor/'.$doctor->cv}}" target="_blank"><i class="fa fa-file"></i></a><br>
                          <label><b>Medical Certificate:</b></label>
                          <a href="{{url('/').'/storage/doctor/'.$doctor->certificate}}" target="_blank"><i class="fa fa-file-text"></i></a>
                      </div>
                  </div>
              </div>
  {{--             <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 mb-4">
                  <div class="card" style="min-height:395px;">
                      <div class="card-body">
                          <h5 class="card-title">Professional Information</h5>
                          <hr>
                          <div class="table-responsive ps ps--theme_default" data-ps-id="be95a16f-bc05-081c-002f-906f0e728ef1">
                             <label><b>Speciality:</b></label> {{$doctor->Speciality}} <br/>
                              <label><b>Driving Licence:</b></label> {{$doctor->Driving_Licence}} <br/>
                              <label><b>Own Transportation:</b></label> {{$doctor->own_transport}} <br/>
                          		<label><b>Name of Society/Union::</b></label> {{$doctor->Training}} <br/>
                          		<label><b>Qualification:</b></label> {{$doctor->Qualification	}}<br/>
                              <label><b>Date Graduated:</b></label> {{$doctor->Date_Graduated }}<br/>
                              <label><b>Are you licensed to practice in Nigeria?</b></label> {{$doctor->licensed_to_practice }}<br/>
                              <label><b>MDCN Portfolio no?</b></label> {{$doctor->Portfolio_no }}<br/>
                          		<label><b>Do you hold Professional Indemnity Insurance::</b></label> {{$doctor->Professional_Insurance}} <br/>
                          		<label><b>Professional Insurance Name:</b></label> {{$doctor->Pro_Name_Insurer}} <br/>
                          		<label><b>Professional Insurance Date Issue:</b></label> {{$doctor->Pro_Date_of_Issue}} <br/>
                          		<label><b>Professional Insurance Expiry Issue:</b></label> {{$doctor->Pro_Date_of_Expiry}} <br/>
                          		<label><b>Do you hold Medical Malpractice Insurance:</b></label> {{$doctor->Medical_Insurance}} <br/>
                          		<label><b>Medical Insurance Name:</b></label> {{$doctor->Medi_Name_Insurer}} <br/>
                              <label><b>Medical Insurance Date Issue: </b></label> {{$doctor->Medi_Date_of_Issue}} <br/>
                              <label><b>Medical Insurance Expiry Issue: </b></label> {{$doctor->Medi_Date_of_Expiry}} <br/>
                              <label><b>Do you hold Union Malpractice Insurance:</b></label> {{$doctor->Union_Insurance}} <br/>
                              <label><b>Union Insurance Name:</b></label> {{$doctor->Union_Name_Insurer}} <br/>
                              <label><b>Union Insurance Date Issue: </b></label> {{$doctor->Union_Date_of_Issue}} <br/>
                              <label><b>Union Insurance Expiry Issue: </b></label> {{$doctor->Union_Date_of_Expiry}} <br/>
                              <label><b>Available from: </b></label> {{$doctor->Available_from}} <br/>
                              <label><b>Nights </b></label> {{$doctor->Nights}} <br/>
                              <label><b>Odd Days </b></label> {{$doctor->Odd_Days}} <br/>
                          		<label><b>Holidays:</b></label> {{$doctor->Holidays }}<br/>
                               <label><b>Weekends </b></label> {{$doctor->Weekends}} <br/>
                              <label><b>Full Time:</b></label> {{$doctor->Full_Time }}<br/>

                          <div class="ps__scrollbar-x-rail" style="left: 0px; bottom: 0px;"><div class="ps__scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__scrollbar-y-rail" style="top: 0px; right: 0px;"><div class="ps__scrollbar-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="row">
              <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 mb-4">
                  <div class="card" style="min-height:395px;height: 100%">
                      <div class="card-body">
                        <h5 class="card-title">Referance Details</h5>
                        <hr>
                        <h4>Referance User 1</h4>
                        <label><b>Name</b></label> {{$doctor->ref_1_Name}} <br/>
                        <label><b>Address</b></label> {{$doctor->ref_1_address}} <br/>
                        <label><b>Email</b></label> {{$doctor->ref_1_email}} <br/>
                        <label><b>Phone</b></label> {{$doctor->ref_1_Telephone}} <br/>
                        <label><b>Post Code</b></label> {{$doctor->ref_1_Post_Code}} <br/>

                        <h4>Referance User 2</h4>
                        <label><b>Name</b></label> {{$doctor->ref_2_Name}} <br/>
                        <label><b>Address</b></label> {{$doctor->ref_2_address}} <br/>
                        <label><b>Email</b></label> {{$doctor->ref_2_email}} <br/>
                        <label><b>Phone</b></label> {{$doctor->ref_2_Telephone}} <br/>
                        <label><b>Post Code</b></label> {{$doctor->ref_2_Post_Code}} <br/>
                      </div>
                    </div>
                  </div>

                   <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 mb-4">
                  <div class="card" style="min-height:395px;">
                      <div class="card-body">
                        <h5 class="card-title">Banking Details</h5>
                        <hr>
                        <label><b>Account Holder Name</b></label> {{$doctor->Account_Name}} <br/>
                        <label><b>Branch Address</b></label> {{$doctor->Branch_Address}} <br/>
                        <label><b>Account Number</b></label> {{$doctor->Account_No}} <br/>
                        <label><b>Bank Name</b></label> {{$doctor->Bank_Name}} <br/>
                        <label><b>IBAN No</b></label> {{$doctor->IBAN}} <br/>
                        <label><b>SWIFT BIC No</b></label> {{$doctor->SWIFT_BIC}} <br/>
                        <label><b>Bank Post Code</b></label> {{$doctor->Bank_Post_Code}} <br/>

                        <h4>Limited Company Bank Details</h4>
                        <label><b>Account Holder Name</b></label> {{$doctor->Limi_Account_Name}} <br/>
                        <label><b>Branch Address</b></label> {{$doctor->Limi_Branch_Address}} <br/>
                        <label><b>Account Number</b></label> {{$doctor->Limi_Account_No}} <br/>
                        <label><b>Bank Name</b></label> {{$doctor->Limi_Bank_Name}} <br/>
                        <label><b>IBAN No</b></label> {{$doctor->Limi_IBAN}} <br/>
                        <label><b>SWIFT BIC No</b></label> {{$doctor->Limi_SWIFT_BIC}} <br/>
                        <label><b>Bank Post Code</b></label> {{$doctor->Limi_Post_Code}} <br/>
                      </div>
                    </div>
                  </div> --}}
</div>
 <div class="modal fade coming_soon" id="myModal" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          {{-- <h4 class="modal-title">Are Sure you want to activate this doctor?</h4> --}}
        </div>
        <div class="modal-body">
          <p style="color: black;">Are sure you want to activate this doctor?</p>
        </div>
        <div style="padding-left: 160px; padding-bottom: 20px;">
          <a href="{{route('activate.doctor',['id'=> $doctor->id])}}"><button type="button" class="btn" style="background-color: rgb(0, 35, 136); border-left-color: rgb(0, 35, 136); border-right-color: rgb(0, 35, 136); color: white;">Yes</button></a>
          <button type="button" class="btn"  data-dismiss="modal" style="display: inline-block; background-color: rgb(187, 35, 67); color: white;">Close</button>
        </div>
      </div>
    </div>
  </div>

   <div class="modal fade coming_soon" id="myModal1" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          {{-- <h4 class="modal-title">Are Sure you want to activate this doctor?</h4> --}}
        </div>
        <div class="modal-body">
          <p style="color: black;">Are sure you want to deactivate this doctor?</p>
        </div>
        <div style="padding-left: 160px; padding-bottom: 20px;">
          <a href="{{route('deactivate.doctor',['id'=> $doctor->id])}}"><button type="button" class="btn" style="background-color: rgb(0, 35, 136); border-left-color: rgb(0, 35, 136); border-right-color: rgb(0, 35, 136); color: white;">Yes</button></a>
          <button type="button" class="btn" data-dismiss="modal" style="display: inline-block; background-color: rgb(187, 35, 67); color: white;">Close</button>
        </div>
      </div>
    </div>
  </div>
@endsection