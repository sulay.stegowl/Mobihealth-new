@extends('layouts.app')
@section('content')
        <div class="content-wrapper">
          <h1 class="page-title">Lab Technician</h1>
          <div class="row grid-margin">
          	@forelse($doctors as $doctor)
                <div class="col-3">
                  <div class="card card-statistics">
                  	<a href="{{route('doctor.details',['id'=> $doctor->id])}}">
                    <div class="card-body tile">
                    	<div>
                        <p class="highlight-text">
                      
                          @if($doctor->image!='')
                          <img src="{{url('/').'/storage/doctor/'.$doctor->image}}" height="100" width="100" class="img-round">
                          @else
                          <img src="https://www.mobihealthinternational.com/assets/images/logo2.png" height="100" width="100" class="img-round">
                          @endif
                        </p>
                         <div class="text-center">
                        <h3>
                          {{$doctor->First_Name.' '.$doctor->Surname}}
                        </h3>
                        </div>
                        <h3>{{$doctor->Profession}}</h3>
                        <h3>Country: {{$doctor->countries}}</h3>
                      </div>
                    </div>
                </a>
                  </div>
                </div>
                @empty
                <h3>No Lab Technician Register Yet!!!</h3>
                @endforelse
            </div>
        </div>
@endsection