@extends('layouts.app')
@section('content')
<div class="content-wrapper">

          <h1 class="page-title">Patients Request</h1>
          <div class="card">
            <div class="card-body">
             
              <div class="row">
                <div class="col-12">
                  <table id="order-listing" class="table" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                          <th>Name</th>
                          <th>Phone</th>
                          <th>Email	</th>
                          <th>Plan</th>
                          <th>Country</th>
                      </tr>
                    </thead>
                    <tbody>
                    	@forelse($data as $value)
                      <tr>
                          <td>{{$value->name}}</td>
                          <td>{{$value->phone}}</td>
                          <td>{{$value->email}}</td>
                          <td>{{$value->plan}}</td>
                          <td>{{$value->country}}</td>
                      </tr>
                      @empty
                     
                      <tr>
                      	<td>No Subscription request yet.</td>
                      </tr>
                      @endforelse
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
         
        </div>
</div>
@endsection