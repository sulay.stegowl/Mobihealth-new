@extends('layouts.app')
@section('content')
<div class="content-wrapper">
    <div class="row grid-margin">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h2 class="card-title">Add a doctor</h2>
                    {{--     
                    <form id="example-validation-form" action="{{route('save.doctor')}}" enctype="multipart/form-data" method="POST">
                        {{csrf_field()}}
                        <div>
                            <h3>Create Account</h3>
                            <section>
                                <label for="userName">First name *</label>
                                <input id="userName" name="fname" type="text" class="form-control" >
                                <label for="password">Last Name *</label>
                                <input id="userName" name="lname" type="text" class="form-control">
                                <label for="confirm">Email *</label>
                                <input id="email" name="email" type="text" class="form-control">
                                <label for="confirm">Specialty *</label>
                                <input id="specialty" name="specialty" type="text" class="form-control">
                            </section>
                            <h3>Profile Info</h3>
                            <section>
                                <label for="name">Title *</label>
                                <select name="title" class="form-control required">
                                    <option>Select</option>
                                    <option value="Dr">Dr.</option>
                                    <option value="Mr">Mr.</option>
                                    <option value="Mrs">Mrs.</option>
                                    <option value="Ms">Ms.</option>
                                    <option value="Prof">Prof.</option>
                                </select>
                                <label for="name">First name *</label>
                                <input id="name" name="first_name" type="text" class="form-control required">
                                <label for="name">Middle name *</label>
                                <input id="name" name="middle_name" type="text" class="form-control required">
                                <label for="surname">Last name *</label>
                                <input id="surname" name="last_name" type="text" class="form-control required">
                                <label for="surname">Suffix *</label>
                                <input id="surname" name="suffix" type="text" class="form-control required">
                                <label for="address">Languages</label>
                                <input id="surname" name="language" type="text" class="form-control">
                                <label for="address">Current Employer</label>
                                <input id="surname" name="current_employer" type="text" class="form-control">
                                <label for="address">Current Employer</label>
                                <input id="surname" name="current_employer" type="text" class="form-control">
                                <label for="address">Gender</label>
                                <input type="radio" name="gender" value="Male" class="dis_inline"> 
                                <p class="dis_inline"> Male </p>
                                <input type="radio" name="gender" value="female" class="dis_inline"> 
                                <p class="dis_inline"> Female </p>
                                <br><br>
                                <label for="address">Upload Photo</label>
                                <input type="file" name="image" class="form-control">
                            </section>
                            <h3>Professional Info</h3>
                            <section>
                                <label for="address">Degree</label>
                                <input id="surname" name="degree" type="text" class="form-control">
                                <label for="address">Professional Registration number</label>
                                <input id="surname" name="prof_regi_no" type="text" class="form-control">
                                <label for="address">MDCN No</label>
                                <input id="surname" name="MDCN_no" type="text" class="form-control">
                                <label for="address">Is your MDCN Active?</label>
                                <input id="surname" name="MDCN_active" type="text" class="form-control">
                                <label for="address">Medical/Graduate School</label>
                                <input id="surname" name="school" type="text" class="form-control">
                                <label for="address">Graduating year</label>
                                <input id="surname" name="graduating_year" type="text" class="form-control">
                                <label for="address">Residency</label>
                                <input id="surname" name="residency" type="text" class="form-control">
                                <label for="address">Year completed</label>
                                <input id="surname" name="year_completed" type="text" class="form-control">
                                <label for="address">Years of Practice</label>
                                <input id="surname" name="years_of_practice" type="text" class="form-control">
                                <label for="address">Do you currently have indemnity?</label>
                                <input id="surname" name="indemnity" type="text" class="form-control">
                                <label for="address">Indemnity Provider?</label>
                                <input id="surname" name="indemnity_provider" type="text" class="form-control">
                                <label for="address">States Licensed In</label>
                                <input id="surname" name="states_licensed" type="text" class="form-control">
                                <label for="address">Do you have any pending/completed investigation with your regulatory body?</label>
                                <input id="surname" name="regulatory_body" type="text" class="form-control">
                                <label for="address">Are you in ‘’good standing’’ with your local regulatory body?</label>
                                <input id="surname" name="regulatory_body_status" type="text" class="form-control">
                                <label for="address">Upload CV</label>
                                <input id="surname" name="cv" type="file" class="form-control">
                                <label for="address">Upload Medical certificates</label>
                                <input id="surname" name="medical_certi" type="file" class="form-control">
                                <label for="address"><b>Referee 1</b></label><br/>
                                <label for="address">Name</label>
                                <input id="surname" name="ref_name_1" type="text" class="form-control">
                                <label for="address">Designation</label>
                                <input id="surname" name="ref_desg_1" type="text" class="form-control">
                                <label for="address">Phone:</label>
                                <input id="surname" name="ref_phone_1" type="text" class="form-control">
                                <label for="address">Email</label>
                                <input id="surname" name="ref_email_1" type="text" class="form-control">
                                <label for="address"><b>Referee 2</b></label><br/>
                                <label for="address">Name</label>
                                <input id="surname" name="ref_name_2" type="text" class="form-control">
                                <label for="address">Designation</label>
                                <input id="surname" name="ref_desg_2" type="text" class="form-control">
                                <label for="address">Phone:</label>
                                <input id="surname" name="ref_phone_2" type="text" class="form-control">
                                <label for="address">Email</label>
                                <input id="surname" name="ref_email_2" type="text" class="form-control">
                            </section>
                            <h3>Availability</h3>
                            <section>
                                <label for="address">How many hours per day/week/month are you available?</label>
                                <input id="surname" name="availability" type="text" class="form-control"> 
                                <label for="address">What is your best availability? AM/PM</label>
                                <input id="surname" name="best_availability" type="text" class="form-control">
                            </section>
                            <h3>Banking Details</h3>
                            <section >
                                <label for="address">Routing Number</label>
                                <input id="surname" name="routing_number" type="text" class="form-control">
                                <label for="address">Account Number</label>
                                <input id="surname" name="account_number" type="text" class="form-control"> 
                                <label for="address">Account Holder Name</label>
                                <input id="surname" name="account_holder_name" type="text" class="form-control">
                                <label for="address">Bank  Name</label>
                                <input id="surname" name="bank_name" type="text" class="form-control">
                                <label for="address">Country</label>
                                <input id="surname" name="country" type="text" class="form-control">
                                <input type="submit" name="submit" value="submit" class="btn btn-primary">
                            </section>
                        </div>
                    </form>
                    --}}
                    <div class="stepwizard">
                        <div class="stepwizard-row setup-panel">
                          <div class="bg_overlay"></div>
                            <div class="stepwizard-step">
                                <a data-link="#step-1"  type="button" class="btn btn-primary btn-circle"> Create Account </a>
                            </div>
                            <div class="stepwizard-step">
                                <a data-link="#step-2"  type="button" class="btn btn-default btn-circle" disabled="disabled">Profile Info</a>
                            </div>
                            <div class="stepwizard-step">
                                <a data-link="#step-3"  type="button" class="btn btn-default btn-circle" disabled="disabled">Professional Info</a>
                            </div>
                            <div class="stepwizard-step">
                                <a data-link="#step-4"  type="button" class="btn btn-default btn-circle" disabled="disabled">Availability</a>
                            </div>
                            <div class="stepwizard-step">
                                <a data-link="#step-5" type="button" class="btn btn-default btn-circle" disabled="disabled">Banking Details</a>
                            </div>
                        </div>
                    </div>
                    <form role="form" action="{{route('save.doctor')}}" enctype="multipart/form-data" method="POST">
                        {{csrf_field()}}
                        <div class="row setup-content" id="step-1">
                            <div class="col-lg-12">
                                <div class="col-md-12">
                                    <h3>Create Account</h3>
                                          <div class="form-group">
                                        <label for="name">Types Of Medical Professionals </label>
                                        <select name="type" class="form-control required select2-selection select2-selection--single" required="required">
                                            <option>Select</option>
                                            <option value="Doctor">Doctor</option>
                                            <option value="Dentist">Dentist</option>
                                            <option value="Laboratory_Technician">Laboratory Technician</option>
                                            <option value="Nurses">Nurses</option>
                                            <option value="Physiotherapist">Physiotherapist</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="userName">First name *</label>
                                        <input name="fname" maxlength="100" type="text" required="required" class="form-control" placeholder="Enter First Name" value="{{old('fname')}}" />
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Last Name *</label>
                                        <input name="lname" maxlength="100" type="text" required="required" class="form-control" placeholder="Enter Last Name" value="{{old('lname')}}"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="confirm">Email *</label>
                                        <input name="email" type="email" required="required" class="form-control" placeholder="Enter Email" value="{{old('email')}}"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="confirm">Specialty *</label>
                                        <input name="specialty" maxlength="100" type="text" required="required" class="form-control" placeholder="Enter Specialty"  value="{{old('specialty')}}"/>
                                    </div>
                                </div>

                                   <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
                            </div>
                         
                        </div>
                        <div class="row setup-content" id="step-2">
                            <div class="col-lg-12">
                                <div class="col-md-12">
                                    <h3>Profile Info</h3>
                                    <div class="form-group">
                                        <label for="name">Title *</label>
                                        <select name="title" class="form-control required select2-selection select2-selection--single" required="required">
                                            <option>Select</option>
                                            <option value="Dr">Dr.</option>
                                            <option value="Mr">Mr.</option>
                                            <option value="Mrs">Mrs.</option>
                                            <option value="Ms">Ms.</option>
                                            <option value="Prof">Prof.</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="name">First name *</label>
                                        <input name="first_name" maxlength="100" type="text" required="required" class="form-control" placeholder="Enter First Name" value="{{old('first_name')}}"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Middle name *</label>
                                        <input name="middle_name" maxlength="100" type="text" required="required" class="form-control" placeholder="Enter Middle Name" value="{{old('middle_name')}}"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="surname">Last name *</label>
                                        <input name="last_name" maxlength="100" type="text" required="required" class="form-control" placeholder="Enter Last Name" value="{{old('last_name')}}"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="surname">Date Of Birth *</label>
                                        <div class="input-group date datepicker b-l-blue">
                                            <input type="text" class="form-control" name="dob" value="{{old('dob')}}">
                                            <div class="input-group-addon">
                                              <span class="mdi mdi-calendar-blank"></span>
                                            </div>
                                          </div>
                                    </div>
                                     <div class="form-group">
                                        <label for="address">About</label>
                                        <textarea class="form-control" name="about" required="required">{{old('about')}}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="address">Residential Address</label>
                                        <textarea class="form-control" name="residential_address" required="required">{{old('residential_address')}}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="surname">Nationality</label>
                                        <input name="nationality" maxlength="100" type="text" required="required" class="form-control" placeholder="Enter Nationality" value="{{old('nationality')}}"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="surname">Country Of Birth</label>
                                        {{-- <input name="country_of_birth" maxlength="100" type="text" required="required" class="form-control" placeholder="Enter Country Of Birth" value="{{old('country_of_birth')}}"/> --}}
                                        <select name="country_of_birth" class="form-control required select2-selection select2-selection--single">
                                        	@foreach($country as $value)
                                        	<option value="{{$value}}">{{$value}}</option>
                                        	@endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="surname">Ethnicity</label>
                                        <input name="ethnicity" maxlength="100" type="text" required="required" class="form-control" placeholder="Enter Ethnicity" value="{{old('ethnicity')}}"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="surname">Previous Forename</label>
                                        <input name="previous_forename" maxlength="100" type="text" required="required" class="form-control" placeholder="Enter Previous Forename" value="{{old('previous_forename')}}"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="surname">Previous Surname</label>
                                        <input name="previous_surname" maxlength="100" type="text" required="required" class="form-control" placeholder="Enter Previous Surname" value="{{old('previous_surname')}}"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="surname">Suffix *</label>
                                        <input name="suffix" maxlength="100" type="text" required="required" class="form-control" placeholder="Enter Suffix " value="{{old('suffix')}}" />
                                    </div>
                                    <div class="form-group">
                                        <label for="address">Languages</label>
                                        <input name="language" maxlength="100" type="text" required="required" class="form-control" placeholder="Enter Languages " value="{{old('language')}}"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="address">Current Employer</label>
                                        <input name="current_employer" maxlength="100" type="text" required="required" class="form-control" placeholder="Enter Current Employer" value="{{old('current_employer')}}"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="address">Gender</label>
                                        <input type="radio" name="gender" value="Male" class="dis_inline" checked> 
                                        <p class="dis_inline"> Male </p>
                                        <input type="radio" name="gender" value="female" class="dis_inline"> 
                                        <p class="dis_inline"> Female </p>
                                    </div>
                                    <div class="form-group">
                                        <label for="address">Upload Photo</label>
                                        <input type="file" name="image" class="form-control" required="required">
                                    </div>
                                </div>
                                  <button class="btn btn-primary preBtn btn-lg " type="button" >Previous</button>
                                 <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
                            </div>
                           
                        </div>
                        <div class="row setup-content" id="step-3">
                            <div class="col-lg-12">
                                <div class="col-md-12">
                                    <h3>Professional Info</h3>
                                    <div class="form-group">
                                        <label for="address">Degree</label>
                                        <input name="degree" maxlength="100" type="text" required="required" class="form-control" placeholder="Enter Degree" value="{{old('degree')}}"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="address">Professional Registration number</label>
                                        <input name="prof_regi_no" maxlength="100" type="text" required="required" class="form-control" placeholder="Enter Professional Registration number" value="{{old('prof_regi_no')}}"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="address">MDCN No</label>
                                        <input name="MDCN_no" maxlength="100" type="text" required="required" class="form-control" placeholder="Enter MDCN No" value="{{old('MDCN_no')}}"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="address">Is your MDCN Active?</label>
                                        <input name="MDCN_active" maxlength="100" type="text" required="required" class="form-control" placeholder="Is your MDCN Active?" value="{{old('MDCN_active')}}"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="address">MDCN Expiry Date?</label>
                                        <input name="MDCN_expiry_date" maxlength="100" type="text" required="required" class="form-control" placeholder="MDCN Expiry Date?" value="{{old('MDCN_expiry_date')}}"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="address">Country Of Practice?</label>
                                        <input name="country_of_practice" maxlength="100" type="text" required="required" class="form-control" placeholder="Country Of Practice?" value="{{old('country_of_practice')}}"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="address">Primary Qualification</label>
                                        <input name="primary_qualification" maxlength="100" type="text" required="required" class="form-control" placeholder="Primary Qualification" value="{{old('primary_qualification')}}"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="address">Grade</label>
                                        <input name="grade" maxlength="100" type="text" required="required" class="form-control" placeholder="Grade" value="{{old('grade')}}"/>
                                    </div>
                                     <div class="form-group">
                                        <label for="address">Expiry date of current license?</label>
                                       {{--  <input name="expirty_date_license" maxlength="100" type="text" required="required" class="form-control" placeholder="Expiry date of current license?" value="{{old('expirty_date_license')}}"/> --}}
                                        <div class="input-group date datepicker b-l-blue">
                                            <input type="text" class="form-control" name="expirty_date_license" value="{{old('expirty_date_license')}}">
                                            <div class="input-group-addon">
                                              <span class="mdi mdi-calendar-blank"></span>
                                            </div>
                                          </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="address">Portfolio No?</label>
                                        <input name="portfolio_no" maxlength="100" type="text" required="required" class="form-control" placeholder="Portfolio No?" value="{{old('portfolio_no')}}"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="address">Medical/Graduate School</label>
                                        <input name="school_name" maxlength="100" type="text" required="required" class="form-control" placeholder="Enter Medical/Graduate School" value="{{old('MDCN_active')}}"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="address">Graduating year</label>
                                        <input name="graduating_year" maxlength="100" type="number" required="required" class="form-control" placeholder="Enter Graduating year" value="{{old('graduating_year')}}"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="address">Residency</label>
                                        <input name="residency" maxlength="100" type="text" required="required" class="form-control" placeholder="Enter Residency" value="{{old('residency')}}"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="address">Year completed</label>
                                        <input name="year_complete" maxlength="100" type="text" required="required" class="form-control" placeholder="Enter Year completed" value="{{old('year_completed')}}"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="address">Years of Practice</label>
                                        <input name="years_of_practice" maxlength="100" type="text" required="required" class="form-control" placeholder="Enter Years of Practice" value="{{old('years_of_practice')}}"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="address">Do you currently have indemnity?</label>
                                        <input name="indemnity" maxlength="100" type="text" required="required" class="form-control" placeholder="Do you currently have indemnity?" value="{{old('indemnity')}}"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="address">Indemnity Provider?</label>
                                        <input name="indemnity_provider" maxlength="100" type="text" required="required" class="form-control" placeholder="Indemnity Provider?" value="{{old('indemnity_provider')}}"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="address">States Licensed In</label>
                                        <input name="states_licensed" maxlength="100" type="text" required="required" class="form-control" placeholder="States Licensed In" value="{{old('states_licensed')}}"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="address">Do you have any pending/completed investigation with your regulatory body?</label>
                                        <input name="regulatory_body" maxlength="100" type="text" required="required" class="form-control"  value="{{old('regulatory_body')}}"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="address">Are you in ‘’good standing’’ with your local regulatory body?</label>
                                        <input name="regulatory_body_status" maxlength="100" type="text" required="required" class="form-control"  value="{{old('regulatory_body_status')}}"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="address">Upload CV</label>
                                        <input name="cv" maxlength="100" type="file" required="required" class="form-control"  />
                                    </div>
                                    <div class="form-group">
                                        <label for="address">Upload Medical certificates</label>
                                        <input name="medical_certi" maxlength="100" type="file" required="required" class="form-control"  />
                                    </div>
                                    <div class="form-group">
                                        <label for="address"><b>Referee 1</b></label><br/>
                                    </div>
                                    <div class="form-group">
                                        <label for="address">Name</label>
                                        <input name="ref_name_1" maxlength="100" type="text" required="required" class="form-control"  />
                                    </div>
                                    <div class="form-group">
                                        <label for="address">Designation</label>
                                        <input name="ref_desg_1" maxlength="100" type="text" required="required" class="form-control"  />
                                    </div>
                                    <div class="form-group">
                                        <label for="address">Phone:</label>
                                        <input name="ref_phone_1" maxlength="100" type="text" required="required" class="form-control"  />
                                    </div>
                                    <div class="form-group">
                                        <label for="address">Email</label>
                                        <input name="ref_email_1" maxlength="100" type="email" required="required" class="form-control"  value="{{old('ref_email_1')}}"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="address"><b>Referee 2</b></label><br/>
                                    </div>
                                    <div class="form-group">
                                        <label for="address">Name</label>
                                        <input name="ref_name_2" maxlength="100" type="text" required="required" class="form-control"  value="{{old('ref_name_2')}}"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="address">Designation</label>
                                        <input name="ref_desg_2" maxlength="100" type="text" required="required" class="form-control"  value="{{old('ref_desg_2')}}"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="address">Phone:</label>
                                        <input name="ref_phone_2" maxlength="100" type="text" required="required" class="form-control" value="{{old('ref_phone_2')}}"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="address">Email</label>
                                        <input name="ref_email_2" maxlength="100" type="email" required="required" class="form-control" value="{{old('ref_email_2')}}"/>
                                    </div>
                                </div>
                                  <button class="btn btn-primary preBtn btn-lg " type="button" >Previous</button>
                                  <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
                            </div>
                          
                        </div>
                        <div class="row setup-content" id="step-4">
                            <div class="col-lg-12">
                                <div class="col-md-12">
                                    <h3>Availability</h3>
                                    <div class="form-group">
                                        <label for="address">How many hours per day/week/month are you available?</label>
                                        <input name="availability" maxlength="100" type="text" required="required" class="form-control" placeholder="Enter Last Name" value="{{old('availability')}}"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="address">What is your best availability? AM/PM</label>
                                        <input name="best_availability" maxlength="100" type="text" required="required" class="form-control" placeholder="Enter Last Name" value="{{old('best_availability')}}"/>
                                    </div>
                                </div>
                                  <button class="btn btn-primary preBtn btn-lg " type="button" >Previous</button>
                                   <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
                            </div>
                         
                        </div>
                        <div class="row setup-content" id="step-5">
                            <div class="col-lg-12">
                                <div class="col-md-12">
                                    <h3>Banking Details</h3>
                                    <div class="form-group">
                                        <label for="address">Routing Number</label>
                                        <input name="routing_number" maxlength="100" type="text" required="required" class="form-control" placeholder="Enter Last Name" value="{{old('routing_number')}}"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="address">Account Number</label>
                                        <input name="account_number" maxlength="100" type="text" required="required" class="form-control" placeholder="Enter Last Name" value="{{old('account_number')}}"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="address">Account Holder Name</label>
                                        <input name="account_holder_name" maxlength="100" type="text" required="required" class="form-control" placeholder="Enter Last Name" value="{{old('account_holder_name')}}"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="address">Bank  Name</label>
                                        <input name="bank_name" maxlength="100" type="text" required="required" class="form-control" placeholder="Enter Last Name" value="{{old('bank_name')}}"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="address">Country</label>
                                        <input name="country" maxlength="100" type="text" required="required" class="form-control" placeholder="Enter Last Name" value="{{old('country')}}"/>
                                    </div>
                                    <input type="hidden" name="status" value="1">
                                </div>
                                  <button class="btn btn-primary preBtn btn-lg " type="button" >Previous</button>
                                 <button class="btn btn-success btn-lg pull-right" type="submit">Finish!</button>
                            </div>
                           
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection