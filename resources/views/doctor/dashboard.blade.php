    @extends('layouts.app')
     @section('content')
        <div class="content-wrapper">
          <h1 class="page-title">Dashboard</h1>
          <div class="row grid-margin">
                <div class="col-3">
                  <a href="{{route('doctor.list')}}">
                  <div class="card card-statistics">
                    <div class="card-body tile1">
                        <p class="highlight-text">
                        <i class="fa fa-user-md text-success"></i>
                          {{$data['doctor']}}
                        </p>
                        <h3>
                          Doctors
                        </h3>
                    </div>
                  </div>
                  </a>
                </div>
                
                <div class="col-3">
                  <a href="{{route('dentists.list')}}">
                  <div class="card card-statistics">
                    <div class="card-body tile1">
                      <p class="highlight-text">
                       <i class="fa fa-stethoscope text-info"></i>
                        {{$data['dentist']}}
                      </p>
                      <h3>
                        Dentist
                      </h3>
                    </div>
                  </div>
                   </a>
                </div>
           
            
                <div class="col-3">
                  <a href="{{route('laborarty.list')}}">
                  <div class="card card-statistics">
                    <div class="card-body tile1">
                      <p class="highlight-text">
                      <i class="fa fa-flask text-danger"></i>
                        {{$data['lab']}}
                      </p>
                      <h3>
                        Lab Technician
                      </h3>
                    </div>
                  </div>
                   </a>
                </div>
             
          {{-- </div> --}}

           {{--  <div class="row grid-margin" > --}}
            
                <div class="col-3">
                   <a href="{{route('nurse.list')}}">
                  <div class="card card-statistics">
                    <div class="card-body tile1">
                        <p class="highlight-text">
                          <i class="fa fa-female text-warning"></i>
                        {{$data['nurse']}}
                        </p>
                        <h3>
                         Nurses
                        </h3>
                    </div>
                  </div>
                     </a> 
                </div>
              
                </div>  
                 <div class="row grid-margin" >   
                
                <div class="col-3">
                    <a href="{{route('physiotherapist.list')}}"> 
                  <div class="card card-statistics">
                    <div class="card-body tile1">
                      <p class="highlight-text">
                       <i class="fa fa-universal-access text-info"></i>
                         {{$data['physiotherapist']}}
                      </p>
                      <h3>
                        Physiotherapist
                      </h3>
                    </div>
                  </div>
                  </a>
                </div>
              
              
                <div class="col-3">
                   <a href="{{route('pharmacist.list')}}"> 
                  <div class="card card-statistics">
                    <div class="card-body tile1">
                      <p class="highlight-text">
                       <i class="fa fa-medkit text-success"></i>
                         {{$data['radio']}}
                      </p>
                      <h3>
                        Pharmacist
                      </h3>
                    </div>
                  </div>
                   </a> 
              </div>  
             
                

          {{-- </div> --}}


          
        </div>
        @endsection
                   <script>
    window.onload = function () {
        if (typeof history.pushState === "function") {
            history.pushState("jibberish", null, null);
            window.onpopstate = function () {
                history.pushState('newjibberish', null, null);
            };
        } else {
            var ignoreHashChange = true;
            window.onhashchange = function () {
                if (!ignoreHashChange) {
                    ignoreHashChange = true;
                    window.location.hash = Math.random();
                } else {
                    ignoreHashChange = false;   
                }
            };
        }
    }
 </script>