@extends('layouts.app')
@section('content')
<div class="content-wrapper">

          <h1 class="page-title">Laboratories List</h1>
          <div class="card">
            <div class="card-body">
              <h2 class="card-title">Laboratories</h2>
              <div class="row">
                <div class="col-12">
                  <table id="order-listing" class="table" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                          <th>Business Name</th>
                          <th>Contact Person</th>
                          <th>Contact Email	</th>
                          <th>Whatsapp No</th>
                          <th>Website</th>
                          <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                    	@if(count($lab)>0)
                    	@foreach($lab as $value)
                      <tr>
                          <td>{{$value->business_name}}</td>
                          <td>{{$value->contact_person}}</td>
                          <td>{{$value->contact_email}}</td>
                          <td>{{$value->whatsapp_no}}</td>
                          <td>{{$value->website}}</td>
                          <td>
                            <button class="btn btn-outline-primary">View</button>
                            <button class="btn btn-outline-info">Edit</button>
                            <button class="btn btn-outline-danger">Delete</button>
                          </td>
                      </tr>
                      @endforeach
                      @else
                      <tr>
                      	<td>No Laborarties Registered.</td>
                      </tr>
                      @endif
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
         
        </div>
</div>
@endsection