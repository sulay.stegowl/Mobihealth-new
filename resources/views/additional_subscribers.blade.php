@extends('layouts.app')
@section('content')
        <div class="content-wrapper">
          <h1 class="page-title">Subscribers</h1>
          <div class="row grid-margin">
            @forelse($data as $value)
                <div class="col-3">
                  <div class="card card-statistics">
                     <a href="{{route('additional.subscriber.details',['id'=> $value->id])}}">  
                    <div class="card-body tile">
                      <div style="display: flex;">
                        <p class="highlight-text">
                      
                          
                        </p>
                        <h3>
                          {{$value->business_name}}
                        </h3>
                        </div>
                    </div>
                </a>
                  </div>
                </div>
                @empty
                <h3>No Subscribers Request Yet!!!</h3>
                @endforelse
            </div>
        </div>
@endsection