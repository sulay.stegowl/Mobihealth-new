@extends('layouts.app')
@section('content')
<div class="content-wrapper">
    <div class="row grid-margin">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h2 class="card-title">Telemedicine</h2>
                  
                  
                    <form role="form" action="{{route('telemedicine_save')}}" enctype="multipart/form-data" method="POST">
                        {{csrf_field()}}
                        <div class="row " >
                            <div class="col-lg-12">
                                <div class="col-md-12">
                                 
                                    
                                    <div class="form-group">
                                        <label for="userName">Title *</label>
                                        <input name="title"  type="text" required="required" class="form-control" value="{{$data->title}}" />
                                    </div>

                                    <div class="form-group">
                                        <label for="userName">Text *</label>
                                        <input name="text"  type="text" required="required" class="form-control textarea" value="{{$data->text}}" />
                                    </div>

                                    <div class="form-group">
                                        <label for="userName">Image *</label>
                                        <input name="image" type="file" class="form-control" />
                                        <img src="{{url('/').'/storage/'.$data->image}}" width="100" height="100">
                                    </div>

                                   
                                   
                                </div>

                                  
                            </div>
                         

                        </div>
                    
                    <div class="form-group">
                        <input type="submit" name="" value="Submit" class="btn btn-primary">
                    </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection