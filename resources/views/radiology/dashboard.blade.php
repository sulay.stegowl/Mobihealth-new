    @extends('layouts.app')
     @section('content')
        <div class="content-wrapper">
          <h1 class="page-title">Dashboard</h1>
          <div class="row grid-margin">
                <div class="col-3">
                  <div class="card card-statistics">
                    <div class="card-body tile1">
                        <p class="highlight-text">
                        <i class="mdi mdi-compass-outline text-success"></i>
                          {{$data['radiology']}}
                        </p>
                        <h3>
                          Radiologiest
                        </h3>
                    </div>
                  </div>
                </div>
                <div class="col-3">
                  <div class="card card-statistics">
                    <div class="card-body tile1">
                      <p class="highlight-text">
                       <i class="fa fa-handshake-o text-info"></i>
                        {{$data['subscriber']}}
                      </p>
                      <h3>
                        Subscriber
                      </h3>
                    </div>
                  </div>
                </div>
            
                <div class="col-3">
                  <div class="card card-statistics">
                    <div class="card-body tile1">
                      <p class="highlight-text">
                      <i class="fa fa-hospital-o text-danger"></i>
                        {{$data['request']}}
                      </p>
                      <h3>
                        Request
                      </h3>
                    </div>
                  </div>
                </div>
          {{-- </div> --}}

           {{--  <div class="row grid-margin" > --}}
                <div class="col-3">
                  <div class="card card-statistics">
                    <div class="card-body tile1">
                        <p class="highlight-text">
                          <i class="fa fa-comment text-warning"></i>
                         {{$data['messages']}}
                        </p>
                        <h3>
                         Messages
                        </h3>
                    </div>
                  </div>
                </div>  
                </div>  

          {{-- </div> --}}


          
        </div>
        @endsection
                   <script>
    window.onload = function () {
        if (typeof history.pushState === "function") {
            history.pushState("jibberish", null, null);
            window.onpopstate = function () {
                history.pushState('newjibberish', null, null);
            };
        } else {
            var ignoreHashChange = true;
            window.onhashchange = function () {
                if (!ignoreHashChange) {
                    ignoreHashChange = true;
                    window.location.hash = Math.random();
                } else {
                    ignoreHashChange = false;   
                }
            };
        }
    }
 </script>