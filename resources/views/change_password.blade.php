    @extends('layouts.app')
     @section('content')
               <div class="content-wrapper">

          <h1 class="page-title">Change Password</h1>
          <div class="row">
              <div class="col-12 col-lg-6 grid-margin">
                  <div class="card">
                      <div class="card-body">
                          <h2 class="card-title">Change Your Account Password</h2>
                          <form class="forms-sample" method="POST" action="{{route('password_change')}}">
                            {{csrf_field()}}
                                <div class="form-group">
                                  <label for="exampleInputPassword1">Old Password</label>
                                  <input type="password" class="form-control p-input" id="exampleInputPassword1" placeholder=" Old Password" name="old">
                              </div>

                              <div class="form-group">
                                  <label for="exampleInputPassword1">New Password</label>
                                  <input type="password" class="form-control p-input" id="exampleInputPassword1" placeholder="New Password" name="new">
                              </div>
                             
                               <div class="form-group">
                                  <label for="exampleInputPassword1">Confirm Password</label>
                                  <input type="password" class="form-control p-input" id="exampleInputPassword1" placeholder="Confirm Password" name="confirm">
                              </div>
                              <button type="submit" class="btn btn-primary">Submit</button>
                          </form>
                      </div>
                  </div>
              </div>
            </div>
          </div>
        @endsection
