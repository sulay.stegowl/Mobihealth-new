@extends('layouts.app')
@section('content')
<div class="content-wrapper">

          <h1 class="page-title">Services Details</h1>
          <a href="{{route('add_services')}}" class="btn btn-primary" style="margin-bottom: 10px;">Add Services</a>
          <div class="card">
            <div class="card-body">
             
              <div class="row">
                <div class="col-12">
                  <table id="order-listing" class="table" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                          <th>Title</th>
                          <th>Type</th>
                          <th>Description	</th>
                          <th>Image</th>
                          <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    	@forelse($services as $value)
                      <tr>
                          <td>{{$value->title}}</td>
                          <td>{{$value->type}}</td>
                          <td>{{substr($value->desc,0,100).'...'}}</td>
                          <td><img src="{{url('/').'/storage/services/'.$value->image}}" height="100" width="100"></td>
                           <td>
                            <a href="{{route('edit_services',['id'=>$value->id])}}"><button class="btn btn-outline-info">Edit</button></a>
                            <form action="{{route('service_delete',['id'=>$value->id])}}" method="POST">                   
                            {{csrf_field()}}
                            {{method_field('DELETE')}}
                            <button class="btn btn-outline-danger">Delete</button>
                          </form>
                          </td>
                      </tr>
                      @empty
                     
                      <tr>
                      	<td>No Services.</td>
                      </tr>
                      @endforelse
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
         
        </div>
</div>
@endsection