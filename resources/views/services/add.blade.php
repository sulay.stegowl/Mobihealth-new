    @extends('layouts.app')
     @section('content')
               <div class="content-wrapper">

          <h1 class="page-title">Add Services</h1>
          <div class="row">
              <div class="col-12  grid-margin">
                  <div class="card">
                      <div class="card-body">
                          
                          <form class="forms-sample" method="POST" action="{{route('save_services')}}" enctype="multipart/form-data">
                            {{csrf_field()}}
                                <div class="form-group">
                                  <label for="exampleInputPassword1">Title</label>
                                  <input type="text" class="form-control p-input" id="exampleInputPassword1" placeholder=" Enter Title" name="title">
                              </div>

                              <div class="form-group">
                                  <label for="exampleInputPassword1">Type</label>
                                 <select class="form-control required select2-selection select2-selection--single" name="type" required="required">
                                   <option value="Pathology">Pathology</option>
                                   <option value="Pharmacies">Pharmacies</option>
                                   <option value="Hospital">Hospital</option>
                                   <option value="Radiology">Radiology</option>
                                 </select>
                              </div>

                                  <div class="form-group">
                                  <label for="exampleInputPassword1">Country</label>
                                 <select class="form-control required select2-selection select2-selection--single" name="country" required="required">
                                   <option value="Ethopia">Ethopia</option>
                                   <option value="Nigeria">Nigeria</option>
                                   <option value="Tanzania">Tanzania</option>
                                   <option value="Ghana">Ghana</option>
                                 </select>
                              </div>
                                <div class="form-group">
                                  <label for="exampleInputPassword1">Email</label>
                                  <input type="text" class="form-control p-input" id="exampleInputPassword1" placeholder=" Enter Email" name="email" value="{{old('$service->email')}}">
                              </div>

                              <div class="form-group">
                                  <label for="exampleInputPassword1">Phone No</label>
                                  <input type="number" class="form-control p-input" id="exampleInputPassword1" placeholder=" Enter Phone Number" name="phone" value="{{old('$service->phone')}}">
                              </div>

                              <div class="form-group">
                                  <label for="exampleInputPassword1">Address</label>
                                  <textarea class="form-control" rows="3" name="address" >{{old('address')}}</textarea>
                              </div>
                               <div class="form-group">
                                  <label for="exampleInputPassword1">Description</label>
                                  <textarea class="form-control" rows="10" name="desc" >{{old('desc')}}</textarea>
                              </div>

                              <div class="form-group">
                                  <label for="exampleInputPassword1">Upload Image</label>
                                  <input type="file" class="form-control" id="exampleInputPassword1" name="image">
                              </div>
                              <button type="submit" class="btn btn-primary">Submit</button>
                          </form>
                      </div>
                  </div>
              </div>
            </div>
          </div>
        @endsection
