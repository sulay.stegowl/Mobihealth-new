    @extends('layouts.app')
     @section('content')
               <div class="content-wrapper">

          <h1 class="page-title">Edit Services</h1>
          <div class="row">
              <div class="col-12  grid-margin">
                  <div class="card">
                      <div class="card-body">
                          
                          <form class="forms-sample" method="POST" action="{{route('update_services')}}" enctype="multipart/form-data">
                            {{csrf_field()}}
                           
                                <div class="form-group">
                                  <label for="exampleInputPassword1">Title</label>
                                  <input type="text" class="form-control p-input" id="exampleInputPassword1" placeholder=" Enter Title" name="title" value="{{$service->title}}">
                              </div>
                              <input type="hidden" name="id" value="{{$service->id}}">
                              <div class="form-group">
                                  <label for="exampleInputPassword1">Type</label>
                                 <select class="form-control required select2-selection select2-selection--single" name="type" required="required">
                                   <option @if($service->type=='Pathology') selected @endif value="Pathology">Pathology</option>
                                   <option @if($service->type=='Pharmacies') selected @endif value="Pharmacies">Pharmacies</option>
                                   <option @if($service->type=='Hospital') selected @endif value="Hospital">Hospital</option>
                                   <option @if($service->type=='Radiology') selected @endif value="Radiology">Radiology</option>
                                 </select>
                              </div>
                              
                                       <div class="form-group">
                                  <label for="exampleInputPassword1">Country</label>
                                 <select class="form-control required select2-selection select2-selection--single" name="country" required="required">
                                   <option @if($service->type=='Ethopia') selected @endif value="Ethopia">Ethopia</option>
                                   <option @if($service->type=='Nigeria') selected @endif value="Nigeria">Nigeria</option>
                                   <option @if($service->type=='Tanzania') selected @endif value="Tanzania">Tanzania</option>
                                   <option @if($service->type=='Ghana') selected @endif value="Ghana">Ghana</option>
                                 </select>
                              </div>
                              <div class="form-group">
                                  <label for="exampleInputPassword1">Email</label>
                                  <input type="text" class="form-control p-input" id="exampleInputPassword1" placeholder=" Enter Email" name="email" value="{{$service->email}}">
                              </div>

                              <div class="form-group">
                                  <label for="exampleInputPassword1">Phone No</label>
                                  <input type="number" class="form-control p-input" id="exampleInputPassword1" placeholder=" Enter Phone Number" name="phone" value="{{$service->phone}}">
                              </div>

                              <div class="form-group">
                                  <label for="exampleInputPassword1">Address</label>
                                 <textarea class="form-control" rows="3" name="address" >{{$service->address}}</textarea>
                              </div>

                               <div class="form-group">
                                  <label for="exampleInputPassword1">Description</label>
                                  <textarea class="form-control" name="desc" required="required">{{$service->desc}}</textarea>
                              </div>

                              <div class="form-group">
                                  <label for="exampleInputPassword1">Upload Image</label>
                                  <input type="file" class="form-control" id="exampleInputPassword1" name="image">
                              </div>
                              <img src="{{url('/').'/storage/services/'.$service->image}}" height="100" width="100">
                              <br/>
                              <button type="submit" class="btn btn-primary">Submit</button>
                            
                          </form>
                      </div>
                  </div>
              </div>
            </div>
          </div>
        @endsection
