@extends('layouts.app')
@section('content')
<div class="content-wrapper">
  <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal"><i class="mdi mdi-check btn-label btn-label-left"></i>Accept</button>
  <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal1"><i class="mdi mdi-close btn-label btn-label-left"></i>Decline</button>
<div class="row" style="padding-top: 10px;">
              <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 mb-4">
                  <div class="card" style="min-height:395px;">
                      <div class="card-body">
                          <h5 class="card-title">Business Information</h5>
                          <hr>
                           <p class="font-italic text-muted mt-3 text-center">
                             {{$data->additional_detail}}
                          </p>
                          <h5 class="text-center font-weight-bold txt-brand-color">{{$data->business_name}}</h5>
                          
                          <label><b>business_type:</b></label> {{$data->business_type}} <br/>
                          <label><b>contact_person:</b></label> {{$data->contact_person}} <br/>
                          <label><b>contact_email:</b></label> {{$data->contact_email}} <br/>
                          <label><b>whatsapp_no:</b></label> {{$data->whatsapp_no}} <br/>
                          <label><b>website:</b></label> {{$data->website}} <br/>
                          <label><b>Do you offer home laboratory service?:</b></label> {{$data->home_laboratory_service}} <br/>
                          <label><b>Do you offer online order?:</b></label> {{$data->online_order}} <br/>
                          <label><b>Do you offer home delivery of medications?:</b></label> {{$data->home_delivery_medications}} <br/>  
                          
                      </div>
                  </div>
              </div>
              
</div>
 <div class="modal fade coming_soon" id="myModal" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          {{-- <h4 class="modal-title">Are Sure you want to activate this data?</h4> --}}
        </div>
        <div class="modal-body">
          <p style="color: black;">Are sure you want to activate this data?</p>
        </div>
        <div style="padding-left: 160px; padding-bottom: 20px;">
          <a href="{{route('activate.data',['id'=> $data->id])}}"><button type="button" class="btn" style="background-color: rgb(0, 35, 136); border-left-color: rgb(0, 35, 136); border-right-color: rgb(0, 35, 136); color: white;">Yes</button></a>
          <button type="button" class="btn"  data-dismiss="modal" style="display: inline-block; background-color: rgb(187, 35, 67); color: white;">Close</button>
        </div>
      </div>
    </div>
  </div>

   <div class="modal fade coming_soon" id="myModal1" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          {{-- <h4 class="modal-title">Are Sure you want to activate this data?</h4> --}}
        </div>
        <div class="modal-body">
          <p style="color: black;">Are sure you want to deactivate this data?</p>
        </div>
        <div style="padding-left: 160px; padding-bottom: 20px;">
          <a href="{{route('deactivate.data',['id'=> $data->id])}}"><button type="button" class="btn" style="background-color: rgb(0, 35, 136); border-left-color: rgb(0, 35, 136); border-right-color: rgb(0, 35, 136); color: white;">Yes</button></a>
          <button type="button" class="btn" data-dismiss="modal" style="display: inline-block; background-color: rgb(187, 35, 67); color: white;">Close</button>
        </div>
      </div>
    </div>
  </div>
@endsection