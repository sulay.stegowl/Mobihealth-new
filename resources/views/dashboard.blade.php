    @extends('layouts.app')
     @section('content')
        <div class="content-wrapper">
          <h1 class="page-title">Dashboard</h1>
          <div class="row grid-margin">
                <div class="col-3">
                  <div class="card card-statistics">
                    <div class="card-body tile1">
                        <p class="highlight-text">
                        <i class="fa fa-user-md text-success"></i>
                          {{$data['professionals']}}
                        </p>
                        <h3>
                          Doctors
                        </h3>
                    </div>
                  </div>
                </div>
                <div class="col-3">
                  <div class="card card-statistics">
                    <div class="card-body tile1">
                      <p class="highlight-text">
                       <i class="fa fa-wheelchair text-info"></i>
                        {{$data['patient']}}
                      </p>
                      <h3>
                        Paitents
                      </h3>
                    </div>
                  </div>
                </div>
            
                <div class="col-3">
                  <div class="card card-statistics">
                    <div class="card-body tile1">
                      <p class="highlight-text">
                      <i class="fa fa-hospital-o text-danger"></i>
                        {{$data['hospital']}}
                      </p>
                      <h3>
                        Hospitals
                      </h3>
                    </div>
                  </div>
                </div>
          {{-- </div> --}}

           {{--  <div class="row grid-margin" > --}}
                <div class="col-3">
                  <div class="card card-statistics">
                    <div class="card-body tile1">
                        <p class="highlight-text">
                          <i class="icon-chemistry text-warning"></i>
                         {{$data['pharmacies']}}
                        </p>
                        <h3>
                         Pharmacies
                        </h3>
                    </div>
                  </div>
                </div>  
                </div>  
                 <div class="row grid-margin" >    
                <div class="col-3">
                  <div class="card card-statistics">
                    <div class="card-body tile1">
                      <p class="highlight-text">
                       <i class="fa fa-pencil-square-o text-info"></i>
                         {{$data['pathology']}}
                      </p>
                      <h3>
                        Pathology
                      </h3>
                    </div>
                  </div>
                </div>
              </div>
          {{-- </div> --}}


          
        </div>
        @endsection
                   <script>
    window.onload = function () {
        if (typeof history.pushState === "function") {
            history.pushState("jibberish", null, null);
            window.onpopstate = function () {
                history.pushState('newjibberish', null, null);
            };
        } else {
            var ignoreHashChange = true;
            window.onhashchange = function () {
                if (!ignoreHashChange) {
                    ignoreHashChange = true;
                    window.location.hash = Math.random();
                } else {
                    ignoreHashChange = false;   
                }
            };
        }
    }
 </script>