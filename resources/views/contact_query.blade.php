@extends('layouts.app')
@section('content')
<div class="content-wrapper">

          <h1 class="page-title">Contact Queries</h1>
          <div class="card">
            <div class="card-body">
             
              <div class="row">
                <div class="col-12">
                  <table id="order-listing" class="table" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                          <th>First Name</th>
                          <th>Last Name</th>
                          <th>Email	</th>
                          <th>Phone</th>
                          <th>Subject</th>
                          <th>Message</th>
                      </tr>
                    </thead>
                    <tbody>
                    	@forelse($contact as $value)
                      <tr>
                          <td>{{$value->first_name}}</td>
                          <td>{{$value->last_name}}</td>
                          <td>{{$value->email}}</td>
                          <td>{{$value->phone}}</td>
                          <td>{{$value->subject}}</td>
                          <td>{{$value->message}}</td>
                      </tr>
                      @empty
                     
                      <tr>
                      	<td>No Contact Queries.</td>
                      </tr>
                      @endforelse
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
         
        </div>
</div>
@endsection