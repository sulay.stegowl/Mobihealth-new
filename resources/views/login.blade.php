<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from www.urbanui.com/zoom/pages/samples/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 14 Oct 2017 05:21:04 GMT -->
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Mobihealth Login</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="{{asset('/assets/node_modules/mdi/css/materialdesignicons.css')}}">
  <link rel="stylesheet" href="{{asset('/assets/node_modules/perfect-scrollbar/dist/css/perfect-scrollbar.min.css')}}">
  <!-- endinject -->
  <!-- inject:css -->
  <link rel="stylesheet" href="{{asset('/assets/css/style.css')}}">
  <!-- endinject -->
  <link rel="shortcut icon" href="{{asset('/assets/images/logo2.png')}}" />


  <style type="text/css">
    img.login-image {
    width: 170px;
    height: auto;
    display: table;
    margin: auto;
}
.bg_none
{
  background-color: transparent;
  
}
.bg_image
{
  background-size: 100% 100%; 
}

  </style>
</head>
{{-- style="background-image:url('http://34.237.167.24/assets/images/slide-doc-1.jpg')" --}}
<body class="bg_image"  style="background-color: lightgrey;">
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper">
      <div class="row">
        <div class="content-wrapper full-page-wrapper d-flex align-items-center auth-pages bg_none">
          <div class="card col-lg-4 mx-auto">
            <div class="card-body px-5 py-5" style="padding: 10px !important;">
              <img src="{{asset('/assets/images/logo2.png')}}" class="login-image">
              <h3 class="card-title text-center mb-3">Login</h3>
               <section class="content-header">
                @include('include.message')
                </section>
              <form method="POST" action={{route('login.check')}}>
                {{csrf_field()}}
                <div class="form-group">
                  <label>Username or email *</label>
                  <input type="text" class="form-control p_input" name="email">
                </div>
                <div class="form-group">
                  <label>Password *</label>
                  <input type="password" class="form-control p_input" name="password">
                </div>
     {{--            <div class="form-group d-flex align-items-center justify-content-between">
                  <div class="form-check">
                      <label class="form-check-label">
                        <input type="checkbox" class="form-check-input">
                        Remember me
                      </label>
                  </div>
                  <a href="#" class="forgot-pass">Forgot password</a>
                </div> --}}
                <div class="text-center">
                  <button type="submit" class="btn btn-primary btn-block enter-btn">Login</button>
                </div>
{{--                 <div class="clearfix">
                  <button class="btn btn-facebook float-left">
                      <i class="mdi mdi-facebook"></i> Facebook
                  </button>
                  <button class="btn btn-google float-right">
                      <i class="mdi mdi-google-plus"></i> Google plus
                  </button>
                </div>
                <p class="sign-up">Don't have an Account?<a href="#">Sign Up</a></p> --}}
              </form>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
      </div>
      <!-- row ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <script src="{{asset('/assets/node_modules/jquery/dist/jquery.min.js')}}"></script>
  <script src="{{asset('/assets/node_modules/popper.js/dist/umd/popper.min.js')}}"></script>
  <script src="{{asset('/assets/node_modules/bootstrap/dist/js/bootstrap.min.js')}}"></script>
  <script src="{{asset('/assets/node_modules/perfect-scrollbar/dist/js/perfect-scrollbar.jquery.min.js')}}"></script>
  <!-- endinject -->
  <!-- inject:js -->
  <script src="{{asset('/assets/js/off-canvas.js')}}"></script>
  <script src="{{asset('/assets/js/hoverable-collapse.js')}}"></script>
  <script src="{{asset('/assets/js/misc.js')}}"></script>
  <!-- endinject -->
           <script>
    window.onload = function () {
        if (typeof history.pushState === "function") {
            history.pushState("jibberish", null, null);
            window.onpopstate = function () {
                history.pushState('newjibberish', null, null);
            };
        } else {
            var ignoreHashChange = true;
            window.onhashchange = function () {
                if (!ignoreHashChange) {
                    ignoreHashChange = true;
                    window.location.hash = Math.random();
                } else {
                    ignoreHashChange = false;   
                }
            };
        }
    }
 </script>
</body>


<!-- Mirrored from www.urbanui.com/zoom/pages/samples/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 14 Oct 2017 05:21:04 GMT -->
</html>
