@extends('frontend.layouts.app')
@section('content')

<div class="tm-breadcrumb">
		<div class="container">
			<h1 class="tm-section-heading">pathology</h1>
			<ul>
				<li>
					<a href="{{route('homepage')}}">home</a>
				</li>
				<li>
					<a href="{{route('pathology')}}">/ pathology</a>
				</li>
			</ul>
		</div>
	</div><!-- /Breadcrumb header -->
	<!-- Shop -->
	<div class="tm-shop-list">

		<div class="dis_inline">
			<h1 class="tm-section-heading">Our Pathology</h1>
		</div>


			<div class="tm-appointment-large tm-appointment-large_none unique">
		<div class="container">
			<h1 class="tm-section-heading">Search By</h1>
			<div class="row">
				<form method="POST" action="{{route('search_pathology')}}">
					{{csrf_field()}}
				<div class="col-sm-4">
					<div class="form-group">
						<div class="tm-timepicker">
							<select class="form-control" name="search_by" id="search">
								<option value="Name"> Name </option>
								<option value="Country"> Country </option>
								<option value="State"> State </option>
								<option value="Location"> Location </option>
							</select>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="form-group" id="change">
						<input class="form-control" name="value" required="required" placeholder="Enter Name" type="text">
					</div>
				</div>
				
				
				<div class="col-sm-4">
					<div class="form-group page_loader_div_2" >

						<button class="tm-btn btn-dark" type="submit" >SEARCH</button>
					</div>
				</div>
			
		</form>
		</div>
	</div><!-- /Appointment -->
</div>

		<div class="container">
		

		<!-- Appointment -->


			<div class="row" >
				<div id="pathology"></div>
				
				<div class="tm-pagination">
					
					<ul id="pathology_link">
						
					</ul>	

					

				</div>
				

			</div>
		
	</div><!-- /Shop -->
	</div>
	@endsection
	<script src="{{asset('/assets/front/js/jquery.js')}}" type="text/javascript">
  </script>
<script type="text/javascript">
	$(document).ready(function(){
		function first(){
	                $.ajax({
                    url: '{{url('/')}}/pathology_result',
                    type: 'GET',
                    //dataType:'json',
                    async: true,     
                 beforeSend: function(){
                 // $.blockUI({ message: $('.loader') }); 
                            },
                complete: function(){          
                 // $.unblockUI();
                  
                },                
                    success: function (data) {
                    	console.log(data);
                    	//var temp=$.parseJSON(data);
                    	var c=[];
       $.each(data.data, function(i, item) {     
       c.push("<div class='col-lg-3 col-md-4 col-sm-6 col-xs-6 smf' id='list'>");
       c.push("<div class='tm-product-box'>");
       c.push("<div class='product-image'>");
       c.push("<img alt='Shop' src='{{url('/')}}/storage/services/"+ item.image +"' class='img-responsive ''></div>");       
       c.push("<div class='product-details'>");
       c.push("<h6 class='product-title'>");
       c.push("<a href='{{url('/')}}/hospital_details/"+item.slug+"'>"+item.title+",<span>"+item.country+" </span></a></h6>");
       c.push("</div></div></div>");
     });

     $('#pathology').html(c.join("")); 
     		var temp=[];                                     
                 for(var i=1;i<=data.last_page;i++)
                 {
                 	 temp.push("<li><a href='#' id='nextPage' data-page='"+i+"' >" + i + "</a></li>");
                 }
                 $('#pathology_link').html(temp.join(""));
                           // jQuery("div#pathology").html(c);           
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                    }); 
	               }
	               first();

          $('#pathology_link').on('click','#nextPage',function(e){
                e.preventDefault();
                  var i = $(this).attr('data-page');
                  $.ajax({
                    url: '{{url('/')}}/pathology_result?page='+i,
                    type: 'GET',
                    //dataType:'json',
                    async: true,     
                 beforeSend: function(){
                 // $.blockUI({ message: $('.loader') }); 
                            },
                complete: function(){          
                 // $.unblockUI();
                  
                },                
                    success: function (data) {
                        console.log(data);
                        //var temp=$.parseJSON(data);
                        var c=[];
                       $.each(data.data, function(i, item) {             
                           c.push("<div class='col-lg-3 col-md-4 col-sm-6 col-xs-6 smf' id='list'>");
                           c.push("<div class='tm-product-box'>");
                           c.push("<div class='product-image'>");
                           c.push("<img alt='Shop' src='{{url('/')}}/storage/services/"+ item.image +"' class='img-responsive ''></div>");       
                           c.push("<div class='product-details'>");
                           c.push("<h6 class='product-title'>");
                           c.push("<a href='{{url('/')}}/hospital_details/"+item.slug+"'>"+item.title+",<span>"+item.country+" </span></a></h6>");
                           c.push("</div></div></div>");            
                        });

                        $('#pathology').html(c.join("")); 

                 
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                    }); 

          });
	 });
</script>