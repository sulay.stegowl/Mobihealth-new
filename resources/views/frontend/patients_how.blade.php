@extends('frontend.layouts.app')
@section('content')

<div class="tm-breadcrumb">
		<div class="container">
			<h1 class="tm-section-heading">patients</h1>
			<ul>
				<li>
					<a href="{{route('homepage')}}">home</a>
				</li>
				<li>
					<a href="{{route('patients_how')}}">/ How It Works</a>
				</li>
			</ul>
		</div>
	</div><!-- /Breadcrumb header -->
	<!-- Shop -->
	<div class="tm-shop-list">

		
		<div class="container">
		

	<h1 class="tm-section-heading">MOBIHEALTH …YOUR GATEWAY TO ACCESSING QUALITY HEALTHCARE SERVICES WORLDWIDE!
HOW IT WORKS
</h1>
		
		<div class="how_it_work">
		
			



<div class="row">
	<div class="col-sm-12">
		<div class="title text-center">STEP 1: CHOOSE A SUBSCRIPTION PLAN</div>
			<p>We have various bundles to suits everyone’s need. We have individual, family bundles. And for those who do not require the packages they can choose from our PAYG plan. Corporate bodies, health insurers who wish to leverage on our telemedicine solution only should contact us for quotes.</p>
			
	</div>
	<div class="col-sm-12">
		<div  class="title text-center" >STEP 2:  COMPLETE OUR APPLICATION FORM	 </div>
			<p>Please read our Term and Conditions carefully before completing our application. Our easy to use sign up and secured application enables you to enter your name, address, email and other relevant medical details. You will be able to add other household members per subscription plan. Please ensure all information is accurate as falsification or providing misleading information or withholding relevant information could invalidate your subscription. We are HIPAA compliant and our Electronic Health record system is second to none worldwide. Our secured video software is also industry best being used by NASA, Banks and other reputable firms globally.</p>
			
	</div>
</div>
			
			<div class="row">
				{{-- <div class="col-sm-12">
						<div class="title text-center">STEP 3: </div>
						<p>You will be required to sign T&C, Privacy Policy</p>
				
				</div> --}}
				<div class="col-sm-12">
					<div class="title text-center">STEP 3: PAYMENT</div>
					<p>You will be required to make payment for your plan which are billed annually upfront. Payment will not be taken until your application has been reviewed and accepted. If we are unable to accept your application you will be notified. On this occasion payment will not be taken. If successful, you will be notified by email and provided with login access to your personalised subscriber’s portal on our platform. This completes your registration.</p>
					
				</div>
			</div>
			
			
<div class="row">
	<div class="col-sm-12">
			<div class="title text-center">Step 4 - Sign In</div>
			<p>Once your application is received and documentation verified we will provide you with login details to access our online on boarding modules that will provide you training on how to conduct a video consultation and also ensure you are familiar with our in-house policies. We offer video tutorial to familiarize you with our platform, and you can schedule a live test visit with a member of our team to walk through the process and ask questions.</p>
			
	</div>
	<div class="col-sm-12">
		<div class="title text-center">Step 5 – Request a Video Consultation</div>
	<img src="{{asset('assets/front/extra-images/IMG-20171007_WA0002.jpg')}}" alt="" style="height: 300px;width: auto">
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
			<div class="title text-center">How does a visit work?</div>
			<p>A telemedicine visit isn’t so different from in-person visit to the doctors. You will be able to request video consultation with our doctors anytime anywhere using our video app on your mobile or on laptops. You will be required to enter your symptoms and choose specialty/doctor from those available. Our doctors will then ask you to confirm suitable time. Your consultation starts once you accept a call from your doctor. You’ll consult your doctor through live video using the realtime capabilities of your smart phone or webcam. Our robust EHR system makes it easy for the doctor to see your medical records, medication history and allergies and he/she can take medical notes during the encounter, prescribe medications, order laboratory/radiology tests and do referrals on a single platform. 
</p>
<p>
	Apart from the realtime capabilities on smart phones that helps doctors assess your condition, the FDA approved telehealth devices like www.tytocare.com will further strengthen remote comprehensive physical examination by patients which can be transmitted via secured cloud platform so you can listen to heart, chest sounds, see images of ear, throat and skin examinations, abdominal examination etc. Not everyone needs this as the camera on smart phones are good enough. You may also upload photos, investigation reports on your patient portal ahead of your consultation so that your doctor can view them.
</p>
			
			<p>
				If questions or technical issues come up, we’re here for you. You’ll have 24/7 access to our support team by phone/email.
			</p>

				<img src="{{asset('assets/front/images/image10.jpg')}}" alt="" style="height: 300px;width: auto">

	</div>
	<div class="col-sm-12">
		<div class="title text-center">
STEP – PAYMENT FOR CONSULTATIONS, MEDICINES AND INVESTIGATIONS
</div>
			<p>Our services are free for subscribers. Your annual subscription plan is valid for one year and payable in advance. Once subscribed you can access all our services for free. Mobihealth is a pioneer telemedicine and healthcare service platform that aims to bridge the gap in access and delivery of quality healthcare services especially in developing countries including cost, shortage of doctors, counterfeit medicines, poor infrastructure and travel distance to health facilities. Research shows that majority of visits to the Emergency room and hospitals are for cases that can be treated through virtual consultations. Our platform is the first of its kind. We provide quick access to doctors, medicines, diagnostic tests and hospital treatment and all our services are free for subscribers. All you need is to sign up to one of our bundles and we take care of all your healthcare needs. No one plans to fall ill but if you do, a hospital admission or fixing a broken leg could easily run into hundreds of thousands. You want to make sure you and loved ones  can access quality care without barriers. Mobihealth doctor is only a call away and you can get immediate medical advise from the comfort of your home 24/7, free medications, investigations and if you need further hospital referral, we sort that out for you too. This is a game changer!</p>
	</div>
</div>

			


		
			

		</div>

		
		</div><!-- /Shop -->
	</div>
	@endsection
