<!DOCTYPE html>
<html lang="zxx">
<head>
  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109553940-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-109553940-1');
</script>
  <meta charset="utf-8">
  <meta content="IE=edge" http-equiv="X-UA-Compatible">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  
  <meta content="ThemesMill" name="author">
  <title>Mobihealth</title><!-- Custom Styling -->
  <link href="{{asset('/assets/front/css/style.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/front/css/style2.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('/assets/front/css/responsive.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('/assets/front/css/flexslider.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('/assets/front/css/bootstrap-datepicker.min.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('/assets/front/css/owl.carousel.min.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('/assets/front/css/owl.theme.default.min.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('/assets/front/css/component.css')}}" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i,900,900i" rel="stylesheet">

    <link rel="icon" href="{{asset('assets/images/logo2.png')}}" type="image/x-icon">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">



@if(Request::segment(1) == "about")
<meta name="title" content="Telehealth Service Provider | Mobihealth International" />
<meta name="description" content="We aim to mobilise quality health services in developing countries. Get in touch with us for a wide range of telehealthcare services. Click for details." />

@elseif(Request::segment(2) == "faq" && Request::segment(1) == "pathology")
<meta name="title" content="Pathology - FAQs | Mobihealth International" />
<meta name="description" content="Want to know more about our home visit pathology services? Read through these FAQs today. For more details, contact us anytime." />

@elseif(Request::segment(2) == "faq" && Request::segment(1) == "pharmacy")
<meta name="title" content="Pharmacy - FAQs | Mobihealth International" />
<meta name="description" content="Some frequently asked questions about our pharmacy and other telemedical services are answered here. Browse now for details." />

@elseif(Request::segment(2) == "faq" && Request::segment(1) == "radiology")
<meta name="title" content="Radiology - FAQs | Mobihealth International" />
<meta name="description" content="Go through the radiology FAQ section to understand how our radiology services are available for you. Got more questions? Contact us." />

@elseif(Request::segment(2) == "faq" && Request::segment(1) == "patients")
<meta name="title" content="Patients - FAQs | Mobihealth International" />
<meta name="description" content="We are committed to offer the best telemedicine and healthcare services to our patients. Here are some FAQs for additional knowledge." />

@elseif(Request::segment(2) == "faq" && Request::segment(1) == "doctor")
<meta name="title" content="Doctors - FAQs | Mobihealth International" />
<meta name="description" content="Our doctors are professional and qualified. Get to know more about how you can connect with them in this FAQ section. Visit now!" />


@elseif(Request::segment(1) == "services_web")
<meta name="title" content="Telehealth Services and Products | Mobihealth International" />
<meta name="description" content="Make the most of our telehealth services. We offer instant video consultation, laboratory and physical investigations, home health services and more." />  

@elseif(Request::segment(2) == "faq" && Request::segment(1) == "hospital")
<meta name="title" content="Hospitals - FAQs | Mobihealth International" />
<meta name="description" content="We sure you have some questions about our hospitals and other clinical health services. Get all your answers in one click. Visit now." />

@elseif(Request::segment(1) == "hospital")
<meta name="title" content="Our Hospitals | Mobihealth International" />
<meta name="description" content="Search our nearby hospital to get the best clinical healthcare services in Nigeria, Ethopia and Tanzania. For more details, visit our website today!" />

@elseif(Request::segment(1) == "pathology")
<meta name="title" content="Pathological Labs | Mobihealth International" />
<meta name="description" content="Get in touch with the finest pathological labs who we trust for all laboratory and radiologic investigations. Click here for more details. " />

@elseif(Request::segment(1) == "pharmacy")
<meta name="title" content="Recognised Pharmacies | Mobihealth International" />
<meta name="description" content="Get access to Mobihealth recognised pharmacies in Africa and other developing countries. Visit our site for more information today!" />

@elseif(Request::segment(1) == "radiology")
<meta name="title" content="Radiology Services | Mobihealth International" />
<meta name="description" content="At Mobihealth, we offer the most effective and affordable range of radiology services at your doorsteps. Browse through our labs here." />

@elseif(Request::segment(1) == "subscriptions")
<meta name="title" content="Subscribe Now | Mobihealth International" />
<meta name="description" content="We have a wide array of subscription plans for different developing countries in Africa. Get your subscription today. Hurry!" />

@elseif(Request::segment(1) == "contact")
<meta name="title" content="Contact Us | Mobihealth International" />
<meta name="description" content="Want to know more about our telemedicine and healthcare services? Get in touch with us today. We would love to answer your queries." />


@elseif( Request::segment(1) == "privacy-policy")
<meta name="title" content="Privacy Policy | Mobihealth International" />
<meta name="description" content="Go through our privacy policy to know what kind of information is collected from our users. We encourage you to read the policy before using the site. " />

@elseif( Request::segment(1) == "disclaimers")
<meta name="title" content="Disclaimer | Mobihealth International" />
<meta name="description" content="All information on the website is for reference purpose. Please go through the disclaimer to understand more about the website policy." />

@else
<meta name="title" content="Telemedicine & Healthcare Services | Mobihealth International" />
<meta name="description" content="Mobihealth International provides innovative telemedicine and healthcare services. Our platform aims to revolutionize healthcare services in developing countries." />

@endif


  <style type="text/css">
    .tm-video-wrapper h2.tm-box-heading {
    font-weight: 400;
}

.service-box h2.tm-box-heading {
    font-size: 20px;
}

.tm-home-services .service-box {
    display: table;
    width: 100%;
    position: relative;
    margin-bottom: 4px;
    min-height: 217px;
}

.footer-widget.latest-news .news-thumbnail img {
    background-color: white;
}

  </style>
</head>
  <body>

  
    @include('frontend.include.header')

     
      @yield('content')
     
     
    

    
   @include('frontend.include.footer')

    <!-- Scripts -->
  <script src="{{asset('/assets/front/js/jquery.js')}}" type="text/javascript">
  </script> 
  <script src="{{asset('/assets/front/js/bootstrap.min.js')}}" type="text/javascript">
  </script> 
  <script src="{{asset('/assets/front/js/jquery.flexslider.js')}}" type="text/javascript">
  </script> 
  <script src="{{asset('/assets/front/js/bootstrap-datepicker.min.js')}}" type="text/javascript">
  </script> 
  <script src="{{asset('/assets/front/js/owl.carousel.min.js')}}" type="text/javascript">
  </script>
  <script src="{{asset('/assets/front/js/imagesloaded.pkgd.min.js')}}" type="text/javascript">
  </script>
  <script src="{{asset('/assets/front/js/jquery.filterizr.js')}}" type="text/javascript">
  </script> 
  <script src="{{asset('/assets/front/js/modernizr.custom.js')}}" type="text/javascript">
  </script> 
  <script src="{{asset('/assets/front/js/jquery.dlmenu.js')}}" type="text/javascript">
  </script> 
  <script src="{{asset('/assets/front/js/bullseye.js')}}" type="text/javascript">
  </script> 
  <script src="{{asset('/assets/front/js/custom.js')}}" type="text/javascript">
  </script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAXtg1Zwpfh87L6gStHAyzHhz76VPXzaH8" type="text/javascript">
  </script> 

{{-- <script src="{{asset('/assets/js/custome_paginate.js')}}" type="text/javascript"> --}}

<script>

$(document).ready(function()
{

  $('.page_loader_div_2 input[type="submit"]').click(function(e){
      $('.page_loader').addClass('dis_block');  
  });




$('.page_loader_div input[type="submit"]').click(function(e){
 
$URL = $('#website').val();


  $pattern_1 = /^(http|https|ftp):\/\/(([A-Z0-9][A-Z0-9_-]*)(\.[A-Z0-9][A-Z0-9_-]*)+.(com|org|net|dk|at|us|tv|info|uk|co.uk|biz|se)$)(:(\d+))?\/?/i;
  $pattern_2 = /^(www)((\.[A-Z0-9][A-Z0-9_-]*)+.(com|org|net|dk|at|us|tv|info|uk|co.uk|biz|se)$)(:(\d+))?\/?/i;
  if($pattern_1.test($URL) || $pattern_2.test($URL)){
     $('.page_loader').addClass('dis_block');  
    return true;
  } else{
       $('.web_url').addClass('dis_block');  
    return false;
  }

});

  $('.datepicker input').datepicker({
     endDate:'today',
     dateFormat: 'Y-mm-dd'
});



     function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result)
                        .width(150)
                        .height(200);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
});
</script>
{{--dropdown script --}}
  <script type="text/javascript">
    $(document).ready(function(){

  

         //alert('helllo');
          $("#add").hide();
       $('#search').change(function(){
         
             var cat = $('option:selected', this).attr('value');
            //var cat=$(this).val();
            //alert(cat);
            if(cat=="Name")
            {
            var data="<div class='form-group' id='change'><input class='form-control' name='value' placeholder='Enter Name' type='text' id='name' required='required'></div>";
            }
            else if(cat=="Country")
            {
              var data="<div class='form-group' id='change'><select class='form-control' id='con' name='value'><option value='Nigeria'>Nigeria</option><option value='Tanzania'>Tanzania</option><option value='Ethopia'>Ethopia</option><option value='Ghana'>Ghana</option><option value='Sierra leone'>Sierra leone</option></select></div>";
            }
            else if(cat=="State")
            {
              var data="<div class='form-group' id='change'><input class='form-control' name='value' placeholder='Enter State' type='text' id='state' required='required'></div>";
            }
            else
            {
              var data="<div class='form-group' id='change'><input class='form-control' name='value' placeholder='Enter Location' type='text' id='location' required='required'></div>";
            }
                    $('#change').html(data);
                   });
    });
</script>
<script type="text/javascript">
  var str=window.location.href;
  var temp=str.substring(39,59);
  if(temp=="search_hospital")
  {
    $('html, body').animate({ scrollTop: $("#list").offset().top }, 1000);
  }
  else if(temp=="search_pathology")
  {
    $('html, body').animate({ scrollTop: $("#pathology_list").offset().top }, 1000);
  }
  else if(temp=="search_pharmacy")
  {
    $('html, body').animate({ scrollTop: $("#pharmacy_list").offset().top }, 1000);
  }
  else if(temp=="search_radiology")
  {
    $('html, body').animate({ scrollTop: $("#radiology_list").offset().top }, 1000);
  }
  else
  {

  }

</script>
<script type="text/javascript">

 


   $("#medical input[type=radio]").on("change",function(){
   if(this.checked) {
    if(this.value=='no')
    {
      $("#medical_hide").hide();
      $('#name_insurer').attr('required', false);
      $('#policy_insurer').attr('required', false);
      $('#issue_insurer').attr('required', false);
      $('#expiry_insurer').attr('required', false);
    }
    else if(this.value=='yes')
    {
      $("#medical_hide").show();
      $('#name_insurer').attr('required', true);
      $('#policy_insurer').attr('required', true);
      $('#issue_insurer').attr('required', true);
      $('#expiry_insurer').attr('required', true);
    }
    }
  });
   $("#union input[type=radio]").on("change",function(){
   if(this.checked) {
    if(this.value=='no')
    {
      $("#union_hide").hide();
      $('#name_union').attr('required', false);
      $('#policy_union').attr('required', false);
      $('#issue_union').attr('required', false);
      $('#expiry_union').attr('required', false);
    }
    else if(this.value=='yes')
    {
      $("#union_hide").show();
       $('#name_union').attr('required', true);
       $('#policy_union').attr('required', true);
      $('#issue_union').attr('required', true);
      $('#expiry_union').attr('required', true);
    }
    }
  });
    $("#indemnity input[type=radio]").on("change",function(){
   if(this.checked) {
    if(this.value=='no')
    {
      $("#indemnity_hide").hide();
        $('#name_indemnity').attr('required', false);
        $('#policy_indemnity').attr('required', false);
        $('#issue_indemnity').attr('required', false);
        $('#expiry_indemnity').attr('required', false);
    }
    else if(this.value=='yes')
    {
      $("#indemnity_hide").show();
       $('#name_indemnity').attr('required', true);
       $('#policy_indemnity').attr('required', true);
        $('#issue_indemnity').attr('required', true);
        $('#expiry_indemnity').attr('required', true);
    }
    }
  });
        $("#tele_agent input[type=radio]").on("change",function(){
   if(this.checked) {
    if(this.value=='no')
    {
      $("#tele_hide").hide();
        $('#which_tele').attr('required', false);
    }
    else if(this.value=='yes')
    {
      $("#tele_hide").show();
       $('#which_tele').attr('required', true);
    }
    }
  });

          $("#license input[type=radio]").on("change",function(){
   if(this.checked) {
    if(this.value=='no')
    {
      $("#license_hide").hide();
        $('#license_no').attr('required', false);
        $('#license_expiry').attr('required', false);
    }
    else if(this.value=='yes')
    {
      $("#license_hide").show();
       $('#license_no').attr('required', true);
        $('#license_expiry').attr('required', true);
    }
    }
  });

    $("#naigeria input[type=radio]").on("change",function(){
   if(this.checked) {
    if(this.value=='no')
    {
      $("#naigeria_hide").hide();
        $('#naigeria_expiry').attr('required', false);
    }
    else if(this.value=='yes')
    {
      $("#naigeria_hide").show();
       $('#naigeria_expiry').attr('required', true);
    }
    }
  });

     $("#upload_license input[type=radio]").on("change",function(){
   if(this.checked) {
    if(this.value=='no')
    {
      $("#upload_hide").hide();
      $("#upload_hide_back").hide();
        $('#driving_licence').attr('required', false);
        $('#driving_licence_back').attr('required', false);
    }
    else if(this.value=='yes')
    {
      $("#upload_hide").show();
      $("#upload_hide_back").show();
       $('#driving_licence').attr('required', true);
       $('#driving_licence_back').attr('required', true);
    }
    }
  }); 
    
     $("#relation").on("change",function(){
      var v=$("#relation").val();
      if(v=='other')
      {
      $( "#add" ).show();
    }
    else
    {
      $( "#add" ).hide();  
    }
      //alert(v);
     });
</script>


</body>
</html>