@extends('frontend.layouts.app')
@section('content')

<div class="tm-breadcrumb">
		<div class="container">
			<h1 class="tm-section-heading">Service Detail</h1>
			<ul>
				<li>
					<a href="{{route('homepage')}}">home</a>
				</li>
				<li>
					<a href="{{route('service_detail_1')}}">/ Laboratory and Radiologic Investigations</a>
				</li>
			</ul>
		</div>
	</div>

	<div class="tm-service-detail-wrap">
		<div class="container">
			<div class="row">
			
				<div class="col-sm-4">
					<div class="tm-sidebar">
					
						<div class="sidebar-widget questions">
							<h4 class="widget-heading">questions and queries</h4>
							<p>Email us if any queries.</p>
							<ul>
								
								<li><span class="icon-at"></span> info@mobihealthinternational.com</li>
							</ul>
						
						</div>
					</div>
				</div>
				
				<div class="col-sm-8">
					<div class="tm-service-detail">
						<div class="service-detail-thumb"><img alt="service" src="{{asset('/assets/front/images/L7.jpg')}}"></div>
						<h4 class="service-title">Laboratory and Radiologic Investigations</h4>
						<p>
We book your diagnostic tests with reputable centers and our doctors evaluate the results remotely to give you medical advice and prescriptions. You can also request home laboratory test depending on your subscription plan or as additional package.
</p>
					
					</div>
				</div>
			</div>
		</div>
	</div>

	@endsection