@extends('frontend.layouts.app')
@section('content')

<div class="tm-breadcrumb">
		<div class="container">
			<h1 class="tm-section-heading">Service Detail</h1>
			<ul>
				<li>
					<a href="{{route('homepage')}}">home</a>
				</li>
				<li>
					<a href="{{route('services_details')}}">/ HOSPITAL REFERRALS AND PAYMENT OF MEDICAL BILLS</a>
				</li>
			</ul>
		</div>
	</div>

	<div class="tm-service-detail-wrap">
		<div class="container">
			<div class="row">
			
				<div class="col-sm-4">
					<div class="tm-sidebar">
					
						<div class="sidebar-widget questions">
							<h4 class="widget-heading">questions and queries</h4>
							<p>Email us if any queries.</p>
							<ul>
								
								<li><span class="icon-at"></span> info@mobihealthinternational.com</li>
							</ul>
						
						</div>
					</div>
				</div>
				
				<div class="col-sm-8">
					<div class="tm-service-detail">
						<div class="service-detail-thumb"><img alt="service" src="{{asset('/assets/front/images/L8.jpg')}}"></div>
						<h4 class="service-title">HOSPITAL REFERRALS AND PAYMENT OF MEDICAL BILLS</h4>
						<p>For those who need further face to face evaluation, our online doctors send a referral to a local specialist and book your appointments with a confirmation sent to your email/mobile. We pay your medical bills for each encounter with no out-of pocket payment from you.</p>
					
					</div>
				</div>
			</div>
		</div>
	</div>

	@endsection