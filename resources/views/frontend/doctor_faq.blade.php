@extends('frontend.layouts.app')
@section('content')

<div class="tm-breadcrumb">
		<div class="container">
			<h1 class="tm-section-heading">Doctor</h1>
			<ul>
				<li>
					<a href="{{route('homepage')}}">home</a>
				</li>
				<li>
					<a href="{{route('doctor_faq')}}">/ Faq</a>
				</li>
			</ul>
		</div>
	</div><!-- /Breadcrumb header -->
	<!-- Shop -->
	<div class="tm-shop-list">

		
		<div class="container">
		

		<h1 class="tm-section-heading">faq</h1>
		
		<div class="how_it_work">

<div class="container">
			<p>Conducting a Video Visit</p>

    
</div>


<div class="container">
 
  <div class="panel-group" id="accordion">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">What conditions do I see?</a>
        </h4>
      </div>
      <div id="collapse1" class="panel-collapse collapse in">
        <div class="panel-body">
          <p>Mobihealth Is a Telemedicine and Healthcare service platform that connects millions
          of patients across Africa to carefully selected licensed doctors worldwide. More than
          60% of ED visits are for non-acute medical conditions –with malaria being a leading
          cause of death in children and pregnant women. Access to immediate consultation,
          prescription, diagnostics and medications- as we provide via this platform - will savemillions of lives. Research has shown that these can be safely diagnosed andtreated through virtual consultation. Our goal is to bridge the gap in healthcareaccess and delivery in developing countries by taking advantage of telemedicine and
          digital revolution. We are also a healthcare service One-stop- portal connecting
          patients to quality healthcare providers locally and this is accessible through our
          mobile apps or website.</p>
          <p>
           As a Mobihealth doctor you will see acute non-emergency cases such including but
            not limited to Malaria, Chest Infections, skin disease, GI problems, Cardiovascular,
            Endocrine conditions etc. Patients with significant pre-existing conditions are
            generally not covered under our regular bundles but can be seen on PAYG basis.
            Don’t worry about the details our triage system will ensure things run smoothly for
            you.
          </p>
          <p>Emergency cases must be asked to immediately go to the hospital as our platform
            only caters for acute not emergencies. We pay for all the care our subscribers
            receive through this platform and the other treatments or investigations when
            referred to local providers. You don’t have to worry about affordability from the
            patient’s side. Only paid subscribers are granted access to consultations and
            services.</p>
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">How will patients find me?</a>
        </h4>
      </div>
      <div id="collapse2" class="panel-collapse collapse">
        <div class="panel-body">Patients come in through our secured telemedicine and healthcare service platform
seeking care for acute non-emergency medical conditions. They can browse services by specialty or location and will see services from providers licensed providers who are available online within a time frame and our admin, other clinical support and pharmacy staff will be on hand to assist you. You will be able to update your availability on our scheduler. Please provide at least 24hrs notice if any cancellations or change to original booking. We need minimum of 1hour consultation time per session during which you will be required to see 4 patients at 15minutes per consultation. </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">What equipment do I need to conduct a video visit?</a>
        </h4>
      </div>
      <div id="collapse3" class="panel-collapse collapse">
        <div class="panel-body">You can conduct video consultations from the comfort of your home/office anytime anywhere You’ll just need one of the following:
          
          <ul class="list_style_none">
            <li>A computer with an internal or external camera. You can use a wide range of web browsers such as Chrome, Explorer or Firefox.</li>
            <li>An Apple mobile phone or iPad with the camera enabled and our provider app downloaded. We recommend using a stand for your phone or iPad during visits to keep the camera stable.</li>
            <li>Ear Phone</li>
          </ul>

        </div>
      </div>
    </div>

      <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">How do I conduct a physical exam remotely?</a>
        </h4>
      </div>
      <div id="collapse4" class="panel-collapse collapse">
        <div class="panel-body"><p>Many aspects of the in-person physical exam can also be replicated remotely using webcam or Telehealth devices which are FDA Approved (e.g www.tytocare.com). The recordings are transmitted to the doctor remotely via secured platform to enhance virtual consultation experience and accuracy in diagnosis.</p>
        <p>For those patients who don’t have these devices, the doctor will be able to visually assess them through realtime capabilities on their smart phone/webcam, and can accurately gauge general distress, skin tone, clarity of thought and speech, respiratory rate, work of breathing, gait, etc. You can also assist the doctor in physical examination by following his instructions to palpate areas of tenderness, assess ROM, assist in moving the camera to visualize areas such as the oropharynx, and assist in abdominal self-exams. In conjunction with a thorough history, many providers feel that the information they can obtain from a video visit is sufficient for accurate diagnosis of many conditions. Don’t worry about quality of video. Our customized video software is specifically designed for low bandwidth, difficult topography with fluctuating signal levels and this is provided by US based industry leader used by NASA, Barclays, HSBC and many reputable industry names. Our software produces a quality stable video consultation experience that ensures accurate diagnosis and management. However, in the event that the doctor feels he has limited information to make accurate diagnosis, he will refer you to a hospital or specialist for thorough evaluation and management. Mobihealth pays for you care so don’t worry about the bill!</p>
        </div>
      </div>
    </div>

      <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">Where can I take a video appointment?</a>
        </h4>
      </div>
      <div id="collapse5" class="panel-collapse collapse">
        <div class="panel-body">
    <img src="{{asset('assets/front/extra-images/IMG-20171007_WA0002.jpg')}}" alt="" style="height: 300px;width: auto">
    <p>You can take an appointment from any private location. Keep in mind that you cannot take an appointment with anyone else in the room with you. To ensure patient privacy, we recommend taking appointments from your home or office with the door closed and no background noise or distractions. Patients will be able to focus on you better if the environment behind you is a light solid color, clean, simple and not distracting. A light blue backdrop is preferable for the optimal video experience. Position yourself in front of a blank wall instead of a wall full of bookshelves.</p>

    <p>
      Also note that although it’s a virtual visit you still have to present yourself in a professional manner both in conduct and dressing. Consultations may be recorded for quality &training purposes but can also be called by us if any medicolegal issues.
    </p>
         </div>
      </div>
    </div>

     <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse6">What type of training do you offer?</a>
        </h4>
      </div>
      <div id="collapse6" class="panel-collapse collapse">
        <div class="panel-body">We offer video-based training on how to use the Mobihealth provider platform. This you can access at any time from anywhere. You can also schedule a live practice visit with one of our team members to familiarize yourself with the process and ask questions along the way. We provide access to the training once you’ve completed sign-up and we’ve approved your credentials.</div>
      </div>
    </div>

      <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse7">How much does this cost me?</a>
        </h4>
      </div>
      <div id="collapse7" class="panel-collapse collapse">
        <div class="panel-body">Our platform is free for you to use. In turn, we benefit from being able to offer our thriving subscriber base, access to a wide range of expert clinicians across the U.S, UK, Canada, Nigeria, Australia, and other countries. We pay our doctors competitive rates. You can work and earn extra income on top of your regular job.</div>
      </div>
    </div>

       <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse8">How much will I get paid?</a>
        </h4>
      </div>
      <div id="collapse8" class="panel-collapse collapse">
        <div class="panel-body">Our prescription portal is specially designed so doctors are easily able to prescribe through choosing from a specifically designed formulary. The clinical support team and pharmacy lead manage the prescription process to ensure prescriptions are ready at the chosen pharmacy within an hour.</div>
      </div>
    </div>


       <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse9">How will I get paid?</a>
        </h4>
      </div>
      <div id="collapse9" class="panel-collapse collapse">
        <div class="panel-body">All consultation fees collected for completed encounters will automatically transfer to a bank account of your choice on a weekly basis upon submission of your timesheets. Patients pre-pay for a visit through our tiered subscription bundles. For those who choose PAYG, We collect payment upfront before a visit starts. You do not need to worry about how to get paid by the patients because we pay our providers directly and you only see those who are already subscribed and have made payments to us.</div>
      </div>
    </div>


  <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse10">How am I sure the doctor has enough information to make correct diagnosis ?</a>
        </h4>
      </div>
      <div id="collapse10" class="panel-collapse collapse">
        <div class="panel-body"> The Mobihealth Electronic Health Record (EHR) and Video Platform is very robust and the doctor can see your medical history, medications your are currently taking, allergy history, blood tests, or radiology reports to assist with his evaluation. This information is based on what you provided during your application, it is therefore important that you enter accurate information as this will impact on the diagnosis made and treatment you receive. Should you need to amend your records please login to your patient’s portal and update such information, our admin will review in line with our policies and if we find you have withheld significant information during registration it will invalidate your subscription. Do not falsify or withhold information about your health. Our doctors may refer you to another specialist on the platform or to a hospital for further evaluation and treatment if need be. The cost of your treatment is paid by Mobihealth so you will not be required to make any out of pocket payment.</div>
      </div>
    </div>


  <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse11">How do I prescribe?</a>
        </h4>
      </div>
      <div id="collapse11" class="panel-collapse collapse">
        <div class="panel-body">You will be able to e-prescribe and manage medications with the highest level of e-prescribing integration including full local formulary*, up-to-date prescriptions and geolocation pharmacy selection. There must be video connection in order to prescribe; physicians cannot prescribe based on a phone or chat encounter. We have a very good supportive team on call each time you are in session.</div>
      </div>
    </div>

      <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse12">What can I prescribe?</a>
        </h4>
      </div>
      <div id="collapse12" class="panel-collapse collapse">
        <div class="panel-body">Based on your credentials and verification you may be able to e-Prescribe on Mobihealth  platform. Medications below are excluded from the formulary:

          <ul class="list_style_none">
            <li>Controlled substances (narcotics, anxiety medications, ADHD medications)</li>
            <li>Muscle relaxants</li>
            <li>Medications for erectile dysfunction</li>
            <li>Any additional state specific controlled medications (additional pain medications, pseudoephedrine)</li>
          </ul>
        </div>
      </div>
    </div>
    
      <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse13">Are Mobihealth Doctors qualified to practice?</a>
        </h4>
      </div>
      <div id="collapse13" class="panel-collapse collapse">
        <div class="panel-body">Our doctors are carefully selected and go a very thorough scrutinization process to ensure we present only the best to you. They are licensed to practice in the UK, USA, Australia, New Zealand, Canada, Nigerian, Tanzania, Ghana, Ethiopia, Sierra Leone and other countries. They have active medical licenses and are in good standing with their respective regulatory bodies with no disciplinary actions or restrictions. If you have any genuine complaint about any doctor’s behaviour please contact complaint@mobihealthinternational.com.</div>
      </div>
    </div>

   <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse14">How does my state licensure work when practicing through Mobihealth?</a>
        </h4>
      </div>
      <div id="collapse14" class="panel-collapse collapse">
        <div class="panel-body">You will be able to see patients located in our countries of operation provided you
have license to practice in your current state/country. It will be helpful if you have other licences i.e. in our country of operation but the law does not prohibit you from conduction virtual consultation, if you have any queries please contact us via email. For those whose licences have expired we can help facilitate rre-registration provided they are in good standing with regulatory bodies at their current country of practice.</div>
      </div>
    </div>


   <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse15">Who covers my malpractice insurance for telemedicine visits?</a>
        </h4>
      </div>
      <div id="collapse15" class="panel-collapse collapse">
        <div class="panel-body">Mobihealth offers all our doctors and employees full comprehensive indemnity cover
for works done on our platform. Don’t worry if your current malpractice insurancedoesn’t cover telemedicine outside your state. Our comprehensive insurance will cover you for work done via our platform. Regardless we still ask that you provide us a copy of your current indemnity for our records.</div>
      </div>
    </div>

  <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse16">What legal agreements are involved in practicing with Mobihealth?</a>
        </h4>
      </div>
      <div id="collapse16" class="panel-collapse collapse">
        <div class="panel-body">There is a basic contract to start practicing which includes a Business Associates Agreement (BAA). You can review this contract in the sign-up process.</div>
      </div>
    </div>


     <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse17">How much time do I need to commit to practicing?</a>
        </h4>
      </div>
      <div id="collapse17" class="panel-collapse collapse">
        <div class="panel-body">You can set your availability based on times that are convenient to you. To keep your practice visible to new patients, you must offer minimum of 2hours appointment slots each week.</div>
      </div>
    </div>

     <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse18">Conducting a Video Visit
Can I do a Virtual Consultation even though I am not licensed to practice in that Country?</a>
        </h4>
      </div>
      <div id="collapse18" class="panel-collapse collapse">
        <div class="panel-body"><p>Yes! You don’t have to be registered or licensed to practice in the country where patient is located to do virtual consultation. You must however hold a valid licensed to practice in the country where you are registered and regulated. For more information see link </p>
        <p><a href="https://www.gmc-uk.org/Telemedicine_statement__Scotland__November_2009.pdf_28769996.pdf">https://www.gmc-uk.org/Telemedicine_statement__Scotland__November_2009.pdf_28769996.pdf</a> </p>

<p>‘’The role of the GMC in the regulation of doctors who practise telemedicine: 
The GMC is not a regulator of medical services but of medical practitioners.The GMC regulates doctors who are on its register. It cannot require doctors practising outside the UK to register with the GMC, or to hold registration with a licence to practise with the GMC. Doctors may of course choose to hold a licence to practise even if there is no legal requirement to do so. The GMC can, and does, regulate doctors who are based outside the UK but on the GMC’s register. For doctors practising outside of the UK, the GMC can,, and does,, regulate doctors who are on the GMC’s register.’’<p></div>
      </div>
    </div>

       <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse19">Can I Prescribe for Patients even though I am not licensed in their country?</a>
        </h4>
      </div>
      <div id="collapse19" class="panel-collapse collapse">
        <div class="panel-body"><p>Yes ! See below. We also have doctors and pharmacies who are licensed to practice in our respective countries of operation working on our platform and will be at hand to assist you with prescribing during a consultation should there be any constraints.<p>  

      <a href="https://www.gmc-uk.org/guidance/ethical_guidance/14326.asp">https://www.gmc-uk.org/guidance/ethical_guidance/14326.asp</a>

<p>‘’Prescribing guidance: Remote prescribing via telephone, video-link or online
60. Before you prescribe for a patient via telephone, video-link or online, you must satisfy yourself that you can make an adequate assessment, establish a dialogue and obtain the patient’s consent in accordance with the guidance at paragraphs 20–29.
61. You may prescribe only when you have adequate knowledge of the patient’s health, and are satisfied that the medicines serve the patient’s needs. 
66. If you prescribe for patients who are overseas, you should consider how you or local healthcare professionals will monitor their condition. You should also have regard to differences in a product’s licensed name, indications and recommended dosage regimen.’’ </p></div>
      </div>
    </div>

  <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse20">Can I Prescribe for Patients even though I am not licensed in their country?</a>
        </h4>
      </div>
      <div id="collapse20" class="panel-collapse collapse">
        <div class="panel-body"><p>Yes! You don’t have to be registered or licensed to practice in the country where patient is located to do virtual consultation. You must however hold a valid licensed to practice in the country where you are registered and regulated. For more information see link<p>  

      <a href="https://www.gmc-uk.org/Telemedicine_statement__Scotland__November_2009.pdf_28769996.pdf">https://www.gmc-uk.org/Telemedicine_statement__Scotland__November_2009.pdf_28769996.pdf</a>

<p>‘’The role of the GMC in the regulation of doctors who practise telemedicine:
The GMC is not a regulator of medical services but of medical practitioners.The GMC
regulates doctors who are on its register. It cannot require doctors practising outside the
UK to register with the GMC, or to hold registration with a licence to practise with the
GMC. Doctors may of course choose to hold a licence to practise even if there is no legal
requirement to do so. The GMC can, and does, regulate doctors who are based outside the
UK but on the GMC’s register. For doctors practising outside of the UK, the GMC can,,
and does,, regulate doctors who are on the GMC’s register.’’ </p></div>
      </div>
    </div>

    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse21">Benefits of working with Mobihealth?</a>
        </h4>
      </div>
      <div id="collapse21" class="panel-collapse collapse">
        <div class="panel-body">Our doctors are well looked after and treated with dignity and respect. The following
are the additional benefit of working as a Mobihealth doctor:
  <ul>
    <li>Competitive hourly rate</li>
    <li>Work from the comfort of your home/office</li>
    <li>Earn extra income</li>
    <li>Full medical indemnity insurance covered for all Mobihealth work</li>
    <li>CPD-qualified training sessions</li>
    <li>Performer Awards for milestone consultation hours</li>
    <li>Collaboration with colleagues worldwide</li>
    <li>Knowledge sharing</li>
    <li>Career advancement</li>
    <li>You will be at the forefront of revolutionizing healthcare access and delivery
across the the globe including the African continent.</li>
  </ul>

</div>
      </div>
    </div>

  </div> 
</div>
    



		</div>

		
		</div><!-- /Shop -->
	</div>
	@endsection
