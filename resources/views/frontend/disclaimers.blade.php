@extends('frontend.layouts.app')
@section('content')

<div class="tm-breadcrumb">
		<div class="container">
			<h1 class="tm-section-heading">Disclaimers</h1>
			<ul>
				<li>
					<a href="{{route('homepage')}}">home</a>
				</li>
				<li>
					<a href="#">/ Disclaimers</a>
				</li>
			</ul>
		</div>
	</div><!-- /Breadcrumb header -->
	<!-- Shop -->
  <div class="container">
  <div  style="padding-top: 40px; text-align: left;">
  	<p>MobiHealth is not responsible for any incorrect or inaccurate Content posted on the Website and App in connection with the Services provided, whether caused by Users of the Services or by any of the equipment or programming associated with or utilized in the Services. Information, advice, references, clinical data, suggestions and strategies supplied by Users or by MobiHealth may or may not be correct.</p>

  	<p>We accept no responsibility for ensuring the accuracy of such information, and strongly advises all Users to exercise caution in the interpretation and implementation of such information. Profiles created and posted by Members of MobiHealth may contain links to other websites. MobiHealth is not responsible for the Content, accuracy or opinions expressed on such websites, and such websites are in no way investigated, monitored or checked for accuracy or completeness by MobiHealth. Inclusion of any linked website on the Website neither implies approval, nor endorsement of the linked website by MobiHealth. When Users access these third-party sites, they do so at their own risk.</p>

  	<p>MobiHealth takes no responsibility for third party advertisements, which are posted on this Website, or through the Services, nor does it take any responsibility for the goods or services provided by its advertisers/providers. MobiHealth is not responsible for the conduct, whether online or offline, of any User of the Services. MobiHealth assumes no responsibility for any error, omission, interruption, deletion, defect, delay in operation or transmission, communications line failure, theft or destruction or unauthorized access to, or alteration of, any User or Member communication.</p>

  	<p>MobiHealth is not responsible for any problems or technical malfunction of any telephone network or lines, computer online systems, servers or providers, computer equipment, software, failure of any email or players due to technical problems or traffic congestion on the Internet or on any of the Services or combination thereof, including any injury or damage to Users or to any person's computer related to or resulting from participation or downloading materials in connection with the Services.</p>

  	<p>Under no circumstances shall MobiHealth be responsible for any loss or damage, including personal injury or death, resulting from use or non-use of the Services, attendance at a MobiHealth event, from any Content posted on or through the Services, or from the conduct of any Users of the Services, whether online or offline. The Services are provided "as are” and as available and MobiHealth expressly disclaims any warranty of fitness for a particular purpose or non-infringement. MobiHealth cannot guarantee and does not promise any specific results from use of the Services.</p>

  	<h3 style="padding-bottom: 20px;">Mobile Devices:</h3>
  	<p>To the extent that MobiHealth provides its mobile Services for free, please be aware that your carrier’s normal rates and fees, such as text messaging and data charges, may still apply and you advised to check same.</p>

  	<h3 style="padding-bottom: 20px;">MobiHealth Software:</h3>
  	<p>By downloading or using our software, such as an App, or a browser plugin, you understand and agree that periodically, the software may need to download and install upgrades, updates and supplementary features in order to advance, improve, and further develop the software. MobiHealth software is the property of MobiHealthcare Ltd. and Users are not permitted to modify, create derivative works of, decompile, or otherwise attempt to extract source code from us, unless as otherwise agreed by MobiHealth in written permission.</p>

  	<p>You must not misuse our Services by knowingly introducing viruses, trojans, worms, logic bombs or other material, which is technologically harmful. You must not try to gain unauthorized or deceptive access to our Services, the server on which our Website is stored or any server, computer or database connected to our Website. By contravening the aforementioned, you would commit a criminal offence under the relevant IT misuse laws. MobiHealth will report any such breach to the relevant law enforcement authorities and we will co-operate with those authorities by divulging your identity to them. In the event of such a breach, your right to use our Services will immediately terminate.</p>

  	<h3 style="padding-bottom: 20px;">Changing or Deleting Your information, Or Cancelling Your Account</h3>
  	<p>You may review, update, amend or delete your personal information in your Member account by logging into your account.</p>

  	<h3 style="padding-bottom: 20px;">Termination and Temporary Suspension:</h3>
  	<p>MobiHealth will judge, in its discretion, whether there has been a breach of its Terms on the part of our users. When a breach of these Terms has occurred, we may take such action as deemed appropriate by us.</p>

  	<p>Failure to comply with the Terms constitutes a material breach of the Terms upon which users are permitted to use our Services, and may result in MobiHealth taking all or any of the following actions:</p>

  	<ul>
  		<li>Immediate, temporary or permanent removal of a User’s right to use our Services;</li>
<li>Immediate, temporary or permanent removal of any posting or material uploaded by a user to our Services.</li>
<li>Issue of a warning to a User.</li>
<li>Legal proceedings against a User for reimbursement of all costs on an indemnity basis (including, but not limited to, reasonable administrative and legal costs) resulting from the breach.</li>
<li>Further legal action and/or disclosure of such information to law enforcement authorities, as deemed appropriate.</li>

  	</ul>

  	<p>Note that MobiHealth excludes liability for actions taken in response to breaches of these acceptable use provisions. The responses described herein are not limited, and we may take any other action we reasonably deem appropriate.</p>

  	<p>These Terms shall remain in full force and effect while the Services are used or the User is a Member. Users may terminate Membership at any time, for any reason, by following the instructions on the Member's account settings page.</p>

  	<p>We may terminate your Membership at any time, without warning. Although please note that even after Membership is terminated, these Terms will remain in effect.</p>

  	<h3 style="padding-bottom: 20px;">Changes to Terms:</h3>
  	<p>
  	MobiHealth may modify these Terms at any time and any such modification shall be effective upon posting of the Terms on the Website. Users agree to be bound by all changes to these Terms when they use the Services after any such modification is posted. It is therefore important that Users review these Terms regularly to ensure you are fully aware of any changes. Users must choose carefully the information posted on MobiHealth and that is provided to other Users.</p>

  	<h3 style="padding-bottom: 20px;">ALL INFORMATION PROVIDED ON THIS WEBSITE IS PROVIDED “AS IS” WITH ALL FAULTS WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED. MOBIHEALTH SHALL NOT BE LIABLE FOR ANY INDIRECT, SPECIAL, CONSEQUENTIAL, OR INCIDENTAL DAMAGES INCLUDING, WITHOUT LIMITATION, LOST PROFITS OR REVENUES, COSTS OF REPLACEMENT GOODS, LOSS OR DAMAGE TO DATA ARISING OUT OF THE USE OR INABILITY TO USE THIS SITE OR ANY MOBIHEALTH PRODUCT, DAMAGES RESULTING FROM USE OF OR RELIANCE ON THE INFORMATION PRESENT, EVEN IF MOBIHEALTH HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.</h3>

  	<h3 style="padding-bottom: 20px;">Intellectual Property Rights:</h3>
  	<p>The Services are protected under the copyright laws of the Federal Government of Nigeria and other countries.  All copyrights in the Services are owned by us or our third-party licensors to the full extent permitted under the relevant Copyright laws and all international copyright laws. You may not publish, reproduce, distribute, display, perform, edit, adapt, modify, or otherwise exploit any part of the Services without our written consent.</p>

  	<p>All rights in the product names, company names, trade names, logos, service marks, trade dress, slogans, product packaging, and designs of the Services, whether or not appearing in large print or with the trademark symbol, belong exclusively to MobiHealth or its licensors and are protected from reproduction, imitation, dilution, or confusing or misleading uses under national and international trademark and copyright laws. The use or misuse of these trademarks or any materials, except as authorized herein, is expressly prohibited, and nothing stated or implied on the Services confers on you any license or right under any patent or trademark of MobiHealth, its affiliates, or any third party.</p>

  	<h3 style="padding-bottom: 20px;">Limitations:</h3>
  	<p>In no event shall MobiHealth be liable to you, any user, or any third party for any indirect, consequential, exemplary, incidental, special or punitive damages, including lost profit damages arising from your use of the services, even if MobiHealth has been advised of the possibility of such damages. Notwithstanding anything to the contrary contained herein, MobiHealth’s liability to you for any cause whatsoever and regardless of the form of the action, will at all times be limited to the amount paid, if any, by you to MobiHealth for the services during the term of membership.</p>

  	<h3 style="padding-bottom: 20px;">Miscellaneous:</h3>
  	<p>These Terms are accepted upon use of the Website or any of the Services and is further affirmed by becoming a Member. These Terms constitute the entire Terms between you and MobiHealth regarding the use of the Services. The failure of MobiHealth to exercise or enforce any right or provision of these Terms shall not operate as a waiver of such right or provision.</p>

  	<p>The section titles in these Terms are for convenience only and have no legal or contractual effect. These Terms operate to the fullest extent permissible by law. If any provision of these Terms is deemed to be unlawful, void or unenforceable, that provision is deemed severable from these Terms and does not affect the validity and enforceability of any remaining provisions.</p>

  	<h3 style="padding-bottom: 20px;">Disputes:</h3>
  	<p>The courts in Our countries of operation will have exclusive jurisdiction over any claim arising from, or related to, our Services, although we retain the right to bring proceedings against you for breach of these conditions in your country of residence or any other relevant country.</p>
<p>These Terms and any dispute or claim arising out of or in connection with them or their subject matter or formation (including non-contractual disputes or claims) shall be governed by and construed in accordance with the law of [State].
</p>

<p>Users agree to indemnify and hold MobiHealth, its subsidiaries, and affiliates, and their respective officers, agents, partners and employees, harmless from any loss, liability, claim, or demand, including reasonable attorneys' fees, made by any third party due to or arising out of use of the Services in violation of these Terms and/or arising from a breach of these Terms and/or any breach of representations and warranties set forth above and/or if any Content that Users post on the Website or through the Services causes MobiHealth to be liable to another.</p>

<p>MobiHealth is neither liable for any distasteful, unsuitable, indecent, illegal nor otherwise unacceptable content or information that be made available through its Services, nor is it responsible for the conduct, of any Users of MobiHealth or otherwise.</p>

	<h3 style="padding-bottom: 20px;">Definitions Section:</h3>
	<ul>
		<li>"application" or “App” we mean any application or website that uses or accesses the Platform, as well as anything else that receives or has received data from MobiHealth.</li>
<li>“Content” means content, messages, text, files, images, photos, video, sounds, profiles, works of authorship, or any other materials as loaded onto the Services by Users.</li>
<li>"data" or "User data" or "User's data" means any data, including a User's content or information that you or third parties can retrieve from MobiHealth or provide to MobiHealth through means of the Platform.</li>
<li>“Individual Membership” means personal MobiHealth membership for doctors, other registered healthcare professionals and healthcare service providers/their representatives or affiliates such as Pharmacies, drug manufactures/Distributors,  Diagnostic centres and Hospitals.</li>
<li>“Institutional Membership” means membership of MobiHealth for hospitals, clinics, societies, companies and other organizations authorized by MobiHealth.</li>
<li>"ME" means MobiHealth E-mail as owned by MobiHealthcare Ltd.</li>
<li>MobiHealth Content means content that is contained within the Services as protected by copyright, trade mark, patent, trade secret and other laws and which is owned by, MobiHealthcare Limited.</li>
<li>“Member” means a MobiHealthcare fully approved registered user of Services.</li>
<li>"MM" means the MobiHealth Messaging tool as owned by MobiHealthcare Ltd.</li>
<li>“personal information" means facts and other information relating to a living individual who is or can be identified, either from that information or from that information in conjunction with other information that is in, or likely to come into, the possession of the data controller, including actions taken by Users and non-users who interact with MobiHealth.</li>
<li>"Platform" means a set of APIs and services (such as content) that enable others, including application developers and website operators, to recover data from MobiHealth or provide data to us.</li>
<li>"post" means a displayed or published Content on or through the Services.</li>
<li>“Services” means MobiHealth brands, products and services such our App, our Website and other application based platforms.</li>
<li>"use" means use, run, copy, publicly perform or display, distribute, modify, translate, and create derivative works of.</li>
<li>“User” means a Visitor or a Member.</li>
<li>“User Registration Data” means the personal information that was provided by Users as part of the registration process to become a Member.</li>
<li>“Visitor” means a visiting user of the Services who is not registered as a member of MobiHealth.</li>
<li>"Website" means the [website] website as owned by MobiHealth Ltd.</li>

	</ul>

	<p>These Terms constitute the entire agreement between you and us, superseding any prior or contemporaneous communications and proposals (whether oral, written or electronic) between you and us.  In the event any provision of these Terms is held unenforceable, it will not affect the validity or enforceability of the remaining provisions and will be replaced by an enforceable provision that comes closest to the intention underlying the unenforceable provision. You agree that no joint venture, partnership, employment, or agency relationship exists between you and us as a result of these Terms or your access to and use of the Services.</p>

	<p>Our failure to enforce any provisions of these Terms or respond to a violation by any party does not waive our right to subsequently enforce any terms or conditions of the Terms or respond to any violations.  Nothing contained in these Terms is in derogation of our right to comply with governmental, court, and law enforcement requests or requirements relating to your use of the Services or information provided to or gathered by us with respect to such use.</p>
</div>
</div>
	@endsection