@extends('frontend.layouts.app')
@section('content')

	<!-- Breadcrumb header -->
	<div class="tm-breadcrumb">
		<div class="container">
			<h1 class="tm-section-heading">News Detail</h1>
			<ul>
				<li>
					<a href="{{route('homepage')}}">home</a>
				</li>
				<li>
					<a href="{{route('news_details')}}">/ News Detail</a>
				</li>
			</ul>
		</div>
	</div><!-- /Breadcrumb header -->
	<!-- Blog Content Area -->
	<div class="blog-detail">
		<div class="container">
			<div class="row">
				<div class="col-sm-8">
					<div class="tm-news-home">
						<div class="row">
							<div class="col-sm-12">
								<div class="news">
									<div class="news-thumbnail">
									<div class="post-date">
										02<br>Oct
					</div><img alt="dentist" src="{{asset('/assets/front/images/blog2.png')}}"></div>
									<div class="news-title">
										<a href="#" class="text-uppercase">nigeria medicine</a>
									</div>
									<div class="news-meta">
										<ul>
											<li><span class="icon-user"></span> by admin</li>
											{{-- <li><span class="icon-eye-close-up"></span> 132 views</li>
											<li><span class="icon-comment"></span> 52 comments</li> --}}
											<li><span class="icon-calendar-one"></span> oct 02, 2017</li>
										</ul>
									</div>
									<div class="separator"></div>
									<p>Telemedicine is going to be a critical capability for practices large and small moving forward. Patients are looking for more convenience, and providers are facing much more competitive pressure both in their geographic markets and online.</p>
									<p>Many doctors assume telemedicine will raise your liability. But actually, that’s not the case.</p>
									<p>Because telemedicine will be such an important part of the sustainability of healthcare practices moving forward, following a rigorous software and vendor selection process is critical to ensuring a successful deployment.</p>
									<p>In this eVisit white paper, we’ve provided a step-by-step guide to defining your telemedicine goals, identifying a business case, and evaluating and selecting the right telemedicine solution for you practice. The paper includes the important features and functions to consider to narrow down your choices, as well as tips for making sure the vendor is the right partner for your business.</p>

								
								
								</div>
							</div>
					
						</div>
					</div>
				</div><!-- Sidebar -->
				<div class="col-sm-4">
					<div class="tm-sidebar">
					


							<!-- Recent Posts -->
						<div class="sidebar-widget sidebar-posts">
							<h4 class="widget-heading">recent posts</h4>
						<ul>
								<li>
									<div class="post-thumbnail"><img alt="blog" src="{{asset('/assets/front/images/blog1.png')}}"></div>
									<div class="content">
										<a class="title" href="{{route('news_details_1')}}">HOW DOES TELEMEDICINE AFFECT MALPRACTICE INSURANCE?</a> <span>Today - by admin</span>
									</div>
								</li>

								<li>
									<div class="post-thumbnail"><img alt="blog" src="{{asset('/assets/front/images/blog2.png')}}"></div>
									<div class="content">
										<a class="title text-uppercase" href="http://www.afro.who.int/sites/default/files/2017-06/nigeria_medicine_prices.pdf" target="_blank">nigeria medicine</a> <span>Today - by admin</span>
									</div>
								</li>

								<li>
									<div class="post-thumbnail"><img alt="blog" src="{{asset('/assets/front/images/blog3.png')}}"></div>
									<div class="content">
										<a class="title text-uppercase" href="{{route('news_details_3')}}">our africa health</a> <span>Today - by admin</span>
									</div>
								</li>
							
								
							</ul>
						</div><!-- /Recent Posts -->

				
					</div>
				</div><!-- /Sidebar -->
			</div>
		</div>
	</div><!-- /Blog Content Area -->
	<!-- Footer -->
	
	@endsection