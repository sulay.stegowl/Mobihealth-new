<footer class="tm-footer">
		<div class="container">
			<div class="row">
				<div class="footer-logo">
					<a class="logo-top" href="{{route('homepage')}}"><img alt="Logo" src="{{asset('/assets/front/images/logo_new2.png')}}"></a>
					<p class="logo-caption">Our mission is to provide people in developing countries timely access to quality healthcare services from around the world when they need it in the most cost effective way!</p>
				</div>
				<div class="col-sm-4">
					<div class="footer-widget opening-hours">
						<h4 class="footer-heading">opening hours</h4>
						<ul>
							<li><span class="pull-left">Opened 24/7 365 days a year</span> {{-- <span class="pull-right">9am - 10pm</span> --}} </li>
							{{-- <li><span class="pull-left">Saturday</span> <span class="pull-right">9am - 3pm</span></li>
							<li><span class="pull-left">Sunday</span> <span class="pull-right">Closed</span></li> --}}
						</ul>
						<p>Mobihealth is for acute non-emergency medical issues, if you require emergency service, please call/visit your local emergency number/centre for help.</p>

						<div class="footer_like_list">
							<a href="{{route('terms_condition')}}">Terms & Condition</a>  <a href="{{route('privacy_policy')}}">Privacy policy</a> <a href="{{route('disclaimers')}}">Disclaimers</a>
						</div>

					</div>
				</div>
				<div class="col-sm-4">
					<div class="footer-widget latest-news">
						<h4 class="footer-heading">latest news</h4>
						<ul>
							<li>
								<div class="news-thumbnail"><img class="he_wi_au" alt="news" src="{{asset('/assets/front/images/blog1.png')}}"></div>
								<div class="news-content">
									<div class="title">
										<a  href="https://blog.evisit.com/telemedicine-affect-malpractice-insurance" target="_blank">HOW DOES TELEMEDICINE AFFECT...</a>
									</div>
									<div class="date">
										Oct 02, 2017
									</div>
								</div>
							</li>

							<li>
								<div class="news-thumbnail"><img alt="news" src="{{asset('/assets/front/images/blog2.png')}}"></div>
								<div class="news-content">
									<div class="title">
										<a href="https://www.afro.who.int/sites/default/files/2017-06/nigeria_medicine_prices.pdf" target="_blank" class="text-uppercase">Medicine Prices In Nigeria</a>
									</div>
									<div class="date">
										Oct 02, 2017
									</div>
								</div>
							</li>
							<li>
								<div class="news-thumbnail"><img alt="news" src="{{asset('/assets/front/images/blog3.png')}}"></div>
								<div class="news-content">
									<div class="title">
										<a  class="text-uppercase" href="https://africacheck.org/factsheets/factsheet-africas-leading-causes-death/" target="_blank" >our africa health </a>
									</div>
									<div class="date">
										Oct 02, 2017
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="footer-widget">
							
					<div class="footer-widget contact-info">
						<h4 class="footer-heading">contact information</h4>
						<ul>
							{{-- <li><span class="icon-phone"></span> +23470007000 </li> --}}
							<li><span class="icon-at"></span> info@mobihealthinternational.com</li>
						</ul>
					</div>


					<div class="footer-widget social-links">
						<h4 class="footer-heading">find us on social media</h4>
						<ul>
							<li>
								<a href="https://www.facebook.com/Mobihealth-International-126398898051472/?notif_id=1510608038297095&notif_t=page_fan" target="_blank"><span class="icon-facebook" ></span></a> 
							</li>
							<li>
								<a href="https://www.instagram.com/mobihealth4u/?hl=en" target="_blank"><span class="icon-instagram"></span></a>
							</li>
							<li>
								<a href="https://twitter.com/MobihealthIntl" target="_blank"> <span class="icon-twitter-logo"></span></a> 
							</li>
							<li>
								<a href="https://plus.google.com/u/0/114500041457788549135" target="_blank"><span class="icon-google-plus"></span></a>
							</li>
							<li>
								<a href="https://linkedin.com/company/11358918" target="_blank"><i class="fa 
								fa-linkedin" aria-hidden="true"></i></a>
							</li>
							{{-- <li><a href="#"><i aria-hidden="true" class="fa fa-pinterest-p"></i></a></li> --}}
						</ul>
					</div>
			
				
					
					</div>
				</div>
			{{-- 	<div class="col-sm-3">
					<div class="footer-widget quick-contact">
						<h4 class="footer-heading">quick contact</h4>
						<form>
							<div class="form-group">
								<input class="form-control" name="f_email" placeholder="Email address" type="text">
							</div>
							<div class="form-group">
								<textarea class="form-control" name="f_message" placeholder="Message"></textarea>
							</div>
							<div class="form-group">
								<input class="tm-btn btn-blue" name="submit" type="submit" value="submit">
							</div>
						</form>
					</div>
				</div> --}}
				<div class='clearfix'></div>
			{{-- 	<div class="col-sm-6">
					<div class="footer-widget subscribe">
						<h4 class="footer-heading">subscribe to our newsletter</h4>
						<form class="form-inline">
							<div class="col-sm-8 nopadding">
								<input class="form-control" name="email_subscribe" placeholder="Email address" type="text">
							</div>
							<div class="col-sm-4 nopadding">
								<input class="tm-btn btn-blue" name="submit" type="submit" value="Subscribe">
							</div>
						</form>
					</div>
				</div> --}}
			
				
			</div>
		</div>
		<div class="copyright-bar">
			<p>Copyright © 2017. Mobihealth</p>
		</div>
	</footer><!-- /Footer -->


 <div class="modal fade coming_soon" id="myModal" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><img alt="Logo" src="{{asset('assets/front/images/logo_new2.png')}}"></h4>
        </div>
        <div class="modal-body">
          <p>COMING SOON</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>