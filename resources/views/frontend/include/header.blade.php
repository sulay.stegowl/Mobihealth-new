

	<div id='backTop'>
		<i aria-hidden="true" class="fa fa-arrow-up"></i>
	</div><!-- Header -->
	<header>
		<!-- Topbar -->
		<div class="topbar">
			<div class="container">
				<div class="t-links">
					
<a class="logo-top hidden-sm hidden-md hidden-lg" href="{{route('homepage')}}"><img alt="Logo" src="{{asset('/assets/front/images/logo_new2.png')}}"></a> 

				</div>
				<div class="t-options">
					<div class="icons_t t-links">
						<!-- {{route('registration')}} -->
						<a href="{{route('registration')}}"> Register </a> 						
					</div>
					<div class="icons_t t-links">
						<a href="{{route('login')}}" target="_blank"> Login </a>
					</div>
				</div>
			</div>
		</div><!-- /Topbar -->

		<marquee behavior="" direction="" class="marquee"> 
			
<p>
		Introducing Mobihealth International, your gateway to quality healthcare services worldwide! Request an instant video consultation on your mobile now…anytime anywhere…as easy as booking a taxi! 
</p>

	</marquee>

		<!-- Info Section -->
		<div class="topinfo">
			<div class="container">
				<div class="row">
					<!-- Information -->
					<div class="col-sm-7 col-lg-7 col-md-8 col-xs-12">
						<div class="c_info">
							<!-- Contact Number -->
							{{-- <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
								<div class="c_contact">
									<span class="icon-phone"></span>
									<div class="detail">
										<span class="number">+23470007000 </span> <span class="subtitle">Call us anytime</span>
									</div>
								</div>
							</div>  --}}<!-- /Contact Number -->
							<!-- Email Info -->
							<div class="col-lg-7 col-md-5 col-sm-12 col-xs-12">
								<div class="c_contact">
									<span class="icon-at"></span>
									<div class="detail">
										<span class="number">info@mobihealthinternational.com</span> <span class="subtitle">Send us an email</span>
									</div>
								</div>
							</div><!-- /Email Info -->
							<!-- Email Info -->
							
						</div>
					</div><!-- /Information -->
					<!-- Social Links -->
					<div class="tm_top_social">

						<a class="tm-btn btn-app btn-dark" href="javascript:void(0)"  data-toggle="modal" data-target="#myModal" >Request video consultation</a> 

						<a href="https://www.facebook.com/Mobihealth-International-126398898051472/?notif_id=1510608038297095&notif_t=page_fan" target="_blank"><span class="icon-facebook" ></span></a> 
						<a href="https://www.instagram.com/mobihealth4u/?hl=en" target="_blank"><span class="icon-instagram"></span></a>
						<a href="https://twitter.com/MobihealthIntl" target="_blank"><span class="icon-twitter-logo"></span></a> 
						 <a href="https://plus.google.com/u/0/114500041457788549135" target="_blank"><span class="icon-google-plus"></span></a>
						 <a href="https://linkedin.com/company/11358918" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
						 
						 
						  
					</div><!-- /Social Links -->
				</div>
			</div>
		</div><!-- /Info Section -->
		<!-- Menu Bar -->
		<div class="tm-main-menu">
			<div class="container">
				<!-- Logo -->
				<a class="logo-top" href="{{route('homepage')}}"><img alt="Logo" src="{{asset('/assets/front/images/logo_new2.png')}}"></a> <!-- /Logo -->
				 <!-- Menu -->
				<div class="nav-wrapper">
					<ul>
						<li>
							<a href="{{route('homepage')}}">Home</a>
						</li>
						<li >
							<a href="{{route('about')}}">About us</a>
							
						</li>
						
						
						<li >
							<a href="{{route('services_web')}}"> Our Services</a>
							
						</li>

						
						<li class="menu-item-has-children">
							<a href="javascript:void(0)">Resources</a>
							<ul class="sub-menu">
								<li>
									<a href="https://www.gmc-uk.org/Telemedicine_statement__Scotland__November_2009.pdf_28769996.pdf" target="_blank">GMC Guidance on Telemedicine </a>
								</li>
								<li>
									<a href="https://circabc.europa.eu/webdav/CircaBC/FISMA/markt_consultations/Library/Online%20services/The%20future%20of%20electronic%20commerce/Regulated%20professions/UK%20GENERAL%20MEDICAL%20COUNCIL%20PR%20782499.pdf" target="_blank">GMC - No Barriers for Doctors</a>
								</li>
								<li>
									<a href="https://www.gmc-uk.org/guidance/ethical_guidance/14326.asp" target="_blank">Prescribing Guidance</a>
								</li>
								<li>
									<a href="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4938925/" target="_blank">The Battle Against Malaria</a>
								</li>
								
							</ul>
						</li>


							<li class="menu-item-has-children">
							<a href="javascript:void(0)">Medical Service  Providers</a>
							<ul class="sub-menu">
								<li>
									<a href="{{route('hospital')}}">Hospital</a>
								</li>
								<li>
									<a href="{{route('pathology')}}">Pathology</a>
								</li>
								<li>
									<a href="{{route('pharmacy')}}">Pharmacy</a>
								</li>
								<li>
									<a href="{{route('radiology')}}">Radiology</a>
								</li>
							</ul>
						</li>


						{{-- <li >
							<a href="{{route('doctors')}}">Doctors</a>
							
						</li> --}}
						
						<li class="menu-item-has-children">
							{{-- <a href="{{route('subscription')}}">subscriptions </a> --}}
							<a href="{{route('subscription')}}">Subscriptions</a>
							<ul class="sub-menu">
								<li>
									<a href="{{route('subscription')}}">individuals/Families</a>
								</li>
								<li>
									<a href="{{route('subscription_plan_2')}}">Corporations/NGOs</a>
								</li>
							</ul>							
						</li>

						{{-- <li>
							<a href="{{route('news')}}">News</a>
							
						</li>
						 --}}
						
						<li>
							<a href="{{route('contact')}}">Contact us</a>
							
						</li>
								<li class="menu-item-has-children">
							<a href="javascript:void(0)">FAQ</a>
							<ul class="sub-menu">
								<li>
									<a href="{{route('hospital_faq')}}">FAQ- Hospitals</a>
								</li>
								<li>
									<a href="{{route('pathology_faq')}}">FAQ- Laboratories</a>
								</li>
								<li>
									<a href="{{route('pharmacy_faq')}}">FAQ-Pharmacies</a>
								</li>
								<li>
									<a href="{{route('radiology_faq')}}">Radiology's FAQ</a>
								</li>
								<li>
									<a href="{{route('patients_faq')}}">FAQ- Subscribers</a>
								</li>
								<li>
									<a href="{{route('doctor_faq')}}">FAQ -Medical Professionals</a>
								</li>
							</ul>
						</li>
					</ul>
				</div><!-- /Menu -->
			</div>
		</div><!-- /Menu Bar -->
		<!-- Mobile Menu -->
		<div class="dl-menuwrapper" id="dl-menu">
			<button class="dl-trigger">Open Menu</button>
			<ul class="dl-menu">
				<li>
					<a href="{{route('homepage')}}">Home</a>
				</li>
				<li>
				<a href="{{route('about')}}">About us</a>
					
				</li>
				
				<li>
				<a href="{{route('services_web')}}">Our Services</a>
					
				</li>


				<li>
						<a href="javascript:void(0)">Medical Service  Providers</a>
					<ul class="dl-submenu">
						<li>
									<a href="{{route('hospital')}}">Hospital</a>
								</li>
								<li>
									<a href="{{route('pathology')}}">Pathology</a>
								</li>
								<li>
									<a href="{{route('pharmacy')}}">Pharmacy</a>
								</li>
								<li>
									<a href="{{route('radiology')}}">Radiology</a>
								</li>
					</ul>
				</li>

				<li>
							<a href="javascript:void(0)">Resources</a>
					<ul class="dl-submenu">
						<li>
									<a href="https://www.gmc-uk.org/Telemedicine_statement__Scotland__November_2009.pdf_28769996.pdf" target="_blank">GMC Guidance on Telemedicine </a>
								</li>
								<li>
									<a href="https://circabc.europa.eu/webdav/CircaBC/FISMA/markt_consultations/Library/Online%20services/The%20future%20of%20electronic%20commerce/Regulated%20professions/UK%20GENERAL%20MEDICAL%20COUNCIL%20PR%20782499.pdf" target="_blank">GMC - No Barriers for Doctors</a>
								</li>
								<li>
									<a href="https://www.gmc-uk.org/guidance/ethical_guidance/14326.asp" target="_blank">Prescribing Guidance</a>
								</li>
					</ul>
				</li>

			



				<li >
					<a href="{{route('subscription')}}">subscriptions</a>
					<ul class="dl-submenu">
						<li>
							<a href="{{route('subscription')}}">individuals/Families</a>
						</li>
						<li>
							<a href="{{route('subscription_plan_2')}}">Corporations/NGOs</a>
						</li>
					</ul>
				</li>

						



				{{-- <li>
						<a href="{{route('doctors')}}">Doctors</a>
					
				</li> --}}
				{{-- <li>
						<a href="{{route('news')}}">News</a>
				
				</li> --}}
				{{-- <li>
					<a href="#">Faq</a>
				
				</li> --}}
				
				<li>
					<a href="{{route('contact')}}">Contact us</a>
					
				</li>
			</ul>
		</div><!-- /dl-menuwrapper -->
		<!-- /Mobile Menu -->
	</header><!-- /Header -->