@extends('frontend.layouts.app')
@section('content')

<div class="tm-breadcrumb">
		<div class="container">
			<h1 class="tm-section-heading">Service Detail</h1>
			<ul>
				<li>
					<a href="{{route('homepage')}}">home</a>
				</li>
				<li>
					<a href="{{route('services_details')}}">/ COMPREHENSIVE PHYSICAL EXAMINATION</a>
				</li>
			</ul>
		</div>
	</div>

	<div class="tm-service-detail-wrap">
		<div class="container">
			<div class="row">
			
				<div class="col-sm-4">
					<div class="tm-sidebar">
					
						<div class="sidebar-widget questions">
							<h4 class="widget-heading">questions and queries</h4>
							<p>Email us if any queries.</p>
							<ul>
								
								<li><span class="icon-at"></span> info@mobihealthinternational.com</li>
							</ul>
						
						</div>
					</div>
				</div>
				
				<div class="col-sm-8">
					<div class="tm-service-detail">
						<div class="service-detail-thumb"><img alt="service" src="{{asset('/assets/front/images/L9.jpg')}}"></div>
						<h4 class="service-title">COMPREHENSIVE PHYSICAL EXAMINATION</h4>
						<p>Our innovative video platform offers quality video consultation specifically designed for low bandwith areas with difficult network and geographical terrain. Our solutions enables patient led comprehensive physical examination remotely such that a doctor’s visit is easily replicated. A doctor any where in the world can easily listen to patient’s heart and chest sounds, examine their throat, ear or skin without need for a face to face doctor’s visit.</p>
					
					</div>
				</div>
			</div>
		</div>
	</div>

	@endsection