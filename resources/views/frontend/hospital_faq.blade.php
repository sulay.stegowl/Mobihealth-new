@extends('frontend.layouts.app')
@section('content')

<div class="tm-breadcrumb">
		<div class="container">
			<h1 class="tm-section-heading">hospital</h1>
			<ul>
				<li>
					<a href="{{route('homepage')}}">home</a>
				</li>
				<li>
					<a href="{{route('hospital_faq')}}">/ Faq</a>
				</li>
			</ul>
		</div>
	</div><!-- /Breadcrumb header -->
	<!-- Shop -->
	<div class="tm-shop-list">

		
		<div class="container">
		

		<h1 class="tm-section-heading">faq</h1>
		
		<div class="how_it_work">

<div class="container">
			<p>
				Want to learn more about how to bring Mobihealth to your business? Search our FAQs for answers to common clinical, operational, and legal questions.
			</p>
</div>


<div class="container">
 
  <div class="panel-group" id="accordion">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">How are my sure the referrals are from a genuine doctor?</a>
        </h4>
      </div>
      <div id="collapse1" class="panel-collapse collapse in">
        <div class="panel-body">We operate within a high level of ethical and regulatory standards. All our doctors are properly checked and registered practitioners licensed to practice in Nigeria as well as UK, US, Canada, Australia, Dubai and other places. They are reputable board certified and in good standing with their local regulatory bodies. All referrals will be transmitted by Mobihealth electronically and a copy will be given to the patient to. We expect a confirmation of appointment from the hospital to Mobihealth which will communicated to patient.
</div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">How Do you send patients to my Hospital?
</a>
        </h4>
      </div>
      <div id="collapse2" class="panel-collapse collapse">
        <div class="panel-body">Our doctors send written referral to the hospital/specialist which are transmitted electronically. A confirmation of date, time, location and name of doctor to see will be expected from the hospital to Mobihealth and this will be communicated to patient by us.
</div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">Who pays for the Hospital Treatment?
</a>
        </h4>
      </div>
      <div id="collapse3" class="panel-collapse collapse">
        <div class="panel-body">Mobihealth pays for the patient’s hospitalization and treatment. You won’t have to worry about patient’s affordability or reimbursement. Once we receive the invoice, we will deposit same amount to designated account immediately/weekly/monthly depending on preference. </div>
      </div>
    </div>
  </div> 
</div>
    



		</div>

		
		</div><!-- /Shop -->
	</div>
	@endsection
