	@extends('frontend.layouts.app')
@section('content')
	<!-- Breadcrumb header -->
	<div class="tm-breadcrumb">
		<div class="container">
			<h1 class="tm-section-heading">about us</h1>
			<ul>
				<li>
					<a href="{{route('homepage')}}">home</a>
				</li>
				<li>
					<a href="{{route('about')}}">/ about us</a>
				</li>
			</ul>
		</div>
	</div><!-- /Breadcrumb header -->
	<!-- Welcome -->
	<div class="tm-welcome about">
		<div class="container">
			<div class="row">
				<div class="col-sm-5">
					<div class="welcome-image">
						{{-- <img alt="welcome" class="img-responsive" src="{{asset('/assets/front/extra-images/IMG_2146.jpg')}}"> --}}
						<img alt="welcome" class="img-responsive" src="{{url('/').'/storage/'.$data->wel_image}}">
					
					</div>
				</div>
				<div class="col-sm-7">
					<h2 class="tm-box-heading">Welcome to <span>mobihealth</span></h2>
					{{-- <h5> Introducing Mobihealth International, your gateway to quality healthcare services worldwide! Request an instant video consultation on your mobile now…anytime anywhere…as easy as booking a taxi!</h5> --}}
				<div>
					{!!$data->wel_text!!}
				</div>

					
				</div>
			</div>
		</div>
	</div><!-- /Welcome -->
	<!-- Services -->
	<div class="tm-services-style-3">
		<div class="container">
			<div class="row">
				<h1 class="tm-section-heading">our product and services</h1>
				<div class="services-wrapper">
					<div class="col-sm-4">
						<div class="service-box">
							<div class="service-image"><img alt="service" src="{{asset('/assets/front/extra-images/IMG-20171007_WA0002.jpg')}}"></div>
							<div class="service-detail">
								<h3 class="service-title"><a href="#">INSTANT VIDEO CONSULTATION</a></h3>
								<p class="service-description">Immediate Video/Audio consultation with a doctor via your mobile device. This eliminates long waits for doctor </p>
							</div>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="service-box">
							<div class="service-image"><img alt="service" src="{{asset('/assets/front/images/L7.jpg')}}"></div>
							<div class="service-detail">
								<h3 class="service-title"><a href="#">LABORATORY AND RADIOLOGIC INVESTIGATIONS</a></h3>
								<p class="service-description">We book your diagnostic tests with reputable centers and our doctors evaluate the results remotely to give you </p>
							</div>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="service-box">
							<div class="service-image"><img alt="service" src="{{asset('/assets/front/images/L4.jpg')}}"></div>
							<div class="service-detail">
								<h3 class="service-title"><a href="#">PRESCRIPTION AND GENUINE MEDICATIONS</a></h3>
								<p class="service-description">You no longer need to worry about where to get genuine medication. We send your prescriptions to reputable </p>
							</div>
						</div>
					</div>
					<div class="col-sm-12 text-center">
						<a class="tm-btn btn-blue see-all" href="{{route('services')}}">see all services</a>
					</div>
				</div>
			</div>
		</div>
	</div><!-- /Services -->
	<!-- Our Dentists -->
	{{-- <div class="tm-doctors-style-3">
		<div class="container">
			<div class="row">
				<h1 class="tm-section-heading">our experienced doctors</h1>
				<div class="doctors-wrapper">
					<div class="col-sm-3">
						<div class="doctor-box">
							<div class="doctor-thumbnail"><img alt="dentist" src="{{asset('/assets/front/images/480562701.jpg')}}"></div>
							<div class="doctor-info">
								<h2 class="tm-box-heading"><a href="#">Jone doe</a></h2>
								
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</p>
								<div class="share">
									<ul>
										<li>
											<a href="#"><span class="icon-facebook"></span></a>
										</li>
										<li>
											<a href="#"><span class="icon-instagram"></span></a>
										</li>
										<li>
											<a href="#"><span class="icon-twitter-logo"></span></a>
										</li>
									</ul>
								</div><a class="tm-btn btn-dark" href="#">view profile</a>
							</div>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="doctor-box">
							<div class="doctor-thumbnail"><img alt="dentist" src="{{asset('/assets/front/images/480562701.jpg')}}"></div>
							<div class="doctor-info">
								<h2 class="tm-box-heading"><a href="#">Jenna Wilson</a></h2>
								
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</p>
								<div class="share">
									<ul>
										<li>
											<a href="#"><span class="icon-facebook"></span></a>
										</li>
										<li>
											<a href="#"><span class="icon-instagram"></span></a>
										</li>
										<li>
											<a href="#"><span class="icon-twitter-logo"></span></a>
										</li>
									</ul>
								</div><a class="tm-btn btn-dark" href="#">view profile</a>
							</div>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="doctor-box">
							<div class="doctor-thumbnail"><img alt="dentist" src="{{asset('/assets/front/images/480562701.jpg')}}"></div>
							<div class="doctor-info">
								<h2 class="tm-box-heading"><a href="#">William Stamr</a></h2>
								
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</p>
								<div class="share">
									<ul>
										<li>
											<a href="#"><span class="icon-facebook"></span></a>
										</li>
										<li>
											<a href="#"><span class="icon-instagram"></span></a>
										</li>
										<li>
											<a href="#"><span class="icon-twitter-logo"></span></a>
										</li>
									</ul>
								</div><a class="tm-btn btn-dark" href="#">view profile</a>
							</div>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="doctor-box">
							<div class="doctor-thumbnail"><img alt="dentist" src="{{asset('/assets/front/images/480562701.jpg')}}"></div>
							<div class="doctor-info">
								<h2 class="tm-box-heading"><a href="#">Ben Hilsom</a></h2>
								
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</p>
								<div class="share">
									<ul>
										<li>
											<a href="#"><span class="icon-facebook"></span></a>
										</li>
										<li>
											<a href="#"><span class="icon-instagram"></span></a>
										</li>
										<li>
											<a href="#"><span class="icon-twitter-logo"></span></a>
										</li>
									</ul>
								</div><a class="tm-btn btn-dark" href="#">view profile</a>
							</div>
						</div>
					</div>
					<div class="col-sm-12 text-center">
						<a class="tm-btn btn-blue" href="#">meet all dentists</a>
					</div>
				</div>
			</div>
		</div>
	</div>  --}}<!-- /Our Dentists -->
	<!-- Footer -->
	
	
	@endsection