@extends('frontend.layouts.app')
@section('content')

<div class="tm-breadcrumb">
		<div class="container">
			<h1 class="tm-section-heading">Privacy Policy</h1>
			<ul>
				<li>
					<a href="{{route('homepage')}}">home</a>
				</li>
				<li>
					<a href="#">/ Privacy Policy</a>
				</li>
			</ul>
		</div>
	</div><!-- /Breadcrumb header -->
	<!-- Shop -->
  <div class="container">
  <div  style="padding-top: 40px; text-align: left;">
  	<h3 style="padding-bottom: 20px">Privacy Policy And User Agreement</h3>
  <p>MobiHealth, located at www.mobihealthinternational.com is a site operated by [MobilHealthcare Ltd] hereinafter “MobiHealth”, “we” or “us”. We are registered in Nigeria and undergoing registration in other African Countries. We are a limited liability company.</p>

  <p>We operate a Platform and an online community that is designed to form a network for doctors, other healthcare professionals and medical service providers to engage, share and learn with view to improving overall healthcare and well being of people in developing countries by offering video consultations remotely and also connecting them to healthcare services within their locality and beyond. The Services offered by MobiHealth include our app (the “App”), the Mobihealthinternational.com website (the "Website"), the MobiHealth Instant Video/SMS Messaging ("MHIVSM"), the MobiHealth E-mail ("MHE"), and any other features, content, or applications offered from time to time by us and our partners.</p>

  <p>MobiHealth is committed to protecting your personal information when using our services; we strive to create a safe and pleasant environment for everyone. In order to provide you with the full spectrum of our Services, we may need to collect information about you. This page aims to help you to understand what information we might collect about you and how we use it, how MobiHealth protects such data and what choices you have on how that information is used.</p>

  <p>MobiHealth is dedicated to safeguarding and respecting your privacy. This Privacy Policy (together with our Terms and Conditions and any other documents referred to in it) outlines the way in which any personal data that MobiHealth collects from you, or that you provide to MobiHealth, will be processed. In accordance with the all applicable data protections laws governing the relevant jurisdiction, the data controller is MobiHealth. Any capitalised terms not explicitly defined in this Privacy Policy are defined in our general Terms and Conditions, which work in conjunction with this Privacy Policy to guide the relationship between MobiHealth and Users.</p>

  <p>This Privacy Policy does not apply to the practices of companies that MobiHealth does not own or control, or to people that MobiHealth does not employ or manage.</p>

  <p>All information that you provide, by means of entering details onto our Website or our App, as well as email such information collection and use will be handled in the following manner set forth in this Privacy Policy.</p>



</div>
<div>
	<h2 style="padding-bottom: 20px;">MOBIHEALTH MAY COLLECT AND PROCESS THE FOLLOWING DATA ABOUT YOU</h2>
	<h3 style="padding-bottom: 20px;">Information Collected:</h3>

	<p>
		Visitors to our Website can access the Website’s home page and browse some areas of the Website, without disclosing any personally identifiable information. Visitors to our App may browse some areas of the App, without disclosing any personally identifiable information.
	</p>

	<p>
		We may track information provided to us by your browser, including the Website you came from (known as the referring URL), any MobiHealth online advertisements located on third party websites that you may have clicked on to access the Website, the type of browser you use, the time and date of access, and other information that does not personally identify you.
	</p>

	<p>In some cases you must register to access portions of the Website and App. In addition, we gather information about you that is automatically collected by our Web server, such as your IP address and domain name. MobiHealth may use Web server and browser information to individually customize its offerings and presentations if you submit your personal information. We may also ask you for information when you report a problem with our Website and/or any of our services.</p>

	<p>Individual Members must be registered with their national regulatory authorities as a doctor, nurse or allied healthcare professional. Therefore, Users who wish to become an Individual Member, will submit to a unique identification pre-registration check upon application to MobiHealth. Personal e-mail addresses determine this unique identifier and provide both a User ID necessary to log in to our Services and a method of communication between us, and our Members. We require Users to confirm their registration through a number of mechanisms including a verification link through a professional email address.</p>

	<p>A Member’s personal e-mail address remains hidden from other Users unless they choose to display it. Members and pre-registered Members in corporate schemes have the opportunity to unsubscribe at any stage whereupon all personal data will be removed. Corporate scheme Member pre-registration requires submission by the corporate scheme client of first name, surname and e-mail address to generate a pre-registered member profile. Corporate scheme clients are provided with an e-mail for all prospective members informing that corporate scheme Membership has been provided for them and that their e-mail address will be released for this purpose, and this purpose alone to us. An e-mail address is provided allowing individuals to unsubscribe preventing submission of their e-mail address. Corporate scheme Members may unsubscribe at any time following pre-registration whereupon all personal data and e-mail addresses will be removed. MobiHealth selects and stores non-personally-identifiable data including IP address, profile information, aggregate user data, and browser type, from Members and Visitors to our Website is an internal service that allows communication between members without the need to disclose personal e-mail addresses to one another. Members may wish to disclose personal e-mail addresses to one another but are advised to do so with caution, and at their own risk.</p>

	<p>Access or sign up to any of our Services, activities or online content, such as our social media pages, our App, newsletters, live chats, message boards, or creation of an account using MobiHealth’s online registration system may result in MobiHealth receiving personal information about you.</p>

	<p>When Visitors register to use services on our Website and/or App, they are asked to provide identifying information. This may consist of information such as your name, gender, email address, postal address, telephone or mobile number or date of birth, depending on the activity. On our registration screen, we may label which information is required for registration, and which information is optional and may be given at your discretion. Our Website and/or App will obtain your consent before collecting personally identifiable information.</p>

	<p>In order to register as a User, you may be asked to sign up to our Services by using your LinkedIn login, Facebook login, by entering your details manually or with other partner organization mechanisms. By doing this, you automatically authorize us to access some of your LinkedIn, Facebook and partner data, such as your public LinkedIn/Facebook/partner profile /email address /interests / gender /birthday /current city /photos /connections, and data about and photos of your LinkedIn connections. You may also be asked to allow us to collect your location data from your device when you download or use our Service. Upon collection of same, we may be classifying data, such as your name, address, email address and telephone number, and, if you conduct business with us, financial information.</p>

	<h3 style="padding-bottom: 20px;">Information collected on registration: </h3>

	<p>All personal information submitted by users outside the relevant jurisdiction will be processed in accordance with this Privacy Policy (and any local terms that apply to certain areas of the site). Where there is a conflict, the local terms will apply.</p>

	<p>Our Services are not intended or designed to attract people under the age of 18. We do not knowingly collect personally identifiable data from site visitors under the age of 18.</p>

	<p>We will make effort to limit the collection, use and holding of your information that we require to deliver exceptional service to you, which includes,</p>

	<p>1.	Advising to you about MobiHealth products, newsletters, services and other opportunities, and to administer MobiHealth’s business and/or provide you with relevant info, products or services that you have requested.<br>
2.	To ensure that content from our Website and/or App is presented in the most effective way for you and your accessing devices.<br>
3.	To present your Member profile, photos and/or posting(s) to the general public or Users.<br>
4.	To permit the Platform to allow your Member profile and/or posting(s) to be included in search engines.<br>
5.	To allow your activity to be visible to others.<br>
6.	To allow users to exchange details with each other, which may include the disclosure of personal information and photo.<br>
7.	Parts of your Member profile, messages, or posting(s) may feature in other parts of the Platform for marketing functions.<br>
8.	To fulfill our obligations arising from any contracts entered into between you and us.<br>
9.	If you contact us, we may keep a copy of, or log your communication.<br>
10.	To notify you about changes to any and all of our Services.<br>
11.	We will permit only authorized employees, consultants, third party vendors or other agents, who are trained in the proper handling of customer information, to have access to that information. Employees who violate our Privacy Policy will be subject to our normal disciplinary process.<br>
12.	To use your data, or permit selected third parties to use your data, to send you info about goods and services, which may be of interest to you and we, or they, may contact you about these by e-mail or SMS. Additionally, we may sometimes send offers or information to selected groups of users. To accomplish this, we may use third parties working on behalf of MobiHealth.<br>
13.	In relation to IP addresses, we may collect information about your computer or laptop (or other such processor), including your IP address, operating system and browser type, for system administration and to report aggregate information to our advertisers. This is statistical data about our users' browsing actions and patterns, and does not identify any individual.<br>
14.	We will take reasonable steps to safeguard any information you share with us. We store the information you provide about yourself in a database in order to provide you with the information you request. The information is stored for the lifetime of the database unless you request that it be removed.<br>
15.	Member accounts are secured and kept private by Member authenticated passwords. Where we have provided you (or where you have chosen) a password, which enables you to access certain parts of our site and/or app, it is your responsibility to keep this password confidential and not to share your Member password with any third parties.<br>
16.	All information as provided by you to us will be kept on our secure servers. We take precautions to ensure that Member account information is kept private. Unauthorised entry or use, hardware or software failure, and other factors may compromise the security of member information, however MobiHealth takes all reasonable precautions to maintain our Services and data security.<br>
17.	Not withstanding all our efforts to secure your info, we remind you that the transmission of data via the Internet is not completely secure. MobiHealth is therefore unable to guarantee the security of your data transmitted to our site or our App; any transmission of your data is at your own risk.<br>
18.	Chat Rooms, Message Boards, Public Forums and Weblogs: Please be aware that whenever you voluntarily post public information to WebLogs or Message Boards or any other Public Forums that that information can be accessed by the public and can in turn be used by those people to send users unsolicited communications<br>
19.	You of course have the right to ask us not to process your personal data for marketing purposes. You can exercise your right to prevent such processing by checking nominated sections on the forms we use to collect your data. You can also contact us privacy@mobihealthinternational.com
</p>

<h3 style="padding-bottom: 20px;">
	THIRD PARTIES
</h3>
<p>MobiHealth may share some kinds of information with third parties as described below,</p>
<p>1.	Companies and people who work for us: our contracts with other companies and individuals to help us provide services. For example, we may host some of our services on another company’s computers, hire technical consultants to maintain our Website and/or App, or work with companies to analyze data and provide customer service. In order to perform their jobs, these other companies may have limited access to some of the personal information that we maintain about our Users.<br>
2.	To contact you in relation to any correspondence we receive from you or any comment or complaint you make about our products or services.
3.	Please note that we will not disclose information about identifiable individuals to our advertising partners, but we may give them collective info about our user base. We may also use such info to help advertisers reach a more tailored audience.<br>
4.	If you do not wish us to use your data in any of the above stated ways, or to pass your personal details on to any third parties for marketing purposes, please tick the relevant box situated on the form on which we collect your data.<br>
5.	Furthermore, under the Act you have the right to request a copy of the personal information that we hold about you and to have any inaccuracies corrected. Any such request made to us may be subject to a fee of US$10 in order to cover our costs in providing you with details of the information that we hold about you.<br>
</p>
<p>MobiHealth may release your personal information to any member of our group, which means our subsidiaries, our ultimate holding company and its subsidiaries. We may disclose your personal information to third parties:</p>

<p>1.	In the event that we sell or buy any business or assets, in which case we may disclose your personal data to the prospective seller or buyer of such business or assets.<br>
2.	If MobiHealth or substantially all of its assets are acquired by a third party, in which case personal data held by it about its users will be one of the transferred assets.<br>
3.	If we are duty bound to divulge or share your personal data in order to meet with any legal requirements, or in order to administer or apply our Terms and Conditions (www.MobiHealthinternational.com/pages/terms) or agreements; or to safeguard the rights, property, or safety of MobiHealth, our User base, or others. This could include, but is not limited to, exchanging info with other businesses and organizations for the purposes of fraud protection.
</p>

<p>Except as described above, MobiHealth will not otherwise use or disclose any of your personally identifiable information, except to the extent reasonably necessary,</p>

<p>1.	To correct technical problems and malfunctions, to technically process your information and to determine the effectiveness of our projects.<br>
2.	To protect our rights and property and the rights and property of others.<br>
3.	To take precautions against liability.<br>
4.	To protect the security and integrity of our website. If you post or send offensive, inappropriate or objectionable content anywhere on or to our Website or otherwise engage in any disruptive behavior on any of our Services, we may use your personal info to prevent such actions. Where we reasonably believe that you are, or may be, in breach of any applicable laws (e.g. because content you have posted may be defamatory), we may use your personal information to inform relevant third parties such as your employer, school email/internet provider or law enforcement agencies about the content and your actions.<br>
5.	To the extent required by law or to respond to judicial process.<br>
6.	To the enforcement agencies or for an investigation on a matter related to public safety, as applicable.
</p>

<p>Cookies, log files, and pixel-tags (Web beacons) are technologies that could be used by the Site to identify a user as the user moves through the Site, or as the user clicks on a online advertisement located on a third party website. Your browser allows us to place some information (session based IDs and/or persistent cookies) on your computer’s hard drive that identifies the computer you are using. We may use to personalize our Site. Your Web browser can be set to allow you to control whether you will accept cookies, reject cookies, or to notify you each time a cookie is sent to you. If your browser is set to reject cookies, websites that are cookie-enabled will not recognize you when you return to the website, and some website functionality may be lost. On occasion, we contract with third parties to place cookies on your computer’s hard drive. Although cookies do not normally contain personally identifiable information, if you have provided us information about you, we may associate your registration information with cookies or other tracking utilities our Website places on your computer’s hard drive.</p>

<p>Associating a cookie with your registration data allows us to offer increased personalization and functionality. Without cookies, this functionality would not be possible. Some of our business partners may use cookies on our sites (for example, links to business partners). We do not want our business partners to use cookies to track our customers’ activities once they leave our sites. However, we may not have total control over how our business partners may use cookies on our Website.</p>

<p>Furthermore, we may use other tracking systems like pixel-tags. Pixel tags, sometimes called Web beacons, are similar in function to a cookie. But because of their insignificant size, it is not visible; though, they are used to pass certain information to our servers to personalize our Website and to track your usage across other MobiHealth websites and third party websites where MobiHealth online advertisements are located. In addition, we may also use pixel tags in our HTML based e-mails.</p>

<p>Our Website may, occasionally, contain links to and from the websites of our partner networks, advertisers and associates. If you follow a link to any of these websites, please note that these websites have their own privacy policies and that we do not accept any responsibility or liability for these policies.</p>

<p>By using this Website, our App and their contents and/or any our Services made available to you, you consent to our collection and use of your information as described above. We will continuously assess its practices to ensure that your privacy is respected.</p>

<p>We may amend this Privacy Policy from time to time. If we make any substantial changes in the way we use your personal information we will notify you by posting a prominent announcement on our Website.</p>

<p>If you any questions or comments about this privacy policy please contact: The Data Protection Officer Email: compliance@mobihealthinternational.com who will be happy to assist you.</p>
</div>
</div>
	@endsection
