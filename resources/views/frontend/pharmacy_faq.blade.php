@extends('frontend.layouts.app')
@section('content')

<div class="tm-breadcrumb">
		<div class="container">
			<h1 class="tm-section-heading">pharmacy</h1>
			<ul>
				<li>
					<a href="{{route('homepage')}}">home</a>
				</li>
				<li>
					<a href="{{route('pharmacy_faq')}}">/ Faq</a>
				</li>
			</ul>
		</div>
	</div><!-- /Breadcrumb header -->
	<!-- Shop -->
	<div class="tm-shop-list">

		
		<div class="container">
		

		<h1 class="tm-section-heading">faq</h1>
		
		<div class="how_it_work">

<div class="container">
			<p>Want to learn more about how to bring Mobihealth to your business? Search our FAQs for answers to common clinical, operational, and legal questions.
</p>
</div>


<div class="container">
 
  <div class="panel-group" id="accordion">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">How are my sure the prescriptions are from a genuine doctor?
</a>
        </h4>
      </div>
      <div id="collapse1" class="panel-collapse collapse in">
        <div class="panel-body">
<p>
        We operate within a high level of ethical and regulatory standards. All our doctors are properly checked and registered practitioners licensed to practice in Nigeria as well as UK, US, Canada, Australia, Dubai and other places.
</p>
<ul class="list_style_none">
        <li>They are reputable board certified and in good standing with their local regulatory bodies. 
</li>
        <li>We also have our in-house Pharmacists who make sure prescriptions are in order to minimize errors. 
</li>
        <li>Our e-prescribing tools are also very useful as they guide the doctors with formularies and local antibiotic policies. 
</li>
        <li>There must be video connection in order to prescribe; physicians cannot prescribe based on a phone or chat encounter.
</li>
        
      </ul>

      <p>In the interest of patient and public safety, Medications below are excluded from the formulary:</p>

<ul class="list_style_none">
        <li>Controlled substances (narcotics, anxiety medications, ADHD medications)
</li>
        <li>Muscle relaxants
</li>
        <li>Medications for erectile dysfunction
</li>
        <li>Any additional state specific controlled medications (additional pain medications, pseudoephedrine)
</li>
        
      </ul>

      </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">How Do you send prescription to my Pharmacy?
</a>
        </h4>
      </div>
      <div id="collapse2" class="panel-collapse collapse">
        <div class="panel-body">Our in-house Pharmacists and doctors send the prescriptions to local Pharmacies electronically and expect acknowledgement that order is being processed. Once completed and ready for pick-up, please notify us via dedicated email and we will either ask patient to pick it up or our courier will collect on behalf of patient. If you offer courier service then please indicate that.
</div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">Who pays for the prescription?
</a>
        </h4>
      </div>
      <div id="collapse3" class="panel-collapse collapse">
        <div class="panel-body">Mobihealth pays for the patient’s prescription. You won’t have to worry about patient’s affordability or reimbursement. Once we receive the invoice and confirmation that medicine has been dispensed we will deposit same amount to designated account immediately/weekly/monthly depending on preference. 
</div>
      </div>
    </div>

    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">What if we don’t have some or all the prescribed medication?
</a>
        </h4>
      </div>
      <div id="collapse4" class="panel-collapse collapse">
        <div class="panel-body">Please notify us immediately via dedicate email so we can source from other Pharmacies
You should let us know which ones are available as we may be able to do different prescriptions depending on what you have in stock.
</div>
      </div>
    </div>


 <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">Do you only prescribe branded products?
</a>
        </h4>
      </div>
      <div id="collapse5" class="panel-collapse collapse">
        <div class="panel-body">We aim to provide our customers with ONLY genuine medications from trusted manufacturers be it branded or generics. We will need to conduct a QA visit prior to accepting providers to ensure only genuine medications from reputable manufacturers are dispensed to our customers. Please see our list of approved manufacturers for branded and generics.</div>
      </div>
    </div>



  </div> 
</div>
    



		</div>

		
		</div><!-- /Shop -->
	</div>
	@endsection
