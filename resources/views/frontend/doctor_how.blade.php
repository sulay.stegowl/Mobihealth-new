@extends('frontend.layouts.app')
@section('content')

<div class="tm-breadcrumb">
		<div class="container">
			<h1 class="tm-section-heading">Doctor</h1>
			<ul>
				<li>
					<a href="{{route('homepage')}}">home</a>
				</li>
				<li>
					<a href="{{route('doctor_how')}}">/ How It Works</a>
				</li>
			</ul>
		</div>
	</div><!-- /Breadcrumb header -->
	<!-- Shop -->
	<div class="tm-shop-list">

		
		<div class="container">
		

	<h1 class="tm-section-heading">Consulting with Mobihealth through Telemedicine: How it works!</h1>
		
		<div class="how_it_work">
			<div class="title">Benefits we offer</div>
			<p>Our doctors are well looked after and treated with dignity and respect. The following are the additional benefit of working as a Mobihealth doctor:</p>
			<ul class="list_style_none">
				<li>Competitive hourly rate</li>
				<li>Work from the comfort of your home/office</li>
				<li>Earn extra income</li>
				<li>Full medical indemnity insurance covered for all Mobihealth work</li>
				<li>CPD-qualified training sessions</li>
				<li>Performer Awards for milestone consultation hours </li>
				<li>Collaboration with colleagues worldwide</li>
				<li>Knowledge sharing</li>
				<li>Career advancement</li>
				<li>You will be at the forefront of revolutionizing healthcare access and delivery across the the globe including the African continent.</li>
			</ul>

			

			<div class="title text-center">Step 1</div>

<div class="row">
	<div class="col-sm-6">
		<div class="title">Sign Up</div>
			<p>Our easy to use sign up and secure application enables you to create your unique online consulting profile. In addition to your name, area of specialty, and contact details, you will also be asked to provide following:</p>
			<img src="{{asset('assets/front/images/image1.png')}}" alt="">
	</div>
	<div class="col-sm-6">
		<div  class="title" >PHOTO </div>
			<p>Provide a high quality, professional photo for your public profile.</p>
			<img src="{{asset('assets/front/images/image2.png')}}" alt="">
	</div>
</div>
			
			<div class="row">
				<div class="col-sm-6">
						<div class="title">Profile Name e.g Dr John Smith</div>
						<p>you should also add your Specialty and grade as well as your current place of work</p>
			<img src="{{asset('assets/front/images/image4.png')}}" alt="">		
				</div>
				<div class="col-sm-6">
					<div class="title">EDUCATION & RESIDENCY</div>
					<p>This will be used to verify your credentials and it will appear in your profile.You will be required to upload your CV and name 2 work referees</p>
					<img src="{{asset('assets/front/images/image3.png')}}" alt="">
				</div>
			</div>
			
			
<div class="row">
	<div class="col-sm-6">
			<div class="title">IDENTITY VERIFICATION</div>
			<p>We will ask you to provide proof identity as part of the verification exercise before you can see patients.</p>
			<img src="{{asset('assets/front/images/image6.png')}}" alt="">
	</div>
	<div class="col-sm-6">
		<div class="title">Bank Details</div>
			<p>We ask for your bank details for direct deposit of your earnings. </p>
	</div>
</div>
			


		
			

			<div class="title  text-center"> Step 2 - Sign In</div>
			<p>Once your application is received and documentation verified we will provide you with login details to access our online on boarding modules that will provide you training on how to conduct a video consultation and also ensure you are familiar with our in-house policies. We offer video tutorial to familiarize you with our platform, and you can schedule a live test visit with a member of our team to walk through the process and ask questions.</p>
	

			<div class="title text-center">Step 3 - See Patients How does a visit work?</div>

			<p>A telemedicine visit isn’t so different from in-person. You’ll consult your patient through live video, Our streamlined EHR makes it easy for you to take medical note taking, prescribe, order laboratory/radiology tests and do referrals on a single platform. Patient’s PMHx, medication and allergy history etc will be available for you during a visitation. Our easy to use electronic scheduler makes it easy for you to schedule appointments and to start seeing patients! Apart from the realtime capabilities on smart phones that helps you assess patients, the FDA approved telehealth devices like www.tytocare.com will further strengthen remote comprehensive physical examination by patients which can be transmitted via secured cloud platform so you can listen to heart, chest sounds, see images of ear, throat and skin examinations, abdominal examination etc.  
</p>

			<p>If questions or technical issues come up, we’re here for you. You’ll have 24/7 access to our support team by phone and email.
</p>

<img src="{{asset('assets/front/images/image10.jpg')}}" alt="" style="height: 300px;width: auto">

			<div class="title text-center">Step 4 - Get Paid</div>
			<p>We’ll automatically deposit your earnings into your bank account on a weekly/monthly basis depending on your preference, thus ensuring regular cash flow. Since consultations are paid by Mobihealth you don’t need to worry about patient’s ability to pay, no insurance or any reimbursement hassles.</p>

	<p>It’s fast, secure and easy! Start seeing patients soon!</p>



	<div class="title text-center">Selection, Interview and Onboarding </div>
	<p>Our doctors are selected for their clinical expertise, their ability to buy into our vision and their understanding of how digital revolution can be optimized towards improving healthcare access globally. Our recruitment process involves simple application process whereby you submit your CV, once shortlisted and passed security checks, we will require your appraisal documents, DBS and references before you can access our portal. Following which you will be required to complete a digital interview at your convenience with modules covering structured consultation, Note taking, computing and communication skills. Thereafter, you will then be invited to our on-boarding process during which you will be required to complete two sessions of simulated video consultations. Once this last stage has been successfully completed you are ready to see your first patient!</p>


<div class="title text-center">How many subscribers does Mobihealth have?</div>
	<p>We are constantly recruiting subscribers so we are growing rapidly our target in the first year is 100 thousand people with estimated annual growth of 10%. Although we started off in Nigeria, we are also expanding into other African countries including Tanzania, Sierra Leone, Ghana and Ethiopia.</p>

	

	<div class="title text-center">Consultation outcomes</div>
	<p>We expect, based on market research, that Over 75% of our consultations will be closed by our doctors meaning they don’t require further medical attention. If a prescription is required, this can be issued and are able to refer to specialists and order diagnostic tests. We have our own sister Pharmacy where prescriptions can be sent or to other local pharmacies around the patient. If a patient’s symptom persists or returns within a 48hours window period, we will offer this consultation free from their annual plan.</p>

	
		<div class="title text-center">How does the prescription process work?</div>
	<p>Our prescription portal is specially designed so doctors are easily able to prescribe through choosing from a specifically designed formulary. The clinical support team and pharmacy lead manage the prescription process to ensure prescriptions are ready at the chosen pharmacy within an hour.</p>

		<div class="title text-center">Training and CPD Points</div>
	<p>Mobihealth supports training courses for doctors. Please email for more details.</p>
	

		</div>

		
		</div><!-- /Shop -->
	</div>
	@endsection
