	@extends('frontend.layouts.app')
@section('content')
	<div class="tm-home-slider">
		<div class="flexslider">
			<ul class="slides">
				<li>
					{{-- MOBIHEALTH…YOUR GATEWAY TO ACCESSING QUALITY HEALTHCARE SERVICES WORLDWIDE! --}}
					<div class="caption">
						<h3>MOBIHEALTH…YOUR GATEWAY TO ACCESSING QUALITY</h3> 
						<h1> HEALTHCARE SERVICES  <span> WORLDWIDE! </span></h1>
						<p>Request video consultation   now</p><a class="tm-btn btn-blue" href="{{route('services')}}">our services</a>
					</div><img alt="img" src="{{asset('/assets/front/extra-images/IMG-20171009-WA0002.jpg')}}">
				</li>
				<li>
					<div class="caption">
					<h3>Your Gateway to Quality</h3> 
						<h1> Healthcare Services  <span> Worldwide </span></h1>
						<p>Request video consultation  now</p><a class="tm-btn btn-blue" href="{{route('services')}}">our services</a>
					</div><img alt="img" src="{{asset('/assets/front/extra-images/IMG_2146.jpg')}}">
				</li>
				<li>
					<div class="caption">
						<h3>Your Gateway to Quality</h3> 
						<h1> Healthcare Services  <span> Worldwide </span></h1>
						<p>Request video consultation now</p><a class="tm-btn btn-blue" href="{{route('services')}}">our services</a>
					</div><img alt="img" src="{{asset('/assets/front/extra-images/IMG_2101.jpg')}}">
				</li>
			</ul>
		</div>
	</div><!-- /Slider -->
	<!-- Appointment -->
	<div class="tm-appointment-large">
		<div class="container">
			<h1 class="tm-section-heading">Search For Services</h1>
			<div class="row">

				<div class="col-sm-4">
					<div class="form-group">
						<div class="tm-timepicker">
							<!-- <select class="form-control" name="phone">
								<option> Services </option>
								<option> Services </option>
								<option> Services </option>
								<option> Services </option>
							</select> -->
							<input class="form-control" name="full_name" placeholder="Services" type="text">
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="form-group">
						<input class="form-control" name="full_name" placeholder="City Name" type="text">
					</div>
				</div>
				
				
				<div class="col-sm-4">
					<div class="form-group">

						<button class="tm-btn btn-dark" type="button" data-toggle="modal" data-target="#myModal" >SEARCH</button>
					</div>
				</div>
			</div>
		</div>
	</div><!-- /Appointment -->
	<!-- Welcome Section -->
	<div class="tm-welcome">
		<div class="container">
			<div class="row">
				<div class="col-sm-7">
					<h2 class="tm-box-heading">Welcome to <span>Mobihealth</span></h2>

					<div>
						{!!$data2->text!!}
					</div>


	<p class="what_we_offer mt-20">Healthcare challenges in Africa</p>
		
				<p>There are many barriers limiting access to quality healthcare services in developing countries these includes;</p>

				<p class="what_we_offer">Cost</p>
				<ul class="list_style_none">
					<li>More than 90% of people in Africa do not have any form of health insurance.</li>
					<li>Majority cannot afford out-of-pocket payment; many are forced to sell asset or go into debt to pay healthcare cost.</li>
					<li>High cost of medicines and counterfeit products.</li>
					<li>Nigerians spend $1bn annually on medical tourism.</li>
				</ul>

					<p class="what_we_offer ">Poor Infrastructure & travel distance to health facilities</p>
				<ul class="list_style_none">
					<li>Many rural areas lack healthcare facilities and many have to travel long distances only to find a poorly equipped one.</li>
				</ul>


<div class="hidden-xs">
					<p class="what_we_offer">Shortage of doctors and other health provider workforce</p>
				<ul class="list_style_none">
					<li>Africa bears 1/4 of world disease burden but has only 2% of world doctors. </li>
					<li>This is worsened by brain drain with many doctors leaving for western countries <br> to seek greener pastures.</li>
					<li>Nigerians spend $1bn on Medical tourism per annum.</li>
				</ul>
</div>


<div class="hidden-sm hidden-md hidden-lg">
	<p class="what_we_offer">Shortage of doctors and other health provider workforce</p>
				<ul class="list_style_none">
					<li>Africa bears 1/4 of world disease burden but has only 2% of world doctors. </li>
					<li>This is worsened by brain drain with many doctors leaving for western countries <br> to seek greener pastures.</li>
					<li>Nigerians spend $1bn on Medical tourism per annum.</li>
				</ul>
			</div>

				</div><!-- Opening Hours -->
				<div class="col-sm-5">

					


		<img src="{{url('/').'/storage/'.$data2->image_1}}" alt="" class="img-responsive">
		<img src="{{url('/').'/storage/'.$data2->image_2}}" alt="" class="img-responsive" style="margin-top: 25px;"> 



					<p class="what_we_offer mt-95">What we offer?</p>

				<ul class="list_style_none">
					<li>Timely access to >100K Doctors worldwide.</li>
					<li>Immediate video consultation via mobile/laptop 24/7.</li>
					<li>Timely diagnosis and treatment from the comfort of your home 24/7.</li>
					<li>Genuine medications and access to pharmacies around you.</li>
					<li>Access to diagnostic centres and pathology laboratories around you.</li>
					<li>Referrals to specialists locally and worldwide.</li>
					<li>Online booking of pathology, radiology tests.</li>
					<li>Online ordering of medications.</li>
					<li>Messaging service for appointment bookings & confirmation.</li>
					<li>Home delivery services.</li>
					<li>Home Health visits.</li>
					<li>No out-of-Pocket payment.</li>
					<li>Same Day repeat prescriptions.</li>
					</ul>


				<p class="what_we_offer text-center"><b class="theme_color ">Our niche</b></p>
<p class="theme_color">One-stop portal connecting people in developing countries to medical experts globally for immediate video consultation, immediate prescription and access to thousands of hospitals, diagnostic centres and pharmacies around them from their mobile phone through a subscription plan that requires no out of pocket payment. This is a game changer!</p>




				

			



				</div><!-- /Opening Hours -->
			</div>

				


		</div>
	</div><!-- /Welcome Section -->


	<div class="tm-video-wrapper">
		<div class="col-sm-6">
			<div class="content-box">
				<ul style="margin-left: -40px;">
					{{-- <li>
						<h2 class="tm-box-heading"><i aria-hidden="true" class="fa fa-check-circle"></i>  Paid subscribers get these services for free</h2>
						
					</li> --}}
					<li>
						<h2 class="tm-box-heading"><i aria-hidden="true" class="fa fa-check-circle"></i>  Free Access to Certified Doctors worldwide 24/7</h2>
						
					</li>
					<li>
						<h2 class="tm-box-heading"><i aria-hidden="true" class="fa fa-check-circle"></i>Free Diagnostic Investigations</h2>
						
					</li>
					<li>
						<h2 class="tm-box-heading"><i aria-hidden="true" class="fa fa-check-circle"></i> Free Prescription & Genuine Medications</h2>
						
					</li>
				</ul>
			</div>
		</div>
		<div class="col-sm-6 nopadding">
			<img alt="video" src="{{asset('/assets/front/extra-images/DrJ_Smith_new.jpg')}}" class="img-responsive">
		</div>
	</div><!-- /Video Section -->

	<div class="tm-video-wrapper">
		<div class="col-sm-6 nopadding">
			<img alt="video" src="{{asset('/assets/front/extra-images/health-mobile-phone.jpg')}}" class="img-responsive">
		</div>
		<div class="col-sm-6">
			<div class="content-box">
				<ul style="margin-left: -40px;">
					<li>
						<h2 class="tm-box-heading"><i aria-hidden="true" class="fa fa-check-circle"></i> Order same day repeat prescription</h2>
						
					</li>
					<li>
						<h2 class="tm-box-heading"><i aria-hidden="true" class="fa fa-check-circle"></i>  Free Referrals and Appointment Booking</h2>
						
					</li>
					<li>
						<h2 class="tm-box-heading text-lowercase"><i aria-hidden="true" class="fa fa-check-circle"></i> ONLINE BOOKINGS WITH HOSPITALS, PHARMACIES, LABORATORIES NEAR YOU</h2>
						
					</li>
				</ul>
			</div>
		</div>
		
	</div><!-- /Video Section -->

	<!-- Services Section -->
	<div class="tm-home-services">
		<div class="container">
			<h1 class="tm-section-heading">our product and services</h1>
			<div class="row">
				<div class="col-sm-6">
					<div class="service-box">
						<div class="service-image">
							<span class="fa fa-user-md content_center"></span>
						</div>
						<div class="service-caption">
							<h2 class="tm-box-heading"><a href="{{route('service_detail_4')}}">Instant Video Consultation</a></h2>
							<p>Immediate Video consultation with a doctor via your mobile device. This eliminates long waits for doctor appointments, delayed diagnosis and suffering. Get diagnosed safely from the comfort of your home, anywhere &  anytime</p>
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="service-box">
						<div class="service-image">
							<span class="fa fa-hospital-o content_center"></span>
						</div>
						<div class="service-caption">
							<h2 class="tm-box-heading"><a href="{{route('service_detail_1')}}">Laboratory and Radiologic Investigations</a></h2>
							<p>We book your diagnostic tests with reputable centers and our doctors evaluate the results remotely to give you medical advice and prescriptions. You can also request home laboratory test depending on your subscription plan.</p>
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="service-box">
						<div class="service-image">
							<span class="fa fa-h-square content_center"></span>
						</div>
						<div class="service-caption">
							<h2 class="tm-box-heading"><a href="{{route('service_detail_5')}}">Prescription and Genuine medications</a></h2>
							<p>You no longer need to worry about where to get genuine medication. We send your prescriptions to pharmacies near you and text you when they are ready for pick up. We have partnered with highly reputable pharmacies & manufacturers of quality branded products from the UK, USA and other countries.</p>
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="service-box">
						<div class="service-image">
							<span class="fa fa-hospital-o content_center"></span>
						</div>
						<div class="service-caption">
							<h2 class="tm-box-heading"><a href="{{route('service_detail_2')}}">Home Health Services</a></h2>
							<p>Request a nurse, physiotherapist visit or laboratory tests from anywhere. Our trained nurses and other healthcare professionals work within a framework of high ethical standards. We also offer home delivery of medications and repeat prescriptions for your convenience.</p>
						</div>
					</div>
				</div>
				<div class="text-center">
					<a class="tm-btn btn-blue" href="{{route('services')}}">see all services</a>
				</div>
			</div>
		</div>
	</div><!-- /Services Section -->

	


	<!-- Project Facts -->
	<div class="tm-project-facts">
		<div class="container">
			<div class="row">
				<h1 class="tm-section-heading">why we are successful</h1>
				<div class="col-sm-3">
					<div class="fact-box">
						<div class="fact-icon">
							<span class="fa fa-user-md content_center"></span>
						</div>
						<div class="fact-label">
							<span class="count_num">100</span>K+ Doctors
						</div>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="fact-box">
						<div class="fact-icon">
							<span class="fa fa-hospital-o content_center"></span>
						</div>
						<div class="fact-label">
							<span class="count_num">1000</span>+ Pharmacy
						</div>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="fact-box">
						<div class="fact-icon">
							<span class="fa fa-h-square content_center"></span>
						</div>
						<div class="fact-label">
							<span class="count_num">300</span>+ Hospitals
						</div>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="fact-box">
						<div class="fact-icon">
							<span class="fa fa-hospital-o content_center"></span>
						</div>
						<div class="fact-label">
							<span class="count_num">1000</span>+ Laboratories
						</div>
					</div>
				</div>
			</div>
		</div>
	</div><!-- /Project Facts -->


<div class="tm-welcome">
		<div class="container">
			<h1 class="text-center tm-section-heading">{{$data->title}} </h1>
			<div class="row">
			
				<div class="col-sm-5">
					{{-- <img src="{{asset('/assets/front/extra-images/IMG-20171007_WA0002.jpg')}}" class="img-responsive width_100" alt=""> --}}
					<img src="{{url('/').'/storage/'.$data->image}}" class="img-responsive width_100" alt="">

				</div><!-- /Opening Hours -->
					<div class="col-sm-7">
					
<div>
	{!!$data->text!!}
</div>


				</div>
			</div>
		</div>
	</div>



	<div class="tm-project-facts our_mission">
		<div class="container">
			<div class="row">
				<h1 class="tm-section-heading">OUR MISSION</h1>
				<p class="text">Our mission is to provide people in developing countries timely access to quality healthcare services from around the world when they need it in the most cost effective way!
</p>
				
				<a class="tm-btn btn-blue about" href="{{route('about')}}">About Us</a>
			
			</div>
		</div>
	</div>

	<!-- Pricing Table -->
	<div class="tm-pricing-home">
		<div class="container">
			<div class="row">
				<h1 class="tm-section-heading">subscription Plans</h1>
				<div class="row text-center" style="padding-bottom: 10px;">
					<h4>Paid subscribers get these services for free</h4>
					</div>
					<div class="pricing_group">
						
		
<div class="subcriptions_plans1">
<div class="container">
 
   <div class="table-responsive">          
  <table class="table">
    <thead>
      <tr>
        <th>FEATURES</th>
        <th>SOLO</th>
        <th>BRONZE</th>
        <th>SILVER</th>
        <th>GOLD</th>
        <th>PAYG</th>
      </tr>
    </thead>
    <tbody>
      {{-- <tr>
        <td>&nbsp;</td>
        <td>N6000 / Month</td>
        <td>N11,000 / Month</td>
        <td>N20,000 / Month</td>
        <td>N30,000 / Month</td>
        <td>N8000 / Consultation</td>
        
      </tr>
 --}}
      <tr>
        <td>No. of persons covered</td>
        <td>1</td>
        <td>4</td>
        <td>4</td>
        <td>4</td>
        <td>1</td>
      </tr>

     <tr>
        <td>No. of video consultation per household per annum</td>
        <td>4</td>
        <td>8</td>
        <td>10</td>
        <td>14</td>
        <td>N/A</td>
      </tr>

       <tr>
        <td>Free access to database of ~100k doctors & healthcare professionals world wide</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
      </tr>

        <tr>
        <td>Free access to reputable pharmacies, diagnostic centers & hospitals locally</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
      </tr>

       <tr>
        <td>Free diagnsotic work-up</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>N/A</td>
      </tr>

        <tr>
        <td>Free hospital treatment</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>N/A</td>
      </tr>

        <tr>
        <td>Free prescription</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>N/A</td>
      </tr>

        <tr>
        <td>Free genuine medications</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>N/A</td>
      </tr>

       <tr>
        <td>Free acute dental atreatment</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>N/A</td>
      </tr>

       <tr>
        <td>Medical bills paid</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>N/A</td>
      </tr>

       <tr>
        <td>Free home laboratory service </td>
        <td>no</td>
        <td>yes * 4</td>
        <td>yes * 8</td>
        <td>yes * 10</td>
        <td>N/A</td>
      </tr>

       <tr>
        <td>Free home delivery of medication</td>
        <td>yes * 2</td>
        <td>yes * 3</td>
        <td>yes * 6</td>
        <td>yes * 6</td>
        <td>N/A</td>
      </tr>

      <tr>
        <td>Free Physiotherapy service</td>
        <td>yes * 2</td>
        <td>yes * 4</td>
        <td>yes * 6</td>
        <td>yes * 10</td>
        <td>N/A</td>
      </tr>


      <tr>
        <td>Free home nursing service</td>
        <td>yes * 2</td>
        <td>yes * 4</td>
        <td>yes * 6</td>
        <td>yes * 10</td>
        <td>N/A</td>
      </tr>

      <tr>
      	<td>Personalised health advise</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>N/A</td>
      </tr>

 <tr>
      	<td>Home Visit by Doctor</td>
        <td>no</td>
        <td>no</td>
        <td>no</td>
        <td>yes * 2</td>
        <td>N/A</td>
      </tr>

        <tr>
        <td>&nbsp;</td>
        <td><a class="tm-btn btn-blue" href="{{route('subscription')}}" >subscribe now</a></td>
        <td><a class="tm-btn btn-blue" href="{{route('subscription')}}" >subscribe now</a></td>
        <td><a class="tm-btn btn-blue" href="{{route('subscription')}}" >subscribe now</a></td>
        <td><a class="tm-btn btn-blue" href="{{route('subscription')}}" >subscribe now</a></td>
        <td><a class="tm-btn btn-blue" href="{{route('subscription')}}" >subscribe now</a></td>
      </tr>


    </tbody>
  </table>
  </div>
</div>
</div>


					
			</div>
		</div>
	</div><!-- /Pricing Table -->

</div>
	
	
	<!-- Testimonials -->
	{{-- <div class="tm-testimonials-home">
		<div class="container">
			<div class="row">
				<h1 class="tm-section-heading">what our clients say</h1>
				<div class="owl-carousel">
					<div class="testimonial">
						<div class="client-photo"><img alt="Client" src="{{asset('/assets/front/extra-images/testimonial-1.png')}}"></div>
						<div class="client-message">
							<h4>Selena robertson</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
						</div>
					</div>
					<div class="testimonial">
						<div class="client-photo"><img alt="Client" src="{{asset('/assets/front/extra-images/testimonial-2.png')}}"></div>
						<div class="client-message">
							<h4>Enna Wilson</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
						</div>
					</div>
					<div class="testimonial">
						<div class="client-photo"><img alt="Client" src="{{asset('/assets/front/extra-images/testimonial-3.png')}}"></div>
						<div class="client-message">
							<h4>Hanna Stephen</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
						</div>
					</div>
					<div class="testimonial">
						<div class="client-photo"><img alt="Client" src="{{asset('/assets/front/extra-images/testimonial-4.png')}}"></div>
						<div class="client-message">
							<h4>Maria Stuart</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
						</div>
					</div>
					<div class="testimonial">
						<div class="client-photo"><img alt="Client" src="{{asset('/assets/front/extra-images/testimonial-1.png')}}"></div>
						<div class="client-message">
							<h4>Selena robertson</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
						</div>
					</div>
					<div class="testimonial">
						<div class="client-photo"><img alt="Client" src="{{asset('/assets/front/extra-images/testimonial-1.png')}}"></div>
						<div class="client-message">
							<h4>Enna Wilson</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
						</div>
					</div>
					<div class="testimonial">
						<div class="client-photo"><img alt="Client" src="{{asset('/assets/front/extra-images/testimonial-3.png')}}"></div>
						<div class="client-message">
							<h4>Hanna Stephen</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
						</div>
					</div>
					<div class="testimonial">
						<div class="client-photo"><img alt="Client" src="{{asset('/assets/front/extra-images/testimonial-4.png')}}"></div>
						<div class="client-message">
							<h4>Maria Stuart</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>  --}}<!-- /Testimonials -->


		<div class="tm-project-facts our_mission">
		<div class="container">
			<div class="row">
				<h1 class="tm-section-heading">Our  Values</h1>
				<p class="text">Our core values are empathy, respect, integrity, excellence and innovation! We are committed to giving our best at all times to patients, partners, employees and operate with transparency and dignity. We have respect for our environment and operate in a manner that will leave a better planet for future generation!
</p>		
			</div>
		</div>
	</div>


<div class="tm-news-home" id="news">
		<div class="container">
			<div class="row">
				<h1 class="tm-section-heading">LATEST  news</h1>

						<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 smf">
					<div class="news">
						<div class="news-thumbnail">
						<div class="post-date">
							02<br>Oct
						</div><img alt="dentist" src="{{asset('/assets/front/images/blog4.png')}}"></div>
						<div class="news-title">
							<a href="http://www.our-africa.org/health" target="_blank" class="text-uppercase">LTFT doctors free to do locum work</a>
						</div>
						<div class="news-meta">
						
						</div>
						<div class="separator"></div>
						{{-- <p>The amount of time patients spend waiting in your office may seem like a small factor when it comes to patient satisfaction. </p> --}}
					</div>
				</div>
				
				<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 smf">
					<div class="news">
						<div class="news-thumbnail">
						<div class="post-date">
							02<br>
							Oct
						</div><img class="blog_2_image" alt="dentist" src="{{asset('/assets/front/images/blog1.png')}}"></div>
						<div class="news-title">
							<a href="http://blog.evisit.com/telemedicine-affect-malpractice-insurance" target="_blank">HOW DOES TELEMEDICINE AFFECT MALPRACTICE INSURANCE?</a>
						</div>
						<div class="news-meta">
							{{-- <ul>
								<li><span class="icon-user"></span> by admin</li>
							</ul> --}}
						</div>
						<div class="separator"></div>
						{{-- <p>As physicians, we don’t normally like to think about malpractice insurance or any scenario where we’d actually need it. </p> --}}
					</div>
				</div>
				<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 smf">
					<div class="news">
						<div class="news-thumbnail">
						<div class="post-date">
							02<br>
							Oct
						</div><img  alt="dentist" src="{{asset('/assets/front/images/blog2.png')}}"></div>
						<div class="news-title">
							<a href="http://www.afro.who.int/sites/default/files/2017-06/nigeria_medicine_prices.pdf" target="_blank" class="text-uppercase">Medicine Prices In Nigeria</a>
						</div>
						<div class="news-meta">
							{{-- <ul>
								<li><span class="icon-user"></span> by admin</li>
							</ul> --}}
						</div>
						<div class="separator"></div>
					{{-- 	<p>Telemedicine is going to be a critical capability for practices large and small moving forward. Patients are looking for more convenience, ...</p> --}}
					</div>
				</div>
				<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 smf">
					<div class="news">
						<div class="news-thumbnail">
						<div class="post-date">
							02<br>Oct
						</div><img alt="dentist" src="{{asset('/assets/front/images/blog3.png')}}"></div>
						<div class="news-title">
							<a href="http://www.our-africa.org/health" target="_blank" class="text-uppercase">our africa health </a>
						</div>
						<div class="news-meta">
							{{-- <ul>
								<li><span class="icon-user"></span> by admin</li>
							</ul> --}}
						</div>
						<div class="separator"></div>
					{{-- 	<p>The amount of time patients spend waiting in your office may seem like a small factor when it comes to patient satisfaction. </p> --}}
					</div>
				</div>
				
		

			</div>
		</div>
	</div>

	<!-- Smartphone App -->
	<div class="tm-app-download">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="app-description text-center">
						<h2>NOW IS available FOR YOUR SMARTPHONES</h2>
						<p>Our mission is to provide people in developing countries timely access to quality healthcare services from around the world when they need it in the most cost effective way! Download Now </p>
						<div class="app_descri_link">
						<a href="javascript:void(0)" data-toggle="modal" data-target="#myModal" ><img alt="app" src="{{asset('/assets/front/images/appstore.png')}}"></a>
						<a href="javascript:void(0)" data-toggle="modal" data-target="#myModal"  ><img alt="app" src="{{asset('/assets/front/images/appstore-2.png')}}"></a>
						</div>
					</div>
				</div>
			
			</div>
		</div>
	</div>
	

	@endsection