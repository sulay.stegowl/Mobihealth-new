@extends('frontend.layouts.app')
@section('content')

<div class="tm-breadcrumb">
		<div class="container">
			<h1 class="tm-section-heading">pharmacy</h1>
			<ul>
				<li>
					<a href="{{route('homepage')}}">home</a>
				</li>
				<li>
					<a href="{{route('pharmacy_how')}}">/ How It Works</a>
				</li>
			</ul>
		</div>
	</div><!-- /Breadcrumb header -->
	<!-- Shop -->
	<div class="tm-shop-list">

		
		<div class="container">
		

		<h1 class="tm-section-heading">How It Works</h1>
		
		<div class="how_it_work">
			<div class="title">Benefits</div>
			<ul class="list_style_none">
				<li>Streamline Online Booking system</li>
				<li>Approved E-prescribing tool</li>
				<li>Increase sales and revenue</li>
				<li>Improve visibility for your business</li>
				<li>Regular and reliable source of revenue</li>
				<li>Access to wide base of medical experts and colleagues from around the world</li>
				<li>Efficient management of resources</li>
				<li>Reduce running cost</li>
			</ul>

			<div class="title">Step 1</div>

<div class="row">
	<div class="col-sm-6">
		<div class="title">Sign Up</div>
			<p>Our easy Sign up and application process enables you to create your unique online business profile. You will also be asked to provide following:</p>	
			<img src="{{asset('assets/front/images/image1.png')}}" alt="">
	</div>
	<div class="col-sm-6">
		<div  class="title" >NAME of PHARMACY, WEBSITE AND MAIN CONTACT PERSON’S NAME, EMAIL & PHONE </div>
		<img src="{{asset('assets/front/images/image2.png')}}" alt="">
	</div>
</div>
			
			<div class="row">
				<div class="col-sm-6">
						<div class="title">PHOTO</div>
			<p>A high quality, professional photo for your business to appear on your public profile.</p>
			<img src="{{asset('assets/front/images/image4.png')}}" alt="">		
				</div>
				<div class="col-sm-6">
			<div class="title">COMPANY REGISTRATION AND NAFDAC/PCN No.</div>

			<p>We’ll use this to verify your credentials, and it will appear in your profile.</p>

			<img src="{{asset('assets/front/images/image3.png')}}" alt="">		
				</div>
			</div>
			
			<div class="row">
				<div class="col-sm-6">
			<div class="title">IDENTITY VERIFICATION </div>

			<p>We’ll ask for personal information of principal owner and Pharmacy director to ensure regulatory compliance</p>

			<img src="{{asset('assets/front/images/image5.png')}}" alt="">					
				</div>

				<div class="col-sm-6">

			<div class="title">LINK TO YOUR INVENTORY AND PRICING LIST or provide us with CSV files of inventory that can be upload</div>

			<img src="{{asset('assets/front/images/image6.png')}}" alt="">

				</div>

			</div>
			

<div class="row">
	<div class="col-sm-6">
			<div class="title">BANK DETAILS</div>

			<p>Provide your business bank details for direct deposit of your invoices</p>
	</div>
</div>
			

			



		

			<div class="title  text-center">Step 2 - Sign In</div>
			<p>Once we receive above information and have completed necessary checks your business profile will be enabled and you will receive login credentials from our admin. You will be able to access our platform from your computer or iOS mobile device.</p>
			<p>We offer video tutorials to get you accustomed with our platform, we also offer a live demo visit with a member of our team to walk through the process and ask your questions.
</p>

			<div class="title text-center">Step 3 – INTEGRATE INTO OUR SECURE ONLINE PLATFORM </div>

			<p>Through our secure electronic prescribing systems our doctors are able to look through formularies and prescribe medications which are checked by our in-house Pharmacist conversant to ensure compliance with local regulatory requirements and as additional oversight before they are sent to you for dispensing. We expect you to provide confirmation time when prescription will be ready for pick up by patient or our courier this is then communicated to patients. We expect you to provide us with your up-to-date inventories with pricing so we can search if you have stock of particular medication before sending prescription over. Our aim is to avoid unnecessary delays and to ensure our patients get only genuine approved medications.  </p>

			<p>If questions or technical issues come up, we’re here for you. You’ll have 24/7 access to our support team by phone and email.</p>

			<img src="{{asset('assets/front/images/image6.jpg')}}" alt="">

			<div class="title text-center">Step 4 - Get Paid</div>
			<p>Once we receive confirmation that medication is ready and invoices are received, we deposit same amount automatically into your designated bank account immediately or on a weekly/monthly basis depending on preference ensuring regular cash flow for your business. Since the costs of medicines are paid by Mobihealth you don’t need to worry about patient’s ability to pay, no insurance or any reimbursement hassles. Dispense and get paid!</p>




		</div>

		
		</div><!-- /Shop -->
	</div>
	@endsection
