@extends('frontend.layouts.app')
@section('content')
	
	<div class="tm-breadcrumb">
		<div class="container">
			<h1 class="tm-section-heading">Pharmacy Detail</h1>
			<ul>
				<li>
					<a href="{{route('homepage')}}">home</a>
				</li>
				<li>
					<a href="{{route('pharmacy_details',['slug' => $data->slug])}}">/ {{$data->title}} </a>
				</li>
			</ul>
		</div>
	</div><!-- /Breadcrumb header -->
	<!-- Shop Detail Section -->
	<section class="tm-shop-detail">
		<div class="container">
			<div class="row">
				<!-- Product information Wrapper -->
				<div class="tm_product_info">
					<!-- Product Image -->
					<div class="col-sm-4">
						<div class="product_image"><img alt="" src="{{asset('/storage/services/'.$data->image)}}"></div>
					</div><!-- /Product Image -->
					<!-- Information -->
					<div class="col-sm-8">
						<div class="product_details">
							<h4>{{$data->title}}</h4>
							
							
							<p>{{$data->desc}}</p>

								<div class="Pharmacy_text_2">
								<b>Address : <span>{{$data->address}} </span> </b>
							</div> <br>
							<div class="Pharmacy_text_2">
								<b>Contact Us : <span>{{$data->phone}}  </span> </b>
							</div> <br>
							<div class="Pharmacy_text_2">
								<b>Email : <span>{{$data->email}} </span> </b>
							</div>

							{{-- <div class="Pharmacy_text_2">
								<b>Address : <span>  L 105 Lorem Ipsum </span> </b>
							</div>
							<div class="Pharmacy_text_2">
								<b>Contact Us : <span>9966557711 </span> </b>
							</div> --}}
						</div>
					</div><!-- /Information -->
				</div><!-- /Product information Wrapper -->
				<!-- Details and Reviews -->
				<div class="col-sm-12">
					<div class="tm_product_reviews_details">
						<ul class="nav nav-tabs">
							<li class="active">
								<a data-toggle="tab" href="#details">Details</a>
							</li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane fade in active" id="details">
								<h3> PHARMACY  DETAILS</h3>
								<p>{{$data->desc}}</p>

								
								<br><br><br><br>
							</div><!-- Reviews -->
						
						</div>
					</div>
				</div><!-- /Details and Reviews -->
			</div>
		</div>
	</section><!-- /Shop Detail Section -->

	
	@endsection