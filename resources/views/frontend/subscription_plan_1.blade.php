@extends('frontend.layouts.app')
@section('content')

<div class="tm-breadcrumb">
		<div class="container">
			<h1 class="tm-section-heading">subscriptions</h1>
			<ul>
				<li>
					<a href="{{route('homepage')}}">home</a>
				</li>
				<li>
					<a href="#">/ Individuals-Families</a>
				</li>
			</ul>
		</div>
	</div><!-- /Breadcrumb header -->
	<!-- Shop -->
  <div class="container">
  <div class="note" style="margin-bottom: 25px;">
  <p> We have subscription bundles to fit everyone, individuals and families. For Corporations, NGOs and insurance companies interested in volume subscription. Send enquiries to <strong>enquiries@mobihealthinternational.com</strong></p>

</div>
</div>
	@endsection
