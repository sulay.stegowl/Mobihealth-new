@extends('frontend.layouts.app')
@section('content')

<div class="tm-breadcrumb">
		<div class="container">
			<h1 class="tm-section-heading">Terms & Conditions</h1>
			<ul>
				<li>
					<a href="{{route('homepage')}}">home</a>
				</li>
				<li>
					<a href="#">/ Terms & Conditions</a>
				</li>
			</ul>
		</div>
	</div><!-- /Breadcrumb header -->
	<!-- Shop -->
  <div class="container">
  <div  style="padding-top: 40px; text-align: left;">
  	<h3 style="padding-bottom: 20px">Terms & Conditions</h3>
  <p>MobiHealth, located at www.mobihealthinternational.com is site operated by MobiHealth Ltd. hereinafter “MobiHealth”, “we” or “us”). We are registered in multiple countries including Nigeria under company number [number]. We are a limited liability company.</p>

  <p>We operate a Platform and an online community that is designed to form a network for doctors and other healthcare professionals and medical service providers to engage, share and learn with view to improving overall healthcare and well being of people in developing countries by offering video consultations remotely and also connecting them to healthcare services within their locality and beyond. The Services offered by MobiHealth include our app (the “App”), the Mobihealthinternational.com website (the "Website"), the MobiHealth Instant Video/SMS Messaging ("MHIVSM"), the MobiHealth E-mail ("MHE"), and any other features, content, or applications offered from time to time by us and our partners.</p>

<h3 style="padding-bottom: 20px;">To Know:</h3>
  <p>MobiHealth requires all Members to behave in a highly professional and polite manner at all times. Our Website and App provide an inclusive global network for consultation, training, discussion, case sharing, education and a range of related activities in various branches of medicine, and related fields.</p>

  <p>Our Website is a members' network with healthcare services and treatment facilities. Our Members can search for doctors and other professionals, pharmacies, laboratories, hospitals, nursing centers etc and other institutions and communicate with other Members using our online application and similar other web-based platform/services. Our Members are able to create a profile for display on the Website that includes personal information, photos, and descriptions determined and edited by the Member. Our Members can edit their profile, hide their profile, and unsubscribe from the service at any time.</p>

  <h3 style="padding-bottom: 20px;">Terms of Use:</h3>
  <p>These terms of use for Users ("Terms") serve as our terms of service that seek to manage our relationship with Users and others who interact with MobiHealthcare Limited, as well as MobiHealth brands, products and services such our App, our Website, online application and similar other web-based platform (“Services”). By using or accessing our Services, you agree to these legally binding Terms.</p>

  <p>This page (together with the documents referred to on it) explains the general terms on which you may make use of our Services, whether as a Visitor or as a Member. Users are only authorized to use the Services (regardless of whether access or use is intended) if they agree to abide by all applicable laws in the country of access, and the laws of relevant jurisdiction, and to these Terms. Please read these Terms carefully before you begin your MobiHealth experience. If you do not agree with all its contents, you should leave the Website and/or App and discontinue use of the Services immediately.</p>

  <p>These Terms include MobiHealth’s policy for acceptable use of the Services and content posted on the Website, Users’ rights, obligations and restrictions regarding your use of the Services and MobiHealth's Privacy Policy as located at [hyperlink]. In order to participate in certain Services, you may be notified that you are required to download software or content and/or agree to additional terms and conditions. Unless otherwise provided by the additional terms and conditions applicable to the services in which you choose to participate, those additional terms are hereby incorporated into these Terms.</p>

  <h3 style="padding-bottom: 20px;">Privacy Policy:</h3>
  <p>MobiHealth is committed to protecting your personal information, as well as patient information whilst using our Services; we strive to create safe and pleasant settings for everyone. Our Privacy Policy guides your use of our Services and we encourage you to read this document in order to make informed decisions about using our Services; this can be found at <a href="{{route('privacy_policy')}}}">Privacy Policy</a></p>

  <h2 style="padding-bottom: 40px;">Membership Eligibility and Information:</h2>
  <h3 style="padding-bottom: 20px;">Registering as a Member:</h3>
  <p>If you wish to register as a Member and communicate with other Members and make use of the Services, you must read these Terms and indicate your acceptance before completing the Registration process. Please note that MobiHealth membership, whether by subscription or provided free of charge by MobiHealth, our partners, agents or other parties is for the use of doctors, medical consultants and registered healthcare professionals and providers directly/indirectly involved in patient care, and for research investigators employed by hospitals, clinics, universities and charitable research organizations.</p>

<h3 style="padding-bottom: 20px;">Available Memberships:</h3>
  <p>Individuals and organizations including but not restricted to the following groups may only register for membership with the prior written consent of MobiHealth: medical industry companies, their employees and agents, including but not restricted to companies involved in the manufacture of pharmaceuticals, devices, equipment and the provision of such industry services; print, broadcast and online journalists; employees of print, broadcast or online media providers or publishers. </p>

  <h3 style="padding-bottom: 20px;">Individual Membership:</h3>
  <p>Individual Membership is for patients, doctors and registered healthcare professionals. Individual membership may also be provided by MobiHealth for medical industry companies, their employees and agents, including but not restricted to companies involved in the manufacture of pharmaceuticals, devices, equipment and the provision of such industry services; print, broadcast and online journalists; employees of print, broadcast or online media providers or publishers. </p>

  <h3 style="padding-bottom: 20px;">Institutional Membership, Commercial Organizations and Corporations:</h3>
  <p>In addition to Individual Memberships, institutions and societies may subject to prior authorization by MobiHealth also become members of MobiHealth and will enjoy the same benefits and facilities and in addition will be able to promote their services and facilities. </p>

  <h3 style="padding-bottom: 20px;">Membership Fees:</h3>
  <p>We reserve the right to change our fees from time to time in our discretion. If we terminate your Membership because you have breached these Terms, you shall not be entitled to the refund of any unused portion of subscription fees. Likewise if you terminate membership of MobiHealth you shall not be entitled to any refund of your subscription fee. </p>

   <h3 style="padding-bottom: 20px;">Password:</h3>
  <p>When you register to become a Member, you will also be asked to choose your own password. Users are entirely responsible for maintaining the confidentiality of the password and ensuring that other members do not use the account. Users agree not to use the account, username, or password of another Member at any time or to disclose the password to any third party. Users agree to notify MobiHealth immediately if they suspect any unauthorized use of the account or access to the password. Users are solely responsible for any and all use of the account. </p>

   <h3 style="padding-bottom: 20px;">Prohibited:</h3>
  <p>Use of and Membership in the Services is void where prohibited. By using the Services, Users represent and warrant that: </p>
  <ul>
  	<li>All registration information submitted is truthful and accurate.</li>
  	<li>Users will maintain the accuracy of such information.</li>
  	<li>Users are 18 years of age or older. and</li>
  	<li>Use of the Services does not violate any applicable law or regulation in the User’s country of residence.</li>
  </ul>

  <p>Any profile may be deleted and Membership may be terminated without warning, if the User is under 18 years of age and for a person below the age of 18 requires consent and authorization from any consenting adult on behalf of such minor.</p>

  <p>The Services are for the personal use of Members only and may not be used in connection with any commercial activities except those that are specifically endorsed or approved by MobiHealth. Unauthorised and/or illegal use of the Services, including collecting usernames and/or email addresses of Members by electronic or other means for the purpose of sending unsolicited email or unauthorized framing of or linking to the Website is prohibited.</p>

  <p>Commercial advertisements, affiliate links, and other forms of solicitation may be removed from Member profiles without notice and may result in termination of Membership privileges. Appropriate legal action will be taken for any illegal or unauthorized use of information, contents images or any part of the Services in any form including edited contents or images. A fee will be levied for unauthorized advertising or promotion without the prior written consent of Mobihealth in line with the industry sponsor fee as varies from time to time, details of which are available at <a href="www.mobihealthinternational.com" >www.mobihealthinternational.com</a> subject to a minimum payment of US$5000.</p>

  <h3 style="padding-bottom: 20px;">Account Safety and Member Behaviour:</h3>
  <p>MobiHealth strives to create a safe Platform for its Users and makes best efforts to deliver this, however we are unable to guarantee it. As such Users agree to behave in secure and appropriate manner in relation to all of our Services, to include, but not limited to the following:</p>
  <ul>
  	<li>Not to bully, threaten, or harass, or otherwise, any User;</li>
<li>Not to post unacceptable or inappropriate content, including content that is intimidating, or pornographic, or provokes violence, or contains nudity, or unnecessary violence;</li>
<li>Not to use our Services to do anything unlawful, misrepresentative, hateful, or prejudiced;</li>
<li>Not to ask (or otherwise) for login information or access an account belonging to someone else;</li>
<li>Not to upload viruses/malicious code;</li>
<li>Not to immobilise, overload, or damage the proper working or appearance of our Website, our App and our Services;</li>
<li>Not to collect Users' content or information using automated means;</li>
<li>Not to post unauthorized commercial communications (e.g. spam or spim) on our App or any of our Services.</li>
<li>Not to enable, or incite any violations of these Terms or our Privacy Policy.</li>
<li>Member profiles may not include the following items:</li>

<li>any photographs containing nudity, or obscene, lewd, excessively violent, harassing, sexually explicit or otherwise objectionable subject matter,</li>
<li>any fake, inaccurate or misleading information.</li>
<li>Not to solicit sex, money, blackmail or any unauthorized activity </li>

  </ul>
  <p>MobiHealth assumes no responsibility or liability for any material made available on its Services by any Member. If Users become aware of misuse of the Services by any person or organization, they should contact us immediately at <a href="compliance@mobihealthinternational.com">compliance@mobihealthinternational.com </a>or click the ‘help or customer service’ link at the bottom of our page.</p>

  <p>Users agree that we may rely on a User’s Registration Data as accurate, current and complete. Users acknowledge that if any of their Member profile is considered to be untrue, inaccurate, not current or incomplete in any respect by us then we reserve the right to terminate these Terms and your use of our Services, and seek suitable recompense for any liability incurred by MobiHealth, Users or any third party.</p>

  <p>We reserve the right, in our sole discretion, to reject, refuse to post or remove any posting (including e-mail) by any User, or to restrict, suspend, or terminate a User’s access to all or any part of the Services at any time, for any or no reason, with or without prior notice, and without liability. We also reserve the right to exclude Users who misuse their access to the Services, including but not limited to those who abuse or threaten other Users or misuse the Services.</p>

  <h3 style="padding-bottom: 20px;">Content Posted:</h3>
  <p>We may delete Content that, in our sole judgment, is deemed to violate these Terms or which may be offensive, illegal or violate the rights, harm, or threaten the safety of any person. MobiHealth assumes no responsibility for monitoring the Services for inappropriate Content or conduct. If at any time MobiHealth chooses, in its sole discretion, to monitor the Services, MobiHealth nonetheless assumes no responsibility for the Content, no obligation to modify or remove any inappropriate Content, and no responsibility for the conduct of the User submitting any such Content.</p>

  <h3 style="padding-bottom: 20px;">Content that requires consent:</h3>
  <p>Any and all Content that is loaded by a User and that contains a photograph or a movie of a patient, including images of surgery and from scopes will require patient consent to be used on MobiHealth. The User is responsible for ensuring that all necessary permissions and authorizations have been obtained for the use of these photograph and movies. For such images consent may be obtained in writing by the User or using the MobiHealth consent screens.</p>

  <p>There is an option to obtain later consent for images that require consent, however such images will not be available for public viewing until such time that consent is confirmed, and will be deleted if consent is not indicated within 24hrs of uploading the image to MobiHealth.</p>

  <h3 style="padding-bottom: 20px;">Content that does not require consent:</h3>
  <p>Consent is not required for x-rays, radiology scans, blood results, ECGS and the likes if the images are anonymous with no identifiable patient data. The User must remove all personal and patient identifiers that specifically identify any patient in Content uploaded or descriptions added to the Website and/or App. </p>

<p>In case discussions, learning modules and other MobiHealth features it is the responsibility of the User to provide sufficient clinical information for assessment and discussion without compromising patient confidentiality. In any situation where the User is uncertain regarding patient confidentiality he must exercise caution and contact the Website administrators for advice at admin@mobihealthinternational.com.</p>

<p>The User is responsible for any local requirements for reproduction or display of personal images, video or other material, and the User must comply with all local regulations.</p>

<p>Users are solely responsible for the Content posted on or through any of the Services, and any material or information that you transmit to other Members and for your interactions with other Users. MobiHealth does not endorse and has no control over the Content. Content is not necessarily reviewed by MobiHealth prior to posting and does not necessarily reflect the opinions or policies of MobiHealth. Furthermore, we make no warranties, express or implied, as to the Content or to the accuracy and reliability of the Content or any material or information that Users transmit to other User</p>


  <h3 style="padding-bottom: 20px;">Content/Activity Prohibited:</h3>
  <p>The following is a partial list of the kind of Content that is illegal or prohibited to post on or through our Services. We reserve the right to investigate and take appropriate legal action against anyone who, in MobiHealth's sole discretion, violates this provision, including without limitation, removing the offending communication from the Services and terminating the Subscription of such violators. Prohibited Content includes, but is not limited to:</p>

  <ul>
  	<li>offensive and promotes racism, bigotry, hatred or physical harm of any kind against any group or individual;</li>
<li>using any information obtained from the Services in order to harass, abuse, or harm another person; or using the Services in a manner inconsistent with any and all applicable laws and regulations;</li>
<li>contains personal data or information that directly identifies any patient;</li>
<li>exploits people in a sexual or violent manner;</li>
<li>contains nudity, violence, or offensive subject matter;</li>
<li>provides any telephone numbers, street addresses, last names, URLs or email addresses where such information is not permitted by MobiHealth;</li>
<li>promotes information that is known to be false or misleading or promotes illegal activities or conduct that is abusive, threatening, obscene, defamatory or libelous;</li>
<li>promotes an illegal or unauthorized copy of another person's copyrighted work, such as providing pirated computer programs or links to them, providing information to circumvent manufactured-installed copy-protect devices, or providing pirated music or links to pirated music files;</li>
<li>involves the transmission of "junk mail," "chain letters," or unsolicited mass mailing, instant messaging, "spimming," or "spamming";</li>
<li>contains restricted or password only access pages or hidden pages or images (those not linked to or from another accessible page);</li>
<li>furthers or promotes any criminal activity or enterprise or provides instructional information about illegal activities including, but not limited to making or buying illegal weapons, violating someone's privacy, or providing or creating computer viruses;</li>
<li>solicits passwords or personal identifying information for commercial or unlawful purposes from other Users;</li>
<li>involves commercial activities and/or sales without our prior written consent such as contests, sweepstakes, barter, advertising, or pyramid schemes; or</li>
<li>includes a photograph of another person that you have posted without that person's consent;</li>
<li>advertising to, or solicitation of, any Member to buy or sell any products or services through the Services, excluding those services that are included with the specific prior written consent of MobiHealth and subject to the conditions of use stipulated by MobiHealth. Users may not transmit any chain letters or junk email to other Members. It is also a violation of these rules to use any information obtained from the Services in order to contact, advertise to, solicit, or sell to any Member without their prior explicit consent. In order to protect Members from such advertising or solicitation, MobiHealth reserves the right to restrict the number of emails which a Member may send to other Members in any 24-hour period to a number which MobiHealth deems appropriate in its sole discretion. If Users breach this Terms and send unsolicited bulk email, instant messages or other unsolicited communications of any kind through the Services, Users acknowledge that they will have caused substantial harm to MobiHealth, but that the amount of such harm would be extremely difficult to ascertain. As a reasonable estimation of such harm, such Users agree to pay MobiHealth £100 for each such unsolicited email or other unsolicited communication you send through the Services;</li>
<li>covering or obscuring the banner advertisements on your personal profile page, or within any of Services via HTML/CSS or any other means;</li>
<li>any automated use of the system, such as using scripts to add friends or to crawl the site to copy files or images;</li>
<li>interfering with, disrupting, or creating an undue burden on the Services or the networks or services connected to the Services;</li>
<li>attempting to impersonate another Member or person;</li>
<li>using the account, username, or password of another Member at any time or disclosing your password to any third party or permitting any third party to access your account;</li>
<li>selling or otherwise transferring a profile.</li>

  </ul>

  <h3 style="padding-bottom: 20px; padding-top: 20px;">
  	Proprietary Rights in Content in MobiHealth:
  </h3>
  <p>By displaying or publishing ("posting") on or through the Services, Users hereby grant to MobiHealth, a non-exclusive, fully-paid and royalty-free, worldwide licence (with the right to sub-licence through unlimited levels of sub-licensees) to use, copy, modify, adapt, translate, publicly perform, publicly display, publish, store, reproduce, transmit, and distribute such Content on and through the Services.</p>

   <h3 style="padding-bottom: 20px;">
  	You represent and warrant that:
  </h3>
  <ul>
  	<li>	Users own the Content posted on or through the Services or otherwise have the right to grant the license set forth in this section,</li>
<li>	the posting of Content on or through the Services does not violate the privacy rights, publicity rights, copyrights, contract rights or any other rights of any person, and</li>
<li>	Users agree to pay for all royalties, fees, and any other monies owing any person by reason of any Content posted to or through the Services.</li>

  </ul>

  <p>MobiHealth’s Content is protected by copyright, trade mark, patent, trade secret and other laws, and MobiHealth owns and retains all rights in the MobiHealth Content and the Services. MobiHealth prohibits any User from reproduction and public display of the MobiHealth Content (including any software code).</p>

  <p>Individual Membership permits the User and Member to view MobiHealth Content, educational material and live transmissions. Users are prohibited from public broadcast, projection or display of any such MobiHealth Content or material without the specific prior written authorization of MobiHealth. Individual Membership and access codes may entitle the Member from time to time to view live or restricted material. Users are prohibited from public broadcast, projection or display of any such MobiHealth content or material without the specific prior written authorization of MobiHealth.</p>

  <p>The Services contain Content of Users and other MobiHealth licensors. Except for Content posted by a User, that User may not copy, modify, translate, publish, broadcast, transmit, distribute, perform, display, or sell any Content appearing on or through the Services.
</p>

<p>MobiHealth and our authorized agents and partners reserve the right to provide Members with free membership of subscription on sites operated by MobiHealth, our agents, our partners, and other member companies of the MobiHealth Directors’ group albeit with the rights of the Member to unsubscribe from such services at any time. This includes the social network sites, as well as professional medical or doctors’ networks.</p>

<h3 style="padding-bottom: 20px;">Copyright Policy:</h3>

<p>Users may not post, modify, distribute, or reproduce in any way any copyrighted material, trade marks, or other proprietary information belonging to others without obtaining the prior written consent of the owner of such proprietary rights.</p>

<p>It is the policy of MobiHealth to terminate Membership privileges of any Member who infringes the copyright rights of others upon receipt of prompt notification to MobiHealth by the copyright owner or the copyright owner's legal agent. Without limiting the foregoing, if you believe that your work has been copied and posted on the Services in a way that constitutes copyright infringement, please provide our Copyright Agent with the following information:</p>

<ul>
	<li>an electronic or physical signature of the person authorized to act on behalf of the owner of the copyright interest.</li>
	<li>a description of the copyrighted work that you claim has been infringed.</li>
	<li>a description of where the material that you claim is infringing is located on the Website.</li>
	<li>your address, telephone number, and email address.</li>
	<li>a written statement by you that you have a good faith belief that the disputed use is not authorized by the copyright owner, its agent, or the law.</li>
	<li>a statement by you, made under penalty of perjury, that the above information in your notice is accurate and that you are the copyright owner or authorized to act on the copyright owner's behalf. </li>
</ul>

<p>MobiHealth's Copyright Agent for notice of claims of copyright infringement can be reached as follows: <a href="support@mobihealthinternational.com">support@mobihealthinternational.com </a></p>

<p>We may amend this Privacy Policy in the future.  In the event changes are made, we will be sure to post changes at the Site and at other places we deem appropriate.</p>
</div>
</div>
	@endsection
