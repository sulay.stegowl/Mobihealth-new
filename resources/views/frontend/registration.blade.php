	@extends('frontend.layouts.app')
@section('content')


		<div class="tm-breadcrumb">
		<div class="container">
			<h1 class="tm-section-heading">register</h1>
			<ul>
				<li>
					<a href="{{route('homepage')}}">home</a>
				</li>
				<li>
					<a href="{{route('registration')}}">/ register</a>
				</li>
			</ul>
		</div>
	</div>


<div class="register_as_page tm-home-services">
	<h1 class="tm-section-heading">Register As</h1>

	<div class="container">
		<div class="row">
			<div class="col-sm-12">
			@include('include.message')
				<div class="register_group">
					<div class="service-image">
						<a href="{{route('doctors_registration')}}">
							<span class=" content_center"><img src="{{asset('assets/front/images/icone1.png')}}"></span>
							<p>Medical Professionals</p>
						</a>
					</div>	
					<div class="service-image">
							<a href="{{route('pathology_register')}}">
						<span class=" content_center"><img src="{{asset('assets/front/images/icone2.png')}}"></span>
						<p>pathology services</p>
					</a>
					</div>
					<div class="service-image">
							<a href="{{route('radiology_register')}}">
						<span class="content_center"><img src="{{asset('assets/front/images/icone3.png')}}"></span>
						<p>radiology services</p>
					</a>
					</div>
				</div>
			</div>
		</div>	
		<div class="row">
			<div class="col-sm-12">
				<div class="register_group">
					<div class="service-image">
							<a href="{{route('hospital_register')}}">
						<span class="content_center"><img src="{{asset('assets/front/images/icone4.png')}}"></span>
						<p>hospital services</p>
					</a>
					</div>	
					<div class="service-image">
							<a href="{{route('pharmacy_register')}}">
						<span class="content_center"><img src="{{asset('assets/front/images/icone5.png')}}"></span>
						<p>pharmacy services</p>
					</a>
					</div>
				</div>
			</div>
		</div>	
	</div>
</div>

@endsection