@extends('frontend.layouts.app')
@section('content')
	<!-- Breadcrumb header -->
	<div class="tm-breadcrumb">
		<div class="container">
			<h1 class="tm-section-heading">Our Doctors</h1>
			<ul>
				<li>
					<a href="{{route('homepage')}}">home</a>
				</li>
				<li>
					<a href="{{route('doctors')}}">/ Our Doctors</a>
				</li>
			</ul>
		</div>
	</div><!-- /Breadcrumb header -->
	<!-- Our Dentists -->
	<div class="tm-doctors-style-2 main">
		<div class="container">
			<h1 class="tm-section-heading">Our experienced doctors</h1>
			<div class="row">
				<div class="doctors-wrapper">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 smf">
						<div class="doctor-box">
							<div class="col-lg-6 col-md-6 col-sm-4 col-xs-12 nopadding">
								<div class="doctor-thumbnail">
									<img alt="dentist" class="img-responsive" src="{{asset('/assets/front/images/480562701.jpg')}}">
									<div class="degree">
										MD
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-8 col-xs-12">
								<div class="doctor-info">
									<h2 class="tm-box-heading"><a href="{{route('doctor_detail')}}">ANNA MARIA</a></h2>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
									<div class="share">
										<ul>
											<li>
												<a href="#"><span class="icon-facebook"></span></a>
											</li>
											<li>
												<a href="#"><span class="icon-instagram"></span></a>
											</li>
											<li>
												<a href="#"><span class="icon-twitter-logo"></span></a>
											</li>
										</ul>
									</div><a class="tm-btn btn-blue"  href="{{route('doctor_detail')}}">view profile</a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 smf">
						<div class="doctor-box">
							<div class="col-lg-6 col-md-6 col-sm-4 col-xs-12 nopadding">
								<div class="doctor-thumbnail">
									<img alt="dentist" class="img-responsive" src="{{asset('/assets/front/images/480562701.jpg')}}">
									<div class="degree">
										MS
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-8 col-xs-12">
								<div class="doctor-info">
									<h2 class="tm-box-heading"><a  href="{{route('doctor_detail')}}">AMELIE JOHNSON</a></h2>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
									<div class="share">
										<ul>
											<li>
												<a href="#"><span class="icon-facebook"></span></a>
											</li>
											<li>
												<a href="#"><span class="icon-instagram"></span></a>
											</li>
											<li>
												<a href="#"><span class="icon-twitter-logo"></span></a>
											</li>
										</ul>
									</div><a class="tm-btn btn-blue"  href="{{route('doctor_detail')}}">view profile</a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 smf">
						<div class="doctor-box">
							<div class="col-lg-6 col-md-6 col-sm-4 col-xs-12 nopadding">
								<div class="doctor-thumbnail">
									<img alt="dentist" class="img-responsive" src="{{asset('/assets/front/images/480562701.jpg')}}">
									<div class="degree">
										M.ch
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-8 col-xs-12">
								<div class="doctor-info">
									<h2 class="tm-box-heading"><a  href="{{route('doctor_detail')}}">ANDY STUART</a></h2>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
									<div class="share">
										<ul>
											<li>
												<a href="#"><span class="icon-facebook"></span></a>
											</li>
											<li>
												<a href="#"><span class="icon-instagram"></span></a>
											</li>
											<li>
												<a href="#"><span class="icon-twitter-logo"></span></a>
											</li>
										</ul>
									</div><a class="tm-btn btn-blue" href="{{route('doctor_detail')}}">view profile</a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 smf">
						<div class="doctor-box">
							<div class="col-lg-6 col-md-6 col-sm-4 col-xs-12 nopadding">
								<div class="doctor-thumbnail">
									<img alt="dentist" class="img-responsive" src="{{asset('/assets/front/images/480562701.jpg')}}">
									<div class="degree">
										DM
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-8 col-xs-12">
								<div class="doctor-info">
									<h2 class="tm-box-heading"><a  href="{{route('doctor_detail')}}">WILLIAM FREDDY</a></h2>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
									<div class="share">
										<ul>
											<li>
												<a href="#"><span class="icon-facebook"></span></a>
											</li>
											<li>
												<a href="#"><span class="icon-instagram"></span></a>
											</li>
											<li>
												<a href="#"><span class="icon-twitter-logo"></span></a>
											</li>
										</ul>
									</div><a class="tm-btn btn-blue"  href="{{route('doctor_detail')}}">view profile</a>
								</div>
							</div>
						</div>
					</div>
				
				</div>
			</div>
		</div>
	</div><!-- /Our Dentists -->
	<!-- Call to Action 2 -->
	<div class="tm-call-to-action-2">
		<div class="container">
			<div class="row">
				<div class="col-sm-8">
					<div class="content">
						<h2>Lorem ipsum dolor sit amet, consectetur</h2>
						<div class="separator"></div>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="ca-btns">
						<a class="tm-btn btn-blue" href="#">Search</a> 
						<input type="text" name="name" class="doctor_search" placeholder="Enter Name"> 
					</div>
				</div>
			</div>
		</div>
	</div><!-- /Call to Action 2 -->
	<!-- Services -->
	<div class="tm-services-style-3">
		<h1 class="tm-section-heading">Treatments we offer</h1>
		<div class="owl-carousel">
			<div class="service-box">
				<div class="service-image"><img alt="service" src="{{asset('/assets/front/images/fevers.jpg')}}"></div>
				<div class="service-detail">
					<h3 class="service-title"><a href="#">Malaria</a></h3>
					<p class="service-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.</p>
				</div>
			</div>
			<div class="service-box">
				<div class="service-image"><img alt="service" src="{{asset('/assets/front/images/community-health-center-in-datano.jpg')}}"></div>
				<div class="service-detail">
					<h3 class="service-title"><a href="#">Typhoid</a></h3>
					<p class="service-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.</p>
				</div>
			</div>
			<div class="service-box">
				<div class="service-image"><img alt="service" src="{{asset('/assets/front/images/fevers.jpg')}}"></div>
				<div class="service-detail">
					<h3 class="service-title"><a href="#">Maxillofacial</a></h3>
					<p class="service-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.</p>
				</div>
			</div>
			<div class="service-box">
				<div class="service-image"><img alt="service" src="{{asset('/assets/front/images/community-health-center-in-datano.jpg')}}"></div>
				<div class="service-detail">
					<h3 class="service-title"><a href="#">ENT problems</a></h3>
					<p class="service-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.</p>
				</div>
			</div>
			<div class="service-box">
				<div class="service-image"><img alt="service" src="{{asset('/assets/front/images/gastro-2.png')}}"></div>
				<div class="service-detail">
					<h3 class="service-title"><a href="#">Gastroenteritis</a></h3>
					<p class="service-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.</p>
				</div>
			</div>
			<div class="service-box">
				<div class="service-image"><img alt="service" src="{{asset('/assets/front/images/fevers.jpg')}}"></div>
				<div class="service-detail">
					<h3 class="service-title"><a href="#">root canal</a></h3>
					<p class="service-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.</p>
				</div>
			</div>
		</div>
	</div><!-- /Services -->
	<!-- Footer -->
	
@endsection