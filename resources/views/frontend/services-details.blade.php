@extends('frontend.layouts.app')
@section('content')

<div class="tm-breadcrumb">
		<div class="container">
			<h1 class="tm-section-heading">Service Detail</h1>
			<ul>
				<li>
					<a href="{{route('homepage')}}">home</a>
				</li>
				<li>
					<a href="{{route('services_details')}}">/ Service Detail</a>
				</li>
			</ul>
		</div>
	</div>

	<div class="tm-service-detail-wrap">
		<div class="container">
			<div class="row">
			
				<div class="col-sm-4">
					<div class="tm-sidebar">
						
					

						{{-- <div class="sidebar-widget">
							<div class="tm-appointment">
								<h4 class="widget-heading">make an appointment</h4>
								<div class="appointment-form">
									<form>
										<div class="col-sm-12">
											<div class="form-group">
												<input class="form-control" name="full_name" placeholder="Full Name" type="text">
											</div>
										</div>
										<div class="col-sm-12">
											<div class="form-group">
												<input class="form-control" name="email" placeholder="Email" type="email">
											</div>
										</div>
										<div class="col-sm-12">
											<div class="form-group">
												<input class="form-control" name="phone" placeholder="Phone (optional)" type="text">
											</div>
										</div>
										<div class="col-sm-12">
											<div class="form-group">
												<div class="tm-datepicker">
													<input class="form-control" data-toggle="datepicker" name="dob" placeholder="Date of Birth" type="text">
												</div>
											</div>
										</div>
										<div class="col-sm-12">
											<div class="form-group">
												<div class="tm-datepicker">
													<input class="form-control" data-toggle="datepicker" name="date" placeholder="Choose Date" type="text">
												</div>
											</div>
										</div>
										<div class="col-sm-12">
											<div class="form-group">
												<div class="tm-timepicker">
													<select class="form-control" name="time">
														<option>
															01:30 AM
														</option>
														<option>
															02:30 AM
														</option>
														<option>
															03:30 AM
														</option>
														<option>
															04:30 AM
														</option>
														<option>
															05:30 AM
														</option>
														<option>
															06:30 AM
														</option>
														<option>
															07:30 AM
														</option>
														<option>
															08:30 AM
														</option>
														<option>
															09:30 AM
														</option>
													</select>
												</div>
											</div>
										</div>
										<div class="col-sm-12 text-right">
											<div class="form-group">
												<input class="tm-btn btn-blue get_appoint" name="submit" type="submit" value="get appointment">
											</div>
										</div>
									</form>
								</div>
							</div>
						</div> --}}
						<div class="sidebar-widget questions">
							<h4 class="widget-heading">questions and queries</h4>
							<p>In case of emergency, contact us on the following email and we will immediately respond to you.</p>
							<ul>
								{{-- <li><span class="icon-phone"></span> 012 345 67 890</li> --}}
								<li><span class="icon-at"></span> info@Mobihealth.com</li>
							</ul>
							{{-- <div class="separator"></div><a class="tm-btn btn-blue pdf get_appoint" href="service-detail.html">download pdf</a> --}}
						</div>
					</div>
				</div>
				
				<div class="col-sm-8">
					<div class="tm-service-detail">
						<div class="service-detail-thumb"><img alt="service" src="{{asset('/assets/front/extra-images/service-detail-1.jpg')}}"></div>
						<h4 class="service-title">Lorem ipsum dolor sit amet, </h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi gravida fringilla neque sit amet sollicitudin. Duis aliquam dictum feugiat. Quisque elit tortor, ullamcorper quis ultrices vitae, mattis non est. Aliquam erat volutpat. Ut ac suscipit lorem. Suspendisse rhoncus tellus ac neque gravida, in tristique urna placerat. Morbi in purus rutrum, hendrerit tellus pulvinar, interdum nisl.</p>
						{{-- <div class='separator'></div>
						<h4 class="tm-box-heading-sm">how we do it</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi gravida fringilla neque sit amet sollicitudin. Duis aliquam dictum feugiat. Quisque elit tortor, ullamcorper quis ultrices vitae, mattis non est. Aliquam erat volutpat. Ut ac suscipit lorem. Suspendisse rhoncus tellus ac neque gravida, in tristique urna placerat. Morbi in purus rutrum, hendrerit tellus pulvinar, interdum nisl.</p>
						<ul>
							<li>Lorem ipsum dolor sit amet</li>
							<li>Consectetur adipiscing elit</li>
							<li>Morbi gravida fringilla neque sit amet sollicitudin</li>
							<li>Duis aliquam dictum feugiat</li>
							<li>Quisque elit tortor, ullamcorper quis ultrices vitae</li>
						</ul>
						<div class='separator'></div>  --}}<!-- Our Pricing -->
						{{-- <div class="tm-inline-pricing">
							<h4 class="tm-box-heading-sm">our pricing</h4>
							<div class="pricing-box">
								<div class="list-row">
									<div class="price-desc">
										<div class="lbl">
											Dental implant
										</div>
										<div class="desc">
											Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi gravida fringilla neque
										</div>
									</div>
									<div class="price-cell">
										$200
									</div>
								</div>
								<div class="list-row">
									<div class="price-desc">
										<div class="lbl">
											Tooth Filling
										</div>
										<div class="desc">
											Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi gravida fringilla neque
										</div>
									</div>
									<div class="price-cell">
										$200
									</div>
								</div>
								<div class="list-row">
									<div class="price-desc">
										<div class="lbl">
											Periodontics
										</div>
										<div class="desc">
											Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi gravida fringilla neque
										</div>
									</div>
									<div class="price-cell">
										$200
									</div>
								</div>
								<div class="list-row">
									<div class="price-desc">
										<div class="lbl">
											Root Canal
										</div>
										<div class="desc">
											Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi gravida fringilla neque
										</div>
									</div>
									<div class="price-cell">
										$200
									</div>
								</div>
							</div>
						</div> --}}
					</div>
				</div>
			</div>
		</div>
	</div>

	@endsection