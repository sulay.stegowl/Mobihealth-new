@extends('frontend.layouts.app')
@section('content')

	<div class="tm-breadcrumb">
		<div class="container">
			<h1 class="tm-section-heading">News</h1>
			<ul>
				<li>
					<a href="{{route('homepage')}}">home</a>
				</li>
				<li>
					<a href="{{route('news')}}">/ News </a>
				</li>
			</ul>
		</div>
	</div>
	<div class="tm-news-home">
		<div class="container">
			<div class="row">
				<h1 class="tm-section-heading">Our news</h1>
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 smf">
					<div class="news">
						<div class="news-thumbnail">
						<div class="post-date">
							02<br>
							Oct
						</div><img alt="dentist" src="{{asset('/assets/front/images/blog1.png')}}"></div>
						<div class="news-title">
							<a href="{{route('news_details_1')}}">HOW DOES TELEMEDICINE AFFECT MALPRACTICE INSURANCE?</a>
						</div>
						<div class="news-meta">
							<ul>
								<li><span class="icon-user"></span> by admin</li>
							</ul>
						</div>
						<div class="separator"></div>
						<p>As physicians, we don’t normally like to think about malpractice insurance or any scenario where we’d actually need it. </p>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 smf">
					<div class="news">
						<div class="news-thumbnail">
						<div class="post-date">
							02<br>
							Oct
						</div><img alt="dentist" src="{{asset('/assets/front/images/blog2.png')}}"></div>
						<div class="news-title">
							<a href="{{route('news_details_2')}}">DEFINITIVE GUIDE TO PURCHASING A TELEMEDICINE PLATFORM [WHITE PAPER]</a>
						</div>
						<div class="news-meta">
							<ul>
								<li><span class="icon-user"></span> by admin</li>
							</ul>
						</div>
						<div class="separator"></div>
						<p>Telemedicine is going to be a critical capability for practices large and small moving forward. Patients are looking for more convenience, ...</p>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 smf">
					<div class="news">
						<div class="news-thumbnail">
						<div class="post-date">
							02<br>Oct
						</div><img alt="dentist" src="{{asset('/assets/front/images/blog3.png')}}"></div>
						<div class="news-title">
							<a href="{{route('news_details_3')}}">INFOGRAPHIC: HOW TO REDUCE PATIENT WAIT TIMES</a>
						</div>
						<div class="news-meta">
							<ul>
								<li><span class="icon-user"></span> by admin</li>
							</ul>
						</div>
						<div class="separator"></div>
						<p>The amount of time patients spend waiting in your office may seem like a small factor when it comes to patient satisfaction. </p>
					</div>
				</div>
				
			</div>
		</div>
	</div>

@endsection