@extends('frontend.layouts.app')
@section('content')
<style type="text/css">
  .coming_soon.modal {
     top: 0% !important; 
}
</style>
<div class="tm-breadcrumb">
		<div class="container">
			<h1 class="tm-section-heading">subscriptions</h1>
			<ul>
				<li>
					<a href="{{route('homepage')}}">home</a>
				</li>
				<li>
					<a href="{{route('subscription')}}">/ subscriptions</a>
				</li>
			</ul>
		</div>
	</div><!-- /Breadcrumb header -->
	<!-- Shop -->
	<div class="tm-shop-list">

		
		<div class="container">
		

    <div class="dis_inline">
      
      <div class="how_it_faq">
        <a href="{{route('patients_how')}}" class="how_it_work">How It Works</a>
        <a href="{{route('patients_faq')}}" class="faq">Faq</a>
      </div>
    </div>


		<h1 class="tm-section-heading">SUBSCRIPTION PRICE PER MONTH BY COUNTRY</h1>
		
		
<div class="subcriptions_plans">
<div class="container">
 
   <div class="table-responsive">          
  <table class="table">
    <thead>
      <tr>
        <th>Levels</th>
        <th>Ethiopia</th>
        <th>Nigeria</th>
        <th>Tanzania</th>
        <th>Ghana</th>
        <th>Sierra Leone</th>
        <th>&nbsp;</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Solo</td>
        <td>328</td>
        <td>4300</td>
        <td>28228</td>
        <td>53</td>
        <td>92522</td>
        <td><a class="tm-btn btn-blue" href="javascript:void(0)" data-toggle="modal" data-target="#solomodel">subscribe now</a></td>
      </tr>

      <tr>
        <td>Bronze</td>
        <td>724</td>
        <td>9500</td>
        <td>62364</td>
        <td>118</td>
        <td>204410</td>
        <td><a class="tm-btn btn-blue" href="javascript:void(0)" data-toggle="modal" data-target="#solomodel">subscribe now</a></td>
      </tr>

       <tr>
        <td>Silver</td>
        <td>1182</td>
        <td>15500</td>
        <td>101751</td>
        <td>192</td>
        <td>333511</td>
        <td><a class="tm-btn btn-blue" href="javascript:void(0)" data-toggle="modal" data-target="#solomodel">subscribe now</a></td>
      </tr>

       <tr>
        <td>Gold</td>
        <td>1125</td>
        <td>20000</td>
        <td>131292</td>
        <td>248</td>
        <td>430337</td>
        <td><a class="tm-btn btn-blue" href="javascript:void(0)" data-toggle="modal" data-target="#solomodel">subscribe now</a></td>
      </tr>

         <tr>
        <td>PAYG</td>
        <td>572</td>
        <td>7501</td>
        <td>49241</td>
        <td>93</td>
        <td>161396</td>
        <td><a class="tm-btn btn-blue" href="javascript:void(0)" data-toggle="modal" data-target="#solomodel">subscribe now</a></td>
      </tr>


    </tbody>
  </table>
  </div>
</div>
</div>

<!--start -->
  <div class="tm-pricing-home">
    <div class="container">
      <div class="row">
        <h1 class="tm-section-heading">subscription Plans</h1>
        <div class="row text-center" style="padding-bottom: 10px;">
          <h4>Paid subscribers get these services for free</h4>
          </div>
          <div class="pricing_group">
            
    
<div class="subcriptions_plans1">
<div class="container">
 
   <div class="table-responsive">          
  <table class="table">
    <thead>
      <tr>
        <th>FEATURES</th>
        <th>SOLO</th>
        <th>BRONZE</th>
        <th>SILVER</th>
        <th>GOLD</th>
        <th>PAYG</th>
      </tr>
    </thead>
    <tbody>
      {{-- <tr>
        <td>&nbsp;</td>
        <td>N6000 / Month</td>
        <td>N11,000 / Month</td>
        <td>N20,000 / Month</td>
        <td>N30,000 / Month</td>
        <td>N8000 / Consultation</td>
        
      </tr>
 --}}
      <tr>
        <td>No. of persons covered</td>
        <td>1</td>
        <td>4</td>
        <td>4</td>
        <td>4</td>
        <td>1</td>
      </tr>

     <tr>
        <td>No. of video consultation per household per annum</td>
        <td>4</td>
        <td>8</td>
        <td>10</td>
        <td>14</td>
        <td>N/A</td>
      </tr>

       <tr>
        <td>Free access to database of ~100k doctors & healthcare professionals world wide</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
      </tr>

        <tr>
        <td>Free access to reputable pharmacies, diagnostic centers & hospitals locally</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
      </tr>

       <tr>
        <td>Free diagnsotic work-up</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>N/A</td>
      </tr>

        <tr>
        <td>Free hospital treatment</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>N/A</td>
      </tr>

        <tr>
        <td>Free prescription</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>N/A</td>
      </tr>

        <tr>
        <td>Free genuine medications</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>N/A</td>
      </tr>

       <tr>
        <td>Free acute dental treatment</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>N/A</td>
      </tr>

       <tr>
        <td>Medical bills paid</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>N/A</td>
      </tr>

       <tr>
        <td>Free home laboratory service </td>
        <td>no</td>
        <td>yes * 4</td>
        <td>yes * 8</td>
        <td>yes * 10</td>
        <td>N/A</td>
      </tr>

       <tr>
        <td>Free home delivery of medication</td>
        <td>yes * 2</td>
        <td>yes * 3</td>
        <td>yes * 6</td>
        <td>yes * 6</td>
        <td>N/A</td>
      </tr>

      <tr>
        <td>Free Physiotherapy service</td>
        <td>yes * 2</td>
        <td>yes * 4</td>
        <td>yes * 6</td>
        <td>yes * 10</td>
        <td>N/A</td>
      </tr>


      <tr>
        <td>Free home nursing service</td>
        <td>yes * 2</td>
        <td>yes * 4</td>
        <td>yes * 6</td>
        <td>yes * 10</td>
        <td>N/A</td>
      </tr>

      <tr>
        <td>Personalised health advise</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>N/A</td>
      </tr>

 <tr>
        <td>Home Visit by Doctor</td>
        <td>no</td>
        <td>no</td>
        <td>no</td>
        <td>yes * 2</td>
        <td>N/A</td>
      </tr>

        <tr>
        <td>&nbsp;</td>
        <td><a class="tm-btn btn-blue" href="{{route('subscription')}}" >subscribe now</a></td>
        <td><a class="tm-btn btn-blue" href="{{route('subscription')}}" >subscribe now</a></td>
        <td><a class="tm-btn btn-blue" href="{{route('subscription')}}" >subscribe now</a></td>
        <td><a class="tm-btn btn-blue" href="{{route('subscription')}}" >subscribe now</a></td>
        <td><a class="tm-btn btn-blue" href="{{route('subscription')}}" >subscribe now</a></td>
      </tr>


    </tbody>
  </table>
  </div>
</div>
</div>


          
      </div>
    </div>
  </div><!-- /Pricing Table -->

</div>
<!--end -->
		
		</div><!-- /Shop -->
	</div>
   <div class="modal fade coming_soon" id="solomodel" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><img alt="Logo" src="{{asset('assets/front/images/logo_new2.png')}}"></h4>
        </div>
        <div class="modal-body">

          <p style="text-align: left;">Coming to you soon. Please fill form below so we can reach out to you first once enrolment starts. Subscription will be on a first come first serve basis. Please complete form below</p>

          <form method="POST" action="{{route('subscriptions_save')}}">
            {{csrf_field()}}
            <div class="register_form new_form">
            
          <div class="form_group">
            <label>Name</label>
           <input type="text" name="name"  placeholder="Enter Name" required="required">
          </div>
           <div class="form_group">
            <label>Country</label>
           <input type="text" name="country"  placeholder="Enter Country" required="required">
          </div>
           <div class="form_group">
            <label>Phone No</label>
           <input type="number" name="number"  placeholder="Enter Phone No" required="required">
          </div>
          <div class="form_group">
            <label>Email</label>
           <input type="email" name="email"  placeholder="Enter Email" required="required">
          </div>
          <div class="form-group form_group">
            <label>Plan</label>
           <select name="plan" id="" required="required">
            <option value="Solo">Solo</option>
            <option value="Bronze">Bronze</option>
            <option value="Silver">Silver</option>
            <option value="Gold">Gold</option>PAYG
            <option value="PAYG">PAYG</option>
           </select>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-default pull-center" >Submit</button>
        </div>
      </form>
      </form>
      </div>
    </div>
  </div>
</div>
	@endsection
  
