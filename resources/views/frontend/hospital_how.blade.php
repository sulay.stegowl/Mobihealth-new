@extends('frontend.layouts.app')
@section('content')

<div class="tm-breadcrumb">
		<div class="container">
			<h1 class="tm-section-heading">hospital</h1>
			<ul>
				<li>
					<a href="{{route('homepage')}}">home</a>
				</li>
				<li>
					<a href="{{route('hospital_how')}}">/ How It Works</a>
				</li>
			</ul>
		</div>
	</div><!-- /Breadcrumb header -->
	<!-- Shop -->
	<div class="tm-shop-list">

		
		<div class="container">
		

		<h1 class="tm-section-heading">How It Works</h1>
		
		<div class="how_it_work">
			<div class="title">Benefits</div>
			<ul class="list_style_none">
				<li>Streamline Online Booking system</li>
				<li>Increase sales and revenue</li>
				<li>Improve visibility for your business</li>
				<li>Regular and reliable source of revenue</li>
				<li>Access to wide base of medical experts and colleagues from around the world</li>
				<li>Efficient management of resources</li>
				<li>Reduce running cost</li>
			</ul>

			<div class="title text-center">Step 1</div>

<div class="row">
	
	<div class="col-sm-6">
				<div class="title">Sign Up</div>
			<p>Sign up using our secure sign-up and streamlined application process which enables you to create your unique online Hospital profile. You will also be asked to provide following:</p>
			<img src="{{asset('assets/front/images/image1.png')}}" alt="">
	</div>

<div class="col-sm-6">
				<div  class="title" >PHOTO </div>
			<p>Provide a high quality, professional photo for your business to appear on your public profile.</p>
			
			<img src="{{asset('assets/front/images/image2.png')}}" alt="">

</div>

</div>
<div class="row">
	<div class="col-sm-6">
		<div class="title">NAME OF HOSPITAL, WEBSITE AND MAIN CONTACT PERSON’S NAME, EMAIL & PHONE</div>
			<img src="{{asset('assets/front/images/image3.png')}}" alt="">
	</div>
	<div class="col-sm-6">
			<div class="title">HOSPITAL REGISTRATION AND MOH No..</div>
			<p>We’ll use this to verify your credentials, and it will appear in your profile.</p>		
			<img src="{{asset('assets/front/images/image4.png')}}" alt="">
	</div>
</div>			

<div class="row">
	<div class="col-sm-6">
		<div class="title">IDENTITY VERIFICATION </div>
			<p>We’ll ask for personal information of principal owner/Management </p>
			<img src="{{asset('assets/front/images/image5.png')}}" alt="">
	</div>
	<div class="col-sm-6">
			<div class="title">LINK TO YOUR ONLINE APPOINTMENT BOOKING SYSTEM </div>
			<img src="{{asset('assets/front/images/image6.png')}}" alt="">
	</div>
</div>
	
	<div class="row">
		<div class="col-sm-6">
			<div class="title">BANK DETAILS</div>
			<p>Provide your bank details for direct deposit of invoices when we refer patients to you.</p>			

		</div>
	</div>
			


			


			


			<div class="title text-center">Step 2 - Sign In</div>
			<p>Once we receive above information and have completed necessary checks your business profile will be enabled and you will receive login credentials from our admin. You will be able to access our platform from your computer or iOS mobile device.</p>
			<p>We offer video tutorials to get you accustomed with our platform, we also offer a live demo visit with a member of our team to walk through the process and ask your questions.</p>

			<div class="title text-center">Step 3 – INTEGRATE INTO OUR SECURE ONLINE PLATFORM </div>

			<p>Following a video consultation, a doctor may decide to refer a patient to a local hospital for further management. We will book appointment on behalf of the patient to be seen by qualified specialist. The cost of treatment and medication will be paid by Mobihealth to the relevant hospital once invoices have been received. We take over invoicing and any form of fraud very seriously and will pursue criminal charges against any individual or organization who engage in such practices. We expect a copy of discharge letters to be sent to referring doctor/Mobihealth for patient’s medical record.</p>

			<p>If questions or technical issues come up, we’re here for you. You’ll have 24/7 access to our support team by phone and email.</p>

			<div class="title text-center">Step 4 - Get Paid</div>
			<p>Once invoices are received, we will deposit same amount automatically into your designated bank account immediately or on a weekly/monthly basis depending on preference ensuring regular cash flow for your business. Since the costs of treatment/hospitalization are paid by Mobihealth you don’t need to worry about patient’s ability to pay, no insurance or any reimbursement hassles. </p>




		</div>

		
		</div><!-- /Shop -->
	</div>
	@endsection
