@extends('frontend.layouts.app')
@section('content')
	<div class="tm-breadcrumb">
		<div class="container">
			<h1 class="tm-section-heading">Hospital Detail</h1>
			<ul>
				<li>
				<a href="{{route('homepage')}}">home</a>
				</li>
				<li>
					<a href="{{route('hospital_details' , ['slug' => $data->slug])}}">/ {{$data->title}} </a>
				</li>
			</ul>
		</div>
	</div><!-- /Breadcrumb header -->
	<!-- Shop Detail Section -->
	<section class="tm-shop-detail">
		<div class="container">
			<div class="row">
				<!-- Product information Wrapper -->
				<div class="tm_product_info">
					<!-- Product Image -->
					<div class="col-sm-4">
						<div class="product_image"><img alt="" src="{{asset('/storage/services/'.$data->image)}}"></div>
					</div><!-- /Product Image -->
					<!-- Information -->
					<div class="col-sm-8">
						<div class="product_details">
							<h4> {{ $data->title }} </h4>
							
							<p> {{ $data->desc}} </p>

								<div class="Pharmacy_text_2">
								<b>Address : <span>{{$data->address}} </span> </b>
							</div> <br>
							<div class="Pharmacy_text_2">
								<b>Contact Us : <span>{{$data->phone}}  </span> </b>
							</div> <br>
							<div class="Pharmacy_text_2">
								<b>Email : <span>{{$data->email}} </span> </b>
							</div>
						
						</div>
					</div><!-- /Information -->
				</div><!-- /Product information Wrapper -->
				<!-- Details and Reviews -->
				<div class="col-sm-12">
					<div class="tm_product_reviews_details">
						<ul class="nav nav-tabs">
							<li class="active">
								<a data-toggle="tab" href="#details">Details</a>
							</li>
							{{-- <li>
								<a data-toggle="tab" href="#reviews">Reviews</a>
							</li> --}}
						</ul>
						<div class="tab-content">
							<div class="tab-pane fade in active" id="details">
								<h3>HOSPITAL DETAILS</h3>

								<p> {{ $data->desc}} </p>

								<br><br><br><br>
							</div><!-- Reviews -->
							{{-- <div class="tab-pane fade" id="reviews">
								<h3>Customer Reviews</h3>
								<div class="tm-comments">
									<ul>
										<li>
											<div class="user-comment">
												<div class="user-image"><img alt="Author" src="{{asset('/assets/front/images/author.jpg')}}"></div>
												<div class="comment-content">
													<h5 class="user-name">Stephen Hawk</h5>
													<h5 class="comment-time"><span class="icon-calendar-one"></span> March 1, 2012 at 3:01 am</h5>
													<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut.</p><button class="tm-btn btn-blue" type="button"><i aria-hidden="true" class="fa fa-reply"></i> reply</button>
												</div>
											</div>
										</li>
										<li>
											<div class="user-comment">
												<div class="user-image"><img alt="Author" src="{{asset('/assets/front/images/author.jpg')}}"></div>
												<div class="comment-content">
													<h5 class="user-name">Stephen Hawk</h5>
													<h5 class="comment-time"><span class="icon-calendar-one"></span> March 1, 2012 at 3:01 am</h5>
													<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut.</p><button class="tm-btn btn-blue" type="button"><i aria-hidden="true" class="fa fa-reply"></i> reply</button>
												</div>
											</div>
										</li>
										<li class="reply">
											<div class="user-comment">
												<div class="user-image"><img alt="Author" src="{{asset('/assets/front/images/author.jpg')}}"></div>
												<div class="comment-content">
													<h5 class="user-name">Stephen Hawk</h5>
													<h5 class="comment-time"><span class="icon-calendar-one"></span> March 1, 2012 at 3:01 am</h5>
													<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut.</p><button class="tm-btn btn-blue" type="button"><i aria-hidden="true" class="fa fa-reply"></i> reply</button>
												</div>
											</div>
										</li>
										<li>
											<div class="user-comment">
												<div class="user-image"><img alt="Author" src="{{asset('/assets/front/images/author.jpg')}}"></div>
												<div class="comment-content">
													<h5 class="user-name">Stephen Hawk</h5>
													<h5 class="comment-time"><span class="icon-calendar-one"></span> March 1, 2012 at 3:01 am</h5>
													<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut.</p><button class="tm-btn btn-blue" type="button"><i aria-hidden="true" class="fa fa-reply"></i> reply</button>
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>  --}}<!-- Reviews -->
						</div>
					</div>
				</div><!-- /Details and Reviews -->
			</div>
		</div>
	</section><!-- /Shop Detail Section -->

	
	@endsection