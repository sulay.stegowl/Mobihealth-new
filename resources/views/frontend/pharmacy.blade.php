@extends('frontend.layouts.app')
@section('content')

<div class="tm-breadcrumb">
		<div class="container">
			<h1 class="tm-section-heading">pharmacy</h1>
			<ul>
				<li>
					<a href="{{route('homepage')}}">home</a>
				</li>
				<li>
						<a href="{{route('pharmacy')}}">/ Pharmacy </a>
				</li>
			</ul>
		</div>
	</div><!-- /Breadcrumb header -->
	<!-- Gallery -->
	<div class="tm-filter-gallery">
		<div class="dis_inline">
			<h1 class="tm-section-heading"> Pharmacy</h1>
			{{-- <div class="how_it_faq">
				<a href="{{route('pharmacy_how')}}" class="how_it_work">How It Works</a>
				<a href="{{route('pharmacy_faq')}}" class="faq">Faq</a>
			</div> --}}
		</div>

				<div class="tm-appointment-large tm-appointment-large_none unique">
		<div class="container">
			<h1 class="tm-section-heading">Search By</h1>
			<div class="row">
				<form method="POST" action="{{route('search_pharmacy')}}">
					{{csrf_field()}}
				<div class="col-sm-4">
					<div class="form-group">
						<div class="tm-timepicker">
							<select class="form-control" name="search_by" id="search">
								<option value="Name"> Name </option>
								<option value="Country"> Country </option>
								<option value="State"> State </option>
								<option value="Location"> Location </option>
							</select>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="form-group" id="change">
						<input class="form-control" name="value" placeholder="Enter Hospital Name" type="text">
					</div>
				</div>
				
				
				<div class="col-sm-4">
					<div class="form-group page_loader_div_2" >

						<button class="tm-btn btn-dark" type="submit" >SEARCH</button>
					</div>
				</div>
			
		</form>
		</div>
	</div><!-- /Appointment -->
</div>
		<div class="container">
<div class="tm-shop-list">

		



	

		

		
				<div class="filtr-container ">
					   @forelse($ans as $data)

					<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 smf" id="list">
					<div class="tm-product-box">
						<div class="product-image"><img alt="Shop" src="{{asset('/storage/services/'.$data->image)}}" class="img-responsive "></div>
						<div class="product-details">
							<h4 class="product-title"><a href="{{route('pharmacy_details',['slug' => $data->slug])}}">{{$data->title}},<span>&nbsp{{$data->country}}</span></a></h4>
							
						</div>
					</div>
				</div>

					@empty
					<h1 id="list">No Search Found</h1>
					@endforelse

				</div>
			</div>
		</div>
	</div><!-- /Gallery -->
	
	@endsection