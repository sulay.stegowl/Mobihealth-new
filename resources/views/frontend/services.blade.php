	@extends('frontend.layouts.app')
@section('content')
<style type="text/css">
  .services-wrapper {
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
}
</style>
	<!-- Breadcrumb header -->
	<div class="tm-breadcrumb">
		<div class="container">
			<h1 class="tm-section-heading">Services</h1>
			<ul>
				<li>
					<a href="{{route('homepage')}}">home</a>
				</li>
				<li>
					<a href="{{route('services')}}">/ Services</a>
				</li>
			</ul>
		</div>
	</div><!-- /Breadcrumb header -->
	<!-- Services -->
<div class="tm-services-style-3">
		<div class="container">
			<div class="row">
				<h1 class="tm-section-heading">our product and services</h1>
				<div class="services-wrapper">
            <div class="col-sm-4">
            <div class="service-box">
              <div class="service-image"><img alt="service" src="{{asset('/assets/front/extra-images/IMG-20171007_WA0002.jpg')}}"></div>
              <div class="service-detail services_height">
                <h3 class="service-title"><a href="{{route('service_detail_4')}}" >Instant Video Consultation</a></h3>
                <p class="service-description">Immediate Video consultation with a doctor via your mobile device. This eliminates long waits for doctor appointments, delayed diagnosis and suffering. Get diagnosed safely from the comfort of your home, anywhere &  anytime</p>
              </div>
            </div>
          </div>
                    <div class="col-sm-4">
            <div class="service-box">
              <div class="service-image"><img alt="service" src="{{asset('/assets/front/images/L9.jpg')}}"></div>
              <div class="service-detail services_height">
                <h3 class="service-title"><a href="{{route('service_detail_3')}}" >Comprehensive Physical Examination </a></h3>
                <p class="service-description">Our innovative video platform offers quality video consultation specifically designed for low bandwith areas with difficult network and geographical terrain. Our solutions enables patient led comprehensive physical examination remotely such that a doctor’s visit is easily replicated. A doctor any where in the world can easily listen to patient’s heart and chest sounds, examine their throat, ear or skin without need for a face to face doctor’s visit. </p>
              </div>
            </div>
          </div>
					<div class="col-sm-4">
						<div class="service-box">
							<div class="service-image"><img alt="service" src="{{asset('/assets/front/images/L7.jpg')}}"></div>
							<div class="service-detail services_height">
								<h3 class="service-title"><a href="{{route('service_detail_1')}}" >Laboratory and Radiologic Investigations</a></h3>
								<p class="service-description">
We book your diagnostic tests with reputable centers and our doctors evaluate the results remotely to give you medical advice and prescriptions. You can also request home laboratory test depending on your subscription plan or as additional package.
</p>
							</div>
						</div>
					</div>
			   <div class="col-sm-4">
            <div class="service-box">
              <div class="service-image"><img alt="service" src="{{asset('/assets/front/images/L8.jpg')}}"></div>
              <div class="service-detail services_height">
                <h3 class="service-title"><a href="{{route('service_detail_6')}}" >Hospital Referrals and Payment of medical bills</a></h3>
                <p class="service-description">For those who need further face to face evaluation, our online doctors send a referral to a local specialist and book your appointments with a confirmation sent to your email/mobile. We pay your medical bills for each encounter with no out-of pocket payment from you.</p>
              </div>
            </div>
          </div>
					<div class="col-sm-4">
						<div class="service-box">
							<div class="service-image"><img alt="service" src="{{asset('/assets/front/images/L4.jpg')}}"></div>
							<div class="service-detail services_height">
								<h3 class="service-title"><a href="{{route('service_detail_5')}}" >Prescription and Genuine medications</a></h3>
								<p class="service-description">You no longer need to worry about where to get genuine medication. We send your prescriptions to reputable pharmacies and text you when they are ready for pick up. We have partnered with highly reputable pharmacies with branded products from the UK, USA and other highly reputable manufacturers.</p>
							</div>
						</div>
					</div>
				    <div class="col-sm-4">
            <div class="service-box">
              <div class="service-image"><img alt="service" src="{{asset('/assets/front/images/L2.jpg')}}"></div>
              <div class="service-detail services_height">
                <h3 class="service-title"><a href="{{route('service_detail_2')}}" >Home Health Services
</a></h3>
                <p class="service-description">Request a nurse, physiotherapist visit or laboratory tests from anywhere. Our trained nurses and other healthcare professionals work within a framework of high ethical standards. We also offer home delivery of medications and repeat prescriptions for your convenience.</p>
              </div>
            </div>
          </div>

					

					
				</div>
			</div>
		</div>
	</div>
	<!-- Call to action -->

{{-- <div class="tm-pricing-home">
		<div class="container">
			<div class="row">
				<h1 class="tm-section-heading">subscription Plans</h1>


<div class="subcriptions_plans1">
<div class="container">
 
   <div class="table-responsive">          
  <table class="table">
    <thead>
      <tr>
        <th>FEATURES</th>
        <th>SOLO</th>
        <th>BRONZE</th>
        <th>SILVER</th>
        <th>GOLD</th>
        <th>PAYG</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>&nbsp;</td>
        <td>N6000 / Month</td>
        <td>N11,000 / Month</td>
        <td>N20,000 / Month</td>
        <td>N30,000 / Month</td>
        <td>N8000 / Consultation</td>
        
      </tr>

      <tr>
        <td>No of persons covered</td>
        <td>1</td>
        <td>4</td>
        <td>4</td>
        <td>4</td>
        <td>1</td>
      </tr>

     <tr>
        <td>No of video consultation per household per annum</td>
        <td>3</td>
        <td>4</td>
        <td>8</td>
        <td>12</td>
        <td>N/A</td>
      </tr>

       <tr>
        <td>Free access to database of ~100k doctors & healthcare professionals world wide</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
      </tr>

        <tr>
        <td>Free access to reputable pharmacies, diagnostic centers & hospitals locally</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
      </tr>

       <tr>
        <td>Free diagnsotic work-up</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>N/A</td>
      </tr>

        <tr>
        <td>Free hospital treatment</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>N/A</td>
      </tr>

        <tr>
        <td>Free prescription</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>N/A</td>
      </tr>

        <tr>
        <td>Free genuine medications</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>N/A</td>
      </tr>

       <tr>
        <td>Free acute dental treatment</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>N/A</td>
      </tr>

       <tr>
        <td>Medical bills paid per encounter</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>N/A</td>
      </tr>

       <tr>
        <td>Free home laboratory service </td>
        <td>no</td>
        <td>no</td>
        <td>yes * 3</td>
        <td>yes * 6</td>
        <td>N/A</td>
      </tr>

       <tr>
        <td>Free home delivery of medication</td>
        <td>no</td>
        <td>yes * 3</td>
        <td>yes * 6</td>
        <td>yes * 6</td>
        <td>N/A</td>
      </tr>

      <tr>
        <td>Free Physiotherapy service</td>
        <td>no</td>
        <td>no</td>
        <td>yes * 3</td>
        <td>yes * 6</td>
        <td>N/A</td>
      </tr>


      <tr>
        <td>Free home nursing service</td>
        <td>no</td>
        <td>no</td>
        <td>yes * 3</td>
        <td>yes * 6</td>
        <td>N/A</td>
      </tr>

      <tr>
        <td>Personalised health advise</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
        <td>N/A</td>
      </tr>

 <tr>
        <td>Home Visit by Doctor</td>
        <td>no</td>
        <td>no</td>
        <td>no</td>
        <td>yes * 2</td>
        <td>N/A</td>
      </tr>

        <tr>
        <td>&nbsp;</td>
        <td><a class="tm-btn btn-blue" href="javascript:void(0)" data-toggle="modal" data-target="#myModal">subscribe now</a></td>
        <td><a class="tm-btn btn-blue" href="javascript:void(0)" data-toggle="modal" data-target="#myModal">subscribe now</a></td>
        <td><a class="tm-btn btn-blue" href="javascript:void(0)" data-toggle="modal" data-target="#myModal">subscribe now</a></td>
        <td><a class="tm-btn btn-blue" href="javascript:void(0)" data-toggle="modal" data-target="#myModal">subscribe now</a></td>
        <td><a class="tm-btn btn-blue" href="javascript:void(0)" data-toggle="modal" data-target="#myModal">subscribe now</a></td>
      </tr>


    </tbody>
  </table>
  </div>
</div>
</div>

			</div>
		</div>
	</div> --}}


	<div class="tm-app-download">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="app-description text-center">
						<h2>NOW IS available FOR YOUR SMARTPHONES</h2>
						<p>Our mission is to provide people in developing countries timely access to quality healthcare services from around the world when they need it in the most cost effective way! Download Now</p>
						<div class="app_descri_link">
						<a href="#"><img alt="app" src="{{asset('/assets/front/images/appstore.png')}}"></a>
						<a href="#"><img alt="app" src="{{asset('/assets/front/images/appstore-2.png')}}"></a>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>

@endsection