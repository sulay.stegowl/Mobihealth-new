@extends('frontend.layouts.app')
@section('content')

	<div class="tm-breadcrumb">
		<div class="container">
			<h1 class="tm-section-heading">New Page</h1>
			<ul>
				<li>
						<a href="{{route('homepage')}}">home</a>
				</li>
				<li>
					<a href="{{route('new_page')}}">/ New Page</a>
				</li>
			</ul>
		</div>
	</div>

	<div class="tm-department-detail">
		<div class="container">
			<div class="row">
				<!-- Sidebar -->
				<div class="col-sm-3">
					<div class="tm-sidebar">
						<!-- Other Services -->
						<div class="sidebar-widget sidebar-categories">
							<h4 class="widget-heading">categories</h4>
							<ul>
								<li>
									<a href="department-detail.html">Endodontics</a>
								</li>
								<li>
									<a href="department-detail.html">Diagnosis</a>
								</li>
								<li>
									<a href="department-detail.html">Prevention</a>
								</li>
								<li>
									<a href="department-detail.html">Operative Dentistry</a>
								</li>
								<li>
									<a href="department-detail.html">Diagnosis</a>
								</li>
							</ul>
						</div><!-- /Other Services -->
						<!-- Appointment -->
						<div class="sidebar-widget">
							<div class="tm-appointment">
								<h4 class="widget-heading">make an appointment</h4>
								<div class="appointment-form">
									<form>
										<div class="col-sm-12">
											<div class="form-group">
												<input class="form-control" name="full_name" placeholder="Full Name" type="text">
											</div>
										</div>
										<div class="col-sm-12">
											<div class="form-group">
												<input class="form-control" name="email" placeholder="Email" type="email">
											</div>
										</div>
										<div class="col-sm-12">
											<div class="form-group">
												<input class="form-control" name="phone" placeholder="Phone (optional)" type="text">
											</div>
										</div>
										<div class="col-sm-12">
											<div class="form-group">
												<div class="tm-datepicker">
													<input class="form-control" data-toggle="datepicker" name="dob" placeholder="Date of Birth" type="text">
												</div>
											</div>
										</div>
										<div class="col-sm-12">
											<div class="form-group">
												<div class="tm-datepicker">
													<input class="form-control" data-toggle="datepicker" name="date" placeholder="Choose Date" type="text">
												</div>
											</div>
										</div>
										<div class="col-sm-12">
											<div class="form-group">
												<div class="tm-timepicker">
													<select class="form-control" name="time">
														<option>
															01:30 AM
														</option>
														<option>
															02:30 AM
														</option>
														<option>
															03:30 AM
														</option>
														<option>
															04:30 AM
														</option>
														<option>
															05:30 AM
														</option>
														<option>
															06:30 AM
														</option>
														<option>
															07:30 AM
														</option>
														<option>
															08:30 AM
														</option>
														<option>
															09:30 AM
														</option>
													</select>
												</div>
											</div>
										</div>
										<div class="col-sm-12 text-right">
											<div class="form-group">
												<input class="tm-btn btn-blue" name="submit" type="submit" value="get appointment">
											</div>
										</div>
									</form>
								</div>
							</div>
						</div><!-- /Appointment -->
						<!-- Questions and Queries -->
						<div class="sidebar-widget questions">
							<h4 class="widget-heading">questions and queries</h4>
							<p>In case of emergency, contact us on the following number and we will immediately respond to you.</p>
							<ul>
								<li><span class="icon-phone"></span> 012 345 67 890</li>
								<li><span class="icon-at"></span> info@themesmill.com</li>
							</ul>
							<div class="separator"></div><a class="tm-btn btn-blue pdf" href="department-detail.html">download pdf</a>
						</div><!-- /Questions and Queries -->
					</div>
				</div> <!-- /Sidebar -->
				<!-- Content Area -->
				<div class="col-sm-6">
					<div class="general-content">
						<div class="dep-image"><img alt="medical" class="img-responsive" src="{{asset('/assets/front/images/service-detail-1.jpg')}}"></div>
						<h2 class="tm-box-heading">Department of Tooth Cleaning</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque fermentum massa vel enim feugiat gravida. Phasellus velit risus, euismod a lacus et, mattis condimentum augue. Vivamus fermentum ex quis imperdiet sodales. Sed aliquam nibh tellus, a rutrum turpis pellentesque ac. Nulla nibh libero, tincidunt cursus gravida ut, sodales ut magna. Sed sodales libero sapien, et rutrum mi placerat eget. Nulla vestibulum lacus vel eros eleifend molestie. Cras dapibus ullamcorper dictum. Vivamus nec erat placerat felis scelerisque porttitor in ac turpis. In nec imperdiet turpis. Suspendisse quis orci ut orci pulvinar eleifend. Nulla eu mattis ipsum. Integer eget sagittis nulla. Praesent consectetur lacus et maximus eleifend. Integer non lacus dui. Mauris tortor diam, laoreet quis commodo vitae, sodales vel augue.</p>
						<div class="separator"></div>
						<h4 class="tm-small-heading">Treatments</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi gravida fringilla neque sit amet sollicitudin. Duis aliquam dictum feugiat. Quisque elit tortor, ullamcorper quis ultrices vitae, mattis non est. Aliquam erat volutpat. Ut ac suscipit lorem. Suspendisse rhoncus tellus ac neque gravida, in tristique urna placerat. Morbi in purus rutrum, hendrerit tellus pulvinar, interdum nisl.</p>
						<ul class="general">
							<li>Lorem ipsum dolor sit amet</li>
							<li>Consectetur adipiscing elit</li>
							<li>Morbi gravida fringilla neque sit amet sollicitudin</li>
							<li>Duis aliquam dictum feugiat</li>
							<li>Quisque elit tortor, ullamcorper quis ultrices vitae</li>
						</ul><!-- Our Dentists -->
						<!-- /Our Dentists -->
						<div class="separator"></div><!-- Appointment -->
						
					</div>
				</div>
				<div class="col-sm-3">
					<div class="tm-sidebar">
						<!-- Other Services -->
						<div class="sidebar-widget sidebar-categories">
							<h4 class="widget-heading">categories</h4>
							<ul>
								<li>
									<a href="department-detail.html">Endodontics</a>
								</li>
								<li>
									<a href="department-detail.html">Diagnosis</a>
								</li>
								<li>
									<a href="department-detail.html">Prevention</a>
								</li>
								<li>
									<a href="department-detail.html">Operative Dentistry</a>
								</li>
								<li>
									<a href="department-detail.html">Diagnosis</a>
								</li>
							</ul>
						</div><!-- /Other Services -->
						<!-- Appointment -->
						<div class="sidebar-widget">
							<div class="tm-appointment">
								<h4 class="widget-heading">make an appointment</h4>
								<div class="appointment-form">
									<form>
										<div class="col-sm-12">
											<div class="form-group">
												<input class="form-control" name="full_name" placeholder="Full Name" type="text">
											</div>
										</div>
										<div class="col-sm-12">
											<div class="form-group">
												<input class="form-control" name="email" placeholder="Email" type="email">
											</div>
										</div>
										<div class="col-sm-12">
											<div class="form-group">
												<input class="form-control" name="phone" placeholder="Phone (optional)" type="text">
											</div>
										</div>
										<div class="col-sm-12">
											<div class="form-group">
												<div class="tm-datepicker">
													<input class="form-control" data-toggle="datepicker" name="dob" placeholder="Date of Birth" type="text">
												</div>
											</div>
										</div>
										<div class="col-sm-12">
											<div class="form-group">
												<div class="tm-datepicker">
													<input class="form-control" data-toggle="datepicker" name="date" placeholder="Choose Date" type="text">
												</div>
											</div>
										</div>
										<div class="col-sm-12">
											<div class="form-group">
												<div class="tm-timepicker">
													<select class="form-control" name="time">
														<option>
															01:30 AM
														</option>
														<option>
															02:30 AM
														</option>
														<option>
															03:30 AM
														</option>
														<option>
															04:30 AM
														</option>
														<option>
															05:30 AM
														</option>
														<option>
															06:30 AM
														</option>
														<option>
															07:30 AM
														</option>
														<option>
															08:30 AM
														</option>
														<option>
															09:30 AM
														</option>
													</select>
												</div>
											</div>
										</div>
										<div class="col-sm-12 text-right">
											<div class="form-group">
												<input class="tm-btn btn-blue" name="submit" type="submit" value="get appointment">
											</div>
										</div>
									</form>
								</div>
							</div>
						</div><!-- /Appointment -->
						<!-- Questions and Queries -->
						<div class="sidebar-widget questions">
							<h4 class="widget-heading">questions and queries</h4>
							<p>In case of emergency, contact us on the following number and we will immediately respond to you.</p>
							<ul>
								<li><span class="icon-phone"></span> 012 345 67 890</li>
								<li><span class="icon-at"></span> info@themesmill.com</li>
							</ul>
							<div class="separator"></div><a class="tm-btn btn-blue pdf" href="department-detail.html">download pdf</a>
						</div><!-- /Questions and Queries -->
					</div>
				</div> 
			</div>
		</div>
	</div> 

@endsection