@extends('frontend.layouts.app')
@section('content')

<div class="tm-breadcrumb">
		<div class="container">
			<h1 class="tm-section-heading">Service Detail</h1>
			<ul>
				<li>
					<a href="{{route('homepage')}}">home</a>
				</li>
				<li>
					<a href="{{route('services_details')}}">/ INSTANT VIDEO CONSULTATION</a>
				</li>
			</ul>
		</div>
	</div>

	<div class="tm-service-detail-wrap">
		<div class="container">
			<div class="row">
			
				<div class="col-sm-4">
					<div class="tm-sidebar">
					
						<div class="sidebar-widget questions">
							<h4 class="widget-heading">questions and queries</h4>
							<p>Email us if any queries.</p>
							<ul>
								
								<li><span class="icon-at"></span> info@mobihealthinternational.com</li>
							</ul>
						
						</div>
					</div>
				</div>
				
				<div class="col-sm-8">
					<div class="tm-service-detail">
						<div class="service-detail-thumb"><img alt="service" src="{{asset('/assets/front/extra-images/IMG-20171007_WA0002.jpg')}}"></div>
						<h4 class="service-title">INSTANT VIDEO CONSULTATION</h4>
						<p>Immediate Video consultation with a doctor via your mobile device. This eliminates long waits for doctor appointments, delayed diagnosis and suffering. Get diagnosed safely from the comfort of your home, anywhere &  anytime</p>
					
					</div>
				</div>
			</div>
		</div>
	</div>

	@endsection