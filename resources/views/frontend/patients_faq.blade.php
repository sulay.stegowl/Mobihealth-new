@extends('frontend.layouts.app')
@section('content')

<div class="tm-breadcrumb">
		<div class="container">
			<h1 class="tm-section-heading">patients</h1>
			<ul>
				<li>
					<a href="{{route('homepage')}}">home</a>
				</li>
				<li>
					<a href="{{route('patients_faq')}}">/ Faq</a>
				</li>
			</ul>
		</div>
	</div><!-- /Breadcrumb header -->
	<!-- Shop -->
	<div class="tm-shop-list">

		
		<div class="container">
		

		<h1 class="tm-section-heading">faq</h1>
		
		<div class="how_it_work">

<div class="container">

</div>


<div class="container">
 
  <div class="panel-group" id="accordion">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">Is Mobihealth a Health Insurance Company?</a>
        </h4>
      </div>
      <div id="collapse1" class="panel-collapse collapse in">
        <div class="panel-body">
          <p>
NO! We are not a Health Insurance company. In fact, Insurance companies are leveraging on telemedicine providers like us to reduce need for hospital visits and high cost.
</p>
          <p>Mobihealth Is a Telemedicine and Healthcare service platform that connects millions of patients across Africa to carefully selected licensed doctors worldwide. More than 60% of ED visits are for non-acute medical conditions and research has shown that these can be safely diagnosed and treated through virtual consultation. Our goal is to bridge the gap in healthcare access and delivery in developing countries by taking advantage of telemedicine and digital revolution. We are also a healthcare service One-stop-portal connecting patients to quality healthcare providers locally and this is accessible through our mobile apps or website.
Our doctors see all range of medical problems that includes acute non-emergency cases such as Malaria, Chest Infections, skin disease, GI problems, Infections etc. If you have life threatening emergency please go to/call your nearest hospital/emergency line. Our platform only caters for acute not life threatening emergencies. We pay for all the care our subscribers receive through this platform and the other treatments or investigations when referred to local providers. You don’t have to worry about out-of-pocket payment. However only paid subscribers are granted access to consultations and services.
</p>
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
 Do you take patients with Pre-existing Medical Conditions?
</a>
        </h4>
      </div>
      <div id="collapse2" class="panel-collapse collapse">
        <div class="panel-body">We provide consultations and treatment for acute conditions. Patients with ‘’significant’’ pre-existing medical conditions’’ should contact us for further advise. Our regular subscription bundles does NOT apply to patients with significant co-morbidities. We however can provide advise on individualized plans. Our PAYG video consultation and prescription only services can also be used by anyone including those with co-existing medical problems. Please contact us for more details.</div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">How can I request consultation?</a>
        </h4>
      </div>
      <div id="collapse3" class="panel-collapse collapse">
        <div class="panel-body">You can request consultation anytime anywhere through Mobihealth app on your smart phone or through your laptop through our secured telemedicine and healthcare service platform seeking care for acute non-emergency medical conditions. You can browse services by specialty or location and will see services from licensed providers who are available for consultation within a time slots. You will be able to request a particular specialist and we will endeavour to send them your consultation request to see if they are available, if not, any other doctor will be able to see you.  As a policy family and friends are not allowed to see or treat patients. You agree not to abuse the use of the platform and to abide by the Terms and Conditions on our website. It is your responsibility to ensure you are prepared before accepting video consultation. To ensure privacy and minimise background distractions, ensure good lightning. Please be aware that your consultations may be recorded for quality and monitoring purposes. </div>
      </div>
    </div>

      <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">What equipment do I need to conduct a video visit?</a>
        </h4>
      </div>
      <div id="collapse4" class="panel-collapse collapse">
        <div class="panel-body">You can conduct video consultations from the comfort of your home/office anytime anywhere You’ll just need one of the following:
          <ul class="list_style_none">
            <li>A computer with an internal or external camera. You can use a wide range of web browsers such as Chrome, Explorer or Firefox.</li>
            <li>An Apple mobile phone or iPad with the camera enabled and our provider app downloaded. We recommend using a stand for your phone or iPad during visits to keep the camera stable.</li>
            <li>Ear Phone</li>
          </ul></div>
      </div>
    </div>

      <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse5"> How do I conduct a physical exam remotely?</a>
        </h4>
      </div>
      <div id="collapse5" class="panel-collapse collapse">
        <div class="panel-body">
    {{-- <img src="{{asset('assets/front/extra-images/IMG-20171007_WA0002.jpg')}}" alt="" style="height: 300px;width: auto"> --}}
    <p>Many aspects of the in-person physical exam can also be replicated remotely using webcam or Telehealth devices which are FDA Approved (e.g www.tytocare.com). The recordings are transmitted to the doctor remotely via secured platform to enhance virtual consultation experience and accuracy in diagnosis. </p>

    <p>For those patients who don’t have these devices, the doctor will be able to visually assess them through realtime capabilities on their smart phone/webcam, and can accurately gauge general distress, skin tone, clarity of thought and speech, respiratory rate, work of breathing, gait, etc. You can also assist the doctor in physical examination by following his instructions to palpate areas of tenderness, assess ROM, assist in moving the camera to visualize areas such as the oropharynx, and assist in abdominal self-exams. In conjunction with a thorough history, many providers feel that the information they can obtain from a video visit is sufficient for accurate diagnosis of many conditions. Don’t worry about quality of video. Our customized video software is specifically designed for low bandwidth, difficult topography with fluctuating signal levels and this is provided by US based industry leader used by NASA, Barclays, HSBC and many reputable industry names. Our software produces a quality stable video consultation experience that ensures accurate diagnosis and management. </p>
         </div>
      </div>
    </div>

     <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse6"> Where can I accept video consultation?</a>
        </h4>
      </div>
      <div id="collapse6" class="panel-collapse collapse">
        <div class="panel-body">
          <img src="{{asset('assets/front/extra-images/IMG-20171007_WA0002.jpg')}}" alt="" style="height: 300px;width: auto">
          <p>
You can request an appointment from any private location. Keep in mind that you cannot take an appointment with anyone else in the room with you unless you have given them permission to be there. To ensure patient privacy, we recommend taking appointments from your home or office with the door closed and no background noise or distractions. Doctors will be able to focus on you better if the environment behind you is a light solid color, clean, simple and not distracting. A light blue backdrop is preferable for the optimal video experience. Position yourself in front of a blank wall instead of a wall full of bookshelves.
</p>
<p>Also note that although it’s a virtual visit you still have to present yourself like you do when you visit your doctor in hospital/clinic. Our doctors speak mainly English language but may also speak other foreign languages. The official language of communication on this platform is currently English. It is your responsibility to ensure you have an interpreter with you during consultation so that you are clearly understood and that you understand the doctor as well. Consultations may be recorded for quality &training purposes but can also be called by us if any medicolegal issues.</p>
        </div>
      </div>
    </div>

      <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse7">What type of training do you offer?</a>
        </h4>
      </div>
      <div id="collapse7" class="panel-collapse collapse">
        <div class="panel-body">We offer video-based training on how to use the Mobihealth provider platform. This you can access at any time from anywhere. You can also schedule a live practice visit with one of our team members to familiarize yourself with the process and ask questions along the way. We provide access to the training once you’ve completed sign-up and we’ve approved your credentials.</div>
      </div>
    </div>

       <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse8"> How much does this cost me?</a>
        </h4>
      </div>
      <div id="collapse8" class="panel-collapse collapse">
        <div class="panel-body">We have various subscription plan. Please visit our subscription page for more info</div>
      </div>
    </div>


       <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse9">  How do the doctors and other service providers get paid?</a>
        </h4>
      </div>
      <div id="collapse9" class="panel-collapse collapse">
        <div class="panel-body">All our providers are paid directly by us. You do not have to pay for consultation, medicines, investigations or hospital bills as long as you are subscribed. No reimbursement paperwork required from you when we refer you for hospital treatment. We have direct partnership agreements with our partner providers.
          <p>It takes minimum of 48hours to activate your subscription so please ensure you are subscribed well ahead of your need to use the service. However we also have PAYG plans for on-demand request, this is available anytime and you can use same day. </p>
        </div>
      </div>
    </div>


 {{--  <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse10"> How does documentation work with a video visit?</a>
        </h4>
      </div>
      <div id="collapse10" class="panel-collapse collapse">
        <div class="panel-body">The Mobihealth EHR and Video Platform lets you document notes and offers a comprehensive formulary including local guidelines. You can choose from our standard notes templates or create your own that can be completed during or after the video visit. The platform also supports secure messaging, patient education materials, e-prescribing and sick slips. If you choose to document during the video visit, it is helpful to tell the patient what you are doing when you are looking away from the patient video. A simple “I’m just looking up some details on your condition” or “I’m locating your prescription now” will put the patient at ease. Our platform is very secured and HIPAA compliant.</div>
      </div>
    </div> --}}


  <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse11"> How do I get prescription?</a>
        </h4>
      </div>
      <div id="collapse11" class="panel-collapse collapse">
        <div class="panel-body">Our doctors are able to e-prescribe and manage medications with the highest level of e-prescribing integration including full local formulary*, up-to-date prescriptions and geolocation pharmacy selection. There must be video connection in order to prescribe; physicians cannot prescribe based on a phone or chat encounter. We have partnership with reputable manufacturers and distributors worldwide. You can be sure you are getting genuine medications and at best prices possible (for PAYG customers). We have our in-house pharmacies as additional layer of Quality Assurance.</div>
      </div>
    </div>

      <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse12">What can Doctors prescribe?</a>
        </h4>
      </div>
      <div id="collapse12" class="panel-collapse collapse">
        <div class="panel-body">Our doctors can prescribe various medications same as with face-to-face consultations. But there has to be a video consultation for that to happen. Medications below are excluded from the formulary:

          <ul class="list_style_none">
            <li> Controlled substances (narcotics, anxiety medications, ADHD medications)</li>
            <li> Muscle relaxants</li>
            <li> Medications for erectile dysfunction</li>
            <li> Any additional state specific controlled medications (additional pain medications, pseudoephedrine)</li>
          </ul>
          <p>Repeat prescriptions can be ordered. This are not included in plans and the medicines have to be paid for.</p>
        </div>
      </div>
    </div>
    
      <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse13">What are the licensure requirements for doctors practicing with Mobihealth?</a>
        </h4>
      </div>
      <div id="collapse13" class="panel-collapse collapse">
        <div class="panel-body">Our doctors are carefully selected and go through a very detailed scrutinizing process to ensure we present only the best to you. They are licensed to practice in the UK, USA, Australia, New Zealand, Canada, Nigerian, Tanzania, Ghana, Ethiopia, Sierra Leone and other countries. They have an active medical licenses and are in good standing with their respective regulatory bodies with no disciplinary actions, restrictions. If you have any complaint about any doctor’s behaviour please contact complaint@mobihealthinternational.com. </div>
      </div>
    </div>

   <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse14"> Do your virtual doctors have malpractice insurance?</a>
        </h4>
      </div>
      <div id="collapse14" class="panel-collapse collapse">
        <div class="panel-body">Our doctors have comprehensive malpractice cover and we also provide indemnity for work done on our platform. </div>
      </div>
    </div>



  </div> 
</div>
    



		</div>

		
		</div><!-- /Shop -->
	</div>
	@endsection
