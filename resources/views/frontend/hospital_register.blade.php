

		@extends('frontend.layouts.app')
@section('content')
	<div class="page_loader">  <p><img src="{{asset('assets/front/images/loading.gif')}}" alt=""></p> </div>

	<div class="tm-breadcrumb">
		<div class="container">
			<h1 class="tm-section-heading">register</h1>
			<ul>
				<li>
					<a href="{{route('homepage')}}">home</a>
				</li>
				<li>
					<a href="{{route('hospital_register')}}">/ register</a>
				</li>
			</ul>
		</div>
	</div>

	<div class="register_page">
		<div class="container">
		

@include('include.message')
	
	<div class="dis_inline">
			
			<div class="how_it_faq">
				<a href="{{route('hospital_how')}}" class="how_it_work">How It Works</a>
				<a href="{{route('hospital_faq')}}" class="faq">Faq</a>
			</div>
		</div>

		{{-- <div class="note">
				<p>	Pls send your CV to following email addresses: </p>
				<p> <strong>Hospitals:</strong>hospital@mobihealthinternational.com </p>
			</div> --}}

	


			<div class="register_form">
				<form action="{{route('submit_register')}}" method="post">
					
					{{csrf_field()}}
					<div class="form_group">
						<label>Name of business</label>
						<input type="text" name="business_name" value=""  placeholder="Enter Name of business">
					</div>

					<input type="hidden" name="business_type" value="Hospital">
					
					{{-- <div class="form-group form_group">
                        <label class="control-label">Type of business</label>
                        <select name="business_type" id="">
                            <option value="Hospital">Hospital</option>
                            <option value="Pharmacies">Pharmacies</option>
                            <option value="Pathology">Pathology</option>
                            <option value="Radiology">Radiology</option>
                        </select>
                    </div> --}}
					<div class="form_group">
						<label>Contact person</label>
						<input type="text" name="contact_person" value="{{old('contact_person')}}"  placeholder="Enter Contact person" >
					</div>
					<div class="form_group">
						<label>Contact email </label>
						<input type="text" id="email" name="contact_email" value="{{old('contact_email')}}" placeholder="Enter Email">
						<div class="email_url">Enter Valid Email Address</div>
					</div>
					<div class="form_group">
						<label>Whatsapp no</label>
						<input type="number" onkeydown="if(this.value.length==15 && event.keyCode>47 && event.keyCode < 58)return false;" name="whatsapp_no" value="{{old('whatsapp_no')}}"  placeholder="Enter Whatsapp no">
					</div>
					<div class="form_group">
						<label>Website</label>
						<input type="text" name="website" id="website" value="www."  placeholder="Enter Website">
						<div class="web_url">Enter Valid Url</div>
					</div>

					

			{{-- 		<div class="form_group radio_group dis_block">
                                            <label>Do you offer home laboratory service?</label>
									<div class="dis_flex_2">
                        <input type="radio" name="home_laboratory_service"  value="yes">
                        <p> Yes </p>
                        <input type="radio" name="home_laboratory_service" value="no">
                        <p> No </p>
									</div>

                                        </div> --}}



<div class="form_group radio_group dis_block">
                                            <label>Do you offer online order?</label>
									<div class="dis_flex_2">
                        <input type="radio" name="online_order"  value="yes">
                        <p> Yes </p>
                        <input type="radio" name="online_order" value="no">
                        <p> No </p>
									</div>

                                        </div>


{{-- <div class="form_group radio_group dis_block">
                                            <label>Do you offer home delivery of medications?</label>
									<div class="dis_flex_2">
                        <input type="radio" name="home_delivery_medications"  value="yes">
                        <p> Yes </p>
                        <input type="radio" name="home_delivery_medications" value="no">
                        <p> No </p>
									</div>

                                        </div> --}}


					
					<div class="form_group">
						<label>Free text for additional detail</label>
						<textarea name="additional_detail" id="" cols="30" rows="10" class="custom" ></textarea>
					</div>

					<div class="form_group page_loader_div">
						<input type="submit" value="Submit">
					</div>
				</form>

			</div>
		</div>
	</div>

@endsection