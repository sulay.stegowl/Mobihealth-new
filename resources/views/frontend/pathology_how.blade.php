@extends('frontend.layouts.app')
@section('content')

<div class="tm-breadcrumb">
		<div class="container">
			<h1 class="tm-section-heading">pathology</h1>
			<ul>
				<li>
					<a href="{{route('homepage')}}">home</a>
				</li>
				<li>
					<a href="{{route('pathology_how')}}">/ How It Work</a>
				</li>
			</ul>
		</div>
	</div><!-- /Breadcrumb header -->
	<!-- Shop -->
	<div class="tm-shop-list">

		
		<div class="container">
		

		<h1 class="tm-section-heading">Diagnostic pathologies: How it works!</h1>
		
		<div class="how_it_work">
			<div class="title">Benefits for your business</div>
			<ul class="list_style_none">
				<li>Streamline Online Booking system</li>
				<li>Increase sales and revenue</li>
				<li>Improve visibility for your business</li>
				<li>Regular and reliable source of revenue</li>
				<li>Access to wide base of medical experts and colleagues from around the world for enhanced diagnosis and management</li>
				<li>Efficient management of resources</li>
				<li>Reduce running cost</li>
			</ul>

			<div class="title">Step 1 </div>

			<div class="row">
				<div class="col-sm-6">
					<div class="title">Sign Up</div>
			<p>Sign up using our secure sign-up and onboarding application which enables you to create your unique online Business profile. You will also be asked to provide following:</p>
			<img src="{{asset('assets/front/images/image1.png')}}" alt="">		
				</div>
				<div class="col-sm-6">
					<div  class="title" >PHOTO </div>
			<p>Provide a high quality, professional photo for your business to appear on your public profile.</p>
			<img src="{{asset('assets/front/images/image2.png')}}" alt="">		
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6">
			<div class="title">DIAGNOSTIC CENTRE NAME AND MAIN CONTACT PERSON’S NAME, EMAIL & PHONE(relevant for appointment booking and confirmation etc) </div>

			<img src="{{asset('assets/front/images/image4.png')}}" alt="">		
				</div>
				<div class="col-sm-6">
			<div class="title">COMPANY REGISTRATION AND REGULATORY No.</div>

			<p>We’ll use this to verify your credentials.</p>

			<img src="{{asset('assets/front/images/image3.png')}}" alt="">		
				</div>
			</div>
			
			
			<div class="row">
				<div class="col-sm-6">

			<div class="title">Identity Verification/proof of address</div>

			<p>We’ll ask for personal information of principal owner/Management </p>

			

			<img src="{{asset('assets/front/images/image6.png')}}" alt="">

				</div>

				<div class="col-sm-6">


			<div class="title">BANK DETAILS</div>

			<p>Provide your bank details for payment of your invoice.</p>

				</div>
			</div>

			

			


			<div class="title  text-center">Step 2 - Sign In</div>
			<p>Once your online profile is enabled, we’ll send you login credentials for our provider platform, which you can access from your computer or iOS mobile device.</p>
			<p>We offer video tutorials to familiarize you with our platform, and you can schedule a live test visit with a member of our team to walk through the process and ask questions.</p>

			<div class="title text-center">Step 3 – INTEGRATE INTO OUR SECURE ONLINE PLATFORM</div>

			<p>Through our secure EHR systems we are able to book diagnostic tests in your center once requested by our doctors. We expect you to provide confirmation of date and time which we then communicate to patients on your behalf. We can then view results once authorized from your lab for our doctor’s review and further management of patients</p>

			<p>If questions or technical issues come up, we’re here for you. You’ll have 24/7 access to our support team by phone and email.</p>

			<div class="title text-center">Step 4 - Get Paid</div>
			<p>Once tests are completed and invoices are raised, we’ll automatically deposit your money into designated bank account on a weekly/monthly basis depending on preference ensuring regular cash flow for your business. Since the costs of diagnostic work-up are paid by Mobihealth you don’t need to worry about patient’s ability to pay, no insurance or any reimbursement hassles. Get test done, get paid!</p>




		</div>

		
		</div><!-- /Shop -->
	</div>
	@endsection
