@extends('frontend.layouts.app')
	@section('content')
				<div class="new_form_page">
				   <div class="container">
				      <div class="register_form new_form">

				         <div class="form-group form_group">
				            <label class="control-label">Profession</label>
				            <select name="Profession" id="" required="required">
				               <option value="Doctor">Doctor</option>
				               <option value="Dentist">Dentist</option>
				               <option value="Radiologist">Radiologist</option>
				               <option value="Nurse">Nurse</option>
				               <option value="Physiotherapist">Physiotherapist</option>
				               <option value="Laboratory_Technician">Laboratory Technician</option>
				            </select>
				         </div>

						<h1>Personal Details </h1>

				         <div class="form-group form_group">
				            <label class="control-label">Title</label>
				            <input class="form-control" type="text" name="Title" value="{{old('Title')}}" placeholder="Enter Title" required="required">
				         </div>

				         <div class="form-group form_group">
				            <label class="control-label">First Name</label>
				            <input class="form-control" type="text" name="First_Name" value="{{old('First_Name')}}" placeholder="Enter First Name" required="required"> 
				         </div>

				         <div class="form_group">
				            <label>Surname</label>
				            <input class="form-control" type="text" name="Surname" value="{{old('Surname')}}" placeholder="Enter Surname Name" required="required">
				         </div>  

				         <div class="form_group radio_group">
				            <label>Sex</label>
				            <input type="radio" name="gender" value="male">
				            <p> Male </p>
				            <input type="radio" name="gender" value="female">
				            <p> Female </p>
				         </div>

				         <div class="form_group datepicker">
				            <label>Date of Birth</label>
				            <input type="text" name="Date_of_Birth" value="{{old('Date_of_Birth')}}" placeholder="Enter Date of Birth" required="required">
				         </div>


				         <div class="form_group">
				            <label>Marital Status</label>
				            <input type="text" name="Marital_Status" value="{{old('Marital_Status')}}" placeholder="Enter Marital Status" required="required">
				         </div>

				         <div class="form_group">
				            <label>Other Names</label>
				            <input type="text" name="Other_Names" value="{{old('Other_Names')}}" placeholder="Enter Other Names" required="required">
				         </div>


<h1>Contact Details</h1>


				         <div class="form_group text_area">
				            <label>Current Address</label>
				            <textarea name="Current_Address" id="" cols="30" rows="10" required="required">{{old('Current_Address')}}</textarea>
				         </div>

				         <div class="form_group">
				            <label>Home Telephone</label>
				            <input type="number" name="Home_Telephone" value="{{old('Home_Telephone')}}" placeholder="Enter Home Telephone" required="required" onkeydown="if(this.value.length>14 && event.keyCode>47 && event.keyCode < 58)return false;">
				         </div>

				         <div class="form_group">
				            <label>Mobile</label>
				            <input type="number" name="Mobile" value="{{old('Mobile')}}" placeholder="Enter Mobile" required="required" onkeydown="if(this.value.length>14 && event.keyCode>47 && event.keyCode < 58)return false;">
				         </div>

				         <div class="form_group">
				            <label>Post Code</label>
				            <input type="number" name="Post_Code" value="{{old('Post_Code')}}" placeholder="Enter Post Code" required="required" onkeydown="if(this.value.length>14 && event.keyCode>47 && event.keyCode < 58)return false;">
				         </div>

				         <div class="form_group">
				            <label>Work Telephone</label>
				            <input type="number" name="Work_Telephone" value="{{old('Work_Telephone')}}" placeholder="Enter Work Telephone" required="required" onkeydown="if(this.value.length>14 && event.keyCode>47 && event.keyCode < 58)return false;">
				         </div>

				         <div class="form_group">
				            <label>Ext. or Bleep</label>
				            <input type="text" name="Ext_Bleep" value="{{old('Ext_Bleep')}}" placeholder="Enter Ext. or Bleep" required="required" >
				         </div>

				         <div class="form_group">
				            <label>Email Address</label>
				            <input type="email" name="Email_Address" value="{{old('Email_Address')}}" placeholder="Enter Email Address" required="required" >
				         </div>

				         <h1>Emergency Contact Details </h1>

						

				         <div class="form_group">
				            <label>Next of Kin: </label>
				            <input type="text" name="Next_of_Kin" value="{{old('Next_of_Kin')}}" placeholder="Enter Next of Kin:" required="required" >
				         </div>

						

						<div class="form_group">
				            <label>Relationship</label>
				            <input type="text" name="Relationship" value="{{old('Relationship')}}" placeholder="Enter Relationship" required="required">
				         </div>

				         <div class="form_group">
				            <label>1st Contact No</label>
				            <input type="number" name="Contact_No_1" value="{{old('Contact_No_1')}}" placeholder="Enter 1st Contact No" required="required" onkeydown="if(this.value.length>14 && event.keyCode>47 && event.keyCode < 58)return false;">
				         </div>

				         <div class="form_group">
				            <label>2st Contact No</label>
				            <input type="number" name="Contact_No_2" value="{{old('Contact_No_2')}}" placeholder="Enter 2st Contact No" required="required" onkeydown="if(this.value.length>14 && event.keyCode>47 && event.keyCode < 58)return false;">
				         </div>


				         <div class="form_group text_area">
				            <label>Address</label>
				            <textarea name="Address" id="" cols="30" rows="10" required="required">{{old('Address')}}</textarea>
				         </div>

				         <h1>Nationality</h1>
						
						  <div class="form-group form_group">
				            <label class="control-label">countries</label>
				            <select name="countries" id="" required="required">
				               <option value="countries">countries 1</option>
				               <option value="countries">countries 2</option>
				               <option value="countries">countries 3</option>
				               <option value="countries">countries 4</option>
				               <option value="countries">countries 5</option>
				               <option value="countries">countries 6</option>
				            </select>
				         </div>

						 <div class="form_group">
				            <label>Passport No</label>
				            <input type="number" name="Passport_No" value="{{old('Passport_No')}}" placeholder="Enter Passport No" required="required" onkeydown="if(this.value.length>14 && event.keyCode>47 && event.keyCode < 58)return false;">
				         </div>

				          <div class="form_group datepicker">
				            <label>Issued At</label>
				            <input type="text" name="Issued_At" value="{{old('Issued_At')}}" placeholder="Enter Issued At" required="required">
				         </div>

				         <div class="form_group datepicker">
				            <label>	Expiry Date</label>
				            <input type="text" name="Expiry_Date" value="{{old('Expiry_Date')}}" placeholder="Enter Expiry Date" required="required">
				         </div>
	
						<h1>Proof of Identification </h1>

						<div class="form_group radio_group">
				            <label>Do you hold a current Driving Licence? </label>
				            <input type="radio" name="Driving_Licence" value="yes">
				            <p> yes </p>
				            <input type="radio" name="Driving_Licence" value="no">
				            <p> no </p>
				         </div>

				         <div class="form_group radio_group">
				            <label>Do you have your own transport? </label>
				            <input type="radio" name="own_transport" value="yes">
				            <p> yes </p>
				            <input type="radio" name="own_transport" value="no">
				            <p> no </p>
				         </div>
						
						<h1>Bank Details </h1>

						<div class="form_group ">
				            <label>Bank Name: </label>
				            <input type="text" name="Bank_Name" value="{{old('Bank_Name')}}" placeholder="Enter Bank Name" required="required">
				         </div>

				         <div class="form_group ">
				            <label>Account Name: </label>
				            <input type="text" name="Account_Name" value="{{old('Account_Name')}}" placeholder="Enter Account Name" required="required">
				         </div>

				          <div class="form_group text_area">
				            <label>Branch Address</label>
				            <textarea name="Branch_Address" id="" cols="30" rows="10" required="required">{{old('Branch_Address')}}</textarea>
				         </div>

						<div class="form_group ">
				            <label>IBAN:</label>
				            <input type="text" name="IBAN" value="{{old('IBAN')}}" placeholder="Enter IBAN" required="required">
				         </div>

				         <div class="form_group ">
				            <label>SWIFT/BIC:</label>
				            <input type="text" name="SWIFT_BIC" value="{{old('SWIFT_BIC')}}" placeholder="Enter SWIFT BIC " required="required">
				         </div>

				         <div class="form_group ">
				            <label>Post Code</label>
				            <input type="number" name="Post_Code" value="{{old('Post_Code')}}" placeholder="Enter Post Code" required="required">
				         </div>

				         <div class="form_group ">
				            <label>Account No</label>
				            <input type="number" name="Account_No" value="{{old('Account_No')}}" placeholder="Enter Account No" required="required">
				         </div>

				         <div class="form_group ">
				            <label>Sort Code</label>
				            <input type="text" name="Bank_Post_Code" value="{{old('Bank_Post_Code')}}" placeholder="Enter Sort Code" required="required">
				         </div>

				         <div class="form_group ">
				            <label>Reference</label>
				            <input type="text" name="Reference" value="{{old('Reference')}}" placeholder="Enter Post Code" required="required">
				         </div>

						<h1>Limited Company Bank Details </h1>

						<div class="form_group ">
				            <label>Bank Name: </label>
				            <input type="text" name="Limi_Bank_Name" value="{{old('Limi_Bank_Name')}}" placeholder="Enter Bank Name" required="required">
				         </div>

				         <div class="form_group ">
				            <label>Account Name: </label>
				            <input type="text" name="Limi_Account_Name" value="{{old('Limi_Account_Name')}}" placeholder="Enter Account Name" required="required">
				         </div>

				          <div class="form_group text_area">
				            <label>Branch Address</label>
				            <textarea name="Limi_Branch_Address" id="" cols="30" rows="10" required="required">{{old('Limi_Branch_Address')}}</textarea>
				         </div>

						<div class="form_group ">
				            <label>IBAN:</label>
				            <input type="text" name="Limi_IBAN" value="{{old('Limi_IBAN')}}" placeholder="Enter IBAN" required="required">
				         </div>

				         <div class="form_group ">
				            <label>SWIFT/BIC:</label>
				            <input type="text" name="Limi_SWIFT_BIC" value="{{old('Limi_SWIFT_BIC')}}" placeholder="Enter SWIFT BIC " required="required">
				         </div>

				         <div class="form_group ">
				            <label>Post Code</label>
				            <input type="number" name="Limi_Post_Code" value="{{old('Limi_Post_Code')}}" placeholder="Enter Post Code" required="required">
				         </div>

				         <div class="form_group ">
				            <label>Account No</label>
				            <input type="number" name="Limi_Account_No" value="{{old('Limi_Account_No')}}" placeholder="Enter Account No" required="required">
				         </div>

				         <div class="form_group ">
				            <label>Sort Code</label>
				            <input type="text" name="Limi_Sort_Code" value="{{old('Limi_Sort_Code')}}" placeholder="Enter Sort Code" required="required">
				         </div>

				         <div class="form_group ">
				            <label>Reference</label>
				            <input type="text" name="Limi_Reference" value="{{old('Limi_Reference')}}" placeholder="Enter Reference" required="required">
				         </div>

				         <h1>Your Current  Employer</h1>


				         <div class="form_group ">
				            <label>Name</label>
				            <input type="text" name="Cur_Name" value="{{old('Name')}}" placeholder="Enter Name" required="required">
				         </div>

				          <div class="form_group text_area">
				            <label>Address</label>
				            <textarea name="Cur_Address" id="" cols="30" rows="10" required="required">{{old('Address')}}</textarea>
				         </div>
								
						 <div class="form_group ">
				            <label>Post Code</label>
				            <input type="text" name="Cur_Post_Code" value="{{old('Post_Code')}}" placeholder="Enter Post Code" required="required">
				         </div>

				         <div class="form_group ">
				            <label>Telephone</label>
				            <input type="number" name="Cur_Telephone" value="{{old('Telephone')}}" placeholder="Enter Telephone" required="required" onkeydown="if(this.value.length>14 && event.keyCode>47 && event.keyCode < 58) return false;">
				         </div>

						<h1>Education and Training including Post Qualification Experience </h1>

						<div class="form_group text_area">
				            <label>University/Institution/Training:</label>
				            <textarea name="Training" id="" cols="30" rows="10" required="required">{{old('Training')}}</textarea>
				         </div>
							
						<div class="form_group ">
				            <label>Qualification</label>
				            <input type="text" name="Qualification" value="{{old('Qualification')}}" placeholder="Enter Qualification" required="required">
				         </div> 

						
						<div class="form_group datepicker">
				            <label> Date Graduated</label>
				            <input type="text" name="Date_Graduated" value="{{old('Date_Graduated')}}" placeholder="Enter Date Graduated" required="required">
				         </div>  

						<h1>Insurance </h1>
			
						<div class="form_group radio_group">
				            <label>Do you hold Professional Indemnity Insurance</label>
				            <input type="radio" name="Professional_Insurance" value="yes">
				            <p> yes </p>
				            <input type="radio" name="Professional_Insurance" value="no">
				            <p> no </p>
				         </div>
						
						<div class="form_group ">
				            <label>Name of Insurer:</label>
				            <input type="text" name="Pro_Name_Insurer" value="{{old('Pro_Name_Insurer')}}" placeholder="Enter Name of Insurer" required="required">
				         </div>  

				         <div class="form_group ">
				            <label>Policy No</label>
				            <input type="text" name="Pro_Policy_No" value="{{old('Pro_Policy_No')}}" placeholder="Enter Policy No" required="required">
				         </div>

				         <div class="form_group datepicker">
				            <label>Date of Issue</label>
				            <input type="text" name="Pro_Date_of_Issue" value="{{old('Pro_Date_of_Issue')}}" placeholder="Enter Date of Issue" required="required">
				         </div> 

				         <div class="form_group datepicker">
				            <label>Date of Expiry</label>
				            <input type="text" name="Pro_Date_of_Expiry" value="{{old('Pro_Date_of_Expiry')}}" placeholder="Enter Date of Expiry" required="required">
				         </div> 



				         <div class="form_group radio_group">
				            <label>Do you hold Medical Malpractice Insurance</label>
				            <input type="radio" name="Medical_Insurance" value="yes">
				            <p> yes </p>
				            <input type="radio" name="Medical_Insurance" value="no">
				            <p> no </p>
				         </div>
							
						<div class="form_group ">
				            <label>Name of Insurer:</label>
				            <input type="text" name="Medi_Name_Insurer" value="{{old('Medi_Name_Insurer')}}" placeholder="Enter Name of Insurer" required="required">
				         </div>  

				         <div class="form_group ">
				            <label>Policy No</label>
				            <input type="text" name="Medi_Policy_No" value="{{old('Medi_Policy_No')}}" placeholder="Enter Policy No" required="required">
				         </div>

				         <div class="form_group datepicker">
				            <label>Date of Issue</label>
				            <input type="text" name="Medi_Date_of_Issue" value="{{old('Medi_Date_of_Issue')}}" placeholder="Enter Date of Issue" required="required">
				         </div> 

				         <div class="form_group datepicker">
				            <label>Date of Expiry</label>
				            <input type="text" name="Medi_Date_of_Expiry" value="{{old('Medi_Date_of_Expiry')}}" placeholder="Enter Date of Expiry" required="required">
				         </div> 



				     	<div class="form_group radio_group">
				            <label>Do you hold Union Indemnity Insurance</label>
				            <input type="radio" name="Union_Insurance" value="yes">
				            <p> yes </p>
				            <input type="radio" name="Union_Insurance" value="no">
				            <p> no </p>
				         </div>
							
							<div class="form_group ">
				            <label>Name of Insurer:</label>
				            <input type="text" name="Union_Name_Insurer" value="{{old('Union_Name_Insurer')}}" placeholder="Enter Name of Insurer" required="required">
				         </div>  

				         <div class="form_group ">
				            <label>Policy No</label>
				            <input type="text" name="Union_Policy_No" value="{{old('Union_Policy_No')}}" placeholder="Enter Policy No" required="required">
				         </div>

				         <div class="form_group datepicker">
				            <label>Date of Issue</label>
				            <input type="text" name="Union_Date_of_Issue" value="{{old('Union_Date_of_Issue')}}" placeholder="Enter Date of Issue" required="required">
				         </div> 

				         <div class="form_group datepicker">
				            <label>Date of Expiry</label>
				            <input type="text" name="Union_Date_of_Expiry" value="{{old('Union_Date_of_Expiry')}}" placeholder="Enter Date of Expiry" required="required">
				         </div> 

						<h1>Availability to Work </h1>

						<div class="form_group ">
				            <label>Available from</label>
				            <input type="text" name="Available_from" value="{{old('Available_from')}}" placeholder="Enter Available from" required="required">
				         </div> 

				          <div class="form_group radio_group">
				            <label>Nights</label>
				            <input type="radio" name="Nights" value="yes">
				            <p> yes </p>
				            <input type="radio" name="Nights" value="no">
				            <p> no </p>
				         </div>

				          <div class="form_group radio_group">
				            <label>Odd Days</label>
				            <input type="radio" name="Odd_Days" value="yes">
				            <p> yes </p>
				            <input type="radio" name="Odd_Days" value="no">
				            <p> no </p>
				         </div>


				          <div class="form_group radio_group">
				            <label>Holidays</label>
				            <input type="radio" name="Holidays" value="yes">
				            <p> yes </p>
				            <input type="radio" name="Holidays" value="no">
				            <p> no </p>
				         </div>

				          <div class="form_group radio_group">
				            <label>Weekends</label>
				            <input type="radio" name="Weekends" value="yes">
				            <p> yes </p>
				            <input type="radio" name="Weekends" value="no">
				            <p> no </p>
				         </div>

				           <div class="form_group radio_group">
				            <label>Full Time</label>
				            <input type="radio" name="Full_Time" value="yes">
				            <p> yes </p>
				            <input type="radio" name="Full_Time" value="no">
				            <p> no </p>
				         </div>

				         <div class="form_group ">
				            <label>Speciality</label>
				            <input type="text" name="Speciality" value="{{old('Speciality')}}" placeholder="Enter Speciality" required="required">
				         </div> 

				         <div class="form_group radio_group">
				            <label>Are you registered with any other telemedicine Agencies</label>
				            <input type="radio" name="telemedicine_Agencies" value="yes">
				            <p> yes </p>
				            <input type="radio" name="telemedicine_Agencies" value="no">
				            <p> no </p>
				         </div>
						
						 <div class="form_group ">
				            <label>Which telemedicine Agencies</label>
				            <input type="text" name="telemedicine_Agencies_name" value="{{old('telemedicine_Agencies_name')}}" placeholder="Enter telemedicine Agencies name" required="required">
				         </div> 

				         <h1>References</h1>

				         <div class="form_group ">
				            <label>Name</label>
				            <input type="text" name="ref_1_Name" value="{{old('ref_1_Name')}}" placeholder="Enter References Name" required="required">
				         </div> 

				         <div class="form_group text_area">
				            <label>Address</label>
				            <textarea name="ref_1_address" id="" cols="30" rows="10" required="required">{{old('ref_1_address')}}</textarea>
				         </div>

				         <div class="form_group ">
				            <label>Post Code</label>
				            <input type="text" name="ref_1_Post_Code" value="{{old('ref_1_Post_Code')}}" placeholder="Enter Post Code" required="required">
				         </div> 

				         <div class="form_group ">
				            <label>Telephone</label>
				            <input type="number" name="ref_1_Telephone" value="{{old('ref_1_Telephone')}}" placeholder="Enter Telephone: " required="required">
				         </div> 

				         <div class="form_group ">
				            <label>Fax</label>
				            <input type="text" name="ref_1_Fax" value="{{old('ref_1_Fax')}}" placeholder="Enter Fax" required="required">
				         </div> 

				         <div class="form_group ">
				            <label>E-mail</label>
				            <input type="text" name="ref_1_email" value="{{old('ref_1_email')}}" placeholder="Enter E-mail" required="required">
				         </div> 



				           <div class="form_group ">
				            <label>Name</label>
				            <input type="text" name="ref_2_Name" value="{{old('ref_1_Name')}}" placeholder="Enter References Name" required="required">
				         </div> 

				         <div class="form_group text_area">
				            <label>Address</label>
				            <textarea name="ref_2_address" id="" cols="30" rows="10" required="required">{{old('ref_1_address')}}</textarea>
				         </div>

				         <div class="form_group ">
				            <label>Post Code</label>
				            <input type="text" name="ref_2_Post_Code" value="{{old('ref_1_Post_Code')}}" placeholder="Enter Post Code" required="required">
				         </div> 

				         <div class="form_group ">
				            <label>Telephone</label>
				            <input type="number" name="ref_2_Telephone" value="{{old('ref_1_Telephone')}}" placeholder="Enter Telephone: " required="required">
				         </div> 

				         <div class="form_group ">
				            <label>Fax</label>
				            <input type="text" name="ref_2_Fax" value="{{old('ref_1_Fax')}}" placeholder="Enter Fax" required="required">
				         </div> 

				         <div class="form_group ">
				            <label>E-mail</label>
				            <input type="email" name="ref_2_email" value="{{old('ref_1_email')}}" placeholder="Enter E-mail" required="required">
				         </div> 

						<h1>Appraisal Details </h1>

						
						 <div class="form_group ">
				            <label>Please supply the name of Medical Practitioner/GP principle that is entered on the specialist register with which formal arrangements have been made to be regularly appraised. </label>
				            <input type="text" name="supply" value="{{old('supply')}}" placeholder="Enter name" required="required">
				         </div> 

						<div class="form_group datepicker">
				            <label>Date of next appraisal</label>
				            <input type="text" name="next_appraisal" value="{{old('next_appraisal')}}" placeholder="Enter Date of next appraisal" required="required">
				         </div>

				         <div class="form_group text_area">
				            <label>Please supply details of your Continual Professional Indemnity (CPD) action plan. </label>
				            <textarea name="supply_details" id="" cols="30" rows="10" required="required">{{old('supply_details')}}</textarea>
				         </div>

				         <div class="form_group ">
				            <label>Appraiser’s Name</label>
				            <input type="text" name="Appraiser_Name" value="{{old('Appraiser_Name')}}" placeholder="Enter Appraiser’s Name" required="required">
				         </div>

				          <div class="form_group ">
				            <label>Professional Society No </label>
				            <input type="text" name="Professional_Society_No" value="{{old('Professional_Society_No')}}" placeholder="Enter Professional Society No" required="required">
				         </div>  

						<h1>Professional Society/Union</h1>

						<div class="form_group ">
				            <label>Name of Society/Union:</label>
				            <input type="text" name="Society" value="{{old('Society')}}" placeholder="Enter Name of Society/Union" required="required">
				         </div>  

						<div class="form_group radio_group">
				            <label>Type of Membership: Full/Provisional</label>
				            <input type="radio" name="membership_type" value="Full">
				            <p> Full </p>
				            <input type="radio" name="membership_type" value="Provisional">
				            <p> Provisional </p>
				         </div>

				         <div class="form_group radio_group">
				            <label>Have you ever been investigated by the Professional Society or any other organisation? </label>
				            <input type="radio" name="investigated_professional_society" value="yes">
				            <p> yes </p>
				            <input type="radio" name="investigated_professional_society" value="no">
				            <p> no </p>
				         </div>

				         <div class="form_group ">
				            <label>Registration No</label>
				            <input type="number" name="Registration_no" value="{{old('Registration')}}" placeholder="Enter Registration No" required="required">
				         </div>  
							
						<div class="form_group datepicker">
				            <label>Renewal Date</label>
				            <input type="text" name="Renewal_Date" value="{{old('Renewal_Date')}}" placeholder="Enter Renewal Date" required="required">
				         </div>

				         <div class="form_group datepicker">
				            <label>Membership No</label>
				            <input type="number" name="Membership_no" value="{{old('Membership_no')}}" placeholder="Enter Renewal Date" required="required">
				         </div>


				         <div class="form_group radio_group">
				            <label>Are you currently under investigation by the Professional Society or any other organisation? </label>
				            <input type="radio" name="under_investigated_professional_society" value="yes">
				            <p> yes </p>
				            <input type="radio" name="under_investigated_professional_society" value="no">
				            <p> no </p>
				         </div>

				          <div class="form_group radio_group">
				            <label>Do you hold license to practice in any other country? </label>
				            <input type="radio" name="other_country" value="yes">
				            <p> yes </p>
				            <input type="radio" name="other_country" value="no">
				            <p> no </p>
				         </div>

				         <div class="form-group form_group">
				            <label class="control-label">Choose Country</label>
				            <select name="Country_practice" id="" required="required">
				               <option value="Country">Country</option>
				               <option value="Country">Country</option>
				               <option value="Country">Country</option>
				               <option value="Country">Country</option>
				               <option value="Country">Country</option>
				               <option value="Country">Country</option>
				            </select>
				         </div>

				         <div class="form_group datepicker">
				            <label>License No:</label>
				            <input type="number" name="License_No" value="{{old('License_No')}}" placeholder="Enter License No" required="required">
				         </div>

  						<div class="form_group datepicker">
				            <label>Expiry Date:</label>
				            <input type="text" name="License_Expiry_Date" value="{{old('Expiry_Date')}}" placeholder="Enter Expiry Date" required="required">
				         </div>


				         <div class="form_group radio_group">
				            <label>Are you licensed to practice in Nigeria?</label>
				            <input type="radio" name="licensed_to_practice" value="yes">
				            <p> yes </p>
				            <input type="radio" name="licensed_to_practice" value="no">
				            <p> no </p>
				         </div>

				         <div class="form_group radio_group">
				            <label>MDCN Portfolio no?</label>
				            <input type="radio" name="Portfolio_no" value="yes">
				            <p> yes </p>
				            <input type="radio" name="Portfolio_no" value="no">
				            <p> no </p>
				         </div>

						<div class="form_group datepicker">
				            <label>Expiry Date:</label>
				            <input type="text" name="Nigeria_licensed_Expiry_Date" value="{{old('Nigeria_licensed_Expiry_Date')}}" placeholder="Enter Expiry Date" required="required">
				         </div>

							<div class="upload_form_group_info regi_upload_form">
                                <div class="form_group custom_file_upload">
                                	<label>Upload Your CV</label>
                                    <input type="file" name="cv" value="Upload File" required="required">
                                </div>
                            </div>
				      </div>
				   </div>
				</div>
	@endsection