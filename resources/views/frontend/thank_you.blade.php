

		@extends('frontend.layouts.app')
@section('content')


	<div class="tm-breadcrumb">
		<div class="container">
			<h1 class="tm-section-heading">Thank You</h1>
			<ul>
				<li>
					<a href="{{route('homepage')}}">home</a>
				</li>
				<li>
					<a href="{{route('thank_you')}}">/ Thank You</a>
				</li>
			</ul>
		</div>
	</div>

	<div class="thanks_page text-center">

	<div class="container">

		<p>
			We are glad to have you on board, your profile is currently under review by our recruitment team. <br>
			Upon approval you will receive an email with your Login Credentials. <br> <a href="{{route('homepage')}}"> Return to home page </a> </p>


		<p>Thank you, <br> Mobihealth  </p>
		

		</div>
	</div>

@endsection