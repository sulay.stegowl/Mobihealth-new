	@extends('frontend.layouts.app')
@section('content')
	<!-- Breadcrumb header -->
	<div class="tm-breadcrumb">
		<div class="container">
			<h1 class="tm-section-heading">Doctor Detail</h1>
			<ul>
				<li>
					<a href="{{route('homepage')}}">home</a>
				</li>
				<li>
					<a href="{{route('doctor_detail')}}">/ Doctor Detail</a>
				</li>
			</ul>
		</div>
	</div><!-- /Breadcrumb header -->
	<!-- Doctor Detail -->
	<div class="tm-doctor-detail">
		<div class="container">
			<div class="row">
				<div class="col-sm-3 nopadding">
					<div class="doctor-image"><img alt="Doctor" class="img-responsive" src="{{asset('/assets/front/images/480562701.png')}}"></div>
				</div>
				<div class="col-sm-8">
					<div class="doctor-info">
						<h2 class="tm-box-heading">dr. alex stuart</h2>
						<div class="doctor-designation">
							Dental Professional - Periodontist
						</div>
						<div class="separator"></div>
						<div class="spacer"></div>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
						<p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
						<div class="separator"></div>
						<div class="additional-doctor-info">
							<!-- Time -->
							<div class="col-sm-4 nopadding">
								<span class="icon-wall-clock"></span>
								<div class="details">
									<h5 class="info-heading">timing</h5>
									<div class="info">
										Mon - Fri : 9am - 10pm
									</div>
								</div>
							</div><!-- /Time -->
							<!-- Contact -->
							<div class="col-sm-6 nopadding">
								<span class="icon-phone"></span>
								<div class="details">
									<h5 class="info-heading">contact</h5>
									<div class="info">
										+1 831 758 7 | alex@themesmill.com
									</div>
								</div>
							</div><!-- /Contact -->
							<!-- Profile -->
							<div class="col-sm-2 nopadding">
								<span class="icon-at"></span>
								<div class="details">
									<h5 class="info-heading">profile</h5>
									<ul class="social">
										<li>
											<a href="#"><i aria-hidden="true" class="fa fa-facebook"></i></a>
										</li>
										<li>
											<a href="#"><i aria-hidden="true" class="fa fa-twitter"></i></a>
										</li>
										<li>
											<a href="#"><i aria-hidden="true" class="fa fa-instagram"></i></a>
										</li>
									</ul>
								</div>
							</div><!-- /Profile -->
						</div>
					</div>
				</div><!-- Experience and Qualification -->
				<div class="other-info">
					<!-- Experience -->
					<div class="col-lg-7 col-md-7 col-sm-6 col-xs-12">
						<h2 class="tm-box-heading-sm">experience</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
						<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
						<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
					</div><!-- /Experience -->
					<!-- Qualification -->
					<div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
						<div class="tm-qualification">
							<h2 class="tm-box-heading-sm">qualification</h2>
							<ul>
								<li>Specialized Dentistry - University of California</li>
								<li>Pediatric Dentistry - Institute of Dental Professionals</li>
								<li>Oral and Maxillofacial Care - Medical University</li>
								<li>Orthodontics - Medical University</li>
							</ul>
						</div>
					</div><!-- Qualification -->
				</div><!-- /Experience and Qualification -->
			</div>
		</div><!-- Awards and Certifications -->
	
	</div><!-- /Doctor Detail -->
	<!-- Appointment -->
	<div class="tm-appointment">
		<div class="container">
			<div class="row">
				<h4 class="tm-section-heading">get appointment</h4>
				<div class="col-sm-12">
					<div class="row">
						<div class="appointment-form">
							<form>
								<div class="col-sm-6">
									<div class="form-group">
										<input class="form-control" name="full_name" placeholder="Full Name" type="text">
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<input class="form-control" name="email" placeholder="Email" type="email">
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<input class="form-control" name="phone" placeholder="Phone (optional)" type="text">
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<div class="tm-datepicker">
											<input class="form-control" data-toggle="datepicker" name="dob" placeholder="Date of Birth" type="text">
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<div class="tm-datepicker">
											<input class="form-control" data-toggle="datepicker" name="date" placeholder="Choose Date" type="text">
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<div class="tm-timepicker">
											<select class="form-control" name="time">
												<option>
													01:30 AM
												</option>
												<option>
													02:30 AM
												</option>
												<option>
													03:30 AM
												</option>
												<option>
													04:30 AM
												</option>
												<option>
													05:30 AM
												</option>
												<option>
													06:30 AM
												</option>
												<option>
													07:30 AM
												</option>
												<option>
													08:30 AM
												</option>
												<option>
													09:30 AM
												</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group">
										<textarea class="form-control" placeholder="Message (optional)" rows="10"></textarea>
									</div>
								</div>
								<div class="col-sm-12 text-right">
									<div class="form-group">
										<input class="tm-btn btn-dark" name="submit" type="submit" value="get appointment">
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div><!-- /Appointment -->


@endsection