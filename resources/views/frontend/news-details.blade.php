@extends('frontend.layouts.app')
@section('content')

	<!-- Breadcrumb header -->
	<div class="tm-breadcrumb">
		<div class="container">
			<h1 class="tm-section-heading">News Detail</h1>
			<ul>
				<li>
					<a href="{{route('homepage')}}">home</a>
				</li>
				<li>
					<a href="{{route('news_details')}}">/ News Detail</a>
				</li>
			</ul>
		</div>
	</div><!-- /Breadcrumb header -->
	<!-- Blog Content Area -->
	<div class="blog-detail">
		<div class="container">
			<div class="row">
				<div class="col-sm-8">
					<div class="tm-news-home">
						<div class="row">
							<div class="col-sm-12">
								<div class="news">
									<div class="news-thumbnail">
									<div class="post-date">
										12<br>
										jan
					</div><img alt="dentist" src="{{asset('/assets/front/images/blog1.png')}}"></div>
									<div class="news-title">
										<a href="#">HOW DOES TELEMEDICINE AFFECT MALPRACTICE INSURANCE?</a>
									</div>
									<div class="news-meta">
										<ul>
											<li><span class="icon-user"></span> by admin</li>
											{{-- <li><span class="icon-eye-close-up"></span> 132 views</li>
											<li><span class="icon-comment"></span> 52 comments</li> --}}
											<li><span class="icon-calendar-one"></span> oct 02, 2017</li>
										</ul>
									</div>
									<div class="separator"></div>
									<p>As physicians, we don’t normally like to think about malpractice insurance or any scenario where we’d actually need it. But because telemedicine is getting popular, physicians and insurance companies are starting to wonder how to handle telemedicine from a malpractice perspective.</p>
									<p>Many doctors assume telemedicine will raise your liability. But actually, that’s not the case.</p>
									<p>As a member of the advisory board of a large medical malpractice company, I’ve had the unique opportunity to research how malpractice insurance companies are responding to telemedicine. Here are the top things you should know.</p>
									<div class="separator"></div>
									<h4><strong>Telemedicine = Low Liability</strong></h4>
									<p>When you think about why we need malpractice insurance, is it because we prescribed the wrong asthma medication? Or because a patient’s family claims an emergency room physician didn’t get the patient to a neurosurgeon fast enough after her traumatic brain injury?</p>
									<p>Because telemedicine physicians are more likely to deal with routine checkups and prescription-writing than complex, high-risk procedures, telemedicine tends to be low liability. And that’s good news when we start looking for malpractice insurance that covers our telemedicine practice.</p>
									<div class="separator"></div>
									<h4><strong>Little History of Past Claims</strong></h4>
									<p>The history of telemedicine stretches back for decades to the days of pagers (remember those?) when doctors would be on-call and “visit” with patients over the phone rather than in the office. And still, little is known about successful malpractice claims made against telemedicine physicians over the years. Those claims seem to be few and far between.</p>
									<p>This could be because physicians reached settlements in these cases, which means minimal reporting of them, and confidentiality agreements signed by patients, according to emergency physician and attorney Joseph P. McMenamin, MD, JD, of Robert J. Waters Center for Telehealth & e-Health Law. But something tells me that even as the number of telemedicine visits rises exponentially each year, the rate of malpractice claims against telemedicine physicians won’t increase at the same pace.</p>


									<div class="separator"></div>
									<h4><strong>Benefits of Documentation</strong></h4>
									<p>An issue with some telemedicine services is that often physicians on-call would talk to patients over the phone and then call in a prescription without ever documenting it. This could be a liability if a patient comes back and says, “Well, why did you prescribe me this medication?” and you have no documentation.</p>
									<p>But with more recent developments in telemedicine technology, secure videochat platforms are replacing old fashioned phone calls, and often provide a place to capture notes from the online visit. That means better documentation of conversations with patients. Better documentation means less liability for you if something goes wrong.</p>



										<div class="separator"></div>
									<h4><strong>Will my Malpractice Insurance Cover Telemedicine?</strong></h4>
									<p>Malpractice insurance can vary significantly between insurance companies and specialties, so it’s hard to say definitively whether your insurance company will cover your telemedicine practice or not. Many insurance companies do cover it by now, though some are slower on the uptake than others.</p>
									<p>But with telemedicine being as prevalent, understood, and asked for by patients, how are some insurance companies still not offering this type of coverage?</p>
									<p>Imagine this line from an insurance company: “We don’t think you’re capable of treating a sinus infection, Dr. Smith. We know you can do it in the office, but you can’t do it over video.” Or in short, “We are unable to cover you to do what you’re trained to do.”</p>
									<p>Now how do you think that conversation would go over?</p>
									<p>Not all insurance companies understand telemedicine, even though the physicians and patients who want to implement it do. It’s new, it’s different. It’s not something all insurance companies have really seen yet. And we all know that people often fear what they don’t understand.</p>
									<p>It’s just a shame that fear can leave capable physicians who want to offer telemedicine services to their patients without the proper malpractice insurance to do it.</p>
									
									<div class="separator"></div>
									<h4><strong>What You Should Do About Malpractice Insurance</strong></h4>
									<p>Again, because all insurance companies and policies are different, there is no one answer for how to go about securing it for your telemedicine practice. But there are a few basic steps anyone can take to at least get the conversation started.</p>
									<ul>
										<li>Contact your carrier to see if they already cover telemedicine services.</li>
										<li>Find out if there is any additional cost or if there will be an added rider to your policy. Sometimes malpractice insurance for telemedicine will increase the policy’s cost, but not always.</li>
										<li>If they don’t, start researching companies that do offer it. They do exist, and often at low cost because they do consider telemedicine low liability.</li>
									</ul>
									<p>Finding malpractice insurance for your telemedicine practice doesn’t have to be complicated, but if you are seriously considering practicing telemedicine—or already do—cover your bases to protect yourself from unforeseeable problems in the future.</p>
								

								
								
								</div>
							</div>
							{{-- <div class="separator"></div> --}}<!-- Post Block of Options -->
							{{-- <div class="post-blocks">
								<div class="post-share">
									<ul>
										<li>
											<a href="#"><i aria-hidden="true" class="fa fa-facebook"></i></a>
										</li>
										<li>
											<a href="#"><i aria-hidden="true" class="fa fa-twitter"></i></a>
										</li>
										<li>
											<a href="#"><i aria-hidden="true" class="fa fa-google-plus"></i></a>
										</li>
										<li>
											<a href="#"><i aria-hidden="true" class="fa fa-instagram"></i></a>
										</li>
										<li>
											<a href="#"><i aria-hidden="true" class="fa fa-pinterest-p"></i></a>
										</li>
									</ul>
								</div>
								<div class="tags-inline">
									<strong>Tags:</strong> <a href="#">Dentist</a>, <a href="#">Business</a>, <a href="#">Creative Template</a>, <a href="#">Modern Design</a>, <a href="#">Clean Design</a>
								</div>
							</div>  --}}<!-- /Post Block of Options -->
							{{-- <div class="separator"></div>  --}}<!-- Post Block of Options -->
							{{-- <div class="post-blocks">
								<div class="post-nav">
									<div class="prev">
										<a href="#"><strong><i aria-hidden="true" class="fa fa-long-arrow-left"></i> previous</strong> Quisque elit tortor, ullamcorper quis ultrices vitae</a>
									</div>
									<div class="next">
										<a href="#"><strong>next <i aria-hidden="true" class="fa fa-long-arrow-right"></i></strong> Quisque elit tortor, ullamcorper quis ultrices vitae</a>
									</div>
								</div>
							</div>  --}}<!-- /Post Block of Options -->
							{{-- <div class="separator"></div> --}}<!-- Post Block of Options -->
						{{-- 	<div class="post-blocks">
								<div class="tm-author">
				<div class="author-photo"><img alt="Author" src="{{asset('/assets/front/images/author.jpg')}}"></div>
									<div class="author-detail">
										<h3 class="author-name">JOHN DOE (aUTHOR)</h3>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
									</div>
								</div>
							</div>  --}}<!-- /Post Block of Options -->
							{{-- <div class='separator'></div> 
							<div class="post-blocks">
								<div class="tm-comments">
									<h4 class="c-heading">comments</h4>
									<ul>
										<li>
											<div class="user-comment">
												<div class="user-image"><img alt="Author" src="{{asset('/assets/front/images/author.jpg')}}"></div>
												<div class="comment-content">
													<h5 class="user-name">Stephen Hawk</h5>
													<h5 class="comment-time"><span class="icon-calendar-one"></span> March 1, 2012 at 3:01 am</h5>
													<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut.</p><button class="tm-btn btn-blue" type="button"><i aria-hidden="true" class="fa fa-reply"></i> reply</button>
												</div>
											</div>
										</li>
										<li>
											<div class="user-comment">
												<div class="user-image"><img alt="Author" src="{{asset('/assets/front/images/author.jpg')}}"></div>
												<div class="comment-content">
													<h5 class="user-name">Stephen Hawk</h5>
													<h5 class="comment-time"><span class="icon-calendar-one"></span> March 1, 2012 at 3:01 am</h5>
													<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut.</p><button class="tm-btn btn-blue" type="button"><i aria-hidden="true" class="fa fa-reply"></i> reply</button>
												</div>
											</div>
										</li>
										<li class="reply">
											<div class="user-comment">
												<div class="user-image"><img alt="Author" src="{{asset('/assets/front/images/author.jpg')}}"></div>
												<div class="comment-content">
													<h5 class="user-name">Stephen Hawk</h5>
													<h5 class="comment-time"><span class="icon-calendar-one"></span> March 1, 2012 at 3:01 am</h5>
													<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut.</p><button class="tm-btn btn-blue" type="button"><i aria-hidden="true" class="fa fa-reply"></i> reply</button>
												</div>
											</div>
										</li>
										<li>
											<div class="user-comment">
												<div class="user-image"><img alt="Author" src="{{asset('/assets/front/images/author.jpg')}}"></div>
												<div class="comment-content">
													<h5 class="user-name">Stephen Hawk</h5>
													<h5 class="comment-time"><span class="icon-calendar-one"></span> March 1, 2012 at 3:01 am</h5>
													<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut.</p><button class="tm-btn btn-blue" type="button"><i aria-hidden="true" class="fa fa-reply"></i> reply</button>
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>  --}}<!-- /Post Block of Options -->
							{{-- <div class='separator'></div> --}}<!-- Post Block of Options -->
							{{-- <div class="post-blocks">
								<div class="tm-comment-form">
									<h4 class="c-heading">leave a comment</h4>
									<div class="row">
										<form>
											<div class="col-sm-6">
												<div class="form-group">
													<input class="form-control" name="name" placeholder="Name" type="text">
												</div>
											</div>
											<div class="col-sm-6">
												<div class="form-group">
													<input class="form-control" name="email" placeholder="Email address" type="text">
												</div>
											</div>
											<div class="col-sm-12">
												<div class="form-group">
													<textarea class="form-control" placeholder="Type your comment..." rows="7"></textarea>
												</div>
											</div>
											<div class="col-sm-12 text-right">
												<div class="form-group">
													<input class="tm-btn btn-blue" name="submit" type="submit" value="publish comment">
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>  --}}<!-- /Post Block of Options -->
						</div>
					</div>
				</div><!-- Sidebar -->
				<div class="col-sm-4">
					<div class="tm-sidebar">
						<!-- Search Widget -->
						{{-- <div class="sidebar-widget tm-search-widget">
							<form>
								<input class="form-control" name="search" placeholder="Search..." type="text">
								<div class="search-btn">
									<input class="tm-btn btn-dark news_button" name="submit" type="submit" value=""> <span class="icon-magnifier icn"></span>
								</div>
							</form>
						</div>  --}}<!-- /Search Widget -->


							<!-- Recent Posts -->
						<div class="sidebar-widget sidebar-posts">
							<h4 class="widget-heading">recent posts</h4>
							<ul>
								<li>
									<div class="post-thumbnail"><img alt="blog" src="{{asset('/assets/front/images/blog1.png')}}"></div>
									<div class="content">
										<a class="title" href="{{route('news_details_1')}}">HOW DOES TELEMEDICINE AFFECT MALPRACTICE INSURANCE?</a> <span>Today - by admin</span>
									</div>
								</li>

								<li>
									<div class="post-thumbnail"><img alt="blog" src="{{asset('/assets/front/images/blog2.png')}}"></div>
									<div class="content">
										<a class="title text-uppercase" href="http://www.afro.who.int/sites/default/files/2017-06/nigeria_medicine_prices.pdf" target="_blank">nigeria medicine</a> <span>Today - by admin</span>
									</div>
								</li>

								<li>
									<div class="post-thumbnail"><img alt="blog" src="{{asset('/assets/front/images/blog3.png')}}"></div>
									<div class="content">
										<a class="title text-uppercase" href="{{route('news_details_3')}}">our africa health</a> <span>Today - by admin</span>
									</div>
								</li>
							
								
							</ul>
						</div><!-- /Recent Posts -->

					
						<!-- Tags -->
					{{-- 	<div class="sidebar-widget tags-cloud">
							<h4 class="widget-heading">tags</h4>
							<ul>
								<li>
									<a href="#">creative template</a>
								</li>
								<li>
									<a href="#">Dentist</a>
								</li>
								<li>
									<a href="#">Business</a>
								</li>
								<li>
									<a href="#">Business</a>
								</li>
								<li>
									<a href="#">Creative Template</a>
								</li>
								<li>
									<a href="#">creative template</a>
								</li>
								<li>
									<a href="#">Dentist</a>
								</li>
								<li>
									<a href="#">Business</a>
								</li>
								<li>
									<a href="#">Business</a>
								</li>
								<li>
									<a href="#">Creative Template</a>
								</li>
							</ul>
						</div>  --}}<!-- /Tags -->
						<!-- Opening Hours -->
						{{-- <div class="sidebar-widget sidebar-opening-hours">
							<div class="tm-opening-hours">
								<h4 class="tm-small-heading">opening hours</h4>
								<ul>
									<li><span class="lbl">Monday - Friday</span> <span class="val">9am - 10pm</span></li>
									<li><span class="lbl">Monday - Friday</span> <span class="val">9am - 10pm</span></li>
									<li><span class="lbl">Monday - Friday</span> <span class="val">9am - 10pm</span></li>
								</ul>
								<p>In case of emergency, contact us on the following number and we will immediately respond to you.</p>
								<div class="cl">
									<span class="icon-phone"></span> <strong>012 345 67 890</strong>
								</div>
							</div>
						</div>  --}}<!-- /Opening Hours -->
					</div>
				</div><!-- /Sidebar -->
			</div>
		</div>
	</div><!-- /Blog Content Area -->
	<!-- Footer -->
	
	@endsection