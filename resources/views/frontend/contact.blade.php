@extends('frontend.layouts.app')
@section('content')

		<div class="tm-breadcrumb">
		<div class="container">
			<h1 class="tm-section-heading">Contact Us</h1>
			<ul>
				<li>
					<a href="index.php">home</a>
				</li>
				<li>
					<a href="contact.php">/ contact us</a>
				</li>
			</ul>
		</div>
	</div>

	/
		{{-- <div class="map-canvas tm_contact_us_map" id="map-canvas"></div> --}}
		{{-- <script src='https://maps.googleapis.com/maps/api/js?v=3.exp'></script><div class="google_map" style='overflow:hidden;height:432px;width:1349px;'><div class="google_map" id='gmap_canvas' style='height:432px;width:1349px;'></div><div><small><a href="https://embedgooglemaps.com/en/how-to-embed-google-maps-into-your-website/"></a></small></div><div><small><a href="https://ww.premiumlinkgenerator.com/supported-file-hosts">PremiumLinkGenerator</a></small></div><style>#gmap_canvas img{max-width:none!important;background:none!important}</style></div><script type='text/javascript'>function init_map(){var myOptions = {zoom:10,center:new google.maps.LatLng(9.0443944,7.523386899999991),mapTypeId: google.maps.MapTypeId.ROADMAP};map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(9.0443944,7.523386899999991)});infowindow = new google.maps.InfoWindow({content:'<strong>Mobihealth</strong><br> Julius Nyerere  , Asokoro District , Abuja Nigeria<br>'});google.maps.event.addListener(marker, 'click', function(){infowindow.open(map,marker);});infowindow.open(map,marker);}google.maps.event.addDomListener(window, 'load', init_map);</script> --}}
	{{-- </div> --}}

	<div class="tm-contact-info-2">
		<div class="container">
			<div class="row">
				<h1 class="tm-section-heading">send us an email</h1>
				<div class="col-sm-6">
					<div class="tm-contact-us-form">
						<form method="post" action="{{route('send.contact')}}">
							{{csrf_field()}}
							<div class="col-sm-6">
								<div class="form-group">
									<input class="form-control" name="first_name" placeholder="First name" type="text">
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<input class="form-control" name="last_name" placeholder="Last name" type="text">
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<input class="form-control" name="email" placeholder="Email address" type="email">
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<input class="form-control" name="phone" placeholder="Phone (optional)" type="text">
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<input class="form-control" name="subject" placeholder="Subject" type="text">
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<textarea class="form-control" placeholder="Message (optional)" rows="7" name="message"></textarea>
								</div>
							</div>
							<div class="col-sm-12 text-right">
								<div class="form-group">
									<input class="tm-btn btn-blue contact_us_submit" name="submit" type="submit" value="SUBMIT" >
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="contact-info-box-2">
						<div class="img-icon">
							<span class="icon-at"></span>
						</div>
						<div class="info">
							<h3>info@mobihealthinternational.com</h3>
							 <h4>Send us an email</h4>
						</div>
					</div>
					 <div class="contact-info-box-2">
						<div class="img-icon">
							<span class="icon-placeholder-on-map"></span>
						</div>
						<div class="info">
							<h3 style="margin-left: 100px; margin-top: -88px;">Suite E1 Bobsar Complex, Plot 1035 Michika Street. Area 11 Garki Abuja, FCT Nigeria.</h3>
							<h4 style="margin-left: 100px;">Contact Address (Nigeria)</h4>
						</div>
					</div>
					<div class="contact-info-box-2">
						<div class="img-icon">
							<span class="icon-placeholder-on-map"></span>
						</div>
						<div class="info">
							<h3 style="margin-left: 100px; margin-top: -88px;">Apt 10820, Chynoweth House, Trevissome Park, Truro, TR4 8UN United Kingdom.</h3>
							<h4 style="margin-left: 100px;">Contact Address (UK)</h4>
						</div>
					</div>
					{{--<div class="contact-info-box-2">
						<div class="img-icon">
							<span class="icon-time-left"></span>
						</div>
						<div class="info">
							<h3>Opening Hour</h3>
							<h4>Mon - Sat 11AM-7PM</h4>
						</div>
					</div> --}}
				</div>
			</div>
		</div>
	</div>


@endsection