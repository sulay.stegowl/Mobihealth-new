@extends('frontend.layouts.app')
@section('content')

<div class="tm-breadcrumb">
		<div class="container">
			<h1 class="tm-section-heading">hospital</h1>
			<ul>
				<li>
					<a href="{{route('homepage')}}">home</a>
				</li>
				<li>
					<a href="{{route('hospital')}}">/ hospital</a>
				</li>
			</ul>
		</div>
	</div><!-- /Breadcrumb header -->
	<!-- Shop -->
	<div class="tm-shop-list">

		<div class="dis_inline">
			<h1 class="tm-section-heading"> Hospitals</h1>
			{{-- <div class="how_it_faq">
				<a href="{{route('hospital_how')}}" class="how_it_work">How It Works</a>
				<a href="{{route('hospital_faq')}}" class="faq">Faq</a>
			</div> --}}
		</div>


			<div class="tm-appointment-large tm-appointment-large_none unique">
		<div class="container">
			<h1 class="tm-section-heading">Search By</h1>
			<div class="row">
				<form method="POST" action="{{route('search_hospital')}}">
					{{csrf_field()}}
				<div class="col-sm-4">
					<div class="form-group">
						<div class="tm-timepicker">
							<select class="form-control" name="search_by" id="search">
								<option value="Name"> Name </option>
								<option value="Country"> Country </option>
								<option value="State"> State </option>
								<option value="Location"> Location </option>
							</select>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="form-group" id="change">
						<input class="form-control" name="value" placeholder="Enter Hospital Name" type="text">
					</div>
				</div>
				
				
				<div class="col-sm-4">
					<div class="form-group page_loader_div_2" >

						<button class="tm-btn btn-dark" type="submit" >SEARCH</button>
					</div>
				</div>
			
		</form>
		</div>
	</div><!-- /Appointment -->
</div>

		<div class="container">
		

		<!-- Appointment -->


			<div class="row">
				@forelse($ans as $data)
				
				<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 smf" id="list">
					<div class="tm-product-box">
						<div class="product-image"><img alt="Shop" src="{{asset('/storage/services/'.$data->image)}}" class="img-responsive "></div>
						<div class="product-details">
							<h6 class="product-title"><a href="{{route('hospital_details',['slug' => $data->slug])}}">{{$data->title}},<span>&nbsp{{$data->country}}</span></a></h6>
							
							
						</div>
					</div>
				</div>
				@empty
				<h1 id="list">No Search Found</h1>
				@endforelse
				
				
				
				{{-- <div class="tm-pagination">
					<ul>
						<li>
							<a href="#">1</a>
						</li>
						<li>
							<a href="#">2</a>
						</li>
						<li class="active">
							<a href="#">3</a>
						</li>
						<li>
							<a href="#">4</a>
						</li>
						<li>
							<a href="#">5</a>
						</li>
					</ul>
				</div> --}}
			</div>
		
	</div><!-- /Shop -->
	</div>
	@endsection
