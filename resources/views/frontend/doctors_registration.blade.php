		@extends('frontend.layouts.app')
@section('content')
	<div class="page_loader">  <p><img src="http://34.237.167.24/assets/front/images/loading.gif" alt=""></p> </div>

	<div class="tm-breadcrumb">
		<div class="container">
			<h1 class="tm-section-heading">register</h1>
			<ul>
				<li>
					<a href="{{route('homepage')}}">home</a>
				</li>
				<li>
					<a href="{{route('doctors_registration')}}">/ register</a>
				</li>
			</ul>
		</div>
	</div>



	<div class="register_page">
		<div class="container">
		

@include('include.message')
	<div class="dis_inline">
		
			<div class="how_it_faq">
				<a href="{{route('doctor_how')}}" class="how_it_work">How It Works</a>
				<a href="{{route('doctor_faq')}}" class="faq">Faq</a>
			</div>
		</div>


{{-- <div class="note">
	<p>	Pls send your CV to following email addresses: </p>
<p> <strong> Doctors: </strong> drs@mobihealthinternational.com </p>
<p> <strong> Nurses: </strong> nurses@mobihealthinternational.com </p>
<p> <strong> Physiotherapist:  </strong> pt@mobihealthinternational.com </p>


	
</div> --}}
			<div class="register_form">
				<form action="{{route('steps')}}" method="post" enctype="multipart/form-data">
					{{csrf_field()}}

				{{-- 	<div class="form-group form_group">
                        <label class="control-label">Choose  Medical Profession</label>
                        <select name="type" id="">
                        	<option value="Doctor">Doctor</option>
                            <option value="Dentist">Dentist</option>
                            <option value="Laboratory_Technician">Laboratory Technician</option>
                            <option value="Nurse">Nurse</option>
                            <option value="Physiotherapist">Physiotherapist</option>
                        </select>
                    </div> --}}
					   <div class="form-group form_group">
                                <label class="control-label">Profession</label>
                                <select name="Profession" id="" required="required" >
                                    <option value="Doctor">Doctor</option>
                                    <option value="Dentist">Dentist</option>
                                    <option value="Pharmacist">Pharmacist</option>
                                    <option value="Nurse">Nurse</option>
                                    <option value="Physiotherapist">Physiotherapist</option>
                                    <option value="Laboratory_Technician">Laboratory Technician</option>
                                </select>
                            </div>

					<div class="form_group">
						<label>first name</label>
						<input type="text" name="First_Name" required="required" value="{{old('First_Name')}}"  placeholder="Enter First name">
					</div>
					<div class="form_group">
						<label>Surname</label>
						<input type="text" name="Surname" required="required" value="{{old('Surname')}}"  placeholder="Enter Surname">
					</div>
					<div class="form_group">
						<label>Email Address</label>
						<input type="email" name="Email_Address" required="required" value="{{old('Email_Address')}}" placeholder="Enter Email">
					</div>
					<div class="form_group">
						<label>Specialty</label>
						<input type="text" name="Speciality" required="required" value="{{old('Speciality')}}"  placeholder="Specialty">
					</div>
					
					<div class="form-group form_group">
                                    <label class="control-label">Choose Country</label>
                                    <select name="countries" id="">
									<option>Select The Country...</option>
                                        @foreach($countries as $country)
                                        
                                        <option value="{{$country}}">{{$country}}</option>
                                        @endforeach
                                    </select>
                                </div>
					
					<div class="form_group datepicker" id="dob">                      
	                                <label>Date of Birth</label>
	                                <input type="text" name="Date_of_Birth" value="{{old('Date_of_Birth')}}" placeholder="Enter Date of Birth" required="required" >                            		
                            </div>
                            <div class="form-group form_group">
                            		<label>Mobile</label>
                                	<input type="number" name="Mobile" value="{{old('Mobile')}}" placeholder="Enter Mobile" required="required" onkeydown="if(this.value.length>14 && event.keyCode>47 && event.keyCode < 58)return false;">
                            	</div>
                            	<div class="form-group form_group">
                            		<label>Grade</label>
                                	<input type="text" name="grade" required="required" value="{{old('Grade')}}" placeholder="Enter Grade" required="required">
                                	<label>(note:-Only Middle Grade & Consultant level Doctors should register with minimum of 5yrs postgraduate working experience)</label>
                            	</div>
                            	<div class="form_group">
									<label>License Number</label>
									<input type="text" name="Driving_Licence" required="required" value="{{old('Driving_Licence')}}"  placeholder="License Number">
								</div>
								<div class="form_group">
									<label>Country of issue</label>
									<input type="text" name="country_of_issue" required="required" value="{{old('country_of_issue')}}"  placeholder="License Number">
								</div>
                            	<div class="upload_form_group_info regi_upload_form">
                                <div class="form_group custom_file_upload">
                                    <label>Upload Your CV</label>
                                    <input type="file" name="cv" value="Upload File" required="required">
                                </div>
                            </div>
                         <div class="upload_form_group_info regi_upload_form">
                            <div class="form_group custom_file_upload">
                                <label>Upload Your Profile Picture</label>
                                <input type="file" name="image" value="Upload File" required="required">
                            </div>
                        </div>
                         <div class="upload_form_group_info regi_upload_form">
                            <div class="form_group custom_file_upload">
                                <label>Upload Your Degree Certificate</label>
                                <input type="file" name="medical_certi" value="Upload File" required="required">
                            </div>
                        </div>
					<div class="form_group radio_group dis_block">
                                            <label>Are you currently licensed to practice?</label>
									<div class="dis_flex_2">
                        <input type="radio" name="states_licensed" value="yes" required="required">
                        <p> Yes </p>
                        <input type="radio" name="states_licensed" value="no" required="required">
                        <p> No </p>
									</div>

                     </div>


					{{-- <div class="form_group page_loader_div_2"> --}}
						<div class="form_group">
						<input type="submit" value="Submit" >
					</div>
				</form> 

			</div>
		</div>
	</div>


@endsection


