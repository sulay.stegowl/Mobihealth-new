

		@extends('frontend.layouts.app')
@section('content')


	<div class="tm-breadcrumb">
		<div class="container">
			<h1 class="tm-section-heading">register</h1>
			<ul>
				<li>
					<a href="{{route('homepage')}}">home</a>
				</li>
				<li>
					<a href="{{route('register_step2')}}">/ register</a>
				</li>
			</ul>
		</div>
	</div>

	<div class="register_page">
		<div class="container">
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores accusantium, hic magnam amet, dolorum atque deleniti blanditiis iure porro ipsa quos cum! Reprehenderit quam labore architecto ullam tenetur exercitationem maxime. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam hic animi vitae facilis eaque, beatae, in dolorum tempora tenetur, architecto placeat deleniti, maiores autem cumque illum sapiente? At, ea, nobis.</p>

			<div class="register_form">
				<form action="{{route('register_step3')}}" method="post">
					{{csrf_field()}}
					<div class="form_group">
						<label>Name of business</label>
						<input type="text" name="fname" required="required" placeholder="Enter Name of business">
					</div>
					<div class="form-group form_group">
                        <label class="control-label">Type of business</label>
                        <select name="" id="" required="required">
                            <option value="pharmacy">pharmacy</option>
                            <option value="lab">lab</option>
                            <option value="hospital">hospital</option>
                        </select>
                    </div>
					<div class="form_group">
						<label>Contact person</label>
						<input type="text" name="lname" required="required" placeholder="Enter Contact person">
					</div>
					<div class="form_group">
						<label>Contact email </label>
						<input type="email" name="lname" required="required" placeholder="Enter Email">
					</div>
					<div class="form_group">
						<label>Whatsapp no</label>
						<input type="number" name="Specialty" required="required" placeholder="Enter Whatsapp no">
					</div>
					<div class="form_group">
						<label>Website</label>
						<input type="url" name="Specialty" required="required" placeholder="Enter Website">
					</div>

					<div class="form_group">
						<label>Do you offer home laboratory service?</label>
						<input type="text" name="Specialty" required="required" placeholder="Yes or No">
					</div>
					<div class="form_group">
						<label>Do you offer online order?</label>
						<input type="text" name="Specialty" required="required" placeholder="Yes or No">
					</div>
					<div class="form_group">
						<label>Do you offer home delivery of medications?</label>
						<input type="text" name="Specialty" required="required" placeholder="Yes or No">
					</div>
					<div class="form_group">
						<label>Free text for additional detail</label>
						<textarea name="" id="" cols="30" rows="10" class="custom"></textarea>
					</div>

					<div class="form_group">
						<input type="submit" value="Submit">
					</div>
				</form>

			</div>
		</div>
	</div>

@endsection