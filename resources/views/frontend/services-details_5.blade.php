@extends('frontend.layouts.app')
@section('content')

<div class="tm-breadcrumb">
		<div class="container">
			<h1 class="tm-section-heading">Service Detail</h1>
			<ul>
				<li>
					<a href="{{route('homepage')}}">home</a>
				</li>
				<li>
					<a href="{{route('services_details')}}">/PRESCRIPTION AND GENUINE MEDICATIONS</a>
				</li>
			</ul>
		</div>
	</div>

	<div class="tm-service-detail-wrap">
		<div class="container">
			<div class="row">
			
				<div class="col-sm-4">
					<div class="tm-sidebar">
					
						<div class="sidebar-widget questions">
							<h4 class="widget-heading">questions and queries</h4>
							<p>Email us if any queries.</p>
							<ul>
								
								<li><span class="icon-at"></span> info@mobihealthinternational.com</li>
							</ul>
						
						</div>
					</div>
				</div>
				
				<div class="col-sm-8">
					<div class="tm-service-detail">
						<div class="service-detail-thumb"><img alt="service" src="{{asset('/assets/front/images/L4.jpg')}}"></div>
						<h4 class="service-title">PRESCRIPTION AND GENUINE MEDICATIONS</h4>
						<p>You no longer need to worry about where to get genuine medication. We send your prescriptions to reputable pharmacies and text you when they are ready for pick up. We have partnered with highly reputable pharmacies with branded products from the UK, USA and other highly reputable manufacturers.</p>
					
					</div>
				</div>
			</div>
		</div>
	</div>

	@endsection