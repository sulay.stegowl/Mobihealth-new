@extends('frontend.layouts.app')
@section('content')

	<!-- Breadcrumb header -->
	<div class="tm-breadcrumb">
		<div class="container">
			<h1 class="tm-section-heading">News Detail</h1>
			<ul>
				<li>
					<a href="{{route('homepage')}}">home</a>
				</li>
				<li>
					<a href="{{route('news_details')}}">/ News Detail</a>
				</li>
			</ul>
		</div>
	</div><!-- /Breadcrumb header -->
	<!-- Blog Content Area -->
	<div class="blog-detail">
		<div class="container">
			<div class="row">
				<div class="col-sm-8">
					<div class="tm-news-home">
						<div class="row">
							<div class="col-sm-12">
								<div class="news">
									<div class="news-thumbnail">
									<div class="post-date">
										02<br>
										Oct
					</div><img alt="dentist" src="{{asset('/assets/front/images/blog3.png')}}"></div>
									<div class="news-title">
										<a href="#" class="text-uppercase">our africa health</a>
									</div>
									<div class="news-meta">
										<ul>
											<li><span class="icon-user"></span> by admin</li>
											<li><span class="icon-calendar-one"></span> oct 02, 2017</li>
										</ul>
									</div>
									<div class="separator"></div>
									<p>Some of these diseases, such as malaria, tuberculosis and HIV/AIDS, are found elsewhere. Others, such as sleeping sickness, are specific to Africa.</p>

<p>
Inoculations, vaccinations and other means of prevention are available. For example, insecticide-treated nets help people avoid the fly bites which lead to infections such as malaria. But the lack of easy access to health clinics and the costs of certain treatments or prevention methods can put help out of the reach of many families.</p>

<p> <a href="http://www.our-africa.org/health" target="_blank" class="read_more"> read more </a> </p>


								
								
								</div>
							</div>
					
						</div>
					</div>
				</div><!-- Sidebar -->
				<div class="col-sm-4">
					<div class="tm-sidebar">
					


							<!-- Recent Posts -->
						<div class="sidebar-widget sidebar-posts">
							<h4 class="widget-heading">recent posts</h4>
							<ul>
								<li>
									<div class="post-thumbnail"><img alt="blog" src="{{asset('/assets/front/images/blog1.png')}}"></div>
									<div class="content">
										<a class="title" href="{{route('news_details_1')}}">HOW DOES TELEMEDICINE AFFECT MALPRACTICE INSURANCE?</a> <span>Today - by admin</span>
									</div>
								</li>

								<li>
									<div class="post-thumbnail"><img alt="blog" src="{{asset('/assets/front/images/blog2.png')}}"></div>
									<div class="content">
										<a class="title text-uppercase" href="http://www.afro.who.int/sites/default/files/2017-06/nigeria_medicine_prices.pdf" target="_blank">nigeria medicine</a> <span>Today - by admin</span>
									</div>
								</li>

								<li>
									<div class="post-thumbnail"><img alt="blog" src="{{asset('/assets/front/images/blog3.png')}}"></div>
									<div class="content">
										<a class="title text-uppercase" href="{{route('news_details_3')}}">our africa health</a> <span>Today - by admin</span>
									</div>
								</li>
							
								
							</ul>
						</div><!-- /Recent Posts -->

				
					</div>
				</div><!-- /Sidebar -->
			</div>
		</div>
	</div><!-- /Blog Content Area -->
	<!-- Footer -->
	
	@endsection