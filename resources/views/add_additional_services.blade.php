@extends('layouts.app')
     @section('content')
               <div class="content-wrapper">

          <h1 class="page-title">Add {{$data['name']}}</h1>
          <div class="row">
              <div class="col-12 col-lg-6 grid-margin">
                  <div class="card">
                      <div class="card-body">
                          <h2 class="card-title">Add a {{$data['name']}} Account </h2>
                          <form class="forms-sample" method="POST" action="{{route('save_additional_services')}}">
                            {{csrf_field()}}
                                <div class="form-group">
                                  <label for="exampleInputPassword1">Business name  </label>
                                  <input type="text" class="form-control p-input" id="exampleInputPassword1" placeholder=" Business name" name="business_name" value="{{ old('business_name') }}">
                              </div>
                              <input type="hidden" name=" business_type" value="{{$data['name']}}">
                              <div class="form-group">
                                  <label for="exampleInputPassword1">Contact Person</label>
                                  <input type="text" class="form-control p-input" id="exampleInputPassword1" placeholder="Contact Person" name="contact_person" value="{{ old('contact_person') }}">
                              </div>
                             
                               <div class="form-group">
                                  <label for="exampleInputPassword1">Contact Email</label>
                                  <input type="email" class="form-control p-input" id="exampleInputPassword1" placeholder="Contact Email" name="contact_email" value="{{ old('contact_email') }}">
                              </div>

                               <div class="form-group">
                                  <label for="exampleInputPassword1">Whatsapp No</label>
                                  <input type="number" class="form-control p-input" id="exampleInputPassword1" placeholder="Whatsapp No" name="whatsapp_no" value="{{ old('whatsapp_no') }}">
                              </div>

                              <div class="form-group">
                                  <label for="exampleInputPassword1">Website</label>
                                  <input type="text" class="form-control p-input" id="exampleInputPassword1" placeholder="Website" name="website" value="{{ old('website') }}">
                              </div>

                               <div class="form-group">
                                  <label for="exampleInputPassword1">Do you offer home laboratory service?</label>
                                  <input type="text" class="form-control p-input" id="exampleInputPassword1" placeholder="Do you offer home laboratory service?" name="home_laboratory_service" value="{{ old('home_laboratory_service') }}">
                              </div>

                               <div class="form-group">
                                  <label for="exampleInputPassword1">Do you offer online order?</label>
                                  <input type="text" class="form-control p-input" id="exampleInputPassword1" placeholder="Do you offer online order?" name="online_order" value="{{ old('online_order') }}">
                              </div>

                              <div class="form-group">
                                  <label for="exampleInputPassword1">Do you offer home delivery of medications?</label>
                                  <input type="text" class="form-control p-input" id="exampleInputPassword1" placeholder="Do you offer home delivery of medications?" name="home_delivery_medications" value="{{ old('home_delivery_medications') }}">
                              </div>

                              <div class="form-group">
                                  <label for="exampleInputPassword1">Additional Details</label>
                                  <textarea class="form-control" name=" additional_detail">{{ old('additional_detail') }}</textarea>
                              </div>
                              <button type="submit" class="btn btn-primary">Submit</button>
                          </form>
                      </div>
                  </div>
              </div>
            </div>
          </div>
        @endsection
