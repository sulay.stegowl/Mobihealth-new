<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from www.urbanui.com/zoom/ by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 14 Oct 2017 05:16:55 GMT -->
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Mobihealth Admin</title>



  <!-- plugins:css -->


  <link rel="stylesheet" href="{{asset('/assets/node_modules/mdi/css/materialdesignicons.css')}}">
  <link rel="stylesheet" href="{{asset('/assets/node_modules/perfect-scrollbar/dist/css/perfect-scrollbar.min.css')}}">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <link rel="stylesheet" href="{{asset('/assets/node_modules/icheck/skins/all.css')}}" />
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="{{asset('/assets/css/style.css')}}">
  <!-- endinject -->
   <link rel="stylesheet" href="{{asset('/assets/node_modules/simple-line-icons/css/simple-line-icons.css')}}" />
  <link rel="shortcut icon" href="{{asset('/assets/images/logo2.png')}}" />
  <link rel="stylesheet" href="{{asset('/assets/node_modules/font-awesome/css/font-awesome.min.css')}}" />
  <link rel="stylesheet" href="{{asset('/assets/node_modules/datatables.net-bs4/css/dataTables.bootstrap4.css')}}" />
  <link rel="stylesheet" type="text/css" href="{{asset('/assets/node_modules/sweetalert2/dist/sweetalert2.min.css')}}">
  <link rel="stylesheet" href="{{asset('/assets/node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}" />
  <link rel="stylesheet" href="{{asset('/assets/bower_components/clockpicker/dist/jquery-clockpicker.min.css')}}" />
  <link rel="stylesheet" href="{{asset('/assets/node_modules/jquery-asColorPicker/dist/css/asColorPicker.min.css')}}" />



<style type="text/css">
  .alert.alert-danger , .alert.alert-success{
    width: 61%;
    float: none;
    display: table;
    margin: 10px auto;
}
.card.card-statistics
{
  margin-bottom: 20px;
}
.form-group.has-error input.form-control {
    border-color: red;
}
.form-group.has-error label.control-label {
    color: red;
}

.stepwizard-row.setup-panel .stepwizard-step {
    display: table-cell;
    width: 22.56%;
}

/*.actions.clearfix ul li:nth-last-child(1) {
    display: none !important;
}*/

.row.setup-content h3 {
    margin-top: 20px;
}
.form-group.has-error label {
    color: red;
}



.stepwizard-row.setup-panel .stepwizard-step a.btn.btn-default.btn-circle {
    background: white;
    border: 1px solid;
    color: #172388;
    font-size: 18px;
    width: 92%;
    padding: 16px 36px;
}

.stepwizard-row.setup-panel .stepwizard-step a.btn.btn-circle.btn-default.btn-primary {
    color: white;
    background: #172388;
}

.row.setup-content button.btn.btn-primary.nextBtn.btn-lg.pull-right {
    float: right !important;
    text-align: right !important;
    display: block;
    padding: 7px 33px;
}
.row.setup-content button.btn.btn-success.btn-lg.pull-right {
    padding: 7px 33px;
}

button.btn.btn-primary.preBtn.btn-lg {
    padding: 7px 13px;
}
.bg_overlay {
    background: rgba(255, 254, 254, 0.08);
    height: 80px;
    position: absolute;
    width: 100%;
    top: -12px;
    left: -8px;
}
.stepwizard-row.setup-panel {
    position: relative;
}

div#mceu_29 {
    display: none;
}

</style>
</head>
  <body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    @include('include.header')

     @include('include.left')
     <section class="content-header">
        @include('include.message')
        </section>
      @yield('content')
    </div>
     <script src="{{asset('/assets/node_modules/jquery/dist/jquery.min.js')}}"></script>
  <script src="{{asset('/assets/node_modules/popper.js/dist/umd/popper.min.js')}}"></script>
  <script src="{{asset('/assets/node_modules/bootstrap/dist/js/bootstrap.min.js')}}"></script>
  <script src="{{asset('/assets/node_modules/perfect-scrollbar/dist/js/perfect-scrollbar.jquery.min.js')}}"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <script src="{{asset('/assets/node_modules/icheck/icheck.min.js')}}"></script>
  <script src="{{asset('/assets/node_modules/chart.js/dist/Chart.min.js')}}"></script>
  <script src="{{asset('/assets/bower_components/jquery.steps/build/jquery.steps.min.js')}}"></script>
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="{{asset('/assets/js/off-canvas.js')}}"></script>
  <script src="{{asset('/assets/js/hoverable-collapse.js')}}"></script>
  <script src="{{asset('/assets/js/misc.js')}}"></script>
{{--   <script src="{{asset('/assets/js/wizard.js')}}"></script> --}}
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="{{asset('/assets/js/chart.js')}}"></script>
  <script src="{{asset('/assets/js/iCheck.js')}}"></script>
  <script src="{{asset('/assets/node_modules/datatables.net/js/jquery.dataTables.js')}}"></script>
    <script src="{{asset('/assets/node_modules/datatables.net-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script src="{{asset('/assets/js/data-table.js')}}"></script>
<script src="{{asset('/assets/node_modules/sweetalert2/dist/sweetalert2.min.js')}}"></script>
 <script src="{{asset('/assets/js/alerts.js')}}"></script>
 <script src="{{asset('/assets/node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
 <script src="{{asset('/assets/js/formpickers.js')}}"></script>
 <script src="{{asset('/assets/bower_components/clockpicker/dist/jquery-clockpicker.min.js')}}"></script>
  <script src="{{asset('/assets/node_modules/jquery-asColor/dist/jquery-asColor.min.js')}}"></script>
  <script src="{{asset('/assets/node_modules/jquery-asColorPicker/dist/jquery-asColorPicker.min.js')}}"></script>
  


<script src="https://cloud.tinymce.com/stable/tinymce.min.js?api=kjqr0kahfoc38kiakil7r8zhycg4ch347dyjm3tkv1laf5b8"></script>

<script>

tinymce.init({
  selector: '.textarea',
  height: 500,
  theme: 'modern',
  plugins: [
    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
    'searchreplace wordcount visualblocks visualchars code fullscreen',
    'insertdatetime media nonbreaking save table contextmenu directionality',
    'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc help'
  ],
  toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
  toolbar2: 'print preview media | forecolor backcolor emoticons | codesample help',
  image_advtab: true,
  templates: [
    { title: 'Test template 1', content: 'Test 1' },
    { title: 'Test template 2', content: 'Test 2' }
  ],
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css'
  ]
 });
  
</script>

{{-- <script type="text/javascript">

$('.actions.clearfix ul li:nth-last-child(2)').click(function(){

var  name = $("input[name='email']" ).val();
var  suffix = $("input[name='suffix']" ).val();
if(name=='')
{
  alert('fsgg');
  return false;
}
});  
</script> --}}



<script>
  
$(document).ready(function () {



    var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn');
             allPreBtn = $('.preBtn');

    allWells.hide();

    navListItems.click(function (e) {

        e.preventDefault();
        var $target = $($(this).attr('data-link')),
                $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

    allNextBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[data-link="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find(" input[type='number'] , input[type='text'],input[type='url'] , input[type='email'] ,  input[type='file'] "),
            isValid = true;

        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }

        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });


     allPreBtn.click(function(){

        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            preStepWizard = $('div.setup-panel div a[data-link="#' + curStepBtn + '"]').parent().prev().children("a"),

            curInputs = curStep.find(" input[type='number'] , input[type='text'],input[type='url'] , input[type='email'] ,  input[type='file'] "),
            isValid = true;

            $('div.setup-panel div a[data-link="#' + curStepBtn + '"]').removeClass('btn-primary');

 preStepWizard.removeAttr('disabled').trigger('click');

        // $(".form-group").removeClass("has-error");
        // for(var i=0; i<curInputs.length; i++){
        //     if (!curInputs[i].validity.valid){
        //         isValid = false;
        //         $(curInputs[i]).closest(".form-group").addClass("has-error");
        //     }
        // }

        // if (isValid)
           
    });
    $('div.setup-panel div a.btn-primary').trigger('click');
});

</script>
</body>
</html>