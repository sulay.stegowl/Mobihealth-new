<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Additional_services extends Model
{
    //
     public $table='services_registration';
      protected $fillable = ['business_name','business_type','contact_person','contact_email','whatsapp_no','website','home_laboratory_service','online_order','home_delivery_medications','additional_detail'];
}
