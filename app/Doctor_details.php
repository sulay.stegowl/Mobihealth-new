<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doctor_details extends Model
{
    //
    public $table='doctor';
    // protected $fillable = ['best_availability','first_name','middle_name','last_name','email','specialty','title','suffix','language','current_employer','image','degree','prof_regi_no','MDCN_no','MDCN_active','school_name','graduating_year','residency','year_completed','years_of_practic','indemnity','indemnity_provider','states_licensed','regulatory_body','regulatory_body_status','cv','medical_certi','ref_name_1','ref_desg_1','ref_email_1','ref_phone_1','ref_name_2','ref_desg_2','ref_email_2','ref_phone_2','availability','routing_number','account_number','account_holder_name','bank_name','country','dob','gender','residential_address','nationality','country_of_birth','ethnicity','previous_forename','previous_surname','MDCN_expiry_date','country_of_practice','primary_qualification','grade','expirty_date_license','portfolio_no','about','status'];
    	
 protected $fillable= ['Profession','Title','First_Name','Surname','gender','Date_of_Birth','Marital_Status',
 'Other_Names','Current_Address','Home_Telephone','Mobile','Post_Code','Work_Telephone','Ext_Bleep','Email_Address','Next_of_Kin','Relationship','Contact_No_1','Contact_No_2','Address','countries','Passport_No','Issued_At','Expiry_Date','Driving_Licence','own_transport','Bank_Name','Account_Name','Branch_Address','IBAN','SWIFT_BIC','Bank_Post_Code','Account_No','Sort_Code','Reference','Limi_Bank_Name','Limi_Account_Name','Limi_Branch_Address','Limi_IBAN','Limi_SWIFT_BIC','Limi_Post_Code','Limi_Account_No','Limi_Sort_Code','Limi_Reference','Cur_Name','Cur_Address','Cur_Post_Code','Cur_Telephone','Training','Qualification','Date_Graduated','Professional_Insurance','Pro_Name_Insurer','Pro_Policy_No','Pro_Date_of_Issue','Pro_Date_of_Expiry','Medical_Insurance','Medi_Name_Insurer','Medi_Policy_No','Medi_Date_of_Issue','Medi_Date_of_Expiry','Union_Insurance','Union_Name_Insurer','Union_Policy_No','Union_Date_of_Issue','Union_Date_of_Expiry','Available_from','Nights','Odd_Days','Holidays','Weekends','Full_Time','Speciality','telemedicine_Agencies','telemedicine_Agencies_name','ref_1_Name','ref_1_address','ref_1_Post_Code','ref_1_Telephone','ref_1_Fax','ref_1_email','ref_2_Name','ref_2_address','ref_2_Post_Code','ref_2_Telephone','ref_2_Fax','ref_2_email','supply','next_appraisal','supply_details','Appraiser_Name','Professional_Society_No','Society','membership_type','investigated_professional_society','Registration_no','Renewal_Date','Membership_no','under_investigated_professional_society','other_country','Country_practice','License_No','License_Expiry_Date','licensed_to_practice','Portfolio_no','Nigeria_licensed_Expiry_Date','cv','image','certificate','status','driving_licence_front','driving_licence_back','university_address','university_contact','university_email','residential_name','residential_address','residential_contact','residential_email','residency_qualification','residency_graduated','medical_certi1','medical_certi2','medical_certi3','about','what_relation','passport_photo','grade','country_of_issue'];


		

		

		

		

		

		

		

		

}
