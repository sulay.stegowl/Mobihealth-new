<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use Auth;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$role=null)
    {
        
        $userId = Auth::id();
        $user = User::find($userId);
        
          
        if(!Auth::check())
        {
            return redirect('/login');
        }
        else
        {
            if ($user->role == $role) {
                return $next($request);            
            }
            return redirect()->back()->with('error','You Have no access!');
        }
    }
}
