<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Additional_services;
use Storage;
use App\Mail\Welcome_email;
use App\User;
class Hospital extends Controller
{

	public function dashboard()
	{
		$data['hospital']=Additional_services::where(['status'=>1,'business_type'=>'Hospital'])->get()->count();
		$data['subscriber']=Additional_services::where(['status'=>0,'business_type'=>'Hospital'])->get()->count();
		$data['messages']=0;
		return view('hospital.dashboard')->with('data',$data);
	}


	public function hospital_list()
	{
		$data=Additional_services::where('business_type','hospital')->get();
		return view('hospital.hospital_list')->with('Hospital',$data);
	}
}