<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Additional_services;
use Storage;
use App\Mail\Welcome_email;
use App\User;
class Pharmacy extends Controller
{

	public function dashboard()
	{
		$data['pharmacy']=Additional_services::where(['status'=>1,'business_type'=>'Pharmacies'])->get()->count();
		$data['subscriber']=Additional_services::where(['status'=>0,'business_type'=>'Pharmacies'])->get()->count();
		$data['request']=0;
		$data['messages']=0;

		return view('pharmacy.dashboard')->with('data',$data);
	}

	public function pharma_list()
	{
		$data=Additional_services::where('business_type','Pharmacies')->get();
		return view('pharmacy.pharmacy_list')->with('pharmacy',$data);
	}
}