<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Additional_services;
use App\Services_details;
use Auth;
use App\User;
use App\Mail\Welcome_email;
use App\Mail\DeclineMail;
use Mail;
use Storage;
class Other_registration extends Controller
{
    //
    public function add()
    {
    	 $role=Auth::user()->role;
    	 if($role==4)
    	 {
    	 	$data['name']='Pathology';
    	 	$data['role']=4;
    	 }
    	 elseif($role==5)
    	 {
    	 	$data['name']='Pharmacies';
    	 	$data['role']=5;
    	 }
    	 elseif($role==6)
    	 {
    	 	$data['name']='Hospital';
    	 	$data['role']=6;
    	 }
    	 else
    	 {
    	 	$data['name']='Radiology';
    	 	$data['role']=7;
    	 }
    	return view('add_additional_services')->with('data',$data);
    }

    public function save(Request $request)
    {
    	$this->validate($request,[
            'business_name'  => 'required',
            'contact_person'      => 'required',
            'contact_email' => 'required|email',
            'whatsapp_no'=>'required',
            'website'=>'required',
            'home_laboratory_service'=>'required',
            'online_order'=>'required',
            'home_delivery_medications'=>'required',
            'additional_detail'=>'required',
             ]);

    	$check=User::where('email',$request->contact_email)->get();
    	if(count($check)>0)
    	{
    		return redirect()->route('add_addtional_services')->with('error','Email-Id is already exists.');
    	}

    	Additional_services::create($request->all());
    	 $role=Auth::user()->role;
    	 if($role==4)
    	 {
			$role=10;
    	 }
    	 elseif($role==5)
    	 {
    	 	$role=11;
    	 }
    	 elseif($role==6)
    	 {
    	 	$role=12;
    	 }
    	 else
    	 {
    	 	$role=13;
    	 }
    	 $password = str_random(8);
    	 $user=new User;
    	 $user->name=$request->contact_person;
    	 $user->email=$request->contact_email;
    	 $user->password=bcrypt($password);
    	 $user->role=$role;
    	 $user->save();

    	 $test=array(
		 		'name'=>$request->contact_person,
		 		'email'=>$request->contact_email,
		 		'password'=>$password,
		 		'url'=>'http://34.237.167.24/login',
		 		'role'=>$role
		 	);
		 	\Mail::to($request->contact_email)->send(new Welcome_email($test));

    	return redirect()->route('add_addtional_services')->with('success','Record added successfully.');
    }

    public function subscriber()
    {
        $role=Auth::user()->role;
            if($role==4)
            {
                $subscriber=Additional_services::where(['business_type'=>'Pathology','status'=>0])->get();
            }
            elseif($role==5)
            {
                $subscriber=Additional_services::where(['business_type'=>'Pharmacies','status'=>0])->get();
            }
             elseif($role==6)
            {
                $subscriber=Additional_services::where(['business_type'=>'Hospital','status'=>0])->get();
            }
            elseif($role==7)
            {
                $subscriber=Additional_services::where(['business_type'=>'Radiology','status'=>0])->get();
            }
            else{
                return redirect()->route('login');
            }

            return view('additional_subscribers')->with('data',$subscriber);
    }
    public function subscriber_details($id)
    {
        $subscriber=Additional_services::find($id);

        return view('additional_subscribers_details')->with('data',$subscriber);
    }

    public function accept_subscription($id)
    {
        $subscriber=Additional_services::find($id);
        $subscriber->status=1;
        $subscriber->save();

         $role=Auth::user()->role;
         if($role==4)
            {
                $actual_role=10;
            }
            elseif($role==5)
            {
               $actual_role=11;
            }
             elseif($role==6)
            {
               $actual_role=12;
            }
            elseif($role==7)
            {
                $actual_role=13;
            }
            else{
                return redirect()->route('login');
            }

             $password = str_random(8);
             $user=new User;
             $user->name=$subscriber->contact_person;
             $user->email=$subscriber->contact_email;
             $user->password=bcrypt($password);
             $user->role=$actual_role;
             $user->save();


         $test=array(
                'name'=>$subscriber->contact_person,
                'email'=>$subscriber->contact_email,
                'password'=>$password,
                'url'=>'http://34.237.167.24/login',
                'role'=>$actual_role
            );
            \Mail::to($subscriber->contact_email)->send(new Welcome_email($test));

        return redirect()->route('subscribers')->with('success','Business Activated!!..');
    }

    public function decline_subscription($id)
    {
        $subscriber=Additional_services::find($id);
        $subscriber->status=2;
        $subscriber->save();

        $test=array(
            'name'=>$subscriber->contact_person,
        );
        \Mail::to($subscriber->contact_email)->send(new DeclineMail($test));
        return redirect()->route('subscribers')->with('success','Business Deactivated!!..');
    }

    public function add_addtional_services()
    {
        $services=Services_details::all();

        return view('services.show')->with('services',$services);
    }

    public function add_services()
    {
        return view('services.add');
    }

    public function save_additional_services(Request $request)
    {
        $this->validate($request,[
            'title'  => 'required',
            'type'      => 'required',
            'desc' => 'required',
            //'email' =>'required|email',
            'phone' => 'required',
            'address' =>'required'
        ]);
        $input = $request->all();
       
            if($request->hasFile('image'))
         {
             $filenamewithExt = $request->file('image')->getClientOriginalName();
             $filename = pathinfo($filenamewithExt,PATHINFO_FILENAME);
             $filename=str_replace(' ','-',$filename);
             $filename=preg_replace("/[^a-zA-Z0-9\s]/", "", $filename);
             $filename = urlencode($filename);
            // Get just Ext
             $extention = $request->file('image')->getClientOriginalExtension();

            //File name to store
             $image = $filename.'_'.time().'.'.$extention;

            //Upload Path
             $path = $request->file('image')->storeAs('public/services',$image);
             $input['image'] = $image;
             
         }
            $input['slug']=str_replace(' ','-',$request->title);
         $service=Services_details::create($input);
         $temp=Services_details::find($service->id);
         $slug=str_replace(' ','-',$service->title);
         $temp->slug=$slug.'-'.$service->id;
         $temp->save();

         return redirect()->route('services')->with('success','Services Added Successfully!!..');
    }

    public function edit_additional_services($id)
    {
        $services=Services_details::find($id);
        return view('services.edit')->with('service',$services);
    }
    public function update_additional_service(Request $request)
    {
         $this->validate($request,[
            'title'  => 'required',
            'type'      => 'required',
            'desc' => 'required',
            //'email' => 'required|email',
            'phone' => 'required',
            'address' => 'required'
        ]);
         $service=Services_details::find($request->id);
       
            if($request->hasFile('image'))
         {
            //delete old file
            Storage::delete('public/services/'.$service->image);

             $filenamewithExt = $request->file('image')->getClientOriginalName();
             $filename = pathinfo($filenamewithExt,PATHINFO_FILENAME);
             $filename=str_replace(' ','-',$filename);
             $filename=preg_replace("/[^a-zA-Z0-9\s]/", "", $filename);
             $filename = urlencode($filename);
            // Get just Ext
             $extention = $request->file('image')->getClientOriginalExtension();

            //File name to store
             $image = $filename.'_'.time().'.'.$extention;

            //Upload Path
             $path = $request->file('image')->storeAs('public/services',$image);
             $service->image = $image;
             
         }
         $service->title=$request->title;
         $slug=str_replace(' ','-',$request->title);
         $request->slug=$slug.'-'.$request->id;
         $service->type=$request->type;
         $service->desc=$request->desc;
         $service->email=$request->email;
         $service->phone=$request->phone;
         $service->address=$request->address;
         $service->save();

         return redirect()->route('services')->with('success','Services Updated Successfully!!..');
    }
    public function delete_additional_service($id)
    {
         $service = Services_details::find($id);
        if($service->image != NULL)
        {
            Storage::delete('public/services/'.$service->image);            
        }

        $service->delete();

        return redirect()->route('services')->with('success','Service deleted successfully!!..');
    }
}
