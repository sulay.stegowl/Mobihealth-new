<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Additional_services;
use Storage;
use App\Mail\Welcome_email;
use App\User;
class Radiology extends Controller
{

	public function dashboard()
	{
		$data['radiology']=Additional_services::where(['status'=>1,'business_type'=>'Radiology'])->get()->count();
		$data['subscriber']=Additional_services::where(['status'=>0,'business_type'=>'Radiology'])->get()->count();
		$data['request']=0;
		$data['messages']=0;
		return view('radiology.dashboard')->with('data',$data);
	}

	public function radiology_list()
	{
		$data=Additional_services::where('business_type','Radiology')->get();
		return view('radiology.show_radiology')->with('radiology',$data);
	}
}