<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Doctor_details;
use Storage;
use App\Mail\Welcome_email;
use App\Mail\DeclineMail;
use App\User;
use App\Subscription_request;
class Doctor extends Controller
{
      public function dashboard()
      {
             $data['doctor']=Doctor_details::where('Profession','Doctor')->where('status',1)->get()->count();
             $data['dentist']=Doctor_details::where('Profession','Dentist')->where('status',1)->get()->count();
             $data['lab']=Doctor_details::where('Profession','Laboratory_Technician')->where('status',1)->get()->count();
             $data['nurse']=Doctor_details::where('Profession','Nurse')->where('status',1)->get()->count();
             $data['physiotherapist']=Doctor_details::where('Profession','Physiotherapist')->where('status',1)->get()->count();
             $data['radio']=Doctor_details::where('Profession','Pharmacist')->where('status',1)->get()->count();
            return view('doctor.dashboard')->with('data',$data);
      }


	public function add()
	{
		//Storage::delete('public/doctor/1428258237117_1509362810.png');
            $countries = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe");
		return view('doctor.new_add_doctor')->with('country',$countries);
	}

	public function save(Request $request)
	{
            
           $check=Doctor_details::where('Email_Address',$request->Email_Address)->get();
            if(count($check)>0)
            {
                  return redirect()->route('add.doctor')->with('error','Email-Id is already exists.');
            }
		$input = $request->all();
		
		 if($request->hasFile('image'))
		 {
		 	 $filenamewithExt = $request->file('image')->getClientOriginalName();
		 	$filename = pathinfo($filenamewithExt,PATHINFO_FILENAME);
		 	 $filename=str_replace(' ','-',$filename);
            $filename=preg_replace("/[^a-zA-Z0-9\s]/", "", $filename);
            $filename = urlencode($filename);
            // Get just Ext
            $extention = $request->file('image')->getClientOriginalExtension();

            //File name to store
            $image = $filename.'_'.time().'.'.$extention;
            //Upload Path
            $path = $request->file('image')->storeAs('public/doctor',$image);
            $input['image'] = $image;
             
		 }
		
		 if($request->hasFile('cv'))
		 {
		 	$filenamewithExt = $request->file('cv')->getClientOriginalName();
		 	$filename = pathinfo($filenamewithExt,PATHINFO_FILENAME);
		 	$filename=str_replace(' ','-',$filename);
            $filename=preg_replace("/[^a-zA-Z0-9\s]/", "", $filename);
            $filename = urlencode($filename);
            // Get just Ext
            $extention = $request->file('cv')->getClientOriginalExtension();
           
            //File name to store
            $cv = $filename.'_'.time().'.'.$extention;
            //Upload Path
            $path = $request->file('cv')->storeAs('public/doctor',$cv);

             $input['cv']=$cv;
		 }
		  if($request->hasFile('certificate'))
		 {
		 	$filenamewithExt = $request->file('certificate')->getClientOriginalName();
		 	$filename = pathinfo($filenamewithExt,PATHINFO_FILENAME);
		 	$filename=str_replace(' ','-',$filename);
            $filename=preg_replace("/[^a-zA-Z0-9\s]/", "", $filename);
            $filename = urlencode($filename);
            // Get just Ext
            $extention = $request->file('certificate')->getClientOriginalExtension();
           
            //File name to store
            $certificate = $filename.'_'.time().'.'.$extention;
            //Upload Path
            $path = $request->file('certificate')->storeAs('public/doctor',$certificate);

            $input['certificate']=$certificate;
		 }		
		 	Doctor_details::create($input);
		 	 $password = str_random(8);
		 	$user=new User;
		 	$user->name=$request->First_Name.' '.$request->Surname;
		 	$user->email=$request->Email_Address;
		 	$user->password=bcrypt($password);
		 	$user->role=8;
		 	$user->save();

		 	$test=array(
		 		'name'=>$request->First_Name.' '.$request->Surname,
		 		'email'=>$request->Email_Address,
		 		'password'=>$password,
		 		'url'=>'http://34.237.167.24/login',
		 		'role'=>8
		 	);
		 	\Mail::to($request->Email_Address)->send(new Welcome_email($test));

		 	return redirect()->route('add.doctor')->with('success','Doctor added successfully.');
	}

      public function doctors_list()
      {
            $doctor=Doctor_details::where('status',1)->where('Profession','Doctor')->orderBy('id', 'DESC')->get();
            return view('doctor.list')->with('doctors',$doctor);
      }

      public function doctor_details($id)
      {
            $doctor=Doctor_details::find($id);
            return view('doctor.doctor_details')->with('doctor',$doctor);
      }

      public function dentists_list()
      {
            $doctor=Doctor_details::where('status',1)->where('Profession','Dentist')->orderBy('id', 'DESC')->get();
            return view('doctor.dentist')->with('doctors',$doctor);
      }

      public function lab_list()
      {
        $doctor=Doctor_details::where('status',1)->where('Profession','Laboratory_Technician')->orderBy('id', 'DESC')->get();
            return view('doctor.lab')->with('doctors',$doctor);
      }
      public function nurse_list()
      {
        $doctor=Doctor_details::where('status',1)->where('Profession','Nurse')->orderBy('id', 'DESC')->get();
            return view('doctor.nurse')->with('doctors',$doctor);
      }

      public function physiotherapist_list()
      {
        $doctor=Doctor_details::where('status',1)->where('Profession','Physiotherapist')->orderBy('id', 'DESC')->get();
            return view('doctor.physiotherapist')->with('doctors',$doctor);
      }

      public function pharmacist_list()
      {
        $doctor=Doctor_details::where('status',1)->where('Profession','Pharmacist')->orderBy('id', 'DESC')->get();
            return view('doctor.pharmacist')->with('doctors',$doctor);
      }

      public function subscriber()
      {
            $doctor=Doctor_details::where('status',0)->orderBy('id', 'DESC')->get();

            return view('doctor.subscriber')->with('doctors',$doctor);
      }

      public function subscriber_details($id)
      {
            $doctor=Doctor_details::find($id);
            return view('doctor.subscriber_details')->with('doctor',$doctor);
      }

      public function activate($id)
      {
            $doctor=Doctor_details::find($id);
            $doctor->status=1;
            $doctor->save();

            $password = str_random(8);
            $user=new User;
            $user->name=$doctor->First_Name.' '.$doctor->Surname;
            $user->email=$doctor->Email_Address;
            $user->password=bcrypt($password);
            $user->role=8;
            $user->save();

            $test=array(
                        'name'=>$doctor->First_Name.' '.$doctor->Surname,
                        'email'=>$doctor->Email_Address,
                        'password'=>$password,
                        'url'=>'http://34.237.167.24/login'
                  );
                  \Mail::to($doctor->Email_Address)->send(new Welcome_email($test));

                  return redirect()->route('doctor.subscriber')->with('success','Doctor Activated..!!');
      }

      public function deactivate($id)
      {
        try{
            $doctor=Doctor_details::find($id);
            $doctor->status=2;
            $doctor->save();

             $test=array(
            'name'=>$doctor->First_Name.' '.$doctor->Surname,
            );
            \Mail::to($doctor->Email_Address)->send(new DeclineMail($test));
            return redirect()->route('doctor.subscriber')->with('success','Doctor Deactivated..!!');
        }
        catch(\Exception $e)
        {
            return redirect()->route('doctor.subscriber')->with('error',$e->getMessage());
        }
      }
      public function subscription()
      {
        $data=Subscription_request::all();
        return view('doctor.subscription_list')->with('data',$data);
      }
}
