<?php

namespace App\Http\Controllers\front;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Contact;
use App\User;
use App\Additional_services;
use App\Services_details;
use App\Mail\Welcome;
use Mail;
use App\Doctor_details;
class Service_details extends Controller
{
	public function temp()
	{
		return view('frontend.home_temp');
	}
	public function services_1()
	{
		 return view('frontend.services-details_1');
	}
	public function services_2()
	{
		 return view('frontend.services-details_2');
	}
	public function services_3()
	{
		 return view('frontend.services-details_3');
	}
	public function services_4()
	{
		 return view('frontend.services-details_4');
	}
	public function services_5()
	{
		 return view('frontend.services-details_5');
	}
	public function services_6()
	{
		 return view('frontend.services-details_6');
	}

	public function search_hospital(Request $request)
	{
		//return $request->value;
		if($request->search_by=="Name")
		{
			$deatils=Services_details::where('title','like','%'.$request->value.'%')->where('type','Hospital')->get();
		}
		else if($request->search_by=="State")
		{
			$deatils=Services_details::where('address','like','%'.$request->value.'%')->where('type','Hospital')->get();
		}
		else if($request->search_by=="Country")
		{
			$deatils=Services_details::where('country',$request->value)->where('type','Hospital')->get();
		}
		else
		{
			$deatils=Services_details::where('address','like','%'.$request->value.'%')->where('type','Hospital')->get();
		}

		return view('frontend.hospital')->with('ans' , $deatils);
		//$deatils=Services_details::where()
	}

	public function search_pathology(Request $request)
	{
		if($request->search_by=="Name")
		{
			$deatils=Services_details::where('title','like','%'.$request->value.'%')->where('type','Pathology')->get();
		}
		else if($request->search_by=="State")
		{
			$deatils=Services_details::where('address','like','%'.$request->value.'%')->where('type','Pathology')->get();
		}
		else if($request->search_by=="Country")
		{
			$deatils=Services_details::where('country',$request->value)->where('type','Pathology')->get();
		}
		else
		{
			$deatils=Services_details::where('address','like','%'.$request->value.'%')->where('type','Pathology')->get();
		}

		return view('frontend.pathology')->with('ans' , $deatils);
	}

	public function search_pharmacy(Request $request)
	{
		if($request->search_by=="Name")
		{
			$deatils=Services_details::where('title','like','%'.$request->value.'%')->where('type','Pharmacies')->get();
		}
		else if($request->search_by=="State")
		{
			$deatils=Services_details::where('address','like','%'.$request->value.'%')->where('type','Pharmacies')->get();
		}
		else if($request->search_by=="Country")
		{
			$deatils=Services_details::where('country',$request->value)->where('type','Pharmacies')->get();
		}
		else
		{
			$deatils=Services_details::where('address','like','%'.$request->value.'%')->where('type','Pharmacies')->get();
		}

		return view('frontend.pathology')->with('ans' , $deatils);
	}


		public function search_radiology(Request $request)
	{
		if($request->search_by=="Name")
		{
			$deatils=Services_details::where('title','like','%'.$request->value.'%')->where('type','Radiology')->get();
		}
		else if($request->search_by=="State")
		{
			$deatils=Services_details::where('address','like','%'.$request->value.'%')->where('type','Radiology')->get();
		}
		else if($request->search_by=="Country")
		{
			$deatils=Services_details::where('country',$request->value)->where('type','Radiology')->get();
		}
		else
		{
			$deatils=Services_details::where('address','like','%'.$request->value.'%')->where('type','Radiology')->get();
		}

		return view('frontend.radiology')->with('ans' , $deatils);
	}

	public function hospital()
	{
		return view('frontend.pagination_view.hospital');
	}

	public function hospital_result()
	{
		$deatils=Services_details::where('type','Hospital')->paginate(8);
		return $deatils;
	}
	
	public function pagination_pathology()
	{
		return view('frontend.paginate_demo');
	}

	public function pathology_result()
	{
		$deatils=Services_details::where('type','Pathology')->paginate(8);
		return $deatils;
	}
	public function pharmacy_result()
	{
		$deatils=Services_details::where('type','Pharmacies')->paginate(8);
		return $deatils;
	}
	public function radiology_result()
	{
		$deatils=Services_details::where('type','Radiology')->paginate(8);
		return $deatils;
	}

	public function privacy_policy()
	{
		return view('frontend.privacy_policy');
	}

	public function terms_condition()
	{
		return view('frontend.terms_condition');
	}

	public function disclaimers()
	{
		return view('frontend.disclaimers');
	}
}
