<?php

namespace App\Http\Controllers\front;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Contact;
use App\User;
use App\Additional_services;
use App\Services_details;
use App\Mail\Welcome;
use Mail;
use App\Doctor_details;
class Service_faq extends Controller
{
	public function hospital_how()
	{
		return view('frontend.hospital_how');
	}
	public function hospital_faq()
	{
		return view('frontend.hospital_faq');
	}

	public function pathology_how()
	{
		return view('frontend.pathology_how');
	}
	public function pathology_faq()
	{
		return view('frontend.pathology_faq');
	}

	public function pharmacy_how()
	{
		return view('frontend.pharmacy_how');
	}
	public function pharmacy_faq()
	{
		return view('frontend.pharmacy_faq');
	}

	public function doctor_how()
	{
		return view('frontend.doctor_how');
	}
	public function doctor_faq()
	{
		return view('frontend.doctor_faq');
	}

	public function radiology_how()
	{
		return view('frontend.radiology_how');
	}
	public function radiology_faq()
	{
		return view('frontend.radiology_faq');
	}

	public function patients_how()
	{
		return view('frontend.patients_how');
	}
	public function patients_faq()
	{
		return view('frontend.patients_faq');
	}

	
	public function subscription()
	{
		return view('frontend.subscription');
	}
	public function subscription_plan_1()
	{
		return view('frontend.subscription_plan_1');
	}
	public function subscription_plan_2()
	{
		return view('frontend.subscription_plan_2');
	}


}
