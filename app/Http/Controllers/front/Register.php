<?php

namespace App\Http\Controllers\front;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Contact;
use App\User;
use App\Additional_services;
use App\Services_details;
use App\Mail\Welcome;
use Mail;
use App\Doctor_details;
class Register extends Controller
{
	public function radiology()
	{
		return view('frontend.radiology_register');
	}

	public function hospital()
	{
		return view('frontend.hospital_register');
	}

	public function pharmacy()
	{
		return view('frontend.pharmacy_register');
	}

}