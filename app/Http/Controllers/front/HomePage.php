<?php

namespace App\Http\Controllers\front;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Contact;
use App\User;
use App\Additional_services;
use App\About_Index;
use App\Telemedicine;
use App\Mail\Welcome;
use Mail;
use App\Doctor_details;
use App\Services_details;
use App\Subscription_request;
use App\Homepage_text;

class HomePage extends Controller
{
	public function index()
	{
		$data1=Telemedicine::find(1);
		$data2 = Homepage_text::find(1);
		
		$data = array( "data" => $data1 , "data2" => $data2 );

		 return view('frontend.homepage')->with($data);
	}
	public function form()
	{
		 return view('frontend.form');
	}
	public function about()
	{
		$data=About_Index::find(1);
		 return view('frontend.about')->with('data',$data);
	}
	public function contact()
	{
		 return view('frontend.contact');
	}
	public function doctor_detail()
	{
		 return view('frontend.doctor-detail');
	}
	public function doctors()
	{
		 return view('frontend.doctors');
	}
	

	public function pathology_details($id)
	{	
		$data = Services_details::where('slug' , $id)->first();
		 return view('frontend.pathology-details')->with('data' , $data);
	}

public function hospital_details($id)
	{
		$data = Services_details::where('slug' , $id)->first();
		 return view('frontend.hospital-details')->with('data' , $data);
	}

	public function hospital()
	{
		$ans = Services_details::where('type' , 'Hospital')->orderBy('id', 'DESC')->get();
		 return view('frontend.pagination_view.hospital')->with('ans' , $ans);
	}


	public function radiology_details($id)
	{
		$data = Services_details::where('slug' , $id)->first();
		 return view('frontend.radiology_details')->with('data' , $data);
	}
	
	public function radiology()
	{
		return view('frontend.pagination_view.radiology');
	}

	
	public function pathology()
	{
		 return view('frontend.pagination_view.pathology');
	}
	public function news_details_1()
	{
		 return view('frontend.news-details');
	}
	public function news_details_2()
	{
		 return view('frontend.news-details_2');
	}
	public function news_details_3()
	{
		 return view('frontend.news-details_3');
	}
	public function news()
	{
		 return view('frontend.news');
	}
	public function pharmacy_details($id)
	{
		$data = Services_details::where('slug' , $id)->first();
		 return view('frontend.pharmacy-details')->with('data' , $data);
	}
	public function pharmacy()
	{
		return view('frontend.pagination_view.pharmacy');
	}
	public function services_details()
	{
		 return view('frontend.services-details');
	}
	public function services()
	{
		 return view('frontend.services');
	}
	public function registration()
	{
		 return view('frontend.registration');
	}
	public function doctors_registration()
	{	
		$countries = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe");
		 	
		 	$data['countries'] = $countries;
		 return view('frontend.doctors_registration')->with($data);
	}
	public function steps(Request $request)
	{
		
$countries = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe");

		// $this->validate($request,[
  //           'first_name'      => 'required',
  //           'last_name'  => 'required',
  //           'email'      => 'required|email',
  //           'specialty' => 'required',
  //           'states_licensed'=>'required'
  //       ]);

		$check=User::where('email',$request->Email_Address)->get();
		$check2=Doctor_details::where('Email_Address',$request->Email_Address)->first();
		//$check3=Additional_services::where('contact_email',$request->contact_email)->get();
		
		if(count($check)>0 || count($check2)>0 )
    	{
    		return redirect()->route('doctors_registration')->with('error','Email-Id is already exists.');
    	}

    	if($request->states_licensed=='no')
    	{
    		return redirect()->route('doctors_registration')->with('error','You can not allow to register without license to practice.');
    	}

    	$date1=date("Y-m-d", strtotime($request->Date_of_Birth));
    	
$date2 = date('Y-m-d');

$diff = abs(strtotime($date2) - strtotime($date1));

$years = floor($diff / (365*60*60*24));
if($years<18)
{
	return redirect()->route('doctors_registration')->with('error','Your age must be greater than 18.');
}
    	$input = $request->all();
		
    	if($request->hasFile('cv'))
		 {
		 	$filenamewithExt = $request->file('cv')->getClientOriginalName();
		 	$filename = pathinfo($filenamewithExt,PATHINFO_FILENAME);
		 	$filename=str_replace(' ','-',$filename);
            $filename=preg_replace("/[^a-zA-Z0-9\s]/", "", $filename);
            $filename = urlencode($filename);
            // Get just Ext
            $extention = $request->file('cv')->getClientOriginalExtension();
           
            //File name to store
            $cv = $filename.'_'.time().'.'.$extention;
			
            //Upload Path
            $path = $request->file('cv')->storeAs('public/doctor',$cv);

             $input['cv']=$cv;
		 }
		 
		  if($request->hasFile('image'))
		 {
		 	 $filenamewithExt = $request->file('image')->getClientOriginalName();
		 	$filename = pathinfo($filenamewithExt,PATHINFO_FILENAME);
		 	 $filename=str_replace(' ','-',$filename);
            $filename=preg_replace("/[^a-zA-Z0-9\s]/", "", $filename);
            $filename = urlencode($filename);
            // Get just Ext
            $extention = $request->file('image')->getClientOriginalExtension();

            //File name to store
            $image = $filename.'_'.time().'.'.$extention;
            //Upload Path
            $path = $request->file('image')->storeAs('public/doctor',$image);
            $input['image'] = $image;
             
		 }

		 if($request->hasFile('medical_certi'))
		 {
		 	$filenamewithExt = $request->file('medical_certi')->getClientOriginalName();
		 	$filename = pathinfo($filenamewithExt,PATHINFO_FILENAME);
		 	$filename=str_replace(' ','-',$filename);
            $filename=preg_replace("/[^a-zA-Z0-9\s]/", "", $filename);
            $filename = urlencode($filename);
            // Get just Ext
            $extention = $request->file('medical_certi')->getClientOriginalExtension();
           
            //File name to store
            $medical_certi = $filename.'_'.time().'.'.$extention;
            //Upload Path
            $path = $request->file('medical_certi')->storeAs('public/doctor',$medical_certi);

            $input['certificate']=$medical_certi;
		 }	
		 
		 $doctor=Doctor_details::create($input);

		 	$test=array(
		 		'name'=>$request->First_Name.' '.$request->Surname,
		 	);
		 	\Mail::to($request->Email_Address)->send(new Welcome($test));

      return redirect()->route('thank_you');
// $data['countries'] = $countries;
// $data['request'] = $request;

// 			return view('frontend.steps_not_require')->with($data);
	 
	}

	public function doctor_save(Request $request)
	{

		
		$input = $request->all();
		
		 if($request->hasFile('image'))
		 {
		 	 $filenamewithExt = $request->file('image')->getClientOriginalName();
		 	$filename = pathinfo($filenamewithExt,PATHINFO_FILENAME);
		 	 $filename=str_replace(' ','-',$filename);
            $filename=preg_replace("/[^a-zA-Z0-9\s]/", "", $filename);
            $filename = urlencode($filename);
            // Get just Ext
            $extention = $request->file('image')->getClientOriginalExtension();

            //File name to store
            $image = $filename.'_'.time().'.'.$extention;
            //Upload Path
            $path = $request->file('image')->storeAs('public/doctor',$image);
            $input['image'] = $image;
             
		 }
		
		 if($request->hasFile('cv'))
		 {
		 	$filenamewithExt = $request->file('cv')->getClientOriginalName();
		 	$filename = pathinfo($filenamewithExt,PATHINFO_FILENAME);
		 	$filename=str_replace(' ','-',$filename);
            $filename=preg_replace("/[^a-zA-Z0-9\s]/", "", $filename);
            $filename = urlencode($filename);
            // Get just Ext
            $extention = $request->file('cv')->getClientOriginalExtension();
           
            //File name to store
            $cv = $filename.'_'.time().'.'.$extention;
            //Upload Path
            $path = $request->file('cv')->storeAs('public/doctor',$cv);

             $input['cv']=$cv;
		 }
		  if($request->hasFile('medical_certi'))
		 {
		 	$filenamewithExt = $request->file('medical_certi')->getClientOriginalName();
		 	$filename = pathinfo($filenamewithExt,PATHINFO_FILENAME);
		 	$filename=str_replace(' ','-',$filename);
            $filename=preg_replace("/[^a-zA-Z0-9\s]/", "", $filename);
            $filename = urlencode($filename);
            // Get just Ext
            $extention = $request->file('medical_certi')->getClientOriginalExtension();
           
            //File name to store
            $medical_certi = $filename.'_'.time().'.'.$extention;
            //Upload Path
            $path = $request->file('medical_certi')->storeAs('public/doctor',$medical_certi);

            $input['certificate']=$medical_certi;
		 }	

		  if($request->hasFile('medical_certi1'))
		 {
		 	$filenamewithExt = $request->file('medical_certi1')->getClientOriginalName();
		 	$filename = pathinfo($filenamewithExt,PATHINFO_FILENAME);
		 	$filename=str_replace(' ','-',$filename);
            $filename=preg_replace("/[^a-zA-Z0-9\s]/", "", $filename);
            $filename = urlencode($filename);
            // Get just Ext
            $extention = $request->file('medical_certi1')->getClientOriginalExtension();
           
            //File name to store
            $medical_certi1 = $filename.'_'.time().'.'.$extention;
            //Upload Path
            $path = $request->file('medical_certi1')->storeAs('public/doctor',$medical_certi1);

            $input['medical_certi1']=$medical_certi1;
		 }		

		   if($request->hasFile('medical_certi2'))
		 {
		 	$filenamewithExt = $request->file('medical_certi2')->getClientOriginalName();
		 	$filename = pathinfo($filenamewithExt,PATHINFO_FILENAME);
		 	$filename=str_replace(' ','-',$filename);
            $filename=preg_replace("/[^a-zA-Z0-9\s]/", "", $filename);
            $filename = urlencode($filename);
            // Get just Ext
            $extention = $request->file('medical_certi2')->getClientOriginalExtension();
           
            //File name to store
            $medical_certi2 = $filename.'_'.time().'.'.$extention;
            //Upload Path
            $path = $request->file('medical_certi2')->storeAs('public/doctor',$medical_certi2);

            $input['medical_certi2']=$medical_certi2;
		 }		
		 
		   if($request->hasFile('medical_certi3'))
		 {
		 	$filenamewithExt = $request->file('medical_certi3')->getClientOriginalName();
		 	$filename = pathinfo($filenamewithExt,PATHINFO_FILENAME);
		 	$filename=str_replace(' ','-',$filename);
            $filename=preg_replace("/[^a-zA-Z0-9\s]/", "", $filename);
            $filename = urlencode($filename);
            // Get just Ext
            $extention = $request->file('medical_certi3')->getClientOriginalExtension();
           
            //File name to store
            $medical_certi3 = $filename.'_'.time().'.'.$extention;
            //Upload Path
            $path = $request->file('medical_certi3')->storeAs('public/doctor',$medical_certi3);

            $input['medical_certi3']=$medical_certi3;
		 }		

		    if($request->hasFile('passport_photo'))
		 {
		 	$filenamewithExt = $request->file('passport_photo')->getClientOriginalName();
		 	$filename = pathinfo($filenamewithExt,PATHINFO_FILENAME);
		 	$filename=str_replace(' ','-',$filename);
            $filename=preg_replace("/[^a-zA-Z0-9\s]/", "", $filename);
            $filename = urlencode($filename);
            // Get just Ext
            $extention = $request->file('passport_photo')->getClientOriginalExtension();
           
            //File name to store
            $passport_photo = $filename.'_'.time().'.'.$extention;
            //Upload Path
            $path = $request->file('passport_photo')->storeAs('public/doctor',$passport_photo);

            $input['passport_photo']=$passport_photo;
		 }		

		   if($request->hasFile('driving_licence_front'))
		 {
		 	$filenamewithExt = $request->file('driving_licence_front')->getClientOriginalName();
		 	$filename = pathinfo($filenamewithExt,PATHINFO_FILENAME);
		 	$filename=str_replace(' ','-',$filename);
            $filename=preg_replace("/[^a-zA-Z0-9\s]/", "", $filename);
            $filename = urlencode($filename);
            // Get just Ext
            $extention = $request->file('driving_licence_front')->getClientOriginalExtension();
           
            //File name to store
            $driving_licence_front = $filename.'_'.time().'.'.$extention;
            //Upload Path
            $path = $request->file('driving_licence_front')->storeAs('public/doctor',$driving_licence_front);

            $input['driving_licence_front']=$driving_licence_front;
		 }		

		  if($request->hasFile('driving_licence_back'))
		 {
		 	$filenamewithExt = $request->file('driving_licence_back')->getClientOriginalName();
		 	$filename = pathinfo($filenamewithExt,PATHINFO_FILENAME);
		 	$filename=str_replace(' ','-',$filename);
            $filename=preg_replace("/[^a-zA-Z0-9\s]/", "", $filename);
            $filename = urlencode($filename);
            // Get just Ext
            $extention = $request->file('driving_licence_back')->getClientOriginalExtension();
           
            //File name to store
            $driving_licence_back = $filename.'_'.time().'.'.$extention;
            //Upload Path
            $path = $request->file('driving_licence_back')->storeAs('public/doctor',$driving_licence_back);

            $input['driving_licence_back']=$driving_licence_back;
		 }		
		
		 	$doctor=Doctor_details::create($input);

		 	$test=array(
		 		'name'=>$request->First_Name.' '.$request->Surname,
		 	);
		 	\Mail::to($request->Email_Address)->send(new Welcome($test));

      return redirect()->route('thank_you');

	//	 	return redirect()->route('registration')->with('success','Your registration have been successfully added.');
	}




	public function register(Request $request)
	{
		 return view('frontend.register')->with('data' , $request);
	}
	public function send_contact_us(Request $request)
	{
		Contact::create($request->all());

		return redirect()->route('contact');
	}
	public function submit_register(Request $request)
	{
		$this->validate($request,[
            'business_name'      => 'required',
            'business_type'  => 'required',
            'contact_person'    => 'required',
            'contact_email' => 'required|email',
            'whatsapp_no'=>'required',
            'website'=>'required',
            'home_laboratory_service'=>'required',
            'online_order'=>'required',
            'home_delivery_medications'=>'required',
            'additional_detail'=>'required',
        ]);

		$check=User::where('email',$request->contact_email)->get();
		$check2=Doctor_details::where('Email_Address',$request->contact_email)->get();
		$check3=Additional_services::where('contact_email',$request->contact_email)->get();
		
		if(count($check)>0 || count($check2)>0 || count($check3)>0)
    	{
    		return redirect()->route('pathology_register')->with('error','Email-Id is already exists.');
    	}
    	Additional_services::create($request->all());

    	  $test=array(
		 		'name'=>$request->contact_person,
		 	);
		 	\Mail::to($request->contact_email)->send(new Welcome($test));


        return redirect()->route('thank_you');
	}

		public function thank_you()
		{
			return view('frontend.thank_you');			
		}

	public function save_subscription(Request $request)
	{
		$data= new Subscription_request;
		$data->name=$request->name;
		$data->country=$request->country;
		$data->email=$request->email;
		$data->phone=$request->number;
		$data->plan=$request->plan;
		$data->save();
		return redirect()->route('subscription')->with('success','Subscriptions Request Sent Successfully.');
	}
	public function new_page()
	{
		return view('frontend.new_page');
	}
}
