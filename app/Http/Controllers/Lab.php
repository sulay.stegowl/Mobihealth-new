<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Additional_services;
use Storage;
use App\Mail\Welcome_email;
use App\User;
class Lab extends Controller
{

	public function dashboard()
	{
		$data['subscriber']=Additional_services::where(['business_type'=>'Pathology','status'=>0])->get()->count();
		$data['pathology']=Additional_services::where(['business_type'=>'Pathology','status'=>1])->get()->count();
		$data['request']=0;
		$data['messages']=0;
		$data['prescription']=0;
		return view('lab.dashboard')->with('data',$data);
	}

	public function lab_list()
	{
		$data=Additional_services::where('business_type','Pathology')->get();
		return view('lab.show')->with('lab',$data);
	}
}