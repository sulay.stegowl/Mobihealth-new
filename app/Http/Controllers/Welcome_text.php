<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Additional_services;
use Storage;
use App\Mail\Welcome_email;
use App\About_Index;
use App\Telemedicine;
use App\Homepage_text;
class Welcome_text extends Controller
{

	public function about()
	{

		$data=About_Index::find(1);

		return view('Front_text.about_text')->with('data' , $data);
	}
	
	public function about_save(Request $request)
	{

		$input = $request->all();
		
		 if($request->hasFile('image'))
		 {
		 	 $filenamewithExt = $request->file('image')->getClientOriginalName();
		 	$filename = pathinfo($filenamewithExt,PATHINFO_FILENAME);
		 	 $filename=str_replace(' ','-',$filename);
            $filename=preg_replace("/[^a-zA-Z0-9\s]/", "", $filename);
            $filename = urlencode($filename);
            // Get just Ext
            $extention = $request->file('image')->getClientOriginalExtension();

            //File name to store
            $image = $filename.'_'.time().'.'.$extention;
            //Upload Path
            $path = $request->file('image')->storeAs('public/',$image);
            $input['image'] = $image;
             
             	$user=About_Index::find(1);
	
		 	$user->wel_image=$image;
		 	$user->save();

		 }
		
		 		
		 	$user=About_Index::find(1);
		 	$user->wel_title=$request->title;
		 	$user->wel_text=$request->text;
		 	
		 	$user->save();

		 	return redirect()->route('aboutus_text')->with('success','Text Updated successfully.');

	}

	public function telemedicine()
	{
		$data=Telemedicine::find(1);

		return view('Front_text.telemedicine_text')->with('data' , $data);
	}


	public function telemedicine_save(Request $request)
	{

		$input = $request->all();
		
		 if($request->hasFile('image'))
		 {
		 	 $filenamewithExt = $request->file('image')->getClientOriginalName();
		 	$filename = pathinfo($filenamewithExt,PATHINFO_FILENAME);
		 	 $filename=str_replace(' ','-',$filename);
            $filename=preg_replace("/[^a-zA-Z0-9\s]/", "", $filename);
            $filename = urlencode($filename);
            // Get just Ext
            $extention = $request->file('image')->getClientOriginalExtension();

            //File name to store
            $image = $filename.'_'.time().'.'.$extention;
            //Upload Path
            $path = $request->file('image')->storeAs('public/',$image);
            $input['image'] = $image;
             
            $user=Telemedicine::find(1);
		 	$user->image=$image;
		 	$user->save();
		 }
		
		 		
		 	$user=Telemedicine::find(1);
		 	$user->title=$request->title;
		 	$user->text=$request->text;
		 	
		 	$user->save();

		 	return redirect()->route('telemedicine_text')->with('success','Text Updated successfully.');

	}


public function homepage()
	{
		$data=Homepage_text::find(1);
		return view('Front_text.homepage_text')->with('data' , $data);
	}


	public function homepage_save(Request $request)
	{

		$input = $request->all();
		
		 if($request->hasFile('image1'))
		 {
		 	 $filenamewithExt = $request->file('image1')->getClientOriginalName();
		 	$filename = pathinfo($filenamewithExt,PATHINFO_FILENAME);
		 	 $filename=str_replace(' ','-',$filename);
            $filename=preg_replace("/[^a-zA-Z0-9\s]/", "", $filename);
            $filename = urlencode($filename);
            // Get just Ext
            $extention = $request->file('image1')->getClientOriginalExtension();

            //File name to store
            $image_1 = $filename.'_'.time().'.'.$extention;
            //Upload Path
            $path = $request->file('image1')->storeAs('public/',$image_1);
            $input['image1'] = $image_1;

            $user=Homepage_text::find(1);
		 	$user->image_1=$image_1;
		 	$user->save();
             
		 }
		
		if($request->hasFile('image2'))
		 {
		 	 $filenamewithExt = $request->file('image2')->getClientOriginalName();
		 	$filename = pathinfo($filenamewithExt,PATHINFO_FILENAME);
		 	 $filename=str_replace(' ','-',$filename);
            $filename=preg_replace("/[^a-zA-Z0-9\s]/", "", $filename);
            $filename = urlencode($filename);
            // Get just Ext
            $extention = $request->file('image2')->getClientOriginalExtension();

            //File name to store
            $image_2 = $filename.'_'.time().'.'.$extention;
            //Upload Path
            $path = $request->file('image2')->storeAs('public/',$image_2);
            $input['image2'] = $image_2;
             
             $user=Homepage_text::find(1);
		 	$user->image_2=$image_2;
		 	$user->save();

		 }
		 		
		 	$user=Homepage_text::find(1);
		 	$user->text=$request->text;
		 	$user->save();

		 	return redirect()->route('homepage_text')->with('success','Text Updated successfully.');

	}


}