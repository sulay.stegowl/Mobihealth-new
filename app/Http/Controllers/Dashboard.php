<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;
use App\Doctor_details;
use App\Additional_services;
class Dashboard extends Controller
{
    //
	public function index()
	{
		$data['professionals']=Doctor_details::where('status',1)->get()->count();
		$data['patient']=0;
		$data['pathology']=Additional_services::where(['status'=>1,'business_type'=>'Pathology'])->get()->count();
		$data['pharmacies']=Additional_services::where(['status'=>1,'business_type'=>'Pharmacies'])->get()->count();
		$data['hospital']=Additional_services::where(['status'=>1,'business_type'=>'Hospital'])->get()->count();
		$data['radiology']=Additional_services::where(['status'=>1,'business_type'=>'Radiology'])->get()->count();
		return view('dashboard')->with('data',$data);
	}

	public function temp_dashboard()
	{
		return view('temp_dashboard');
	}

	public function contact_query()
	{
		$contact=Contact::all();
		
		return view('contact_query')->with('contact',$contact);
	}
}
