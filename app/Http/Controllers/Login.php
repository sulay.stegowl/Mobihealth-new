<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use Hash;
class Login extends Controller
{
    //
	public function index()
	{      
        if(Auth::check()){
            $role=Auth::user()->role;
             if($role==1)
             {
                return redirect()->route('dashboard');
            }
            elseif($role==2)
            {
                return redirect()->route('paitent.dashboard');
            }
            elseif($role==3){
                return redirect()->route('doctor.dashboard');
            }
            elseif($role==4){
                return redirect()->route('lab.dashboard');
            }
            elseif($role==5){
                return redirect()->route('pharmacies.dashboard');
            }
            elseif($role==6){
                return redirect()->route('hospital.dashboard');
            }
            elseif($role==7){
                return redirect()->route('radiology.dashboard');
            }
            elseif($role==8){
                return redirect()->route('doctors.dashboard');
            }
            elseif($role==9){
                return redirect()->route('patients.dashboard');
            }
            elseif($role==10){
                return redirect()->route('laboratry.dashboard');
            }
            elseif($role==11){
                return redirect()->route('pharmacy.dashboard');
            }
            elseif($role==12){
                return redirect()->route('hospitals.dashboard');
            }
            elseif($role==13){
                return redirect()->route('radiologys.dashboard');
            }
            else
            {
                return redirect()->route('data.dashboard');
            }
    }
            else{
              return view('login');  
            }
        
		
        
	}


    public function check(Request $request)
    {
    	if(Auth::attempt(['email'=>$request->email,'password'=>$request->password]))
    	{
    		 $role=Auth::user()->role;
    		 if($role==1)
    		 {
    			return redirect()->route('dashboard');
    		}
    		elseif($role==2)
    		{
    			return redirect()->route('paitent.dashboard');
    		}
    		elseif($role==3){
    			return redirect()->route('doctor.dashboard');
    		}
    		elseif($role==4){
    			return redirect()->route('lab.dashboard');
    		}
    		elseif($role==5){
    			return redirect()->route('pharmacies.dashboard');
    		}
            elseif($role==6){
                return redirect()->route('hospital.dashboard');
            }
            elseif($role==7){
                return redirect()->route('radiology.dashboard');
            }
            elseif($role==8){
                return redirect()->route('doctors.dashboard');
            }
            elseif($role==9){
                return redirect()->route('patients.dashboard');
            }
            elseif($role==10){
                return redirect()->route('laboratry.dashboard');
            }
            elseif($role==11){
                return redirect()->route('pharmacy.dashboard');
            }
            elseif($role==12){
                return redirect()->route('hospitals.dashboard');
            }
    		else{
    			return redirect()->route('radiologys.dashboard');
    		}
    	}
    	else{
    		return redirect()->route('login')->with('error','invalid credential');
    	}
    }

    public function change_password()
    {
        return view('change_password');
    }

    public function password_change(Request $request)
    {
    		$this->validate($request,[
			'old'=>'required',
			'new' => 'required',
			'confirm' => 'required|same:new',
			]);

			if (Hash::check($request->old, Auth::user()->password)) {
				$user=User::find(Auth::user()->id);
				$user->password=bcrypt($request->new);
				$user->save();
    	
    			return redirect()->route('change_password')->with('success','Password change successfully');
			}
			else
			{
    			return redirect()->route('change_password')->with('error','Old Password is incorrect');
			}
    }

    public function logout()
    {
        Auth::logout();
        auth()->logout();
        return redirect('/login')->with('success','Successfully Logged out.');
    }

    public function add()
    {
    	$user=new User;
    	$user->name='Hospital Admin';
    	$user->email='hospital_admin@mobihealth.com';
    	$user->password=bcrypt('123');
    	$user->role=6;
    	$user->save();

    	return redirect()->route('dashboard')->with('success','user added successfully');
    }
}
