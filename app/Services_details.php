<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Services_details extends Model
{
    //
     public $table='services_details';
      protected $fillable = ['title','type','desc','image','email','phone','address','country'];

}
