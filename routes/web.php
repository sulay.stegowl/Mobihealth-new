<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return 'hello';
// });

// Web site  Route


Route::get('/','front\HomePage@index')->name('homepage');

Route::get('/new_page','front\HomePage@new_page')->name('new_page');

//Route::get('/','front\Service_details@temp')->name('homepage');
Route::get('/about','front\HomePage@about')->name('about');
Route::get('/contact','front\HomePage@contact')->name('contact');
Route::get('/doctor_detail','front\HomePage@doctor_detail')->name('doctor_detail');
Route::get('/doctors','front\HomePage@doctors')->name('doctors');
Route::get('/hospital_details/{slug}','front\HomePage@hospital_details')->name('hospital_details');
Route::get('/hospital','front\Service_details@hospital')->name('hospital');
Route::get('/pathology_details/{slug}','front\HomePage@pathology_details')->name('pathology_details');
Route::get('/pathology','front\HomePage@pathology')->name('pathology');
Route::get('/news_details','front\HomePage@news_details')->name('news_details');
Route::get('/news','front\HomePage@news')->name('news');
Route::get('/pharmacy_details/{slug}','front\HomePage@pharmacy_details')->name('pharmacy_details');
Route::get('/pharmacy','front\HomePage@pharmacy')->name('pharmacy');
Route::get('/services_details','front\HomePage@services_details')->name('services_details');
Route::get('/services_web','front\HomePage@services')->name('services_web');
Route::post('/submit_contact_us','front\HomePage@send_contact_us')->name('send.contact');
Route::get('/registration','front\HomePage@registration')->name('registration');
Route::get('/doctors_registration','front\HomePage@doctors_registration')->name('doctors_registration');
Route::post('/doctors_registration/steps','front\HomePage@steps')->name('steps');
Route::post('/doctor_save','front\HomePage@doctor_save')->name('doctor_save');
Route::get('/pathology/register','front\HomePage@register')->name('pathology_register');
Route::post('/submit_register','front\HomePage@submit_register')->name('submit_register');
Route::get('/news_details_1','front\HomePage@news_details_1')->name('news_details_1');
Route::get('/news_details_2','front\HomePage@news_details_2')->name('news_details_2');
Route::get('/news_details_3','front\HomePage@news_details_3')->name('news_details_3');
Route::get('/thank_you','front\HomePage@thank_you')->name('thank_you');

Route::get('/radiology_details/{slug}','front\HomePage@radiology_details')->name('radiology_details');
Route::get('/radiology','front\HomePage@radiology')->name('radiology');


Route::get('/service_detail_1','front\Service_details@services_1')->name('service_detail_1');
Route::get('/service_detail_2','front\Service_details@services_2')->name('service_detail_2');
Route::get('/service_detail_3','front\Service_details@services_3')->name('service_detail_3');
Route::get('/service_detail_4','front\Service_details@services_4')->name('service_detail_4');
Route::get('/service_detail_5','front\Service_details@services_5')->name('service_detail_5');
Route::get('/service_detail_6','front\Service_details@services_6')->name('service_detail_6');

Route::get('/form','front\HomePage@form')->name('form');

Route::post('/search_hospital','front\Service_details@search_hospital')->name('search_hospital');
Route::post('/search_pathology','front\Service_details@search_pathology')->name('search_pathology');
Route::post('/search_pharmacy','front\Service_details@search_pharmacy')->name('search_pharmacy');
Route::post('/search_radiology','front\Service_details@search_radiology')->name('search_radiology');

Route::get('/hospital/how_it_work','front\Service_faq@hospital_how')->name('hospital_how');
Route::get('/hospital/faq','front\Service_faq@hospital_faq')->name('hospital_faq');
Route::get('/pathology/how_it_work','front\Service_faq@pathology_how')->name('pathology_how');
Route::get('/pathology/faq','front\Service_faq@pathology_faq')->name('pathology_faq');
Route::get('/pharmacy/how_it_work','front\Service_faq@pharmacy_how')->name('pharmacy_how');
Route::get('/pharmacy/faq','front\Service_faq@pharmacy_faq')->name('pharmacy_faq');
Route::get('/radiology/how_it_work','front\Service_faq@radiology_how')->name('radiology_how');
Route::get('/radiology/faq','front\Service_faq@radiology_faq')->name('radiology_faq');
Route::get('/subscriptions','front\Service_faq@subscription')->name('subscription');
Route::get('/subscriptions/individuals-families','front\Service_faq@subscription_plan_1')->name('subscription_plan_1');
Route::get('/subscriptions/corporations-NGOs','front\Service_faq@subscription_plan_2')->name('subscription_plan_2');

Route::get('/radiology/register','front\Register@radiology')->name('radiology_register');
Route::get('/hospital/register','front\Register@hospital')->name('hospital_register');
Route::get('/pharmacy/register','front\Register@pharmacy')->name('pharmacy_register');
Route::get('/privacy-policy','front\Service_details@privacy_policy')->name('privacy_policy');
Route::get('/terms-condition','front\Service_details@terms_condition')->name('terms_condition');
Route::get('/disclaimers','front\Service_details@disclaimers')->name('disclaimers');

Route::get('/hospital_result','front\Service_details@hospital_result');
Route::get('/pathology_result','front\Service_details@pathology_result');
Route::get('/pharmacy_result','front\Service_details@pharmacy_result');
Route::get('/radiology_result','front\Service_details@radiology_result');


Route::get('/doctor/how_it_work','front\Service_faq@doctor_how')->name('doctor_how');
Route::get('/doctor/faq','front\Service_faq@doctor_faq')->name('doctor_faq');


Route::get('/patients/how_it_work','front\Service_faq@patients_how')->name('patients_how');
Route::get('/patients/faq','front\Service_faq@patients_faq')->name('patients_faq');

Route::post('/subscriptions_save','front\HomePage@save_subscription')->name('subscriptions_save');
// Route::get('/resources','front\HomePage@resources')->name('resources');

// End of Website Route






Route::get('/login','Login@index')->name('login');
Route::get('/logout','Login@logout')->name('logout');

Route::post('/authentication','Login@check')->name('login.check');
Route::middleware('auth')->group(function(){
Route::get('/change_password','Login@change_password')->name('change_password');
Route::post('/password_change','Login@password_change')->name('password_change');
Route::get('/add_user','Other_registration@add')->name('add_addtional_services');
Route::post('/save_user','Other_registration@save')->name('save_additional_services');

Route::get('/subscribers','Other_registration@subscriber')->name('subscribers');
Route::get('/subscriber_details/{id}','Other_registration@subscriber_details')->name('additional.subscriber.details');
Route::get('/activate_doctor/{id}','Other_registration@accept_subscription')->name('activate.data');
Route::get('/deactivate_doctor/{id}','Other_registration@decline_subscription')->name('deactivate.data');
//Route::middleware('auth')->group(function(){
});
Route::middleware('ChckRole:1')->group(function(){

	Route::prefix('admin')->group(function(){


Route::get('/about','Welcome_text@about')->name('aboutus_text');
Route::post('/about_save','Welcome_text@about_save')->name('about_save');
Route::get('/telemedicine','Welcome_text@telemedicine')->name('telemedicine_text');
Route::post('/telemedicine_save','Welcome_text@telemedicine_save')->name('telemedicine_save');

Route::get('/homepage','Welcome_text@homepage')->name('homepage_text');
Route::post('/homepage_save','Welcome_text@homepage_save')->name('homepage_save');



Route::get('/add','Login@add')->name('add');
Route::get('/dashboard', 'Dashboard@index')->name('dashboard');
Route::get('/contact_query','Dashboard@contact_query')->name('contact_query');
Route::get('/services','Other_registration@add_addtional_services')->name('services');
Route::get('/services/add','Other_registration@add_services')->name('add_services');
Route::post('/services/save','Other_registration@save_additional_services')->name('save_services');
Route::get('/services/edit/{id}','Other_registration@edit_additional_services')->name('edit_services');
Route::post('/services/update','Other_registration@update_additional_service')->name('update_services');
Route::delete('services/delete/{id}','Other_registration@delete_additional_service')->name('service_delete');
	});//prefix end

});//role 1 end

Route::middleware('ChckRole:2')->group(function(){

	Route::prefix('paitents')->group(function(){

Route::get('/dashboard', 'Dashboard@index')->name('paitent.dashboard');
// Route::get('/change_password','Login@change_password')->name('paitent.change_password');
// Route::post('/password_change','Login@password_change')->name('paitent.password_change');
	});//prefix end

});//role 1 end

Route::middleware('ChckRole:3')->group(function(){

	Route::prefix('Doctors')->group(function(){

Route::get('/dashboard', 'Doctor@dashboard')->name('doctor.dashboard');
Route::get('/add_doctor', 'Doctor@add')->name('add.doctor');
Route::post('/submit_doctor','Doctor@save')->name('save.doctor');
Route::get('/all','Doctor@doctors_list')->name('doctor.list');
Route::get('details/{id}','Doctor@doctor_details')->name('doctor.details');
Route::get('/subscriber','Doctor@subscriber')->name('doctor.subscriber');
Route::get('/subscriber_details/{id}','Doctor@subscriber_details')->name('doctor.subscriber.details');
Route::get('/activate_doctor/{id}','Doctor@activate')->name('activate.doctor');
Route::get('/deactivate_doctor/{id}','Doctor@deactivate')->name('deactivate.doctor');

Route::get('/dentists-list','Doctor@dentists_list')->name('dentists.list');
Route::get('/laborarty-list','Doctor@lab_list')->name('laborarty.list');
Route::get('/nurse-list','Doctor@nurse_list')->name('nurse.list');
Route::get('/physiotherapist-list','Doctor@physiotherapist_list')->name('physiotherapist.list');
Route::get('/pharmacist-list','Doctor@pharmacist_list')->name('pharmacist.list');

Route::get('/subscriptions','Doctor@subscription')->name('main_subscription');
	});//prefix end

});//role 3 end
	

	Route::middleware('ChckRole:4')->group(function(){

Route::prefix('Pathologies')->group(function(){

Route::get('/dashboard', 'Lab@dashboard')->name('lab.dashboard');
Route::get('/list','Lab@lab_list')->name('lab.list');

	});//prefix end

});//role 4 end


	Route::middleware('ChckRole:5')->group(function(){

Route::prefix('pharmacies')->group(function(){

Route::get('/dashboard', 'Pharmacy@dashboard')->name('pharmacies.dashboard');
Route::get('/list','Pharmacy@pharma_list')->name('pharma.list');
	});//prefix end

});//role 5 end


	Route::middleware('ChckRole:6')->group(function(){

Route::prefix('hospitals')->group(function(){

Route::get('/dashboard', 'Hospital@dashboard')->name('hospital.dashboard');
Route::get('/list','Hospital@hospital_list')->name('hospital.list');
	});//prefix end

});//role 6 end



	Route::middleware('ChckRole:7')->group(function(){

Route::prefix('radiology')->group(function(){

Route::get('/dashboard', 'Radiology@dashboard')->name('radiology.dashboard');
	});//prefix end

});//role 7 end


	Route::middleware('ChckRole:8')->group(function(){

Route::prefix('doctor')->group(function(){

Route::get('/dashboard', 'Dashboard@temp_dashboard')->name('doctors.dashboard');
	});//prefix end

});//role 8 end

	Route::middleware('ChckRole:9')->group(function(){

Route::prefix('patient')->group(function(){

Route::get('/dashboard', 'Dashboard@temp_dashboard')->name('patients.dashboard');
	});//prefix end

});//role 9 end

	Route::middleware('ChckRole:10')->group(function(){

Route::prefix('Pathology')->group(function(){

Route::get('/dashboard', 'Dashboard@temp_dashboard')->name('laboratry.dashboard');
	});//prefix end

});//role 10 end

	Route::middleware('ChckRole:11')->group(function(){

Route::prefix('pharmacy')->group(function(){

Route::get('/dashboard', 'Dashboard@temp_dashboard')->name('pharmacy.dashboard');
	});//prefix end

});//role 11 end

	Route::middleware('ChckRole:12')->group(function(){

Route::prefix('hospital')->group(function(){

Route::get('/dashboard', 'Dashboard@temp_dashboard')->name('hospitals.dashboard');
	});//prefix end

});//role 12 end

	Route::middleware('ChckRole:13')->group(function(){

Route::prefix('radiologys')->group(function(){

Route::get('/dashboard', 'Dashboard@temp_dashboard')->name('radiologys.dashboard');
	});//prefix end

});//role 13 end

	Route::middleware('ChckRole:14')->group(function(){

Route::prefix('Data_entry_admin')->group(function(){

Route::get('/dashboard', 'Dashboard@temp_dashboard')->name('data.dashboard');
Route::get('/services','Other_registration@add_addtional_services')->name('services');
Route::get('/services/add','Other_registration@add_services')->name('add_services');
Route::post('/services/save','Other_registration@save_additional_services')->name('save_services');
Route::get('/services/edit/{id}','Other_registration@edit_additional_services')->name('edit_services');
Route::post('/services/update','Other_registration@update_additional_service')->name('update_services');
Route::delete('services/delete/{id}','Other_registration@delete_additional_service')->name('service_delete');
	});//prefix end

});//role 14 end
//}); //auth end
